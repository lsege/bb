<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>详情单</title>
    <link rel="shortcut icon" href="<%=path%>static/hplus/favicon.ico">
    <link href="<%=path%>/static/hplus/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/animate.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/style.css?v=4.1.0" rel="stylesheet">
    <%--<link href="<%=path%>/statics2/saas/scm/app2_release/css/common.css?ver=20150522" rel="stylesheet" type="text/css">--%>
    <link href="<%=path%>/statics2/saas/scm/app2_release/css/green/ui.min.css?ver=20150522" rel="stylesheet">
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/jquery/jquery-1.10.2.min.js"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/json3.min.js"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/common.js?ver=20150522"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/grid.js?ver=20150522"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/plugins.js?ver=20150522"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/plugins/jquery.dialog.js?self=true&ver=20150522"></script>
    <link href="<%=path%>/statics2/css/base.css" rel="stylesheet" type="text/css">
    <link href="<%=path%>/statics2/css/green/default.css?ver=20150522" rel="stylesheet" type="text/css" id="defaultFile">
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/tabs.js?ver=20150522"></script>
    <script src="<%=path%>/static/hplus/js/jquery.min.js?v=2.1.4"></script>
    <script src="<%=path%>/static/hplus/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="<%=path%>/static/hplus/js/content.js?v=1.0.0"></script>
    <script src="<%=path%>/static/hplus/js/plugins/flot/jquery.flot.js"></script>
    <script src="<%=path%>/static/hplus/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="<%=path%>/static/hplus/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="<%=path%>/static/hplus/js/plugins/chartJs/Chart.min.js"></script>
    <script src="<%=path%>/static/hplus/js/plugins/peity/jquery.peity.min.js"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/grid.js?ver=20150522"></script>
    <script src="<%=path%>/static/hplus/js/demo/peity-demo.js"></script>
    <script src="<%=path%>/statics2/js/dist/cashBill.js?ver=20150522"></script>
    <script src="<%=path%>/static/sales/sales_input.js"></script>
    <script src="<%=path%>/static/hplus/js/plugins/suggest/bootstrap-suggest.min.js"></script>
    <link href="<%=path%>/static/hplus/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <script src="<%=path%>/static/hplus/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<%=path%>/static/hplus/js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script type="text/javascript">
        var DOMAIN = document.domain;
        var WDURL = "http://127.0.0.1/erpv2/index.php";
        var SCHEME= "green";
        try{
            document.domain = 'http://127.0.0.1/erpv2/';
        }catch(e){
        }
        //ctrl+F5 增加版本号来清空iframe的缓存的
        $(document).keydown(function(event) {
            /* Act on the event */
            if(event.keyCode === 116 && event.ctrlKey){
                var defaultPage = Public.getDefaultPage();
                var href = defaultPage.location.href.split('?')[0] + '?';
                var params = Public.urlParam();
                params['version'] = Date.parse((new Date()));
                for(i in params){
                    if(i && typeof i != 'function'){
                        href += i + '=' + params[i] + '&';
                    }
                }
                defaultPage.location.href = href;
                event.preventDefault();
            }
        });
    </script>
    <style>
        .wrapper{
            height: 100%;
        }
    </style>
    <script>
        var CONFIG = {
            DEFAULT_PAGE: true,
            SERVICE_URL: 'http://127.0.0.1/erpv2/'
        };
        //系统参数控制
        var SYSTEM = {
            version: 1,
            skin: "green",
            language:"",
            site:"",
            curDate: "1033737952010",  //系统当前日期
            DBID: "88886683", //账套ID
            serviceType: "12", //账套类型，13：表示收费服务，12：表示免费服务
            realName: "模板俱乐部", //真实姓名
            userName: "admin", //用户名
            companyName: "模板俱乐部ERP系统",	//公司名称
            companyAddr: "模板俱乐部",	//公司地址
            phone: "17701030513",	//公司电话
            fax: "17701030513",	//公司传真
            postcode: "17701030513",	//公司邮编
            startDate: "", //启用日期
            currency: "RMB",	//本位币
            qtyPlaces: "1",	//数量小数位
            pricePlaces: "1",	//单价小数位
            amountPlaces: "2", //金额小数位
            valMethods:	"movingAverage",	//存货计价方法
            invEntryCount: "",//试用版单据分录数
            rights: {},//权限列表
            billRequiredCheck: 0, //是否启用单据审核功能  1：是、0：否
            requiredCheckStore: 1, //是否检查负库存  1：是、0：否
            hasOnlineStore: 0,	//是否启用网店
            enableStorage: 0,	//是否启用仓储
            genvChBill: 0,	//生成凭证后是否允许修改单据
            requiredMoney: 1, //是否启用资金功能  1：是、0：否
            taxRequiredCheck: 0,
            taxRequiredInput: 17,
            isAdmin:true, //是否管理员
            siExpired:false,//是否过期
            siType:1, //服务版本，1表示基础版，2表示标准版
            siVersion:4, //1表示试用、2表示免费（百度版）、3表示收费，4表示体验版
            Mobile:"",//当前用户手机号码
            isMobile:true,//是否验证手机
            isshortUser:false,//是否联邦用户
            shortName:"",//shortName
            isOpen:false,//是否弹出手机验证
            enableAssistingProp:0, //是否开启辅助属性功能  1：是、0：否
            ISSERNUM: 0, //是否启用序列号 1：是、0：否 （与enableAssistingProp对立，只能启用其一）
            ISWARRANTY: 0 //是否启用保质期  1：是、0：否
        };
        //区分服务支持
        SYSTEM.servicePro = SYSTEM.siType === 2 ? 'forbscm3' : 'forscm3';
        var cacheList = {};	//缓存列表查询
    </script>
    <script>
        var _hmt = _hmt || [];
        (function() {
            var hm = document.createElement("script");
            hm.src = "//hm.baidu.com/hm.js?0613c265aa34b0ca0511eba4b45d2f5e";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
</head>
<body class="top-navigation full-height-layout gray-bg">
<div id="wrapper" style="background-color: transparent; height: 800px; width: auto;" >
    <div class="row border-bottom white-bg">
        <!-- 页面顶部¨ -->
        <%@ include file="top.jsp"%>
    </div>
    <div id="page-wrapper" class="gray-bg" style="height: 90%; min-height: auto;">
        <div class="bills cf">
            <div class="con-header">
                <div class="row-item" style="text-align: center ">
                    <h1>唐山军荣铝业有限公司客户付款单列表</h1>
                </div>
                <div style="height: 30px; background-color: slategray"></div>
                <div style="height: 30px;"></div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="display: none">ID</th>
                                            <th>单据编号</th>
                                            <th>客户名称</th>
                                            <th>客户联系方式</th>
                                            <th>付款日期</th>
                                            <th>录入人</th>
                                            <th>经手人</th>
                                            <th>单据类型</th>
                                            <th>创建时间</th>
                                            <th>承兑汇票号</th>
                                            <th>承兑到款日期</th>
                                            <th>备注</th>
                                            <th>退款到款时间</th>
                                            <th>退款类型</th>
                                            <th>退款账户</th>
                                            <th>操作</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${list}" var="list">
                                            <tr>
                                                <td style="display: none">${list.id}</td>
                                                <td>${list.listNum}</td>
                                                <td>${list.customrtName}</td>
                                                <td>${list.customerPhone}</td>
                                                <td>${list.paymentDate}</td>
                                                <td>${list.inputMan}</td>
                                                <td>${list.brokerage}</td>
                                                <td>${list.documentType}</td>
                                                <td>${list.creationDate}</td>
                                                <td>${list.numChengdui}</td>
                                                <td>${list.dateChengdui}</td>
                                                <td>${list.note}</td>
                                                <td>${list.endData}</td>
                                                <td>${list.returnType}</td>
                                                <td>${list.customerAccount}</td>
                                                <td>
                                                    <button name="edit" aid="${list.id}" atype="${list.documentType}">
                                                        修改
                                                    </button>
                                                    &nbsp;&nbsp;&nbsp;
                                                     <button name="delete" aid="${list.id}" atype="${list.documentType}">
                                                         删除
                                                     </button>
                                                    </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                                <div style="font-size: 16px">
                                    <div class="left" style="float: left;">共${total}条记录</div>
                                    <div class="right" style="margin-left: 1500px; float: left;">
                                        <input type="button" value="首页" onclick="change(1)">
                                        <c:if test="${currentPage != 1}">
                                            <input type="button" value="上一页" onclick="change(${currentPage-1})">
                                        </c:if>
                                        <input type="button" value="${currentPage-1}" onclick="change(${currentPage-1})">
                                        <input type="button" value="${currentPage}" style="background-color:deepskyblue">
                                        <input type="button" value="${currentPage+1}" onclick="change(${currentPage+1})">
                                        <input type="button" value="下一页" onclick="change(${currentPage+1})">
                                        <input type="button" value="尾页" onclick="change(${pageCount})">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="height: 30px;"></div>
                <div style="height: 30px; background-color: slategray"></div>
                <div style="height: 30px;"></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('button[name=edit]').click(function(){
            var id=$(this).attr("aid");
            var type=$(this).attr("atype");
            if(type == "电汇回款单"){
                alert("此单据不可修改");
            }
            window.location.href="<%=path%>/index/edit?id="+id+"&type="+type;
        })
        $('button[name=delete]').click(function(){
            var id=$(this).attr("aid");
            var type=$(this).attr("atype");
            if(type == "电汇回款单"){
                alert("此单据不可修改");
            } else {
                result=confirm("是否确定删除");
                if(result==true){
                    <%--window.location.href="<%=path%>/index/delete?id="+id;--%>
                    $.ajax({
                        type: "POST",
                        url: "<%=path%>/index/delete?id="+id,
                        dataType: "json",
                        success: function(data){
                           if(data.result){
                               window.location.reload();
                           }
                        }
                    });
                }
            }
        })
        $('.dataTables-example').dataTable();
        /* Init DataTables */
        var oTable = $('#editable').dataTable();
        /* Apply the jEditable handlers to the table */
        oTable.$('td').editable('../example_ajax.php', {
            "callback": function (sValue, y) {
                var aPos = oTable.fnGetPosition(this);
                oTable.fnUpdate(sValue, aPos[0], aPos[1]);
            },
            "submitdata": function (value, settings) {
                return {
                    "row_id": this.parentNode.getAttribute('id'),
                    "column": oTable.fnGetPosition(this)[2]
                };
            },
            "width": "90%",
            "height": "100%"
        });
    });
</script>
<script>
    function change(page) {
        window.location = "<%=path%>/index/index2?page="+page;
    }
</script>
</body>
</html>