<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
%>
<link href="<%=path%>/static/cds/css/zzsc.css" rel="stylesheet">
<link href="<%=path%>/static/cds/css/maps.css" rel="stylesheet">
<script type="text/javascript" src="<%=path%>/static/cds/js/maps.js"></script>
<script type="text/javascript" src="<%=path%>/static/cds/js/google.js"></script>
<script type="text/javascript">$(document).ready(function(){$().maps();});</script>
<nav class="navbar navbar-static-top" role="navigation" style="background-color: SkyBlue">
    <div style="text-align: center">
        <font style="font-size: 36px;"><b>业务员[<span style="color: red">${sessionScope.names}</span>]销售管理系统</b></font>
    </div>
    <div class="navbar-collapse collapse" id="navbar" style="background-color: SkyBlue; margin-top: -30px">
        <ul class="venus-menu" style="background-color: SkyBlue">
            <li class="dropdown">
                <a aria-expanded="false" role="button" class="dropdown-toggle" data-toggle="dropdown"> 产品销售录入 </a>
                <ul role="menu" class="dropdown-menu">
                    <li><a href="<%=path%>/index/home" target="_parent">销货单</a></li>
                    <li><a href="<%=path%>/index/index9" target="_parent">退货单</a> </li>
                    <li><a href="<%=path%>/index/salesOrdersPage" target="_parent">销货单管理</a> </li>
                    <li><a href="<%=path%>/index/index10?type=1" target="_parent">退货单管理</a> </li>
                </ul>
            </li>
            <li class="dropdown">
                <a aria-expanded="false" role="button" class="dropdown-toggle" data-toggle="dropdown"> 客户回款录入 </a>
                <ul role="menu" class="dropdown-menu">
                    <li><a href="<%=path%>/index/toReceivableEntry" target="_parent">回款单录入</a></li>
                    <li><a href="<%=path%>/index/index8" target="_parent">退款单录入</a></li>
                    <li><a href="<%=path%>/index/toReceivableList" target="_parent">回款单列表</a></li>
                </ul>
            </li>
            <!--带改动-->
            <li class="dropdown">
                <a aria-expanded="false" role="button" class="dropdown-toggle" data-toggle="dropdown"> 售出产品报表 </a>
                <ul role="menu" class="dropdown-menu">
                    <li><a href="<%=path%>/index/salesDetail">铝瓶销售明细</a></li>
                    <li><a href="<%=path%>/index/salesSummary">铝瓶销售汇总</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a aria-expanded="false" role="button" class="dropdown-toggle" data-toggle="dropdown"> 客户采购报表 </a>
                <ul role="menu" class="dropdown-menu">
                    <li><a href="<%=path%>/index/procurementDetail">采购铝瓶明细</a></li>
                    <li><a href="<%=path%>/index/procurementSummary">采购铝瓶汇总</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a aria-expanded="false" role="button" class="dropdown-toggle" data-toggle="dropdown"> 客户回款报表 </a>
                <ul role="menu" class="dropdown-menu">
                    <li><a href="<%=path%>/index/receivableDetail">客户回款明细</a></li>
                    <li><a href="<%=path%>/index/receivableSummary">客户回款汇总</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a aria-expanded="false" role="button" class="dropdown-toggle" data-toggle="dropdown"> 业务提成报表 </a>
                <ul role="menu" class="dropdown-menu">
                    <li><a href="<%=path%>/index/commissionDetail">业务提成明细</a></li>
                    <li><a href="<%=path%>/index/commissionSummary">业务提成汇总</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a aria-expanded="false" role="button" class="dropdown-toggle" data-toggle="dropdown"> 系统服务 </a>
                <ul role="menu" class="dropdown-menu">
                    <li><a>目录</a>
                        <ul>
                            <li><a href="<%=path%>/index/customerListPage">客户目录</a></li>
                            <li><a href="<%=path%>/index/productListPage">商品目录</a></li>
                        </ul>
                    </li>
                    <li><a>添加管理</a>
                        <ul>
                            <li><a href="<%=path%>/index/addCustomerPage">添加客户</a></li>
                            <li><a href="<%=path%>/index/addProductPage">添加商品</a></li>
                        </ul>
                    </li>
                    <li><a>业务员管理</a>
                        <ul>
                            <li><a href="<%=path%>/index/operatorAdminPage">业务员管理</a></li>
                            <li><a href="<%=path%>/index/commissionPage">提成比例调整</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a aria-expanded="false" role="button" href="<%=path%>/index/exit" class="dropdown-toggle">退出</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>