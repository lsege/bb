<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <script></script>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>销货单添加</title>
    <meta name="keywords" content="科研项目管理">
    <meta name="description" content="科研项目管理">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE">
    <link href="<%=path%>/statics2/saas/scm/app2_release/css/common.css?ver=20150522" rel="stylesheet" type="text/css">
    <link href="<%=path%>/statics2/saas/scm/app2_release/css/green/ui.min.css?ver=20150522" rel="stylesheet">
    <link href="<%=path%>/statics2/css/green/bills.css?ver=20150522" rel="stylesheet" type="text/css">
    <link href="<%=path%>/static/hplus/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/animate.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/bootstrap.min.css?v=4.1.0" rel="stylesheet">
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/jquery/jquery-1.10.2.min.js"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/json3.min.js"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/common.js?ver=20150522"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/grid.js?ver=20150522"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/plugins.js?ver=20150522"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/plugins/jquery.dialog.js?self=true&ver=20150522"></script>
    <script src="<%=path%>/static/hplus/js/bootstrap.min.js"></script>
    <style type="text/css">
        th{
            text-align: center;
        }
        td{
            text-align: center;
        }
    </style>
</head>
<body>
    <form>
        <table border="1" width="1000px" align="center">
            <tr>
                <th>用户</th>><th>提成类型</th><th colspan="2">业绩范围</th><th>提成标准</th><th>提成比例</th><th>操作</th>
            </tr>
            <c:forEach items="${list}" var="list" varStatus="count">
                <tr>
                    <td>${list.userName}</td>
                    <td>${list.dataType}</td>
                    <td>${list.min}</td>
                    <td>${list.max}</td>
                    <td>
                        ${list.commissionStandard}
                    </td>
                    <td>
                        <input type="text" value="${list.proportion}" disabled id="${count.count}">
                    </td>
                    <td style="text-align: center">
                        <input type="button" value="修改" name="a${count.count}" onclick="changeCommission(${count.count})">
                        <input type="button" value="保存" name="${count.count}" onclick="reload(${list.id},${count.count})" style="display: none">
                    </td>
                </tr>
            </c:forEach>
        </table>
    </form>
</body>
<script>
    function changeCommission(index) {
        var inputs = document.getElementById(index);
        inputs.removeAttribute("disabled");
        inputs.focus();
        $("input[name='a"+index+"']").css("display","none");
        $("input[name='"+index+"']").css("display","block");
    }
    function reload(id,index) {
        var commission = $("#"+index).val();
        $.ajax({
            type:"POST",
            url:"<%=path%>/commission/editCommission?id="+id+"&commission="+commission,
            dataType:"json",
            success:function(e){
                if (e.result){
                    alert(e.msg);
                    top.location=self.location.href = "<%=path%>/index/commission";
                } else {
                    alert(e.msg);
                }

            }
        });
    }
</script>
</html>