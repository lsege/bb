<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="../top.jsp" %>
<script type="text/javascript">
    var DOMAIN = document.domain;
    var WDURL = "";
    var SCHEME = "green";
    try {
        document.domain = '<%=path%>';
    } catch (e) {
    }
</script>
<link href="<%=path%>/static/css/green/bills.css?ver=20150522" rel="stylesheet" type="text/css">
</head>
<body class="bgwh">
<input type="hidden" value="<%=path%>" id="hid">
<div class="container fix p20">
    <div class="mod-search m0 cf">
        <div class="fl">
            <%--<ul class="ul-inline">--%>
                <%--<li>--%>
                    <%--<input type="text" id="matchCon" class="ui-input ui-input-ph" value="请输入商品编号或名称或型号">--%>
                <%--</li>--%>
                <%--<li><a class="ui-btn mrb" id="search">查询</a><!-- <a class="ui-btn" id="refresh">刷新</a> --></li>--%>
            <%--</ul>--%>
        </div>
    </div>
    <div class="grid-wrap">
        <table id="grid">
        </table>
        <div id="page"></div>
    </div>
</div>
<script src="<%=path%>/statics2/js/dist/goodsBatch.js?2"></script>
</body>
</html>