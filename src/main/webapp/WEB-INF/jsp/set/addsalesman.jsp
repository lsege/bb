<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <title>Title</title>
    <script src="<%=path%>/static/hplus/js/jquery.min.js?v=2.1.4"></script>
    <script src="<%=path%>/static/hplus/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="<%=path%>/static/hplus/js/content.js?v=1.0.0"></script>
    <script src="<%=path%>/static/hplus/js/plugins/flot/jquery.flot.js"></script>
    <script src="<%=path%>/static/hplus/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="<%=path%>/static/hplus/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="<%=path%>/static/hplus/js/plugins/chartJs/Chart.min.js"></script>
    <script src="<%=path%>/static/hplus/js/plugins/peity/jquery.peity.min.js"></script>
    <script src="<%=path%>/static/hplus/js/demo/peity-demo.js"></script>
    <link rel="stylesheet" type="text/css" href="<%=path%>/static/bootstrap-4.0.0-alpha.6-dist/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/static/bootstrap-4.0.0-alpha.6-dist/css/bootstrap-grid.css"/>
    <link rel="stylesheet" type="text/css" href="<%=path%>/static/bootstrap-4.0.0-alpha.6-dist/css/bootstrap-reboot.css"/>
    <script src="<%=path%>/static/bootstrap-4.0.0-alpha.6-dist/js/bootstrap.js"></script>
    <script src="<%=path%>/static/bootstrap-4.0.0-alpha.6-dist/js/bootstrap.min.js"></script>
    <script src="http://cdn.bootcss.com/blueimp-md5/1.1.0/js/md5.js"></script>
    <style>
        #matchCon { width: 200px; }
    </style>
</head>
<body>

<div class="container" style="background-color: #F0F0F0">
    <div class="row" style="background-color: lightgrey;margin-top: 20px">
        <span style="margin-left: 20px;font-size: 20px">业务员添加</span>
    </div>
    <form action="" id="texform" style="margin-top: 20px">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-1">姓名：</div>
            <div class="col-md-3" >
                <input type="text" style="width: 100%"  id="name" name="name">
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="row">
            &nbsp;
        </div>
        <div class="row" >
            <div class="col-md-4"></div>
            <div class="col-md-1">phone：</div>
            <div class="col-md-3">
                <input type="text" style="width: 100%"  id="phone" name="phone">
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="row">
            &nbsp;
        </div>
    </form>
    <div class="row" style="text-align: center">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <input type="button" value="业务员添加" style="width: 100%" onclick="test()">
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row">
        &nbsp;
    </div>
</div>
<div class="container" style="background-color: #F0F0F0">
    <div class="row" style="background-color: lightgrey;margin-top: 20px">
        <span style="margin-left: 20px;font-size: 20px">管理员添加</span>
    </div>
    <form action="" id="usrform" style="margin-top: 20px">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-1">姓名：</div>
            <div class="col-md-3" >
                <input type="text" style="width: 100%"  id="username" name="username">
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="row">
            &nbsp;
        </div>
        <div class="row" >
            <div class="col-md-4"></div>
            <div class="col-md-1">密码：</div>
            <div class="col-md-3">
                <input type="text" style="width: 100%"  id="userpwd" name="userpwd">
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="row">
            &nbsp;
        </div>
    </form>
    <div class="row" style="text-align: center">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <input type="button" value="管理员添加" style="width: 100%" onclick="test2()">
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row">
        &nbsp;
    </div>
</div>
<div class="container" style="background-color: #F0F0F0">
    <div class="row" style="background-color:lightgrey">
        <span style="margin-left: 20px;font-size: 20px">顾客添加</span>
    </div>
    <form action="" id="texform1" style="margin-top: 20px">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-2"  style="text-align: right">客户姓名：</div>
            <div class="col-md-3" >
                <input type="text" style="width: 100%"  id="name1" name="name1">
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="row">
            &nbsp;
        </div>
        <div class="row" >
            <div class="col-md-3"></div>
            <div class="col-md-2" style="text-align: right">联系方式：</div>
            <div class="col-md-3">
                <input type="text" style="width: 100%"  id="phone1" name="phone1">
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="row">
            &nbsp;
        </div>
        <div class="row" >
            <div class="col-md-3"></div>
            <div class="col-md-2"  style="text-align: right">详细地址：</div>
            <div class="col-md-3">
                <input type="text" style="width: 100%"  id="address" name="address">
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="row">
            &nbsp;
        </div>
        <div class="row" >
            <div class="col-md-3"></div>
            <div class="col-md-2"  style="text-align: right">联系人：</div>
            <div class="col-md-3">
                <input type="text" style="width: 100%"  id="people" name="people">
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="row">
            &nbsp;
        </div>
        <div class="row" >
            <div class="col-md-3"></div>
            <div class="col-md-2"  style="text-align: right">是否是老客户：</div>
            <div class="col-md-3">
                <label><input name="wheh" type="radio" value="1" />是 </label>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <label><input name="wheh" type="radio" value="2" />否 </label>
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="row">
            &nbsp;
        </div>
    </form>
    <div class="row" style="text-align: center">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <input type="button" value="顾客添加" style="width: 100%" onclick="test1()">
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row">
        &nbsp;
    </div>
</div>
<script type="text/javascript">
    function test() {
        var data = $( '#texform').serialize();
        console.log(data)
        $.ajax({
            url: "/basedata/addSalebb",
            data: data,
            processData: false,
            contentType: false,
            success: function (data) {
                if(data){
                    $("#disappare").show().show(300).delay(3000).hide(300);
                }else {
                    $("#disappare2").show().show(300).delay(3000).hide(300);
                }
            }
        })
    }
</script>
<script type="text/javascript">
    function test1() {
        var data = $( '#texform1').serialize();
        console.log(data)
        $.ajax({
            url: "/basedata/addCustomer",
            data: data,
            processData: false,
            contentType: false,
            success: function (data) {
                if(data){
                    $("#disappare").show().show(300).delay(3000).hide(300);
                }else {
                    $("#disappare2").show().show(300).delay(3000).hide(300);
                }
            }
        })
    }
</script>
<script type="text/javascript">
    function test2() {
        var data = $( '#usrform1').serialize();
        console.log(data)
        $.ajax({
            url: "/basedata/addUser",
            data: data,
            processData: false,
            contentType: false,
            success: function (data) {
                if(data){
                    $("#disappare").show().show(300).delay(3000).hide(300);
                }else {
                    $("#disappare2").show().show(300).delay(3000).hide(300);
                }
            }
        })
    }
</script>
<div id="disappare"  style="background-color: greenyellow;z-index:999;position:fixed;top:0px;right:0px;display:none;text-align: center;width: 200px;height: 60px">
    <br>
    <span >添加成功！！</span>
</div>
<div id="disappare2"  style="background-color: red;z-index:999;position:fixed;top:0px;right:0px;display:none;text-align: center;width: 200px;height: 60px">
    <br>
    <span style="color: whitesmoke">添加失败！！</span>
</div>
</body>
</html>
