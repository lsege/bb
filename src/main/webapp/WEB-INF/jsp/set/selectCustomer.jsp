<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="../top.jsp" %>

<script type="text/javascript">
    var DOMAIN = document.domain;
    var WDURL = "";
    var SCHEME = "green";
    try {
        document.domain = '<%=path%>';
    } catch (e) {
    }
</script>
<link href="<%=path%>/static/css/green/bills.css?ver=20150522" rel="stylesheet" type="text/css">

<style>
    #matchCon { width: 200px; }
</style>
</head>

<body class="bgwh">
<input type="hidden" value="<%=path%>" id="hid">
<div class="container fix p20">
    <div class="mod-search m0 cf">
        <div class="fl">
            <ul class="ul-inline">
                <li>
                    <span id="catorage"></span>
                </li>
                <li>
                    <input type="text" id="matchCon" class="ui-input ui-input-ph" value="输入编号 / 名称 / 联系人 / 电话查询">
                </li>
                <li><a class="ui-btn mrb" id="search">查询</a><!-- <a class="ui-btn" id="refresh">刷新</a> --></li>
            </ul>
        </div>
    </div>
    <div class="grid-wrap" style="width: 735px; ">
        <table id="grid">
        </table>
        <div id="page"></div>
    </div>
</div>
<script src="<%=path%>/statics2/js/dist/selectCustomer.js?ver=20140430"></script>
</body>
</html>




