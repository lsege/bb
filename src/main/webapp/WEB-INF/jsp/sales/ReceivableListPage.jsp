<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>详情单</title>
    <link href="<%=path%>/static/hplus/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <%--<script src="<%=path%>/static/hplus/js/plugins/sweetalert/content.min.js?v=1.0.0"></script>--%>
    <script src="<%=path%>/static/hplus/js/plugins/sweetalert/sweetalert.min.js"></script>

    <link href="<%=path%>/static/hplus/css/bootstrap.min.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus//css/bootstrap.min.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/style.css?v=4.1.0" rel="stylesheet">
    <style>
        .wrapper{
            height: 100%;
        }
    </style>
    <style>
        #table-3 thead, #table-3 tr {
            border-top-width: 1px;
            border-top-style: solid;
            border-top-color: rgb(235, 242, 224);
        }
        #table-3 {
            border-bottom-width: 1px;
            border-bottom-style: solid;
            border-bottom-color: rgb(235, 242, 224);
        }

        /* Padding and font style */
        #table-3 td, #table-3 th {
            padding: 5px 10px;
            font-size: 12px;
            font-family: Verdana;
            /*color: rgb(149, 170, 109);*/
        }

        /* Alternating background colors */
        #table-3 tr:nth-child(even) {
            background: #eaf5ff;
        }
        #table-3 tr:nth-child(odd) {
            background: #c7e5ff;
        }
    </style>
</head>
<body class="top-navigation full-height-layout gray-bg">
<div id="wrapper" style="background-color: transparent; height: 800px; width: auto;" >
    <div class="row border-bottom white-bg">
        <!-- 页面顶部¨ -->
        <%@ include file="../admin/top.jsp"%>
    </div>
    <div class="row">
        <div class="col-sm-6" style="width: 100%; height: 95%;">
        <div class="tab-content">
            <div id="tab-1" class="tab-pane active">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <table id="table-3" style="width: 100%" border="1">
                            <thead>
                                <tr style="background-color: grey;">
                                    <th style="display: none">ID</th>
                                    <th style="font-size: 10px; color: black; text-align: center">单据编号</th>
                                    <th style="font-size: 10px; color: black; text-align: center">客户名称</th>
                                    <th style="font-size: 10px; color: black; text-align: center">回款金额</th>
                                    <th style="font-size: 10px; color: black; text-align: center">单据类型</th>
                                    <th style="font-size: 10px; color: black; text-align: center">客户联系方式</th>
                                    <th style="font-size: 10px; color: black; text-align: center">承兑汇票号</th>
                                    <th style="font-size: 10px; color: black; text-align: center">承兑到款日</th>
                                    <th style="font-size: 10px; color: black; text-align: center">承兑寄票单位</th>
                                    <th style="font-size: 10px; color: black; text-align: center">承兑转下单位</th>
                                    <th style="font-size: 10px; color: black; text-align: center">承兑付款银行</th>
                                    <th style="font-size: 10px; color: black; text-align: center">电汇银行名称</th>
                                    <th style="font-size: 10px; color: black; text-align: center">电汇付款账号</th>
                                    <th style="font-size: 10px; color: black; text-align: center">电汇收款账号</th>
                                    <th style="font-size: 10px; color: black; text-align: center">电汇内容</th>
                                    <th style="font-size: 10px; color: black; text-align: center">转账付款人账号</th>
                                    <th style="font-size: 10px; color: black; text-align: center">转账收款人账号</th>
                                    <th style="font-size: 10px; color: black; text-align: center" width="135">操作</th>
                                </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${list}" var="list">
                                <tr>
                                    <td style="display: none">${list.id}</td>
                                    <td>${list.listNum}</td>
                                    <td>${list.customrtName}</td>
                                    <td>${list.money}</td>
                                    <c:if test="${list.documentType == 1}">
                                        <td>承兑回款单</td>
                                    </c:if>
                                    <c:if test="${list.documentType == 2}">
                                        <td>电汇回款单</td>
                                    </c:if>
                                    <c:if test="${list.documentType == 3}">
                                        <td>转账回款单</td>
                                    </c:if>
                                    <c:if test="${list.documentType == 4}">
                                        <td>现金回款单</td>
                                    </c:if>
                                    <td>${list.customerPhone}</td>
                                    <td>${list.numChengdui}</td>
                                    <td>${list.dateChengdui}</td>
                                    <td>${list.chengduiSendUnit}</td>
                                    <td>${list.chengduiToUnit}</td>
                                    <td>${list.chengduiBang}</td>
                                    <td>${list.dianhuiBankName}</td>
                                    <td>${list.dianhuiPayAccount}</td>
                                    <td>${list.dianhuiCollectionAccount}</td>
                                    <td>${list.dianhuiContext}</td>
                                    <td>${list.zhuanzhangPayAccount}</td>
                                    <td>${list.zhuanzhangCollectionAccount}</td>
                                    <td>
                                        <button name="edit" id="${list.id}" atype="${list.documentType}" type="button" class="btn btn-primary">修改</button>
                                        <button name="delete" id="${list.id}" atype="${list.documentType}" type="button" class="btn btn-default">删除</button>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                        <div style="font-size: 16px; width: 95%">
                            <div class="left" style="float: left; width: 75%">共${total}条记录</div>
                            <div class="right" style="float: left;">
                                <div class="btn-group">
                                    <button class="btn btn-white" onclick="change(1)">首页</button>
                                    <button type="button" class="btn btn-white" onclick="change(${currentPage-1})">
                                        <i class="fa fa-chevron-left"></i>
                                    </button>
                                    <button class="btn btn-white" onclick="change(${currentPage-1})">${currentPage-1}</button>
                                    <button class="btn btn-white  active" onclick="change(${currentPage})">${currentPage}</button>
                                    <button class="btn btn-white" onclick="change(${currentPage+1})">${currentPage+1}</button>
                                    <button type="button" class="btn btn-white" onclick="change(${currentPage+1})">
                                        <i class="fa fa-chevron-right"></i>
                                    </button>
                                    <button class="btn btn-white" onclick="change(${pageCount})">尾页</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        $('button[name=edit]').click(function () {
            var id = $(this).attr("id");
            var type = $(this).attr("atype");
            window.location.href = "<%=path%>/order/editOrderPage?id=" + id + "&type=" + type;
        });

        $('button[name=delete]').click(function () {
            var id = $(this).attr("id");
            swal({
                title: "您确定要删除这条信息吗",
                text: "删除后将无法恢复，请谨慎操作！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "是的，我要删除！",
                cancelButtonText: "让我再考虑一下…",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url:"<%=path%>/order/deleteReceivableInfo?id=" + id,
                        dataType:'json',
                        cache:false,
                        success:function (b) {
                            if(b.result){
                                swal({
                                    title: "成功!!",
                                    text: b.msg,
                                    type: "success",
                                    confirmButtonText: "确定",
                                    closeOnConfirm: false
                                }, function () {
                                    window.location.reload();
                                });
                            }else {
                                swal({
                                    title: "失败!!",
                                    text: b.msg,
                                    type: "warning",
                                    confirmButtonText: "确定",
                                    closeOnConfirm: false
                                }, function () {
                                    window.location.reload();
                                });
                            }

                        },
                        error:function (b) {
                            swal({
                                title: "失败!!",
                                text: b.msg,
                                type: "warning",
                                confirmButtonText: "确定",
                                closeOnConfirm: false
                            }, function () {
                                window.location.reload();
                            });
                        }
                    })

                } else {
                    swal({
                        title: "已取消!!",
                        text: "您已取消了操作",
                        type: "error",
                        confirmButtonText: "确定",
                        closeOnConfirm: false
                    }, function () {
                        window.location.reload();
                    });
                }
            })
        });
    });
</script>
<script>
    function change(page) {
        window.location = "<%=path%>/index/toReceivableList?page="+page;
    }
</script>
</html>