<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <script></script>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>销货单修改</title>
    <meta name="keywords" content="科研项目管理">
    <meta name="description" content="科研项目管理">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE">
    <link href="<%=path%>/statics2/saas/scm/app2_release/css/common.css?ver=20150522" rel="stylesheet" type="text/css">
    <link href="<%=path%>/statics2/saas/scm/app2_release/css/green/ui.min.css?ver=20150522" rel="stylesheet">
    <link href="<%=path%>/statics2/css/green/bills.css?ver=20150522" rel="stylesheet" type="text/css">
    <link href="<%=path%>/static/hplus/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/animate.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/bootstrap.min.css?v=4.1.0" rel="stylesheet">
    <style type="text/css">
        th{
            text-align: center;
        }
    </style>

    <link href="<%=path%>/static/hplus/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <script src="<%=path%>/static/hplus/js/plugins/sweetalert/sweetalert.min.js"></script>


    <link href="<%=path%>/static/hplus/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <script src="<%=path%>/static/hplus/js/plugins/sweetalert/sweetalert.min.js"></script>

    <link href="<%=path%>/static/hplus/css/bootstrap.min.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus//css/bootstrap.min.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/style.css?v=4.1.0" rel="stylesheet">

    <style>
        #table-3 thead, #table-3 tr {
            border-top-width: 1px;
            border-top-style: solid;
            border-top-color: rgb(235, 242, 224);
        }
        #table-3 {
            border-bottom-width: 1px;
            border-bottom-style: solid;
            border-bottom-color: rgb(235, 242, 224);
        }

        /* Padding and font style */
        #table-3 td, #table-3 th {
            padding: 5px 10px;
            font-size: 12px;
            font-family: Verdana;
            /*color: rgb(149, 170, 109);*/
        }

        /* Alternating background colors */
        #table-3 tr:nth-child(even) {
            background: #eaf5ff;
        }
        #table-3 tr:nth-child(odd) {
            background: #c7e5ff;
        }
    </style>
</head>
<body>
    <div class="wrapper" style="height: 100%" id="body">
        <div class="mod-toolbar-top mr0 cf dn" id="toolTop"></div>
        <div class="bills cf" style="height: 100%">
            <div class="con-header" style="width: 100%">
                <div class="row-item" style="text-align: center">
                    <h1 style="font-size: 23px">唐山军荣铝业有限公司销货单管理</h1>
                </div>
                <div class="row">
                    <div class="col-sm-6" style="width: 100%; height: 95%;">
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel panel-primary">
                                    <div class="panel-body">
                                        <table id="table-3" style="width: 100%" border="1">
                                            <thead>
                                                <tr style="background-color: grey;">
                                                    <th style="font-size: 16px; color: black; text-align: center">编号</th>
                                                    <th style="font-size: 16px; color: black; text-align: center">单据日期</th>
                                                    <th style="font-size: 16px; color: black; text-align: center">单据编号</th>
                                                    <th style="font-size: 16px; color: black; text-align: center">销售人员</th>
                                                    <th style="font-size: 16px; color: black; text-align: center">客户名称</th>
                                                    <th style="font-size: 16px; color: black; text-align: center">销售金额</th>
                                                    <th style="font-size: 16px; color: black; text-align: center">收款状态</th>
                                                    <th style="font-size: 16px; color: black; text-align: center">录入人</th>
                                                    <th style="font-size: 16px; color: black; text-align: center" width="150">操作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <!-管理员登陆，显示全部单据->
                                                <c:if test="${sessionScope.level == 0}">
                                                    <c:forEach items="${list}" var="list">
                                                        <tr style="height: 40px;">
                                                            <td align="center">${list.index}</td>
                                                            <td align="center">${list.createDate}</td>
                                                            <td align="center">${list.documentNumber}</td>
                                                            <td align="center">${list.salesmanName}</td>
                                                            <td align="center">${list.customerName}</td>
                                                            <td align="center">${list.saleMoney}</td>
                                                            <td align="center">${list.payDate}</td>
                                                            <td align="center">${list.inputMan}</td>
                                                            <td>
                                                                <button onclick="edit(${list.id})" type="button" class="btn btn-primary">修改</button>
                                                                <button aid="${list.id}" name="delete" type="button" class="btn btn-default">删除</button>
                                                            </td>
                                                        </tr>
                                                    </c:forEach>
                                                </c:if>
                                                <!--业务员登陆显示属于当前用户的单据-->
                                                <c:if test="${sessionScope.level == 1}">
                                                    <c:forEach items="${list}" var="list" >
                                                        <tr style="height: 40px;">
                                                            <td align="center">${list.index}</td>
                                                            <td align="center">${list.createDate}</td>
                                                            <td align="center">${list.documentNumber}</td>
                                                            <td align="center">${list.salesmanName}</td>
                                                            <td align="center">${list.customerName}</td>
                                                            <td align="center">${list.saleMoney}</td>
                                                            <td align="center">${list.payDate}</td>
                                                            <td align="center">${list.inputMan}</td>
                                                            <td>
                                                                <button onclick="edit(${list.id})" type="button" class="btn btn-primary">修改</button>
                                                                <button aid="${list.id}" name="delete" type="button" class="btn btn-default">删除</button>
                                                            </td>
                                                        </tr>
                                                    </c:forEach>
                                                </c:if>
                                            </tbody>
                                        </table>
                                        <div style="font-size: 16px; width: 95%">
                                            <div class="left" style="float: left; width: 75%">共${total}条记录</div>
                                            <div class="right" style="float: left;">
                                                <div class="btn-group">
                                                    <button class="btn btn-white" onclick="change(1)">首页</button>
                                                    <button type="button" class="btn btn-white" onclick="change(${currentPage-1})">
                                                        <i class="fa fa-chevron-left"></i>
                                                    </button>
                                                    <button class="btn btn-white" onclick="change(${currentPage-1})">${currentPage-1}</button>
                                                    <button class="btn btn-white  active" onclick="change(${currentPage})">${currentPage}</button>
                                                    <button class="btn btn-white" onclick="change(${currentPage+1})">${currentPage+1}</button>
                                                    <button type="button" class="btn btn-white" onclick="change(${currentPage+1})">
                                                        <i class="fa fa-chevron-right"></i>
                                                    </button>
                                                    <button class="btn btn-white" onclick="change(${pageCount})">尾页</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/jquery/jquery-1.10.2.min.js"></script>
<script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/json3.min.js"></script>
<script src="<%=path%>/statics2/saas/scm/app2_release/js/common/common.js?ver=20150522"></script>
<script src="<%=path%>/statics2/saas/scm/app2_release/js/common/grid.js?ver=20150522"></script>
<script src="<%=path%>/statics2/saas/scm/app2_release/js/common/plugins.js?ver=20150522"></script>
<script src="<%=path%>/statics2/saas/scm/app2_release/js/common/plugins/jquery.dialog.js?self=true&ver=20150522"></script>
<script src="<%=path%>/static/hplus/js/bootstrap.min.js"></script>
<script>
    /*分页方法*/
    function change(page) {
        window.location = "<%=path%>/index/salesOrdersList?page="+page;
    };
    /*修改操作方法*/
    function edit(id) {
        $.ajax({
            type: "POST",
            data: {id:id},
            url:"/ssm/sales/isEdit",
            dataType:'json',
            cache:false,
            success:function (b) {
                if(b.result){
                    top.location=self.location.href = "<%=path%>/sales/editSalesOrder?id="+id;
                }else {
                    swal({
                        title: "失败!!",
                        text: b.msg,
                        type: "success",
                        confirmButtonText: "确定",
                        closeOnConfirm: false
                    }, function () {
                        window.location.reload();
                    });
                }

            },
            error:function (b) {
                swal({
                    title: "失败!!",
                    text: b.msg,
                    type: "success",
                    confirmButtonText: "确定",
                    closeOnConfirm: false
                }, function () {
                    window.location.reload();
                });
            }
        })
    };
    /*删除操作方法*/
    $('[name=delete]').click(function(){
        var aId=$(this).attr("aid");
        swal({
            title: "您确定要删除这条信息吗",
            text: "删除后将无法恢复，请谨慎操作！",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "是的，我要删除！",
            cancelButtonText: "让我再考虑一下…",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    data: {id:aId},
                    url:"/ssm/sales/delete",
                    dataType:'json',
                    cache:false,
                    success:function (b) {
                        if(b.result){
                            swal({
                                title: "成功!!",
                                text: b.msg,
                                type: "success",
                                confirmButtonText: "确定",
                                closeOnConfirm: false
                            }, function () {
                                window.location.reload();
                            });
                        }else {
                            swal({
                                title: "失败!!",
                                text: b.msg,
                                type: "warning",
                                confirmButtonText: "确定",
                                closeOnConfirm: false
                            }, function () {
                                window.location.reload();
                            });
                        }

                    },
                    error:function (b) {
                        swal({
                            title: "失败!!",
                            text: b.msg,
                            type: "warning",
                            confirmButtonText: "确定",
                            closeOnConfirm: false
                        }, function () {
                            window.location.reload();
                        });
                    }
                })

            } else {
                swal({
                    title: "已取消!!",
                    text: "您已取消了操作",
                    type: "error",
                    confirmButtonText: "确定",
                    closeOnConfirm: false
                }, function () {
                    window.location.reload();
                });
            }
        })
    });
</script>
</html>