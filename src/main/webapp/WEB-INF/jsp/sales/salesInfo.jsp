<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <script></script>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>销货单添加</title>
    <meta name="keywords" content="科研项目管理">
    <meta name="description" content="科研项目管理">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE">
    <link href="<%=path%>/statics2/saas/scm/app2_release/css/common.css?ver=20150522" rel="stylesheet" type="text/css">
    <link href="<%=path%>/statics2/saas/scm/app2_release/css/green/ui.min.css?ver=20150522" rel="stylesheet">
    <link href="<%=path%>/statics2/css/green/bills.css?ver=20150522" rel="stylesheet" type="text/css">
    <link href="<%=path%>/static/hplus/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/animate.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/bootstrap.min.css?v=4.1.0" rel="stylesheet">
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/jquery/jquery-1.10.2.min.js"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/json3.min.js"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/common.js?ver=20150522"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/grid.js?ver=20150522"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/plugins.js?ver=20150522"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/plugins/jquery.dialog.js?self=true&ver=20150522"></script>
    <script src="<%=path%>/static/hplus/js/bootstrap.min.js"></script>

    <link href="<%=path%>/static/hplus/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <script src="<%=path%>/static/hplus/js/plugins/sweetalert/content.min.js?v=1.0.0"></script>
    <script src="<%=path%>/static/hplus/js/plugins/sweetalert/sweetalert.min.js"></script>
    <style type="text/css">
        th{
            text-align: center;
        }
    </style>
</head>
<body>
<input type="hidden" value="<%=path%>" id="hid">
<div class="wrapper" style="height: 100%;" id="body" align="center">
    <span id="config" class="ui-icon ui-state-default ui-icon-config"></span>
    <div class="mod-toolbar-top mr0 cf dn" id="toolTop"></div>
    <div class="bills cf" style="width:1400px;">
        <div style="width: 100%;">
            <form action="" method="post" id="form1">
                <div style="width: 100%" align="center">
                    <table>
                        <tr style="height: 100px">
                            <td></td>
                            <td colspan="3"><h1 style="font-size: 23px">唐山军荣铝业有限公司销货单修改</h1></td>
                        </tr>
                        <td style="width:25%;">
                            <label>客户：</label>
                            <span class="ui-combo-wrap" id="customer">
                                <input type="hidden" name="id" value="${id}">
                                <input type="text" name="customerName" class="input-txt" autocomplete="off" value="${salesOrders.customerName}" data-ref="date" id="customerName">
                                <a id="chooseCustomer"><i class="ui-icon-ellipsis"></i></a>
                            </span>
                        </td>
                        <td style="width:25%;">
                            <label>发货日期:</label>
                            <input type="text" id="createDate" class="ui-input ui-datepicker-input" value="${salesOrders.createDate}" name="creationDate">
                        </td>
                        <!-管理员登陆显示选择销售人员->
                        <c:if test="${sessionScope.level == 0}">
                            <td style="width:25%;">
                                <label style="color: red">销售人员:</label>
                                <span class="ui-combo-wrap" id="sales">
                                      <input type="text" class="input-txt" autocomplete="off" value="${salesOrders.salesmanName}" name="salesmanName" id="salesManName" style="width: 120px;">
                                      <a id="chooseSalesMan"><i class="ui-icon-ellipsis"></i></a>
                                    </span>
                            </td>
                        </c:if>
                        <!-业务员登陆隐藏选择销售人员，使用当前用户ID->
                        <c:if test="${sessionScope.level == 1}">
                            <td style="width:25%;" hidden>
                                <label style="color: red">销售人员:</label>
                                <input type="text" class="input-txt" autocomplete="off" value="${salesOrders.salesmanName}" name="salesmanName" style="width: 120px;" id="salesManName">
                            </td>
                        </c:if>
                        <td style="width:25%;">
                            <label>单据编号:</label>
                            <input id="number" type="text" class="input-txt ui-input" autocomplete="off" value="${salesOrders.documentNumber}" name="documentNumber" style="width: 120px">
                        </td>
                    </table>
                </div>
            </form>
        </div>
        <table style="width: 100%; height: 40px"></table>
        <div style="width: 100%">
            <form action="" method="post" id="form2">
                <div style="width: 100%" align="center">
                    <table style="border: 1px solid black" border="1" cellspacing="0" align="center" id="table">
                        <tr>
                            <th width="20"></th><th></th><th colspan="5">本厂生产的产品</th><th style="width: 1px; background-color: black"></th><th colspan="4">外购产品</th><th>运费</th>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td><span style="font-size: 18px">产品规格</br>型号名称</span></td>
                            <td><span style="font-size: 18px">数量</span></td>
                            <td><span style="font-size: 18px">含税单价（元）</span></td>
                            <td><span style="font-size: 18px">含税金额（元）</span></td>
                            <td><span style="font-size: 18px">备注</span></td>
                            <td style="background-color: black;width: 1px"></td>
                            <td><span style="font-size: 18px">外购产品规</br>格型号名称</span></td>
                            <td><span style="font-size: 18px">数量</span></td>
                            <td><span style="font-size: 18px">含税单价（元）</span></td>
                            <td><span style="font-size: 18px">含税金额（元）</span></td>
                            <td></td>
                        </tr>
                        <c:forEach items="${payment}" var="payment" varStatus="count">
                            <tr>
                                <td align="center" width="20">
                                    <span>${payment.index}</span>
                                </td>
                                <td width="40">
                                    <i class="ui-icon ui-icon-plus" title="新增行" style="float: left" ></i>
                                    <i class="ui-icon ui-icon-trash" title="删除行" style="float: left"></i>
                                </td>
                                <td width="180">
                                <span class="ui-combo-wrap" style="width: 100%">
                                  <input type="text" class="input-txt" autocomplete="off" value="${payment.bProduct}" name="BProductName" id="BProductName${payment.index}" style="width: 80%">
                                  <a name="chooseBProductName" tid="BProductName${payment.index}"><i class="ui-icon-ellipsis" style="width: 20%"></i></a>
                                </span>
                                </td>
                                <td width="60">
                                <span class="ui-combo-wrap" style="width: 100%">
                                  <input type="text" class="input-txt" autocomplete="off" value="${payment.bCount}" name="BCount" BName="BCount" id="BCount${payment.index}" style="width: 100%">
                                </span>
                                </td>
                                <td width="140">
                                <span class="ui-combo-wrap" style="width: 100%">
                                  <input type="text" class="input-txt" autocomplete="off" value="${payment.bPrice}" name="BPrice" BName="BPrice" id="BPrice${payment.index}" style="width: 100%">
                                </span>
                                </td>
                                <td width="140">
                                <span class="ui-combo-wrap" style="width: 100%">
                                  <input type="text" class="input-txt" autocomplete="off" value="${payment.bMoney}" name="BMoney" BName="BMoney" id="BMoney${payment.index}" style="width: 100%">
                                </span>
                                </td>
                                <td width="60">
                                <span class="ui-combo-wrap" style="width: 100%">
                                  <input type="text" class="input-txt" autocomplete="off" value="${payment.bBak}" name="BBak" style="width: 100%">
                                </span>
                                </td>
                                <td style="background-color: black;width: 1px"></td>
                                <td width="180">
                                <span class="ui-combo-wrap" style="width: 100%">
                                  <input type="text" class="input-txt" autocomplete="off" value="${payment.wProduct}" name="WProductName" id="WProductName${payment.index}" style="width: 80%">
                                  <a name="chooseWProductName" tid="WProductName${payment.index}"><i class="ui-icon-ellipsis" style="width: 20%"></i></a>
                                </span>
                                </td>
                                <td width="60">
                                <span class="ui-combo-wrap" style="width: 100%">
                                  <input type="text" class="input-txt" autocomplete="off" value="${payment.wCount}" name="WCount" id="WCount${payment.index}" style="width: 100%">
                                </span>
                                </td>
                                <td width="140">
                                <span class="ui-combo-wrap" style="width: 100%">
                                  <input type="text" class="input-txt" autocomplete="off" value="${payment.wPrice}" name="WPrice" id="WPrice${payment.index}" style="width: 100%">
                                </span>
                                </td>
                                <td width="140">
                                <span class="ui-combo-wrap" style="width: 100%">
                                  <input type="text" class="input-txt" autocomplete="off" value="${payment.wMoney}" name="WMoney" id="WMoney${payment.index}" style="width: 100%">
                                </span>
                                </td>
                                ${payment.html}
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </form>
            <form id="form4">
                <div style="width: 100%" align="center">
                    <table style="border: 1px solid black; width: 1205px" border="1" cellspacing="0" align="center" id="table1">
                        <tr>
                            <td width="61"></td>
                            <td width="380"><span style="font-size: 18px; color: red">本厂金额合计</span></td>
                            <td width="140">
                                <input type="text" id="PSum" class="input-txt ui-input" autocomplete="off" value="${salesOrders.pSum}" name="pSum" style="width: 100%;">
                            </td>
                            <td width="60"></td>
                            <td style="width: 1px"></td>
                            <td width="380"><span style="font-size: 18px; color: red">外购金额合计</span></td>
                            <td width="140">
                                <input type="text" id="WSum" class="input-txt ui-input" autocomplete="off" value="${salesOrders.wSum}" name="wSum" style="width: 100%;">
                            </td>
                            <td></td>
                        </tr>
                    </table>
                </div>
            </form>
        </div>
        <table style="width: 100%; height: 40px"></table>
        <div style="width: 100%">
            <form action="" method="post" id="form3">
                <div style="width: 100%;" align="center">
                    <table width="1215px">
                        <tr>
                            <td style="width: 33%" align="center">
                                <label>录入人员：</label>
                                <input type="text" id="inputMan" class="input-txt ui-input" autocomplete="off" value="${salesOrders.inputMan}" name="inputMan">
                            </td>
                            <td style="width: 33%" align="center">
                                <label>复核人员：</label>
                                <input type="text" id="reviewerMan" class="input-txt ui-input" autocomplete="off" value="${salesOrders.reviewerMan}" name="reviewerMan">
                            </td>
                            <td style="width: 33%" align="center">
                                <label>是否老客户：</label>
                                <c:if test="${salesOrders.regularCustomer == 0}">
                                    <input type="radio" name="regularCustomer" class="regular_customer"  value="1">是&nbsp&nbsp&nbsp
                                    <input type="radio" name="regularCustomer" class="regular_customer" checked value="0">否
                                </c:if>
                                <c:if test="${salesOrders.regularCustomer == 1}">
                                    <input type="radio" name="regularCustomer" class="regular_customer" checked value="1">是&nbsp&nbsp&nbsp
                                    <input type="radio" name="regularCustomer" class="regular_customer" value="0">否
                                </c:if>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 33%" align="center">
                                <label>应付款日期:</label>
                                <input type="text" id="dueDate1" class="input-txt ui-input" value="${salesOrders.dueDate1}" name="dueDate1" style="width: 120px"></br>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="text" id="dueDate2" class="input-txt ui-input" value="${salesOrders.dueDate2}" name="dueDate2" style="width: 120px"></br>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="text" id="dueDate3" class="input-txt ui-input" value="${salesOrders.dueDate3}" name="dueDate3" style="width: 120px"></br>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="text" id="dueDate4" class="input-txt ui-input" value="${salesOrders.dueDate4}" name="dueDate4" style="width: 120px">
                                <%--<input type="text" id="dueDate" class="input-txt ui-input" value="" name="dueDate">--%>
                            </td>
                            <td style="width: 33%" align="center">
                                <label>实际付款日期:</label>
                                <input type="text" id="payDate" class="input-txt ui-input" value="${salesOrders.payDate}" name="payDate" style="width: 120px">
                            </td>
                            <td style="width: 33%" align="center">
                                <label>是否超期&nbsp&nbsp&nbsp：</label>
                                <c:if test="${salesOrders.whetherExtended ==0}">
                                    <input type="radio" name="whetherExtended" class="regular_customer"  value="1">是&nbsp&nbsp&nbsp
                                    <input type="radio" name="whetherExtended" class="regular_customer" checked value="0">否
                                </c:if>
                                <c:if test="${salesOrders.whetherExtended ==1}">
                                    <input type="radio" name="whetherExtended" class="regular_customer" checked value="1">是&nbsp&nbsp&nbsp
                                    <input type="radio" name="whetherExtended" class="regular_customer" value="0">否
                                </c:if>
                            </td>
                        </tr>
                        <tr style="padding-top: 60px; height: 120px">
                            <td colspan="2">
                                <label>不含税不含运费销售总金额：</label>
                                <input type="text" class="input-txt ui-input" autocomplete="off" value="${salesOrders.saleMoney}" id="salesMoney" name="saleMoney">
                            </td>
                            <td align="right">
                                <input id="submit" type="button" value="修改销货单" style="background-color:#B5CBEC;">
                            </td>
                        </tr>
                    </table>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
<!-- 商品列表一 -->
<div class="modal inmodal fade" id="product_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" >
                <div class="modal-header">
                    <label>选择商品</label>
                </div>
                <div style="margin-top: 10px">
                    <label>
                        <input id="selectProductName" type="text" placeholder="请输入商品名称" oninput="selectProductByName1(this.value)">
                    </label>
                </div>
                <div class="modal-body" style="overflow:scroll; height:400px;" id="tableDiv">
                    <table style="width: 100%; border: 1px solid #b1b1b1" border="1">
                        <c:forEach items="${rows}" var="rows">
                            <tr style="height: 50px">
                                <td align="center"><input type="radio" value="${rows.name1}" tValue="${rows.size1}" name="selectName" id="${rows.name1}"></td>
                                <td align="center" ondblclick="shuangji('${rows.name1}')">${rows.type1}&nbsp${rows.name1}</td>
                                <td align="center"><input type="radio" value="${rows.name2}" tValue="${rows.size2}" name="selectName" id="${rows.name2}"></td>
                                <td align="center" ondblclick="shuangji('${rows.name2}')">${rows.type2}&nbsp${rows.name2}</td>
                                <td align="center"><input type="radio" value="${rows.name3}" tValue="${rows.size3}" name="selectName" id="${rows.name3}"></td>
                                <td align="center" ondblclick="shuangji('${rows.name3}')">${rows.type3}&nbsp${rows.name3}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="modal-footer">
                    <div style="float: left; width: 25%">
                        <h4>共${total}件商品</h4>
                    </div>
                    <div style="float: left; width: 50%" id="page">
                        <input type="button" value="首页" onclick="change(1)"><input type="button" value="上一页" onclick="change(1)"><input type="button" value="1" onclick="change(1)" style="background-color:deepskyblue; width: 20px"><input type="button" value="2" onclick="change(2)" style="width: 20px"><input type="button" value="3" onclick="change(3)" style="width: 20px"><input type="button" value="下一页" onclick="change(${currentPage+1})"><input type="button" value="尾页" onclick="change(${pageCount})">
                    </div>
                    <div style="float: left; width: 25%">
                        <button id="close_add" type="button" class="btn btn-white" data-dismiss="modal">取消</button>
                        <button class="btn btn-primary" type="submit" id="modal_add">确定</button>
                    </div>
                    <div style="float: none"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- 商品列表二 -->
<div class="modal inmodal fade" id="product_modal2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <label>选择商品</label>
                </div>
                <div style="margin-top: 10px">
                    <label>
                        <input id="selectProductName2" type="text" placeholder="请输入商品名称" oninput="selectProductByName2(this.value)">
                    </label>
                </div>
                <div class="modal-body" style="overflow:scroll; height:400px;" id="tableDiv2">
                    <table style="width: 100%; border: 1px solid #b1b1b1" border="1">
                        <c:forEach items="${rows}" var="rows">
                            <tr style="height: 50px">
                                <td align="center"><input type="radio" value="${rows.name1}" tValue="${rows.size1}"  name="selectName2" id="${rows.id1}"></td>
                                <td align="center" ondblclick="shuangji2('${rows.id1}')">${rows.type1}&nbsp${rows.name1}</td>
                                <td align="center"><input type="radio" value="${rows.name2}" tValue="${rows.size2}"  name="selectName2" id="${rows.id2}"></td>
                                <td align="center" ondblclick="shuangji2('${rows.id2}')">${rows.type2}&nbsp${rows.name2}</td>
                                <td align="center"><input type="radio" value="${rows.name3}" tValue="${rows.size3}"  name="selectName2" id="${rows.id3}"></td>
                                <td align="center" ondblclick="shuangji2('${rows.id3}')">${rows.type3}&nbsp${rows.name3}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="modal-footer">
                    <div style="float: left; width: 25%">
                        <h4>共${total}件商品</h4>
                    </div>
                    <div style="float: left; width: 50%" id="page2">
                        <input type="button" value="首页" onclick="change2(1)"><input type="button" value="上一页" onclick="change2(1)"><input type="button" value="1" onclick="change2(1)" style="background-color:deepskyblue; width: 20px"><input type="button" value="2" onclick="change2(2)" style="width: 20px"><input type="button" value="3" onclick="change2(3)" style="width: 20px"><input type="button" value="下一页" onclick="change2(${currentPage+1})"><input type="button" value="尾页" onclick="change2(${pageCount})">
                    </div>
                    <div style="float: left; width: 25%">
                        <button id="close_add2" type="button" class="btn btn-white" data-dismiss="modal">取消</button>
                        <button class="btn btn-primary" type="submit" id="modal_add2">确定</button>
                    </div>
                    <div style="float: none"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--客户列表-->
<div class="modal inmodal fade" id="customer_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <label>选择客户</label>
                </div>
                <div style="margin-top: 10px">
                    <label>
                        <input id="selectCustomerName" type="text" placeholder="请输入客户名称" oninput="selectCustomerByName(this.value)">
                    </label>
                </div>
                <div class="modal-body" style="overflow:scroll; height:400px;" id="customerTable">
                    <table style="width: 100%; border: 1px solid #b1b1b1" border="1">
                        <c:forEach items="${customerRows}" var="customerRows">
                            <tr style="height: 50px">
                                <td align="center"><input type="radio" value="${customerRows.name1}" name="selectCustomer" id="${customerRows.name1}"></td>
                                <td align="center" ondblclick="shuangji4('${customerRows.name1}')">${customerRows.name1}</td>
                                <td align="center"><input type="radio" value="${customerRows.name2}" name="selectCustomer" id="${customerRows.name2}"></td>
                                <td align="center" ondblclick="shuangji4('${customerRows.name2}')">${customerRows.name2}</td>
                                <td align="center"><input type="radio" value="${customerRows.name3}" name="selectCustomer" id="${customerRows.name3}"></td>
                                <td align="center" ondblclick="shuangji4('${customerRows.name3}')">${customerRows.name3}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="modal-footer">
                    <div style="float: left; width: 25%">
                        <h4>共${size}家客户</h4>
                    </div>
                    <div style="float: left; width: 50%" id="page3">
                        <input type="button" value="首页" onclick="changeCustomer(1)"><input type="button" value="上一页" onclick="changeCustomer(1)"><input type="button" value="1" onclick="changeCustomer(1)" style="background-color:deepskyblue; width: 20px"><input type="button" value="2" onclick="changeCustomer(2)" style="width: 20px"><input type="button" value="3" onclick="changeCustomer(3)" style="width: 20px"><input type="button" value="下一页" onclick="changeCustomer(${currentPage+1})"><input type="button" value="尾页" onclick="changeCustomer(${pageCount})">
                    </div>
                    <div style="float: left; width: 25%">
                        <button id="close_customer" type="button" class="btn btn-white" data-dismiss="modal">取消</button>
                        <button class="btn btn-primary" type="submit" id="customer_end" >确定</button>
                    </div>
                    <div style="float: none"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--销售人员列表-->
<div class="modal inmodal fade" id="salesMan_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 650px">
            <div class="modal-body" >
                <div class="modal-header">
                    <label>选择选择销售人员</label>
                </div>
                <div class="modal-body" style="overflow:scroll; height:400px;" id="salesManTable">
                    <table style="width: 100%; border: 1px solid #b1b1b1" border="1">
                        <c:forEach items="${saleManRows}" var="saleManRows">
                            <tr style="height: 50px">
                                <td align="center"><input type="radio" value="${saleManRows.name1}" name="selectSales" id="${saleManRows.name1}"></td>
                                <td align="center" ondblclick="shuangji6('${saleManRows.name1}')">${saleManRows.name1}</td>
                                <td align="center"><input type="radio" value="${saleManRows.name2}" name="selectSales" id="${saleManRows.name2}"></td>
                                <td align="center" ondblclick="shuangji6('${saleManRows.name2}')">${saleManRows.name2}</td>
                                <td align="center"><input type="radio" value="${saleManRows.name3}" name="selectSales" id="${saleManRows.name3}"></td>
                                <td align="center" ondblclick="shuangji6('${saleManRows.name3}')">${saleManRows.name3}</td>
                            </tr>
                        </c:forEach>>
                    </table>
                </div>
                <div class="modal-footer">
                    <div style="float: left; width: 25%">
                        <h4>共${saleSize}位销售人员</h4>
                    </div>
                    <div style="float: left; width: 50%" id="page4">
                        <input type="button" value="首页" onclick="changeSalesMan(1)"><input type="button" value="上一页" onclick="changeSalesMan(1)"><input type="button" value="1" onclick="changeSalesMan(1)" style="background-color:deepskyblue; width: 20px"><input type="button" value="2" onclick="changeSalesMan(2)" style="width: 20px"><input type="button" value="3" onclick="changeSalesMan(3)" style="width: 20px"><input type="button" value="下一页" onclick="changeSalesMan(${currentPage+1})"><input type="button" value="尾页" onclick="changeSalesMan(${pageCount})">
                    </div>
                    <div style="float: right; width: 25%">
                        <button id="close_salesMan" type="button" class="btn btn-white" data-dismiss="modal">取消</button>
                        <button class="btn btn-primary" type="submit" id="salesMan_end">确定</button>
                    </div>
                    <div style="float: none"></div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<!--表格编号方法-->
<script>
    /*表格下标*/
    var index = ${payment.size()};
    /*初始化表格下标*/
</script>
<!--选择客户方法-->
<script>
    /*所选客户*/
    var customerName = "";
    /*打开客户面板*/
    $("a[id='chooseCustomer']").click(function () {
        $("#customer_modal").modal();
    });
    /*选择客户*/
    $("input[name='selectCustomer']").click(function () {
        customerName = $(this).val();
    })
    /*确定所选客户*/
    $("button[id='customer_end']").click(function () {
        var name = document.getElementById("customerName");
        name.value = customerName;
        $("#close_customer").click();
        changeCustomer(1);
        customerName = "";
    })
    function selectCustomer(value) {
        customerName = value;
    }
</script>
<!--填写订货日期方法-->
<script>
    $(function() {
        $("#createDate").datepicker();
    });
</script>
<!--选择销售人员方法-->
<script>
    /*所选销售人员*/
    var salesManName = "";
    /*打开销售人员面板*/
    $("a[id='chooseSalesMan']").click(function () {
        $("#salesMan_modal").modal();
    });
    /*选择销售人员*/
    $("input[name='selectSales']").click(function () {
        salesManName = $(this).val();
    });
    /*确定所选销售人员*/
    $("button[id='salesMan_end']").click(function () {
        var saleName = document.getElementById("salesManName");
        saleName.value = salesManName;
        $("#close_salesMan").click();
        salesManName = "";
    })
    function selectSalesMan(value) {
        salesManName = value;
    }
</script>
<!--填写应付款日期方法-->
<script>
    $("#createDate").blur(function () {
        var a = new Date($(this).val());
        var b = new Date(a.getTime() + 1000 * 60 * 60 * 24 * 120).format("yyyy-MM-dd");
        var c = new Date(a.getTime() + 1000 * 60 * 60 * 24 * 180).format("yyyy-MM-dd");
        var d = new Date(a.getTime() + 1000 * 60 * 60 * 24 * 240).format("yyyy-MM-dd");
        var e = new Date(a.getTime() + 1000 * 60 * 60 * 24 * 360).format("yyyy-MM-dd");

        var dueDateTime1 = b+"/120";
        var dueDateTime2 = c+"/180";
        var dueDateTime3 = d+"/240";
        var dueDateTime4 = e+"/360";

        $("#dueDate1").val(dueDateTime1);
        $("#dueDate2").val(dueDateTime2);
        $("#dueDate3").val(dueDateTime3);
        $("#dueDate4").val(dueDateTime4);
    });
</script>
<!--填写实际付款日期方法-->
<script>
    $(function () {
        $("#payDate").datepicker();
    })
</script>
<!--添加一行表格方法-->
<script>
    $("i[class='ui-icon ui-icon-plus']").click(function () {
        addRow();
    })
    function addRow() {
        var tr =
            "<tr>"+
            "<td align='center'width='20'>"+
            "<span>"+(index+1)+"</span>"+
            "</td>"+
            "<td width='40'>" +
            "<i class='ui-icon ui-icon-plus' title='新增行' style='float: left' onclick='addRow()'></i><i class='ui-icon ui-icon-trash' title='删除行' style='float: left' onclick='deleteRow(this)'></i></td>"+
            "<td width='180'>"+
            "<span class='ui-combo-wrap' style='width: 100%;'>"+
            "<input type='text' class='input-txt' autocomplete='off' value='' name='BProductName' id='BB"+index+"' style='width: 80%;'>"+
            "<a xid='BB"+index+"' onclick='openProductPenal(this)'><i class='ui-icon-ellipsis' style='width: 20%;'></i></a>"+
            "</span>"+
            "</td>"+
            "<td width='60'>"+
            "<span class='ui-combo-wrap' style='width: 100%;'>"+
            "<input type='text' class='input-txt' autocomplete='off' value='' name='BCount' PId='B"+index+"' onblur='changeCount(this)' style='width: 100%;'>"+
            "</span>"+
            "</td>"+
            "<td width='140'>"+
            "<span class='ui-combo-wrap' style='width: 100%;'>"+
            "<input type='text' class='input-txt' autocomplete='off' value='' name='BPrice'  PId='B"+index+"' onblur='changePrice(this)' style='width: 100%;'>"+
            "</span>"+
            "</td>"+
            "<td width='140'>"+
            "<span class='ui-combo-wrap' style='width: 100%;'>"+
            "<input type='text' class='input-txt' autocomplete='off' value='' name='BMoney'  id='B"+index+"' style='width: 100%;'>"+
            "</span>"+
            "</td>"+
            "<td width='60'>"+
            "<span class='ui-combo-wrap' style='width: 100%;'>"+
            "<input type='text' class='input-txt' autocomplete='off' value='' name='BBak' style='width: 100%;'>"+
            "</span>"+
            "</td>"+
            "<td style='background-color: black;width: 1px'></td>"+
            "<td width='180'>"+
            "<span class='ui-combo-wrap' style='width: 100%;'>"+
            "<input type='text' class='input-txt' autocomplete='off' value='' name='WProductName' id='WW"+index+"' style='width: 80%;'>"+
            "<a xid='WW"+index+"' onclick='openProductPenal2(this)'><i class='ui-icon-ellipsis' style='width: 20%;'></i></a>"+
            "</span>"+
            "</td>"+
            "<td width='60'>"+
            "<span class='ui-combo-wrap' style='width: 100%;'>"+
            "<input type='text' class='input-txt' autocomplete='off' value='' name='WCount'  WId='W"+index+"' onblur='WChangeCount(this)' style='width: 100%;'>"+
            "</span>"+
            "</td>"+
            "<td width='140'>"+
            "<span class='ui-combo-wrap' style='width: 100%;'>"+
            "<input type='text' class='input-txt' autocomplete='off' value='' name='WPrice'  WId='W"+index+"' onblur='WChangePrice(this)' style='width: 100%;'>"+
            "</span>"+
            "</td>"+
            "<td width='140'>"+
            "<span class='ui-combo-wrap' style='width: 100%;'>"+
            "<input type='text' class='input-txt' autocomplete='off' value='' name='WMoney'  id='W"+index+"' style='width: 100%;'>"+
            "</span>"+
            "</td>"+
            "</tr>";
        $("#table").append(tr);
        var x=document.getElementById('table').rows[2].cells;
        x[12].rowSpan = index+1;
        index+=1;
    }
</script>
<!--删除一行表格-->
<script>
    $("i[class='ui-icon ui-icon-trash']").click(function () {
        debugger
        var tr = this.parentNode.parentNode;
        var table = tr.parentNode;
        table.removeChild(tr);
        index-=1;
        $("#PSum").val(sum());
        sumAll();
        $("#WSum").val(WSum());
        sumAll();
    });
    function deleteRow(a) {
        var tr = a.parentNode.parentNode;
        var table = tr.parentNode;
        table.removeChild(tr);
        index-=1;
        $("#PSum").val(sum());
        sumAll();
        $("#WSum").val(WSum());
        sumAll();
    };
</script>
<!--选择本厂商品方法-->
<script>
    /*所选商品*/
    var chooseProductName = "";
    /*操作行*/
    var productLine = "";
    /*第一行操作*/
    /*打开商品面板*/
    $("a[name='chooseBProductName']").click(function () {
        productLine =$(this).attr("tid");
        $("#product_modal").modal();
    });
    /*选择商品*/
    $("input[name='selectName']").click(function () {
        chooseProductName = $(this).val();
    });
    function enterSelect(value) {
        chooseProductName = value;
    };
    /*确定所选商品*/
    $("#modal_add").click(function () {
        var product = document.getElementById(productLine);
        $("#close_add").click();
        change(1);
        product.value = chooseProductName;
        chooseProductName = "";
    });
    /*第二行操作*/
    /*打开商品面板*/
    function openProductPenal(value) {
        productLine = $(value).attr("xid");
        $("#product_modal").modal();
    }
</script>
<!--选择外购商品方法-->
<script>
    /*所选商品*/
    var chooseWProduct = "";
    /*操作行*/
    var WProductLine = "";
    /*打开商品面板*/
    $("a[name='chooseWProductName']").click(function () {
        WProductLine =$(this).attr("tid");
        $("#product_modal2").modal();
    });
    /*选择商品*/
    $("input[name='selectName2']").click(function () {
        chooseWProduct = $(this).val();
    });
    function enterSelect2(value) {
        chooseWProduct = value;
    };
    /*确定所选商品*/
    $("#modal_add2").click(function () {
        var WProduct = document.getElementById(WProductLine);
        $("#close_add2").click();
        change2(1)
        WProduct.value = chooseWProduct;
        chooseWProduct = "";
    });
    /*第二行操作*/
    /*打开商品面板*/
    function openProductPenal2(value) {
        WProductLine = $(value).attr("xid");
        $("#product_modal2").modal();
    }
</script>
<!--计算含税金额方法一-->
<script>
    var i = 1;
    /*数量*/
    var BproductCount = 0;
    var WproductCount = 0;
    /*单价*/
    var BproductPrice = 0;
    var WproductPrice = 0;
    /*合计*/
    var PSum = 0;
    /*本厂商品数量填写完成*/
    $("input[id='BCount1']").blur(function () {
        var count = $(this).val();
        var price = $("#BPrice1").val();
        if (isNaN(count) ||  count < 0){
            alert("数量必须为必须为大于0的数字");
        } else {
            $("input[id='BMoney1']").val((count * price).toFixed(2));
            $("#PSum").val(sum());
            sumAll();
        }
    });
    $("input[id='BPrice1']").blur(function () {
        var price = $(this).val();
        var count = $("#BCount1").val();
        if (isNaN(price) ||  price < 0){
            alert("数量必须为必须为大于0的数字");
        } else {
            $("input[id='BMoney1']").val((count * price).toFixed(2));
            $("#PSum").val(sum());
            sumAll();
        }
    });

    $("input[id='BCount2']").blur(function () {
        var count = $(this).val();
        var price = $("#BPrice2").val();
        if (isNaN(count) ||  count < 0){
            alert("数量必须为必须为大于0的数字");
        } else {
            $("input[id='BMoney2']").val((count * price).toFixed(2));
            $("#PSum").val(sum());
            sumAll();
        }
    });
    $("input[id='BPrice2']").blur(function () {
        var price = $(this).val();
        var count = $("#BCount2").val();
        if (isNaN(price) ||  price < 0){
            alert("数量必须为必须为大于0的数字");
        } else {
            $("input[id='BMoney2']").val((count * price).toFixed(2));
            $("#PSum").val(sum());
            sumAll();
        }
    });

    $("input[id='BCount3']").blur(function () {
        var count = $(this).val();
        var price = $("#BPrice3").val();
        if (isNaN(count) || count < 0){
            alert("数量必须为必须为大于0的数字");
        } else {
            $("input[id='BMoney3']").val((count * price).toFixed(2));
            $("#PSum").val(sum());
            sumAll();
        }
    });
    $("input[id='BPrice3']").blur(function () {
        var price = $(this).val();
        var count = $("#BCount3").val();
        if (isNaN(price) ||  price < 0){
            alert("数量必须为必须为大于0的数字");
        } else {
            $("input[id='BMoney3']").val((count * price).toFixed(2));
            $("#PSum").val(sum());
            sumAll();
        }
    });

    $("input[id='BCount4']").blur(function () {
        var count = $(this).val();
        var price = $("#BPrice4").val();
        if (isNaN(count) || count < 0){
            alert("数量必须为必须为大于0的数字");
        } else {
            $("input[id='BMoney4']").val((count * price).toFixed(2));
            $("#PSum").val(sum());
            sumAll();
        }
    });
    $("input[id='BPrice4']").blur(function () {
        var price = $(this).val();
        var count = $("#BCount4").val();
        if (isNaN(price) ||  price < 0){
            alert("数量必须为必须为大于0的数字");
        } else {
            $("input[id='BMoney4']").val((count * price).toFixed(2));
            $("#PSum").val(sum());
            sumAll();
        }
    });

    $("input[id='BCount5']").blur(function () {
        var count = $(this).val();
        var price = $("#BPrice5").val();
        if (isNaN(count) || count < 0){
            alert("数量必须为必须为大于0的数字");
        } else {
            $("input[id='BMoney5']").val((count * price).toFixed(2));
            $("#PSum").val(sum());
            sumAll();
        }
    });
    $("input[id='BPrice5']").blur(function () {
        var price = $(this).val();
        var count = $("#BCount5").val();
        if (isNaN(price) ||  price < 0){
            alert("数量必须为必须为大于0的数字");
        } else {
            $("input[id='BMoney5']").val((count * price).toFixed(2));
            $("#PSum").val(sum());
            sumAll();
        }
    });

    $("input[id='BCount6']").blur(function () {
        var count = $(this).val();
        var price = $("#BPrice6").val();
        if (isNaN(count) || count < 0){
            alert("数量必须为必须为大于0的数字");
        } else {
            $("input[id='BMoney6']").val((count * price).toFixed(2));
            $("#PSum").val(sum());
            sumAll();
        }
    });
    $("input[id='BPrice6']").blur(function () {
        var price = $(this).val();
        var count = $("#BCount6").val();
        if (isNaN(price) ||  price < 0){
            alert("数量必须为必须为大于0的数字");
        } else {
            $("input[id='BMoney6']").val((count * price).toFixed(2));
            $("#PSum").val(sum());
            sumAll();
        }
    });

    /*外购商品填写完成*/
    $("input[id='WCount1']").blur(function () {
        var count = $(this).val();
        var price = $("#WPrice1").val();
        if (isNaN(count) ||  count < 0){
            alert("数量必须为必须为大于0的数字");
        } else {
            $("input[id='WMoney1']").val((count * price).toFixed(2));
            $("#WSum").val(WSum());
            sumAll();
        }
    });
    $("input[id='WPrice1']").blur(function () {
        var price = $(this).val();
        var count = $("#WCount1").val();
        if (isNaN(price) ||  price < 0){
            alert("数量必须为必须为大于0的数字");
        } else {
            $("input[id='WMoney1']").val((count * price).toFixed(2));
            $("#WSum").val(WSum());
            sumAll();
        }
    });

    $("input[id='WCount2']").blur(function () {
        var count = $(this).val();
        var price = $("#WPrice2").val();
        if (isNaN(count) ||  count < 0){
            alert("数量必须为必须为大于0的数字");
        } else {
            $("input[id='WMoney2']").val((count * price).toFixed(2));
            $("#WSum").val(WSum());
            sumAll();
        }
    });
    $("input[id='WPrice2']").blur(function () {
        var price = $(this).val();
        var count = $("#WCount2").val();
        if (isNaN(price) ||  price < 0){
            alert("数量必须为必须为大于0的数字");
        } else {
            $("input[id='WMoney2']").val((count * price).toFixed(2));
            $("#WSum").val(WSum());
            sumAll();
        }
    });

    $("input[id='WCount3']").blur(function () {
        var count = $(this).val();
        var price = $("#WPrice3").val();
        if (isNaN(count) ||  count < 0){
            alert("数量必须为必须为大于0的数字");
        } else {
            $("input[id='WMoney3']").val((count * price).toFixed(2));
            $("#WSum").val(WSum());
            sumAll();
        }
    });
    $("input[id='WPrice3']").blur(function () {
        var price = $(this).val();
        var count = $("#WCount3").val();
        if (isNaN(price) ||  price < 0){
            alert("数量必须为必须为大于0的数字");
        } else {
            $("input[id='WMoney3']").val((count * price).toFixed(2));
            $("#WSum").val(WSum());
            sumAll();
        }
    });

    $("input[id='WCount4']").blur(function () {
        var count = $(this).val();
        var price = $("#WPrice4").val();
        if (isNaN(count) ||  count < 0){
            alert("数量必须为必须为大于0的数字");
        } else {
            $("input[id='WMoney4']").val((count * price).toFixed(2));
            $("#WSum").val(WSum());
            sumAll();
        }
    });
    $("input[id='WPrice4']").blur(function () {
        var price = $(this).val();
        var count = $("#WCount4").val();
        if (isNaN(price) ||  price < 0){
            alert("数量必须为必须为大于0的数字");
        } else {
            $("input[id='WMoney4']").val((count * price).toFixed(2));
            $("#WSum").val(WSum());
            sumAll();
        }
    });

    $("input[id='WCount5']").blur(function () {
        var count = $(this).val();
        var price = $("#WPrice5").val();
        if (isNaN(count) ||  count < 0){
            alert("数量必须为必须为大于0的数字");
        } else {
            $("input[id='WMoney5']").val((count * price).toFixed(2));
            $("#WSum").val(WSum());
            sumAll();
        }
    });
    $("input[id='WPrice5']").blur(function () {
        var price = $(this).val();
        var count = $("#WCount5").val();
        if (isNaN(price) ||  price < 0){
            alert("数量必须为必须为大于0的数字");
        } else {
            $("input[id='WMoney5']").val((count * price).toFixed(2));
            $("#WSum").val(WSum());
            sumAll();
        }
    });

    $("input[id='WCount6']").blur(function () {
        var count = $(this).val();
        var price = $("#WPrice6").val();
        if (isNaN(count) ||  count < 0){
            alert("数量必须为必须为大于0的数字");
        } else {
            $("input[id='WMoney6']").val((count * price).toFixed(2));
            $("#WSum").val(WSum());
            sumAll();
        }
    });
    $("input[id='WPrice6']").blur(function () {
        var price = $(this).val();
        var count = $("#WCount6").val();
        if (isNaN(price) ||  price < 0){
            alert("数量必须为必须为大于0的数字");
        } else {
            $("input[id='WMoney6']").val((count * price).toFixed(2));
            $("#WSum").val(WSum());
            sumAll();
        }
    });

</script>
<!--综合计算合计方法本场-->
<script>
    function sum() {
        var sumA = 0;
        var inputs  = $("#table tr td input");
        for (var i = 0; i<10; i++){
            if (i==3){
                sumA += Number(inputs[i].value);
            }
        };
        if (inputs.size() > 10){
            for (var j=10; j<inputs.size(); j+=9){
                sumA += Number(inputs[j+3].value);
            }
        }
        return sumA.toFixed(2);
    }
</script>
<!--综合计算合计方法外购-->
<script>
    function WSum() {
        var sumB = 0;
        var inputs  = $("#table tr td input");
        for (var i = 0; i<10; i++){
            if (i==8){
                sumB += Number(inputs[i].value);
            }
        };
        if (inputs.size() > 10){
            for (var j=10; j<inputs.size(); j+=9){
                sumB += Number(inputs[j+8].value);
            }
        }
        return sumB.toFixed(2);
    }
</script>
<!--计算含税金额方法二-->
<script>
    /*位置*/
    var i = "";
    /*数量*/
    var BPCount = 0.0;
    var WPCount = 0.0;
    /*单价*/
    var BPPrice = 0.0;
    var WPPrice = 0.0;
    function changeCount(value) {
        i = $(value).attr("PId");
        BPCount = $(value).val();
        $("input[id='"+i+"']").val((BPCount*BPPrice).toFixed(2));
        $("#PSum").val(sum());
        sumAll();
        i = "";
    }
    function changePrice(value) {
        i = $(value).attr("PId");
        BPPrice = $(value).val();
        $("input[id='"+i+"'").val((BPCount*BPPrice).toFixed(2));
        $("#PSum").val(sum());
        sumAll();
        i = "";
    }
    function WChangeCount(value) {
        i = $(value).attr("WId");
        WPCount = $(value).val();
        $("input[id='"+i+"']").val((BPCount*BPPrice).toFixed(2));
        $("#WSum").val(WSum());
        sumAll();
        i="";
    }
    function WChangePrice(value) {
        i = $(value).attr("WId");
        WPPrice = $(value).val();
        $("input[id='"+i+"'").val((BPCount*BPPrice).toFixed(2));
        $("#WSum").val(WSum());
        sumAll();
        i="";
    }
</script>
<!--计算不含运费销售总金额-->
<script>
    $("#pushMoney").blur(function () {
        sumAll();
    })
    function sumAll() {
        var sumA = 0;
        var sumB = 0;
        var inputs  = $("#table tr td input");
        for (var i = 0; i<10; i++){
            if (i==3){
                sumA += Number(inputs[i].value);
            }
            if (i==8){
                sumB += Number(inputs[i].value);
            }
        };
        if (inputs.size() > 10){
            for (var j=10; j<inputs.size(); j+=9){
                sumA += Number(inputs[j+3].value);
                sumB += Number(inputs[j+8].value);
            }
        }
        var pushMoney = document.getElementById("pushMoney");
        var salesMoney = document.getElementById("salesMoney");

        var sum = parseFloat(sumA-sumB).toFixed(2);
        var avg = parseFloat((sum/1.17)).toFixed(2);
        var end = parseFloat(avg-(pushMoney.value)).toFixed(2);
        salesMoney.value = end;
        sumA = 0;
        sumB = 0;
    }
</script>
<!--异步提交表单方法-->
<script>
    $("input[id='submit']").click(function () {
        if ($("#customerName").val() == "" || $("#customerName").val() == null){
            alert("请选择客户");
        } else if ($("#createDate").val() == "" || $("#createDate").val() == null){
            alert("请填写发货日期");
        } else if ($("#salesManName").val() == "" || $("#salesManName").val() == null){
            alert("请选择销售人员");
        } else if ($("#number").val() == "" || $("#number").val() == null) {
            alert("请填写单据编号");
        } else {
            var data1 = $("#form1").serialize().replace(/&/g,',');
            var data2 = $("#form2").serialize().replace(/&/g,',');
            var data3 = $("#form3").serialize().replace(/&/g,',');
            var data4 = $("#form4").serialize().replace(/&/g,',');
            console.log(data4);
            $.ajax({
                type:"POST",
                url:"<%=path%>/sales/editSales?data1="+data1+"&data2="+data2+"&data3="+data3+"&data4="+data4,
                dataType:"json",
                success:function(e){
                    if (e.result){
                        swal({
                            title: "成功!!",
                            text: e.msg,
                            type: "success",
                            confirmButtonText: "确定",
                            closeOnConfirm: false
                        }, function () {
                            window.location.reload();
                        });
                    } else {
                        swal({
                            title: "失败!!",
                            text: e.msg,
                            type: "warning",
                            confirmButtonText: "确定",
                            closeOnConfirm: false
                        }, function () {
                            window.location.reload();
                        });
                    }
                }
            });
        }
    })
</script>
<!--分页方法-->
<script>
    function changeSalesMan(page) {
        $.ajax({
            type:"POST",
            url:"<%=path%>/sales/paginationSalesMan?page="+page,
            dataType:"json",
            success:function(e){
                if (e.result){
                    var table = "" +
                        "<table style='width: 100%; border: 1px solid #b1b1b1' border='1'>";
                    for (var i=0; i<e.t.saleManRows.length; i++){
                        var newTable = "" +
                            "<tr style='height: 50px'>"+
                            "<td align='center'><input type='radio' value='"+e.t.saleManRows[i].name1+"' name='selectSalesMan' onfocus='selectSalesMan(this.value)'></td>"+
                            "<td align='center'>"+e.t.saleManRows[i].name1+"</td>"+
                            "<td align='center'><input type='radio' value="+e.t.saleManRows[i].name2+" name='selectSalesMan' onfocus='selectSalesMan(this.value)'></td>"+
                            "<td align='center'>"+e.t.saleManRows[i].name2+"</td>"+
                            "<td align='center'><input type='radio' value="+e.t.saleManRows[i].name3+" name='selectSalesMan' onfocus='selectSalesMan(this.value)'></td>"+
                            "<td align='center'>"+e.t.saleManRows[i].name3+"</td>"+
                            "</tr>";
                        table = table+newTable;
                    }
                    table+="</table>";
                    var page = "" +
                        "<input type='button' value='首页' onclick='changeSalesMan(1)'>"+
                        "<input type='button' value='上一页' onclick='changeSalesMan("+(e.t.currentPage-1)+")'>"+
                        "<input type='button' value='"+(e.t.currentPage-1)+"' onclick='changeSalesMan("+(e.t.currentPage-1)+")' style='width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage)+"' onclick='changeSalesMan("+(e.t.currentPage)+")' style='background-color:deepskyblue; width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage+1)+"' onclick='changeSalesMan("+(e.t.currentPage+1)+")' style='width: 20px'>"+
                        "<input type='button' value='下一页' onclick='changeSalesMan("+(e.t.currentPage+1)+")'>"+
                        "<input type='button' value='尾页' onclick='changeSalesMan('"+e.t.pageCount+")'>"
                    $("#salesManTable").html(table);
                    $("#page4").html(page);
                }
            }
        });
    }
    function changeCustomer(page) {
        var name = document.getElementById("selectCustomerName").value;
        if (name != ""){
            $.ajax({
                type:"POST",
                url:"<%=path%>/sales/selectCustomerByName?page="+page+"&name="+name,
                dataType:"json",
                success:function(e){
                    if (e.result){
                        var table = "" +
                            "<table style='width: 100%; border: 1px solid #b1b1b1' border='1'>";
                        for (var i=0; i<e.t.customerRows.length; i++){
                            var newTable = "" +
                                "<tr style='height: 50px'>"+
                                "<td align='center'><input type='radio' value='"+e.t.customerRows[i].name1+"' name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name1+"'></td>"+
                                "<td align='center' ondblclick='shuangji5(this)'>"+e.t.customerRows[i].name1+"</td>"+
                                "<td align='center'><input type='radio' value="+e.t.customerRows[i].name2+" name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name2+"'></td>"+
                                "<td align='center' ondblclick='shuangji5(this)'>"+e.t.customerRows[i].name2+"</td>"+
                                "<td align='center'><input type='radio' value="+e.t.customerRows[i].name3+" name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name3+"'></td>"+
                                "<td align='center' ondblclick='shuangji5(this)'>"+e.t.customerRows[i].name3+"</td>"+
                                "</tr>";
                            table = table+newTable;
                        }
                        table+="</table>";
                        var page = "" +
                            "<input type='button' value='首页' onclick='changeCustomer(1)'>"+
                            "<input type='button' value='上一页' onclick='changeCustomer("+(e.t.currentPage-1)+")'>"+
                            "<input type='button' value='"+(e.t.currentPage-1)+"' onclick='changeCustomer("+(e.t.currentPage-1)+")' style='width: 20px'>"+
                            "<input type='button' value='"+(e.t.currentPage)+"' onclick='changeCustomer("+(e.t.currentPage)+")' style='background-color:deepskyblue; width: 20px'>"+
                            "<input type='button' value='"+(e.t.currentPage+1)+"' onclick='changeCustomer("+(e.t.currentPage+1)+")' style='width: 20px'>"+
                            "<input type='button' value='下一页' onclick='changeCustomer("+(e.t.currentPage+1)+")'>"+
                            "<input type='button' value='尾页' onclick='changeCustomer('"+e.t.pageCount+")'>"
                        $("#customerTable").html(table);
                        $("#page3").html(page);
                    }
                }
            });
        } else {
            $.ajax({
                type:"POST",
                url:"<%=path%>/sales/paginationCustomer?page="+page,
                dataType:"json",
                success:function(e){
                    if (e.result){
                        var table = "" +
                            "<table style='width: 100%; border: 1px solid #b1b1b1' border='1'>";
                        for (var i=0; i<e.t.customerRows.length; i++){
                            var newTable = "" +
                                "<tr style='height: 50px'>"+
                                "<td align='center'><input type='radio' value='"+e.t.customerRows[i].name1+"' name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name1+"'></td>"+
                                "<td align='center' ondblclick='shuangji5(this)'>"+e.t.customerRows[i].name1+"</td>"+
                                "<td align='center'><input type='radio' value="+e.t.customerRows[i].name2+" name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name2+"'></td>"+
                                "<td align='center' ondblclick='shuangji5(this)'>"+e.t.customerRows[i].name2+"</td>"+
                                "<td align='center'><input type='radio' value="+e.t.customerRows[i].name3+" name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name3+"'></td>"+
                                "<td align='center' ondblclick='shuangji5(this)'>"+e.t.customerRows[i].name3+"</td>"+
                                "</tr>";
                            table = table+newTable;
                        }
                        table+="</table>";
                        var page = "" +
                            "<input type='button' value='首页' onclick='changeCustomer(1)'>"+
                            "<input type='button' value='上一页' onclick='changeCustomer("+(e.t.currentPage-1)+")'>"+
                            "<input type='button' value='"+(e.t.currentPage-1)+"' onclick='changeCustomer("+(e.t.currentPage-1)+")' style='width: 20px'>"+
                            "<input type='button' value='"+(e.t.currentPage)+"' onclick='changeCustomer("+(e.t.currentPage)+")' style='background-color:deepskyblue; width: 20px'>"+
                            "<input type='button' value='"+(e.t.currentPage+1)+"' onclick='changeCustomer("+(e.t.currentPage+1)+")' style='width: 20px'>"+
                            "<input type='button' value='下一页' onclick='changeCustomer("+(e.t.currentPage+1)+")'>"+
                            "<input type='button' value='尾页' onclick='changeCustomer('"+e.t.pageCount+")'>"
                        $("#customerTable").html(table);
                        $("#page3").html(page);
                    }
                }
            });
        }
    }
    function change(page) {
        var name = document.getElementById("selectProductName").value;
        if (name != ""){
            $.ajax({
                type:"POST",
                url:"<%=path%>/sales/selectProductByName?page="+page+"&name="+name,
                dataType:"json",
                success:function(e){
                    console.log(e.t)
                    if (e.result){
                        var table = "" +
                            "<table style='width: 100%; border: 1px solid #b1b1b1' border='1'>";
                        for (var i=0; i<e.t.row.length; i++){
                            var newTable = "" +
                                "<tr style='height: 50px'>"+
                                "<td align='center'><input type='radio' value='"+e.t.row[i].name1+"' tValue='"+e.t.row[i].size1+"' name='selectName' onfocus='enterSelect(this.value)' id='"+e.t.row[i].name1+"'></td>"+
                                "<td align='center' ondblclick='shuangji1(this)'>"+e.t.row[i].type1+"&nbsp"+e.t.row[i].name1+"</td>"+
                                "<td align='center'><input type='radio' value='"+e.t.row[i].name2+"' tValue='"+e.t.row[i].size2+"' name='selectName' onfocus='enterSelect(this.value)' id='"+e.t.row[i].name2+"'></td>"+
                                "<td align='center' ondblclick='shuangji1(this)'>"+e.t.row[i].type2+"&nbsp"+e.t.row[i].name2+"</td>"+
                                "<td align='center'><input type='radio' value='"+e.t.row[i].name3+"' tValue='"+e.t.row[i].size3+"' name='selectName' onfocus='enterSelect(this.value)' id='"+e.t.row[i].name3+"'></td>"+
                                "<td align='center' ondblclick='shuangji1(this)'>"+e.t.row[i].type3+"&nbsp"+e.t.row[i].name3+"</td>"+
                                "</tr>";
                            table = table+newTable;
                        }
                        table+="</table>";
                        var page = "" +
                            "<input type='button' value='首页' onclick='change(1)'>"+
                            "<input type='button' value='上一页' onclick='change("+(e.t.currentPage-1)+")'>"+
                            "<input type='button' value='"+(e.t.currentPage-1)+"' onclick='change("+(e.t.currentPage-1)+")' style='width: 20px'>"+
                            "<input type='button' value='"+(e.t.currentPage)+"' onclick='change("+(e.t.currentPage)+")' style='background-color:deepskyblue; width: 20px'>"+
                            "<input type='button' value='"+(e.t.currentPage+1)+"' onclick='change("+(e.t.currentPage+1)+")' style='width: 20px'>"+
                            "<input type='button' value='下一页' onclick='change("+(e.t.currentPage+1)+")'>"+
                            "<input type='button' value='尾页' onclick='change("+(e.t.pageCount)+">"
                        $("#tableDiv").html(table);
                        $("#page").html(page);
                    }
                }
            });
        } else {
            $.ajax({
                type:"POST",
                url:"<%=path%>/sales/pagination?page="+page,
                dataType:"json",
                success:function(e){
                    console.log(e.t)
                    if (e.result){
                        var table = "" +
                            "<table style='width: 100%; border: 1px solid #b1b1b1' border='1'>";
                        for (var i=0; i<e.t.row.length; i++){
                            var newTable = "" +
                                "<tr style='height: 50px'>"+
                                "<td align='center'><input type='radio' value='"+e.t.row[i].name1+"' tValue='"+e.t.row[i].size1+"' name='selectName' onfocus='enterSelect(this.value)' id='"+e.t.row[i].name1+"'></td>"+
                                "<td align='center' ondblclick='shuangji1(this)'>"+e.t.row[i].type1+"&nbsp"+e.t.row[i].name1+"</td>"+
                                "<td align='center'><input type='radio' value='"+e.t.row[i].name2+"' tValue='"+e.t.row[i].size2+"' name='selectName' onfocus='enterSelect(this.value)' id='"+e.t.row[i].name2+"'></td>"+
                                "<td align='center' ondblclick='shuangji1(this)'>"+e.t.row[i].type2+"&nbsp"+e.t.row[i].name2+"</td>"+
                                "<td align='center'><input type='radio' value='"+e.t.row[i].name3+"' tValue='"+e.t.row[i].size3+"' name='selectName' onfocus='enterSelect(this.value)' id='"+e.t.row[i].name3+"'></td>"+
                                "<td align='center' ondblclick='shuangji1(this)'>"+e.t.row[i].type3+"&nbsp"+e.t.row[i].name3+"</td>"+
                                "</tr>";
                            table = table+newTable;
                        }
                        table+="</table>";
                        var page = "" +
                            "<input type='button' value='首页' onclick='change(1)'>"+
                            "<input type='button' value='上一页' onclick='change("+(e.t.currentPage-1)+")'>"+
                            "<input type='button' value='"+(e.t.currentPage-1)+"' onclick='change("+(e.t.currentPage-1)+")' style='width: 20px'>"+
                            "<input type='button' value='"+(e.t.currentPage)+"' onclick='change("+(e.t.currentPage)+")' style='background-color:deepskyblue; width: 20px'>"+
                            "<input type='button' value='"+(e.t.currentPage+1)+"' onclick='change("+(e.t.currentPage+1)+")' style='width: 20px'>"+
                            "<input type='button' value='下一页' onclick='change("+(e.t.currentPage+1)+")'>"+
                            "<input type='button' value='尾页' onclick='change("+(e.t.pageCount)+")'>"
                        $("#tableDiv").html(table);
                        $("#page").html(page);
                    }
                }
            });
        }

    };
    function change2(page) {
        var name = document.getElementById("selectProductName2").value;
        if (name != ""){
            $.ajax({
                type:"POST",
                url:"<%=path%>/sales/selectProductByName?page="+page+"&name="+name,
                dataType:"json",
                success:function(e){
                    if (e.result){
                        var table = "" +
                            "<table style='width: 100%; border: 1px solid #b1b1b1' border='1'>";
                        for (var i=0; i<e.t.row.length; i++){
                            var newTable = "" +
                                "<tr style='height: 50px'>"+
                                "<td align='center'><input type='radio' value='"+e.t.row[i].name1+"' tValue='"+e.t.row[i].size1+"' name='selectName' onclick='enterSelect2(this.value)' id='"+e.t.row[i].name1+"'></td>"+
                                "<td align='center' ondblclick='shuangji3(this)'>"+e.t.row[i].type1+"&nbsp"+e.t.row[i].name1+"</td>"+
                                "<td align='center'><input type='radio' value='"+e.t.row[i].name2+"' tValue='"+e.t.row[i].size2+"' name='selectName' onfocus='enterSelect2(this.value)' id='"+e.t.row[i].name2+"'></td>"+
                                "<td align='center' ondblclick='shuangji3(this)'>"+e.t.row[i].type2+"&nbsp"+e.t.row[i].name2+"</td>"+
                                "<td align='center'><input type='radio' value='"+e.t.row[i].name3+"' tValue='"+e.t.row[i].size3+"' name='selectName' onfocus='enterSelect2(this.value)' id='"+e.t.row[i].name3+"'></td>"+
                                "<td align='center' ondblclick='shuangji3(this)'>"+e.t.row[i].type3+"&nbsp"+e.t.row[i].name3+"</td>"+
                                "</tr>";
                            table = table+newTable;
                        }
                        table+="</table>";
                        var page = "" +
                            "<input type='button' value='首页' onclick='change2(1)'>"+
                            "<input type='button' value='上一页' onclick='change2("+(e.t.currentPage-1)+")'>"+
                            "<input type='button' value='"+(e.t.currentPage-1)+"' onclick='change2("+(e.t.currentPage-1)+")' style='width: 20px'>"+
                            "<input type='button' value='"+(e.t.currentPage)+"' onclick='change2("+(e.t.currentPage)+")' style='background-color:deepskyblue; width: 20px'>"+
                            "<input type='button' value='"+(e.t.currentPage+1)+"' onclick='change2("+(e.t.currentPage+1)+")' style='width: 20px'>"+
                            "<input type='button' value='下一页' onclick='change2("+(e.t.currentPage+1)+")'>"+
                            "<input type='button' value='尾页' onclick='change2("+(e.t.pageCount)+")'>"
                        $("#tableDiv2").html(table);
                        $("#page2").html(page);
                    }
                }
            });
        } else {
            $.ajax({
                type:"POST",
                url:"<%=path%>/sales/pagination?page="+page,
                dataType:"json",
                success:function(e){
                    if (e.result){
                        var table = "" +
                            "<table style='width: 100%; border: 1px solid #b1b1b1' border='1'>";
                        for (var i=0; i<e.t.row.length; i++){
                            var newTable = "" +
                                "<tr style='height: 50px'>"+
                                "<td align='center'><input type='radio' value='"+e.t.row[i].name1+"' tValue='"+e.t.row[i].size1+"' name='selectName' onclick='enterSelect2(this.value)' id='"+e.t.row[i].name1+"'></td>"+
                                "<td align='center' ondblclick='shuangji3(this)'>"+e.t.row[i].type1+"&nbsp"+e.t.row[i].name1+"</td>"+
                                "<td align='center'><input type='radio' value='"+e.t.row[i].name2+"' tValue='"+e.t.row[i].size2+"' name='selectName' onfocus='enterSelect2(this.value)' id='"+e.t.row[i].name2+"'></td>"+
                                "<td align='center' ondblclick='shuangji3(this)'>"+e.t.row[i].type2+"&nbsp"+e.t.row[i].name2+"</td>"+
                                "<td align='center'><input type='radio' value='"+e.t.row[i].name3+"' tValue='"+e.t.row[i].size3+"' name='selectName' onfocus='enterSelect2(this.value)' id='"+e.t.row[i].name3+"'></td>"+
                                "<td align='center' ondblclick='shuangji3(this)'>"+e.t.row[i].type3+"&nbsp"+e.t.row[i].name3+"</td>"+
                                "</tr>";
                            table = table+newTable;
                        }
                        table+="</table>";
                        var page = "" +
                            "<input type='button' value='首页' onclick='change2(1)'>"+
                            "<input type='button' value='上一页' onclick='change2("+(e.t.currentPage-1)+")'>"+
                            "<input type='button' value='"+(e.t.currentPage-1)+"' onclick='change2("+(e.t.currentPage-1)+")' style='width: 20px'>"+
                            "<input type='button' value='"+(e.t.currentPage)+"' onclick='change2("+(e.t.currentPage)+")' style='background-color:deepskyblue; width: 20px'>"+
                            "<input type='button' value='"+(e.t.currentPage+1)+"' onclick='change2("+(e.t.currentPage+1)+")' style='width: 20px'>"+
                            "<input type='button' value='下一页' onclick='change2("+(e.t.currentPage+1)+")'>"+
                            "<input type='button' value='尾页' onclick='change2("+(e.t.pageCount)+")'>"
                        $("#tableDiv2").html(table);
                        $("#page2").html(page);
                    }
                }
            });
        }

    };
</script>
<script>
    function shuangji(value) {
        var radio  = document.getElementById(value);
        radio.click();
        $("#modal_add").click();
    }
    function shuangji1(name) {
        var aaa = $(name).text();
        var radio  = document.getElementById(aaa);
        enterSelect(radio.value);
        $("#modal_add").click();
    }
    function shuangji2(value) {
        var radio  = document.getElementById(value);
        radio.click();
        $("#modal_add2").click();
    }
    function shuangji3(name) {
        var aaa = $(name).text();
        var radio  = document.getElementById(aaa);
        enterSelect2(radio.value);
        $("#modal_add2").click();
    }
    function shuangji4(value) {
        var radio  = document.getElementById(value);
        radio.click();
        $("#customer_end").click();
    }
    function shuangji5(name) {
        var aaa = $(name).text();
        var radio  = document.getElementById(aaa);
        selectCustomer(radio.value);
        $("#customer_end").click();
    }
    function shuangji6(value) {
        var radio  = document.getElementById(value);
        radio.click();
        $("#salesMan_end").click();
    }
</script>
<!--客户模糊查询-->
<script>
    function selectCustomerByName(value) {
        $.ajax({
            type:"POST",
            url:"<%=path%>/sales/selectCustomerByName?name="+value,
            dataType:"json",
            success:function(e){
                if (e.result){
                    var table = "" +
                        "<table style='width: 100%; border: 1px solid #b1b1b1' border='1'>";
                    for (var i=0; i<e.t.customerRows.length; i++){
                        var newTable = "" +
                            "<tr style='height: 50px'>"+
                            "<td align='center'><input type='radio' value='"+e.t.customerRows[i].name1+"' name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name1+"'></td>"+
                            "<td align='center' ondblclick='shuangji5(this)'>"+e.t.customerRows[i].name1+"</td>"+
                            "<td align='center'><input type='radio' value="+e.t.customerRows[i].name2+" name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name2+"'></td>"+
                            "<td align='center' ondblclick='shuangji5(this)'>"+e.t.customerRows[i].name2+"</td>"+
                            "<td align='center'><input type='radio' value="+e.t.customerRows[i].name3+" name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name3+"'></td>"+
                            "<td align='center' ondblclick='shuangji5(this)'>"+e.t.customerRows[i].name3+"</td>"+
                            "</tr>";
                        table = table+newTable;
                    }
                    table+="</table>";
                    var page = "" +
                        "<input type='button' value='首页' onclick='changeCustomer(1)'>"+
                        "<input type='button' value='上一页' onclick='changeCustomer("+(e.t.currentPage-1)+")'>"+
                        "<input type='button' value='"+(e.t.currentPage-1)+"' onclick='changeCustomer("+(e.t.currentPage-1)+")' style='width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage)+"' onclick='changeCustomer("+(e.t.currentPage)+")' style='background-color:deepskyblue; width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage+1)+"' onclick='changeCustomer("+(e.t.currentPage+1)+")' style='width: 20px'>"+
                        "<input type='button' value='下一页' onclick='changeCustomer("+(e.t.currentPage+1)+")'>"+
                        "<input type='button' value='尾页' onclick='changeCustomer('"+e.t.pageCount+")'>"
                    $("#customerTable").html(table);
                    $("#page3").html(page);
                } else {

                }
            }
        });
    }
</script>
<!--商品模糊查询 一-->
<script>
    function selectProductByName1(value) {
        $.ajax({
            type:"POST",
            url:"<%=path%>/sales/selectProductByName?name="+value,
            dataType:"json",
            success:function(e){
                if (e.result){
                    var table = "" +
                        "<table style='width: 100%; border: 1px solid #b1b1b1' border='1'>";
                    for (var i=0; i<e.t.row.length; i++){
                        var newTable = "" +
                            "<tr style='height: 50px'>"+
                            "<td align='center'><input type='radio' value='"+e.t.row[i].name1+"' tValue='"+e.t.row[i].size1+"' name='selectName' onclick='enterSelect(this.value)' id='"+e.t.row[i].name1+"'></td>"+
                            "<td align='center' ondblclick='shuangji1(this)'>"+e.t.row[i].type1+"&nbsp"+e.t.row[i].name1+"</td>"+
                            "<td align='center'><input type='radio' value='"+e.t.row[i].name2+"' tValue='"+e.t.row[i].size2+"' name='selectName' onfocus='enterSelect(this.value)' id='"+e.t.row[i].name2+"'></td>"+
                            "<td align='center' ondblclick='shuangji1(this)'>"+e.t.row[i].type2+"&nbsp"+e.t.row[i].name2+"</td>"+
                            "<td align='center'><input type='radio' value='"+e.t.row[i].name3+"' tValue='"+e.t.row[i].size3+"' name='selectName' onfocus='enterSelect(this.value)' id='"+e.t.row[i].name3+"'></td>"+
                            "<td align='center' ondblclick='shuangji1(this)'>"+e.t.row[i].type3+"&nbsp"+e.t.row[i].name3+"</td>"+
                            "</tr>";
                        table = table+newTable;
                    }
                    table+="</table>";
                    var page = "" +
                        "<input type='button' value='首页' onclick='change(1)'>"+
                        "<input type='button' value='上一页' onclick='change("+(e.t.currentPage-1)+")'>"+
                        "<input type='button' value='"+(e.t.currentPage-1)+"' onclick='change("+(e.t.currentPage-1)+")' style='width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage)+"' onclick='change("+(e.t.currentPage)+")' style='background-color:deepskyblue; width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage+1)+"' onclick='change("+(e.t.currentPage+1)+")' style='width: 20px'>"+
                        "<input type='button' value='下一页' onclick='change("+(e.t.currentPage+1)+")'>"+
                        "<input type='button' value='尾页' onclick='change('"+e.t.pageCount+")'>"
                    $("#tableDiv").html(table);
                    $("#page").html(page);
                }
            }
        });
    }

    function selectProductByName2(value) {
        $.ajax({
            type:"POST",
            url:"<%=path%>/sales/selectProductByName?name="+value,
            dataType:"json",
            success:function(e){
                if (e.result){
                    debugger
                    var table = "" +
                        "<table style='width: 100%; border: 1px solid #b1b1b1' border='1'>";
                    for (var i=0; i<e.t.row.length; i++){
                        var newTable = "" +
                            "<tr style='height: 50px'>"+
                            "<td align='center'><input type='radio' value='"+e.t.row[i].name1+"' tValue='"+e.t.row[i].size1+"' name='selectName' onclick='enterSelect2(this.value)' id='"+e.t.row[i].name1+"'></td>"+
                            "<td align='center' ondblclick='shuangji3(this)'>"+e.t.row[i].type1+"&nbsp"+e.t.row[i].name1+"</td>"+
                            "<td align='center'><input type='radio' value='"+e.t.row[i].name2+"' tValue='"+e.t.row[i].size2+"' name='selectName' onfocus='enterSelect2(this.value)' id='"+e.t.row[i].name2+"'></td>"+
                            "<td align='center' ondblclick='shuangji3(this)'>"+e.t.row[i].type2+"&nbsp"+e.t.row[i].name2+"</td>"+
                            "<td align='center'><input type='radio' value='"+e.t.row[i].name3+"' tValue='"+e.t.row[i].size3+"' name='selectName' onfocus='enterSelect2(this.value)' id='"+e.t.row[i].name3+"'></td>"+
                            "<td align='center' ondblclick='shuangji3(this)'>"+e.t.row[i].type3+"&nbsp"+e.t.row[i].name3+"</td>"+
                            "</tr>";
                        table = table+newTable;
                    }
                    table+="</table>";
                    var page = "" +
                        "<input type='button' value='首页' onclick='change2(1)'>"+
                        "<input type='button' value='上一页' onclick='change2("+(e.t.currentPage-1)+")'>"+
                        "<input type='button' value='"+(e.t.currentPage-1)+"' onclick='change2("+(e.t.currentPage-1)+")' style='width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage)+"' onclick='change2("+(e.t.currentPage)+")' style='background-color:deepskyblue; width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage+1)+"' onclick='change2("+(e.t.currentPage+1)+")' style='width: 20px'>"+
                        "<input type='button' value='下一页' onclick='change2("+(e.t.currentPage+1)+")'>"+
                        "<input type='button' value='尾页' onclick='change2('"+e.t.pageCount+")'>"
                    $("#tableDiv2").html(table);
                    $("#page2").html(page);
                }
            }
        });
    }
</script>
</html>