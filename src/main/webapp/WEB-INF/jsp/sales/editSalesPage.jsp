<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>铝瓶销售明细表</title>
</head>
<body>
    <div id="page-wrapper" class="gray-bg" style="height: 90%; min-height: auto;">
        <div class="row border-bottom white-bg">
            <!-- 页面顶部¨ -->
            <%@ include file="../admin/top.jsp"%>
        </div>
        <div style="height: 700px;">
            <iframe class="J_iframe" name="mainFrame" width="100%" height="100%"
                    src="<%=path%>/sales/salesInfo?id=${id}" frameborder="0" data-id="index_v1.html" seamless></iframe>
        </div>
    </div>
</body>
</html>