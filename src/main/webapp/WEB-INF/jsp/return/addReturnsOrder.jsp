
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <script></script>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>销货单添加</title>
    <meta name="keywords" content="科研项目管理">
    <meta name="description" content="科研项目管理">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE">
    <link href="<%=path%>/statics2/saas/scm/app2_release/css/common.css?ver=20150522" rel="stylesheet" type="text/css">
    <link href="<%=path%>/statics2/saas/scm/app2_release/css/green/ui.min.css?ver=20150522" rel="stylesheet">
    <link href="<%=path%>/statics2/css/green/bills.css?ver=20150522" rel="stylesheet" type="text/css">
    <link href="<%=path%>/static/hplus/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/animate.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/bootstrap.min.css?v=4.1.0" rel="stylesheet">
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/jquery/jquery-1.10.2.min.js"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/json3.min.js"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/common.js?ver=20150522"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/grid.js?ver=20150522"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/plugins.js?ver=20150522"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/plugins/jquery.dialog.js?self=true&ver=20150522"></script>
    <script src="<%=path%>/static/hplus/js/bootstrap.min.js"></script>
    <style type="text/css">
        th{
            text-align: center;
        }
    </style>
</head>
<body>
    <input type="hidden" value="<%=path%>" id="hid">
    <div class="wrapper" style="height: 100%;" id="body" align="center">
        <span id="config" class="ui-icon ui-state-default ui-icon-config"></span>
        <div class="mod-toolbar-top mr0 cf dn" id="toolTop"></div>
        <div class="bills cf" style="width:1400px;">
            <div style="width: 100%;">
                <form action="" method="post" id="form1">
                    <div style="width: 100%" align="center">
                        <table>
                            <tr style="height: 100px">
                                <td></td>
                                <td colspan="3"><h1 style="font-size: 23px">唐山军荣铝业有限公司退货单</h1></td>
                            </tr>
                            <td style="width:25%;">
                                <label>客户：</label>
                                <span class="ui-combo-wrap" id="customer">
                                    <input type="text" name="customerName" class="input-txt" autocomplete="off" value="" data-ref="date" id="customerName" style="width: 120px;">
                                    <a id="chooseCustomer"><i class="ui-icon-ellipsis"></i></a>
                            </span>
                            </td>
                            <td style="width:25%;">
                                <label>退货日期:</label>
                                <input type="text" id="createDate" class="ui-input ui-datepicker-input" value="" name="creationDate" style="width: 100px">
                            </td>
                            <!-管理员登陆显示选择销售人员->
                            <c:if test="${sessionScope.level == 0}">
                                <td style="width:25%;">
                                    <label>销售人员:</label>
                                    <span class="ui-combo-wrap" id="sales">
                                  <input type="text" class="input-txt" autocomplete="off" value="" name="salesmanName" id="salesManName" style="width: 120px;">
                                  <a id="chooseSalesMan"><i class="ui-icon-ellipsis"></i></a>
                                </span>
                                </td>
                            </c:if>
                            <!-业务员登陆隐藏选择销售人员，使用当前用户ID->
                            <c:if test="${sessionScope.level == 1}">
                                <td style="width:25%;" hidden>
                                    <label>销售人员:</label>
                                    <input type="text" class="input-txt" autocomplete="off" value="${sessionScope.names}" name="salesmanName" style="width: 120px;">
                                    </span>
                                </td>
                            </c:if>
                            <td style="width:25%;">
                                <label>单据编号:</label>
                                <input id="number" type="text" class="input-txt ui-input" autocomplete="off" value="${number}" name="documentNumber" style="width: 140px">
                            </td>
                        </table>
                    </div>
                </form>
            </div>
            <table style="width: 100%; height: 40px"></table>
            <div style="width: 100%">
                <form action="" method="post" id="form2">
                    <div style="width: 100%" align="center">
                        <table style="border: 1px solid black;" border="1" cellspacing="0" align="center" id="table">
                            <tr>
                                <td width="20"></td>
                                <td width="40"></td>
                                <td width="220"><span style="font-size: 18px">产品规格型号名称</span></td>
                                <td width="60"><span style="font-size: 18px; width: 60px">数量</span></td>
                                <td width="60"><span style="font-size: 18px">备注</span></td>
                            </tr>
                            <tr>
                                <td align="center" width="20">
                                    <span id="index1"></span>
                                </td>
                                <td width="40">
                                    <i class="ui-icon ui-icon-plus" title="新增行" style="float: left" ></i>
                                    <i class="ui-icon ui-icon-trash" title="删除行" style="float: left"></i>
                                </td>
                                <td width="220">
                                <span class="ui-combo-wrap">
                                  <input type="text" class="input-txt" autocomplete="off" value="" name="BProductName" id="BProductName">
                                  <a name="chooseBProductName" tid="BProductName"><i class="ui-icon-ellipsis"></i></a>
                                </span>
                                </td>
                                <td width="60">
                                <span class="ui-combo-wrap">
                                  <input type="text" class="input-txt" autocomplete="off" value="" name="BCount" BName="BCount" id="BCount1" style="width: 60px">
                                </span>
                                </td>
                                <td width="60">
                                <span class="ui-combo-wrap">
                                  <input type="text" class="input-txt" autocomplete="off" value="" name="BBak" style="width: 60px">
                                </span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </form>
            </div>
            <table style="width: 100%; height: 40px"></table>
            <div style="width: 100%">
                <form action="" method="post" id="form3">
                    <div style="width: 100%;" align="center">
                        <table width="1215px">
                            <tr>
                                <td style="width: 33%" align="center">
                                    <label>填表人：</label>
                                    <input type="text" id="inputMan" class="input-txt ui-input" autocomplete="off" value="" name="inputMan">
                                </td>
                                <td style="width: 33%" align="center">
                                    <label>审核人：</label>
                                    <input type="text" id="reviewerMan" class="input-txt ui-input" autocomplete="off" value="" name="reviewerMan">
                                </td>
                            </tr>
                        </table>
                    </div>
                </form>
                <input id="submit" type="button" value="保存">
            </div>
        </div>
    </div>
</div>
<!-- 商品列表一 -->
<div class="modal inmodal fade" id="product_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" >
                <div class="modal-header">
                    <label>选择商品</label>
                </div>
                <div class="modal-body" style="overflow:scroll; height:400px;" id="tableDiv">
                    <table style="width: 100%; border: 1px solid #b1b1b1" border="1">
                        <c:forEach items="${rows}" var="rows">
                            <tr style="height: 50px">
                                <td align="center"><input type="radio" value="${rows.name1}" tValue="${rows.size1}" name="selectName" id="${rows.name1}"></td>
                                <td align="center" ondblclick="shuangji('${rows.name1}')">${rows.type1}&nbsp${rows.name1}</td>
                                <td align="center"><input type="radio" value="${rows.name2}" tValue="${rows.size2}" name="selectName" id="${rows.name2}"></td>
                                <td align="center" ondblclick="shuangji('${rows.name2}')">${rows.type2}&nbsp${rows.name2}</td>
                                <td align="center"><input type="radio" value="${rows.name3}" tValue="${rows.size3}"  name="selectName" id="${rows.name3}"></td>
                                <td align="center" ondblclick="shuangji('${rows.name3}')">${rows.type3}&nbsp${rows.name3}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="modal-footer">
                    <div style="float: left; width: 25%">
                        <h4>共${total}件商品</h4>
                    </div>
                    <div style="float: left; width: 50%" id="page">
                        <input type="button" value="首页" onclick="change(1)"><input type="button" value="上一页" onclick="change(1)"><input type="button" value="1" onclick="change(1)" style="background-color:deepskyblue; width: 20px"><input type="button" value="2" onclick="change(2)" style="width: 20px"><input type="button" value="3" onclick="change(3)" style="width: 20px"><input type="button" value="下一页" onclick="change(${currentPage+1})"><input type="button" value="尾页" onclick="change(${pageCount})">
                    </div>
                    <div style="float: left; width: 25%">
                        <button id="close_add" type="button" class="btn btn-white" data-dismiss="modal">取消</button>
                        <button class="btn btn-primary" type="submit" id="modal_add">确定</button>
                    </div>
                    <div style="float: none"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--客户列表-->
<div class="modal inmodal fade" id="customer_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <label>选择客户</label>
                </div>
                <div class="modal-body" style="overflow:scroll; height:400px;" id="customerTable">
                    <table style="width: 100%; border: 1px solid #b1b1b1" border="1">
                        <c:forEach items="${customerRows}" var="customers">
                            <tr style="height: 50px">
                                <td align="center"><input type="radio" value="${customers.name1}" name="selectCustomer" id="${customers.name1}"></td>
                                <td align="center" ondblclick="shuangji4('${customers.name1}')">${customers.name1}</td>
                                <td align="center"><input type="radio" value="${customers.name2}" name="selectCustomer" id="${customers.name2}"></td>
                                <td align="center" ondblclick="shuangji4('${customers.name2}')">${customers.name2}</td>
                                <td align="center"><input type="radio" value="${customers.name3}" name="selectCustomer" id="${customers.name3}"></td>
                                <td align="center" ondblclick="shuangji4('${customers.name3}')">${customers.name3}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="modal-footer">
                    <div style="float: left; width: 25%">
                        <h4>共${size}家客户</h4>
                    </div>
                    <div style="float: left; width: 50%" id="page3">
                        <input type="button" value="首页" onclick="changeCustomer(1)"><input type="button" value="上一页" onclick="changeCustomer(1)"><input type="button" value="1" onclick="changeCustomer(1)" style="background-color:deepskyblue; width: 20px"><input type="button" value="2" onclick="changeCustomer(2)" style="width: 20px"><input type="button" value="3" onclick="changeCustomer(3)" style="width: 20px"><input type="button" value="下一页" onclick="changeCustomer(${currentPage+1})"><input type="button" value="尾页" onclick="changeCustomer(${pageCount})">
                    </div>
                    <div style="float: left; width: 25%">
                        <button id="close_customer" type="button" class="btn btn-white" data-dismiss="modal">取消</button>
                        <button class="btn btn-primary" type="submit" id="customer_end" >确定</button>
                    </div>
                    <div style="float: none"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--销售人员列表-->
<div class="modal inmodal fade" id="salesMan_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 650px">
            <div class="modal-body" >
                <div class="modal-header">
                    <label>选择选择销售人员</label>
                </div>
                <div class="modal-body" style="overflow:scroll; height:400px;" id="salesManTable">
                    <table style="width: 100%; border: 1px solid #b1b1b1" border="1">
                        <c:forEach items="${saleManRows}" var="saleManRows">
                            <tr style="height: 50px">
                                <td align="center"><input type="radio" value="${saleManRows.name1}" name="selectSales" id="${saleManRows.name1}"></td>
                                <td align="center" ondblclick="shuangji6('${saleManRows.name1}')">${saleManRows.name1}</td>
                                <td align="center"><input type="radio" value="${saleManRows.name2}" name="selectSales" id="${saleManRows.name2}"></td>
                                <td align="center" ondblclick="shuangji6('${saleManRows.name2}')">${saleManRows.name2}</td>
                                <td align="center"><input type="radio" value="${saleManRows.name3}" name="selectSales" id="${saleManRows.name3}"></td>
                                <td align="center" ondblclick="shuangji6('${saleManRows.name3}')">${saleManRows.name3}</td>
                            </tr>
                        </c:forEach>>
                    </table>
                </div>
                <div class="modal-footer">
                    <div style="float: left; width: 25%">
                        <h4>共${saleSize}位销售人员</h4>
                    </div>
                    <div style="float: left; width: 50%" id="page4">
                        <input type="button" value="首页" onclick="changeSalesMan(1)"><input type="button" value="上一页" onclick="changeSalesMan(1)"><input type="button" value="1" onclick="changeSalesMan(1)" style="background-color:deepskyblue; width: 20px"><input type="button" value="2" onclick="changeSalesMan(2)" style="width: 20px"><input type="button" value="3" onclick="changeSalesMan(3)" style="width: 20px"><input type="button" value="下一页" onclick="changeSalesMan(${currentPage+1})"><input type="button" value="尾页" onclick="changeSalesMan(${pageCount})">
                    </div>
                    <div style="float: right; width: 25%">
                        <button id="close_salesMan" type="button" class="btn btn-white" data-dismiss="modal">取消</button>
                        <button class="btn btn-primary" type="submit" id="salesMan_end">确定</button>
                    </div>
                    <div style="float: none"></div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<!--表格编号方法-->
<script>
    /*表格下标*/
    var index = 1;
    /*初始化表格下标*/
    window.onload = function () {
        $("span[id='index1']").text(index);
        $("span[id='index2']").text(index+1);
        $("span[id='index3']").text(index+2);
        $("span[id='index4']").text(index+3);
        $("span[id='index5']").text(index+4);
        $("span[id='index6']").text(index+5);
    }
</script>
<!--选择客户方法-->
<script>
    /*所选客户*/
    var customerName = "";
    /*打开客户面板*/
    $("a[id='chooseCustomer']").click(function () {
        $("#customer_modal").modal();
    });
    /*选择客户*/
    $("input[name='selectCustomer']").click(function () {
        customerName = $(this).val();
    })
    /*确定所选客户*/
    $("button[id='customer_end']").click(function () {
        var name = document.getElementById("customerName");
        name.value = customerName;
        $("#close_customer").click();
        customerName = "";
    })
    function selectCustomer(value) {
        customerName = value;
    }
</script>
<!--填写订货日期方法-->
<script>
    $(function() {
        $("#createDate").datepicker();
    });
</script>
<!--选择销售人员方法-->
<script>
    /*所选销售人员*/
    var salesManName = "";
    /*打开销售人员面板*/
    $("a[id='chooseSalesMan']").click(function () {
        $("#salesMan_modal").modal();
    });
    /*选择销售人员*/
    $("input[name='selectSales']").click(function () {
        salesManName = $(this).val();
    });
    /*确定所选销售人员*/
    $("button[id='salesMan_end']").click(function () {
        var saleName = document.getElementById("salesManName");
        saleName.value = salesManName;
        $("#close_salesMan").click();
        salesManName = "";
    });
    function selectSalesMan(value) {
        salesManName = value;
    }
</script>
<!--添加一行表格方法-->
<script>
    $("i[class='ui-icon ui-icon-plus']").click(function () {
        addRow();
    })
    function addRow() {
        var tr =
            "<tr>"+
                "<td align='center'>"+
                    "<span>"+(index+6)+"</span>"+
                "</td>"+
                "<td>" +
                    "<i class='ui-icon ui-icon-plus' title='新增行' style='float: left' onclick='addRow()'></i><i class='ui-icon ui-icon-trash' title='删除行' style='float: left' onclick='deleteRow(this)'></i></td>"+
                "<td>"+
                    "<span class='ui-combo-wrap'>"+
                        "<input type='text' class='input-txt' autocomplete='off' value='' name='BProductName' id='BB"+index+"'>"+
                        "<a xid='BB"+index+"' onclick='openProductPenal(this)'><i class='ui-icon-ellipsis'></i></a>"+
                    "</span>"+
                "</td>"+
                "<td>"+
                    "<span class='ui-combo-wrap'>"+
                        "<input type='text' class='input-txt' autocomplete='off' value='' name='BCount' PId='B"+index+"' onblur='changeCount(this)'>"+
                    "</span>"+
                "</td>"+
                "<td>"+
                    "<span class='ui-combo-wrap'>"+
                        "<input type='text' class='input-txt' autocomplete='off' value='' name='BBak'>"+
                    "</span>"+
                "</td>"+
            "</tr>";
        $("#table").append(tr);
        var x=document.getElementById('table').rows[2].cells;
        x[12].rowSpan = index+6;
        index+=1;
    }
</script>
<!--删除一行表格-->
<script>
    $("i[class='ui-icon ui-icon-trash']").click(function () {
        var tr = this.parentNode.parentNode;
        var table = tr.parentNode;
        table.removeChild(tr);
        index-=1;
        $("#PSum").val(sum());
        sumAll();
        $("#WSum").val(WSum());
        sumAll();
    });
    function deleteRow(a) {
        var tr = a.parentNode.parentNode;
        var table = tr.parentNode;
        table.removeChild(tr);
        index-=1;
        $("#PSum").val(sum());
        sumAll();
        $("#WSum").val(WSum());
        sumAll();
    };
</script>
<!--选择本厂商品方法-->
<script>
    /*所选商品*/
    var chooseProductName = "";
    /*操作行*/
    var productLine = "";
    /*第一行操作*/
    /*打开商品面板*/
    $("a[name='chooseBProductName']").click(function () {
        productLine =$(this).attr("tid");
        $("#product_modal").modal();
    });
    /*选择商品*/
    $("input[name='selectName']").click(function () {
        chooseProductName = $(this).val();
    });
    function enterSelect(value) {
        chooseProductName = value;
    };
    /*确定所选商品*/
    $("#modal_add").click(function () {
        var product = document.getElementById(productLine);
        $("#close_add").click();
        product.value = chooseProductName;
        chooseProductName = "";
    });
//    function enter() {
//        var product = document.getElementById(productLine);
//        $("#close_add").click();
//        product.value = chooseProductName;
//        chooseProductName = "";
//    };
    /*第二行操作*/
    /*打开商品面板*/
    function openProductPenal(value) {
        productLine = $(value).attr("xid");
        $("#product_modal").modal();
    }
</script>
<!--选择外购商品方法-->
<script>
    /*所选商品*/
    var chooseWProduct = "";
    /*操作行*/
    var WProductLine = "";
    /*打开商品面板*/
    $("a[name='chooseWProductName']").click(function () {
        WProductLine =$(this).attr("tid");
        $("#product_modal2").modal();
    });
    /*选择商品*/
    $("input[name='selectName2']").click(function () {
        chooseWProduct = $(this).val();
    });
    function enterSelect2(value) {
        chooseWProduct = value;
    };
    /*确定所选商品*/
    $("#modal_add2").click(function () {
        var WProduct = document.getElementById(WProductLine);
        $("#close_add2").click();
        WProduct.value = chooseWProduct;
        chooseWProduct = "";
    });
    /*第二行操作*/
    /*打开商品面板*/
    function openProductPenal2(value) {
        WProductLine = $(value).attr("xid");
        $("#product_modal2").modal();
    }
</script>
<!--异步提交表单方法-->
<script>
    $("input[id='submit']").click(function () {
        var level = ${sessionScope.level};
        if ($("#customerName").val() == "" || $("#customerName").val() == null){
            alert("请选择客户");
        } else if ($("#createDate").val() == "" || $("#createDate").val() == null){
            alert("请填写退货日期");
        } else if (level == 0 && ($("#salesManName").val() == "" || $("#salesManName").val() == null)){
            alert("请选择销售人员");
        } else if ($("#number").val() == "" || $("#number").val() == null){
            alert("请填写单据编号");
        } else {
            var data1 = $("#form1").serialize().replace(/&/g,',');
            var data2 = $("#form2").serialize().replace(/&/g,',');
            var data3 = $("#form3").serialize().replace(/&/g,',');
            console.log(data2);
            $.ajax({
                type:"POST",
                url:"<%=path%>/returnsOrder/addReyurnOrder?data1="+data1+"&data2="+data2+"&data3="+data3,
                dataType:"json",
                success:function(e){
                    if (e.result){
                        alert(e.msg);
                        window.location.reload();
                    } else {
                        alert(e.msg);
                    }
                }
            });
        }
    })
</script>
<!--商品分页方法-->
<script>
    function changeCustomer(page) {
        $.ajax({
            type:"POST",
            url:"<%=path%>/sales/paginationCustomer?page="+page,
            dataType:"json",
            success:function(e){
                if (e.result){
                    var table = "" +
                        "<table style='width: 100%; border: 1px solid #b1b1b1' border='1'>";
                    for (var i=0; i<e.t.customerRows.length; i++){
                        var newTable = "" +
                            "<tr style='height: 50px'>"+
                                "<td align='center'><input type='radio' value='"+e.t.customerRows[i].name1+"' name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name1+"'></td>"+
                                "<td align='center' ondblclick='shuangji5(this)'>"+e.t.customerRows[i].name1+"</td>"+
                                "<td align='center'><input type='radio' value="+e.t.customerRows[i].name2+" name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name2+"'></td>"+
                                "<td align='center' ondblclick='shuangji5(this)'>"+e.t.customerRows[i].name2+"</td>"+
                                "<td align='center'><input type='radio' value="+e.t.customerRows[i].name3+" name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name3+"'></td>"+
                                "<td align='center' ondblclick='shuangji5(this)'>"+e.t.customerRows[i].name3+"</td>"+
                            "</tr>";
                        table = table+newTable;
                    }
                    table+="</table>";
                    var page = "" +
                        "<input type='button' value='首页' onclick='changeCustomer(1)'>"+
                    "<input type='button' value='上一页' onclick='changeCustomer("+(e.t.currentPage-1)+")'>"+
                    "<input type='button' value='"+(e.t.currentPage-1)+"' onclick='changeCustomer("+(e.t.currentPage-1)+")' style='width: 20px'>"+
                    "<input type='button' value='"+(e.t.currentPage)+"' onclick='changeCustomer("+(e.t.currentPage)+")' style='background-color:deepskyblue; width: 20px'>"+
                    "<input type='button' value='"+(e.t.currentPage+1)+"' onclick='changeCustomer("+(e.t.currentPage+1)+")' style='width: 20px'>"+
                    "<input type='button' value='下一页' onclick='changeCustomer("+(e.t.currentPage+1)+")'>"+
                    "<input type='button' value='尾页' onclick='changeCustomer('"+e.t.pageCount+")'>"
                    $("#customerTable").html(table);
                    $("#page3").html(page);
                }
            }
        });
    }
    function changeSalesMan(page) {
        $.ajax({
            type:"POST",
            url:"<%=path%>/sales/paginationSalesMan?page="+page,
            dataType:"json",
            success:function(e){
                if (e.result){
                    var table = "" +
                        "<table style='width: 100%; border: 1px solid #b1b1b1' border='1'>";
                    for (var i=0; i<e.t.saleManRows.length; i++){
                        var newTable = "" +
                            "<tr style='height: 50px'>"+
                            "<td align='center'><input type='radio' value='"+e.t.saleManRows[i].name1+"' name='selectSalesMan' onfocus='selectSalesMan(this.value)'></td>"+
                            "<td align='center'>"+e.t.saleManRows[i].name1+"</td>"+
                            "<td align='center'><input type='radio' value="+e.t.saleManRows[i].name2+" name='selectSalesMan' onfocus='selectSalesMan(this.value)'></td>"+
                            "<td align='center'>"+e.t.saleManRows[i].name2+"</td>"+
                            "<td align='center'><input type='radio' value="+e.t.saleManRows[i].name3+" name='selectSalesMan' onfocus='selectSalesMan(this.value)'></td>"+
                            "<td align='center'>"+e.t.saleManRows[i].name3+"</td>"+
                            "</tr>";
                        table = table+newTable;
                    }
                    table+="</table>";
                    var page = "" +
                        "<input type='button' value='首页' onclick='changeSalesMan(1)'>"+
                        "<input type='button' value='上一页' onclick='changeSalesMan("+(e.t.currentPage-1)+")'>"+
                        "<input type='button' value='"+(e.t.currentPage-1)+"' onclick='changeSalesMan("+(e.t.currentPage-1)+")' style='width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage)+"' onclick='changeSalesMan("+(e.t.currentPage)+")' style='background-color:deepskyblue; width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage+1)+"' onclick='changeSalesMan("+(e.t.currentPage+1)+")' style='width: 20px'>"+
                        "<input type='button' value='下一页' onclick='changeSalesMan("+(e.t.currentPage+1)+")'>"+
                        "<input type='button' value='尾页' onclick='changeSalesMan('"+e.t.pageCount+")'>"
                    $("#salesManTable").html(table);
                    $("#page4").html(page);
                }
            }
        });
    }
    function change(page) {
        $.ajax({
            type:"POST",
            url:"<%=path%>/sales/pagination?page="+page,
            dataType:"json",
            success:function(e){
                if (e.result){
                    var table = "" +
                        "<table style='width: 100%; border: 1px solid #b1b1b1' border='1'>";
                            for (var i=0; i<e.t.row.length; i++){
                                var newTable = "" +
                                    "<tr style='height: 50px'>"+
                                    "<td align='center'><input type='radio' value='"+e.t.row[i].name1+"' tValue='"+e.t.row[i].size1+"' name='selectName' onclick='enterSelect(this.value)' id='"+e.t.row[i].name1+"'></td>"+
                                    "<td align='center' ondblclick='shuangji1(this)'>"+e.t.row[i].type1+"&nbsp"+e.t.row[i].name1+"</td>"+
                                    "<td align='center'><input type='radio' value='"+e.t.row[i].name2+"' tValue='"+e.t.row[i].size2+"' name='selectName' onfocus='enterSelect(this.value)' id='"+e.t.row[i].name2+"'></td>"+
                                    "<td align='center' ondblclick='shuangji1(this)'>"+e.t.row[i].type2+"&nbsp"+e.t.row[i].name2+"</td>"+
                                    "<td align='center'><input type='radio' value='"+e.t.row[i].name3+"' tValue='"+e.t.row[i].size3+"' name='selectName' onfocus='enterSelect(this.value)' id='"+e.t.row[i].name3+"'></td>"+
                                    "<td align='center' ondblclick='shuangji1(this)'>"+e.t.row[i].type3+"&nbsp"+e.t.row[i].name3+"</td>"+
                                    "</tr>";
                                table = table+newTable;
                            }
                            table+="</table>";
                    var page = "" +
                            "<input type='button' value='首页' onclick='change(1)'>"+
                            "<input type='button' value='上一页' onclick='change("+(e.t.currentPage-1)+")'>"+
                            "<input type='button' value='"+(e.t.currentPage-1)+"' onclick='change("+(e.t.currentPage-1)+")' style='width: 20px'>"+
                            "<input type='button' value='"+(e.t.currentPage)+"' onclick='change("+(e.t.currentPage)+")' style='background-color:deepskyblue; width: 20px'>"+
                            "<input type='button' value='"+(e.t.currentPage+1)+"' onclick='change("+(e.t.currentPage+1)+")' style='width: 20px'>"+
                            "<input type='button' value='下一页' onclick='change("+(e.t.currentPage+1)+")'>"+
                            "<input type='button' value='尾页' onclick='change('"+e.t.pageCount+")'>"
                    $("#tableDiv").html(table);
                    $("#page").html(page);
                }
            }
        });
    };
    function change2(page) {
        $.ajax({
            type:"POST",
            url:"<%=path%>/sales/pagination?page="+page,
            dataType:"json",
            success:function(e){
                if (e.result){
                    debugger
                    var table = "" +
                        "<table style='width: 100%; border: 1px solid #b1b1b1' border='1'>";
                    for (var i=0; i<e.t.row.length; i++){
                        var newTable = "" +
                            "<tr style='height: 50px'>"+
                            "<td align='center'><input type='radio' value='"+e.t.row[i].name1+"' tValue='"+e.t.row[i].size1+"' name='selectName' onclick='enterSelect2(this.value)' id='"+e.t.row[i].name1+"'></td>"+
                            "<td align='center' ondblclick='shuangji3(this)'>"+e.t.row[i].type1+"&nbsp"+e.t.row[i].name1+"</td>"+
                            "<td align='center'><input type='radio' value='"+e.t.row[i].name2+"' tValue='"+e.t.row[i].size2+"' name='selectName' onfocus='enterSelect2(this.value)' id='"+e.t.row[i].name2+"'></td>"+
                            "<td align='center' ondblclick='shuangji3(this)'>"+e.t.row[i].type2+"&nbsp"+e.t.row[i].name2+"</td>"+
                            "<td align='center'><input type='radio' value='"+e.t.row[i].name3+"' tValue='"+e.t.row[i].size3+"' name='selectName' onfocus='enterSelect2(this.value)' id='"+e.t.row[i].name3+"'></td>"+
                            "<td align='center' ondblclick='shuangji3(this)'>"+e.t.row[i].type3+"&nbsp"+e.t.row[i].name3+"</td>"+
                            "</tr>";
                        table = table+newTable;
                    }
                    table+="</table>";
                    var page = "" +
                        "<input type='button' value='首页' onclick='change2(1)'>"+
                        "<input type='button' value='上一页' onclick='change2("+(e.t.currentPage-1)+")'>"+
                        "<input type='button' value='"+(e.t.currentPage-1)+"' onclick='change2("+(e.t.currentPage-1)+")' style='width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage)+"' onclick='change2("+(e.t.currentPage)+")' style='background-color:deepskyblue; width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage+1)+"' onclick='change2("+(e.t.currentPage+1)+")' style='width: 20px'>"+
                        "<input type='button' value='下一页' onclick='change2("+(e.t.currentPage+1)+")'>"+
                        "<input type='button' value='尾页' onclick='change2('"+e.t.pageCount+")'>"
                    $("#tableDiv2").html(table);
                    $("#page2").html(page);
                }
            }
        });
    };
</script>
<script>
    function shuangji(value) {
        var radio  = document.getElementById(value);
        radio.click();
        $("#modal_add").click();
    }
    function shuangji1(name) {
        var aaa = $(name).text();
        var radio  = document.getElementById(aaa);
        enterSelect(radio.value);
        $("#modal_add").click();
    }
    function shuangji2(value) {
        var radio  = document.getElementById(value);
        radio.click();
        $("#modal_add2").click();
    }
    function shuangji3(name) {
        var aaa = $(name).text();
        var radio  = document.getElementById(aaa);
        enterSelect2(radio.value);
        $("#modal_add2").click();
    }
    function shuangji4(value) {
        var radio  = document.getElementById(value);
        radio.click();
        $("#customer_end").click();
    }
    function shuangji5(name) {
        var aaa = $(name).text();
        var radio  = document.getElementById(aaa);
        selectCustomer(radio.value);
        $("#customer_end").click();
    }
    function shuangji6(value) {
        var radio  = document.getElementById(value);
        radio.click();
        $("#salesMan_end").click();
    }
</script>
</html>