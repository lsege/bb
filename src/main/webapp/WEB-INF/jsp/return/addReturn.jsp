<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <script></script>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>退货单添加</title>
    <meta name="keywords" content="科研项目管理">
    <meta name="description" content="科研项目管理">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE">
    <link href="<%=path%>/statics2/saas/scm/app2_release/css/common.css?ver=20150522" rel="stylesheet" type="text/css">
    <link href="<%=path%>/statics2/saas/scm/app2_release/css/green/ui.min.css?ver=20150522" rel="stylesheet">
    <link href="<%=path%>/statics2/css/green/bills.css?ver=20150522" rel="stylesheet" type="text/css">
    <link href="<%=path%>/static/hplus/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/animate.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/bootstrap.min.css?v=4.1.0" rel="stylesheet">
    <style type="text/css">
        th{
            text-align: center;
        }
    </style>
</head>
<body>
<input type="hidden" value="<%=path%>" id="hid">
<form action="sales/addSalesOrder" method="post" id="form">
    <div class="wrapper" style="height: 100%" id="body">
        <span id="config" class="ui-icon ui-state-default ui-icon-config"></span>
        <div class="mod-toolbar-top mr0 cf dn" id="toolTop"></div>
        <div class="bills cf">
            <div class="con-header" style="width: 100%">
                <div class="row-item" style="text-align: center">
                    <h1 style="font-size: 23px">唐山军荣铝业有限公司退货单添加</h1>
                </div>
                <div style="width: 100%">
                    <dl>
                        <dd style="padding-left: 20px">
                            <label>客户：</label>
                            <span class="ui-combo-wrap" id="customer">
                            <input type="text" name="customerName" class="input-txt" autocomplete="off" value="" data-ref="date" id="customerName">
                            <a id="chooseCustomer"><i class="ui-icon-ellipsis"></i></a>
                        </span>
                        </dd>
                        <dd style="padding-left: 250px">
                            <label>销售人员:</label>
                            <span class="ui-combo-wrap" id="sales">
                          <input type="text" class="input-txt" autocomplete="off" value="" name="salesmanName" id="salesManName">
                          <a id="chooseSalesMan"><i class="ui-icon-ellipsis"></i></a>
                        </span>
                        </dd>
                        <dd style="padding-left: 250px">
                            <label>单据编号:</label>
                            <input id="number" type="text" class="input-txt ui-input" autocomplete="off" value="${number}" name="documentNumber">
                        </dd>
                    </dl>
                </div>
            </div>
            <div align="center">
                <table width="700px" style="border: 1px solid #b1b1b1;" cellspacing="0" id="table">
                    <tr>
                        <th>操作</th><th>商品</th><th></th><th>数量</th><th>退货原因，备注</th>
                    </tr>
                    <tr name="info">
                        <td>
                            <i class="ui-icon ui-icon-plus" title="新增行" style="float: left"></i>
                            <i class="ui-icon ui-icon-trash" title="删除行" style="float: left"></i>
                        </td>
                        <td><input type="text" width="60" value="" id="productName" name="productName" >
                        </td>
                        <td>
                            <input type="button" value="选择商品" tid="productName" name="chooseProduct" sid="sizes">
                        </td>
                        <td><input type="text" value="" name="count"></td>
                        <td><input type="text" value="" name="bak"></td>
                    </tr>
                </table>
            </div>
            <div style="margin-top: 160px">
                <ul>
                    <li style="padding-left: 20px; float: left">
                        <label>日期:</label>
                        <input type="text" id="date" class="ui-input ui-datepicker-input" value="" name="dateIssuance">
                    </li>
                    <li style="padding-left: 150px; float: left">
                        <label>录入人员：</label>
                        <input type="text" id="inputMan" class="input-txt ui-input" autocomplete="off" value="" name="entryPersonnel">
                    </li>
                    <li style="padding-left: 150px; float: left">
                        <label>复核人员：</label>
                        <input type="text" id="reviewerMan" class="input-txt ui-input" autocomplete="off" value="" name="reviewOfficer">
                    </li>
                    <li style="padding-left: 150px; float: left">
                        <input type="radio" name="whetherElectint" class="regular_customer" value="1">是&nbsp&nbsp&nbsp
                        <input type="radio" name="whetherElectint" class="regular_customer" checked value="0">否
                    </li>
                    <li style="padding-left: 150px; float: left">
                        <label>是否老客户：</label>
                        <input type="radio" name="regularCustomer" class="regular_customer"  value="1">是&nbsp&nbsp&nbsp
                        <input type="radio" name="regularCustomer" class="regular_customer" checked value="0">否
                    </li>
                </ul>
            </div>
            <div style="margin-top: 200px;">
                <span style="padding-left: 78%">
                    <input type="button" value="确定修改" style="background-color:#B5CBEC" onclick="send()">
                </span>
            </div>
        </div>
    </div>
</form>
</div>
<!-- 商品列表 -->
<div class="modal inmodal fade" id="myModal_add" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <label>选择商品</label>
                </div>
                <div class="modal-body" style="overflow:scroll; height:400px;">
                    <table style="width: 100%; border: 1px solid #b1b1b1" border="1">
                        <c:forEach items="${rows}" var="rows">
                            <tr style="height: 50px">
                                <td align="center"><input type="radio" value="${rows.name1}" tValue="${rows.size1}" onclick="clickInfos(this)" name="selectName"></td>
                                <td align="center">${rows.name1}</td>
                                <td align="center"><input type="radio" value="${rows.name2}" tValue="${rows.size2}" onclick="clickInfos(this)" name="selectName"></td>
                                <td align="center">${rows.name2}</td>
                                <td align="center"><input type="radio" value="${rows.name3}" tValue="${rows.size3}" onclick="clickInfos(this)" name="selectName"></td>
                                <td align="center">${rows.name3}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="modal-footer">
                    <button id="close_add" type="button" class="btn btn-white" data-dismiss="modal">取消</button>
                    <button class="btn btn-primary" type="submit" id="modal_add">确定</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--客户列表-->
<div class="modal inmodal fade" id="customer_add" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <label>选择客户</label>
                </div>
                <div class="modal-body" style="overflow:scroll; height:400px;">
                    <table style="width: 100%; border: 1px solid #b1b1b1" border="1">
                        <c:forEach items="${customers}" var="customers">
                            <tr style="height: 50px">
                                <td align="center"><input type="radio" value="${customers.customerName}" onclick="clickCustomer(this)" name="selectCustomer"></td>
                                <td align="center">${customers.customerName}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="modal-footer">
                    <button id="close_customer" type="button" class="btn btn-white" data-dismiss="modal">取消</button>
                    <button class="btn btn-primary" type="submit" id="customer_end" onclick="customerEnd()">确定</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--销售人员列表-->
<div class="modal inmodal fade" id="select_salesMan" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 350px">
            <div class="modal-body" >
                <div class="modal-header">
                    <label>选择选择销售人员</label>
                </div>
                <div class="modal-body" style="overflow:scroll; height:400px;">
                    <table style="width: 100%; border: 1px solid #b1b1b1" border="1">
                        <c:forEach items="${salesMan}" var="salesMan">
                            <tr style="height: 50px">
                                <td align="center"><input type="radio" value="${salesMan.name}" onclick="clickSalesMan(this)" name="selectCustomer"></td>
                                <td align="center">${salesMan.name}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="modal-footer">
                    <button id="salesMan_close" type="button" class="btn btn-white" data-dismiss="modal">取消</button>
                    <button class="btn btn-primary" type="submit" id="salesMan_end" onclick="closeSalesMan()">确定</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/jquery/jquery-1.10.2.min.js"></script>
<script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/json3.min.js"></script>
<script src="<%=path%>/statics2/saas/scm/app2_release/js/common/common.js?ver=20150522"></script>
<script src="<%=path%>/statics2/saas/scm/app2_release/js/common/grid.js?ver=20150522"></script>
<script src="<%=path%>/statics2/saas/scm/app2_release/js/common/plugins.js?ver=20150522"></script>
<script src="<%=path%>/statics2/saas/scm/app2_release/js/common/plugins/jquery.dialog.js?self=true&ver=20150522"></script>
<script src="<%=path%>/static/hplus/js/bootstrap.min.js"></script>
</body>
<script type="text/javascript">
    var i=0;
    /*商品名称全局变量*/
    var clickInfo = "";
    /*客户名称全局变量*/
    var customer = "";
    /*销售人员全局变量*/
    var salesMan = "";
    /*商品输入框控件全局变量*/
    var productName = "";
    /*点击打开商品面板事件*/
    $("input[name='chooseProduct']").click(function () {
        productName =$(this).attr("tid");
        $("#myModal_add").modal();
    });
    /*新增打开商品面板事件*/
    function openProductPenal(value) {
        productName = $(value).attr("xid");
        $("#myModal_add").modal();
    }
    /*点击打开客户面板事件*/
    $('#chooseCustomer').click(function () {
        $("#customer_add").modal();
    });
    /*点击打开销售人员面板事件*/
    $('#chooseSalesMan').click(function () {
        $("#select_salesMan").modal();
    });
    /*选择商品事件*/
    function clickInfos(info) {
        if(info.checked){
            clickInfo = info.value;
        }
    }
    /*选择客户事件*/
    function clickCustomer(info) {
        if(info.checked){
            customer = info.value;
        }
    }
    /*选择销售人员事件*/
    function clickSalesMan(info) {
        if(info.checked){
            salesMan = info.value;
        }
    }
    /*关闭选择商品面板*/
    $("#modal_add").click(function () {
        var productNames = document.getElementById(productName);
        $("#close_add").click();
        productNames.value = clickInfo;
        clickInfo = "";
    });
    /*关闭选择客户面板*/
    function customerEnd() {
        var customerName = document.getElementById("customerName");
        $("#close_customer").click();
        customerName.value = customer;
        customerName = "";
    }
    /*关闭选择销售人员面板*/
    function closeSalesMan() {
        var salesManName = document.getElementById("salesManName");
        $("#salesMan_close").click();
        salesManName.value = salesMan;
        salesMan = "";
    }
    /*添加一组数据行*/
    $("i[class='ui-icon ui-icon-plus']").click(function () {
        addRow();
    })
    function addRow() {
        var tr = "<tr>"+
            "<td>"+
            "<i class='ui-icon ui-icon-plus' title='新增行' style='float: left' onclick='addRow()'></i>"+
            "<i class='ui-icon ui-icon-trash' title='删除行' style='float: left' onclick='deleteRow(this)'></i>"+
            "</td>"+
            "<td><input type='text' width='60px' name='productName' id='"+i+"'></td>"+
            "<td>"+
            "<input type='button' value='选择商品' name='chooseProduct' onclick='openProductPenal(this)' xid='"+i+"'  sid='sizes"+i+"'>"+
            "</td>"+
            "<td><input type='text' name='count'></td>"+
            "<td><input type='text' name='bak'></td>"+
            "</tr>";
        $("#table").append(tr);
        i+=1;
    }
    /*删除一组数据*/
    $("i[class='ui-icon ui-icon-trash']").click(function () {
        var tr = this.parentNode.parentNode;
        var table = tr.parentNode;
        table.removeChild(tr);
    });
    function deleteRow(a) {
        var tr = a.parentNode.parentNode;
        var table = tr.parentNode;
        table.removeChild(tr);
    }
</script>
<script>
    $(function() {
        $( "#date" ).datepicker();
    });
    $("input[id='number']").blur(function () {
        var value =$("#number").val();
        var number = document.getElementById("number");
        $.ajax({
            type: 'POST',
            url: "sales/validationAccount",
            data: {account:value},
            success: function (date) {
                if (!date.result){
                    alert("单据编号已存在，请重新输入");
                }
            }
        });
    })
    function send() {
        var value =$("#number").val();
        if (value == "" || value == null){
            alert("单据编号不可为空");
            var number = document.getElementById("number");
            number.focus();
        } else {
            $("form[id='form']").submit();
            setTimeout(function () {
                $("#form").submit;
            },600);

        }
    }
</script>
</html>