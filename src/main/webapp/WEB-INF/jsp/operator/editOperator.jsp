<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>操作员添加</title>
    <meta name="keywords" content="科研项目管理">
    <meta name="description" content="科研项目管理">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE">
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/jquery/jquery-1.10.2.min.js"></script>

    <link href="<%=path%>/statics2/saas/scm/app2_release/css/common.css?ver=20150522" rel="stylesheet" type="text/css">
    <link href="<%=path%>/statics2/saas/scm/app2_release/css/green/ui.min.css?ver=20150522" rel="stylesheet">
    <link href="<%=path%>/statics2/css/green/bills.css?ver=20150522" rel="stylesheet" type="text/css">
    <link href="<%=path%>/static/hplus/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/animate.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/bootstrap.min.css?v=4.1.0" rel="stylesheet">
    <style type="text/css">
        th{
            text-align: center;
        }
    </style>

    <link href="<%=path%>/static/hplus/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <script src="<%=path%>/static/hplus/js/plugins/sweetalert/sweetalert.min.js"></script>


    <link href="<%=path%>/static/hplus/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <script src="<%=path%>/static/hplus/js/plugins/sweetalert/sweetalert.min.js"></script>

    <link href="<%=path%>/static/hplus/css/bootstrap.min.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus//css/bootstrap.min.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/style.css?v=4.1.0" rel="stylesheet">
</head>
    <body>
    <div class="wrapper">
        <span id="config" class="ui-icon ui-state-default ui-icon-config"></span>
        <div class="mod-toolbar-top mr0 cf dn" id="toolTop"></div>
        <div class="bills cf">
            <div class="con-header">
                <div class="row-item" style="text-align: center ">
                    <h1>操作员修改</h1>
                </div>
                <div style="height: 30px; background-color: slategray"></div>
                <div style="height: 30px;"></div>
                <form method="post" id="form">
                    <div style="height: 400px; border: 1px black solid;">
                        <table align="center" style="margin-top: 50px; height: 300px;" border="0" cellspacing="0" width="600px">
                            <tr>
                                <td>请输入操作员昵称：</td>
                                <td>
                                    <input type="text" name="name" id="name" value="${salesMan.name}">
                                </td>
                            </tr>
                            <tr>
                                <td>请输入操作员电话：</td>
                                <td>
                                    <input type="hidden" name="id" value="${salesMan.id}" id="id">
                                    <input type="text" name="phone" id="phone" value="${salesMan.mobile}">
                                </td>
                            </tr>
                            <tr>
                                <td>请输入操作员密码：</td>
                                <td>
                                    <input type="password" name="password" id="password">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right">
                                    <input type="button" value="确定" onclick="send()">
                                </td>
                            </tr>
                        </table>
                    </div>
                </form>
                <div style="height: 30px;"></div>
                <div style="height: 30px; background-color: slategray"></div>
            </div>
        </div>
    </div>
    </body>
<script>
    function send() {
        var id = document.getElementById("id");
        var name = document.getElementById("name");
        var phone = document.getElementById("phone");
        var password = document.getElementById("password");
        if(phone.value == ""){
            swal({
                title: "成功!!",
                text: "电话不可为空",
                type: "success",
                confirmButtonText: "确定",
                closeOnConfirm: false
            }, function () {
                window.location.reload();
            });
        } else if (name.value == ""){
            swal({
                title: "成功!!",
                text: "名称不可为空",
                type: "success",
                confirmButtonText: "确定",
                closeOnConfirm: false
            }, function () {
                window.location.reload();
            });
        } else {
            swal({
                title: "您确定要修改此业务员的信息吗",
                text: "修改后将无法恢复，请谨慎操作！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "是的，我要修改！",
                cancelButtonText: "让我再考虑一下…",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        data: {id:id.value,name:name.value,phone:phone.value,password:password.value},
                        url:"<%=path%>/operator/editOperator",
                        dataType:'json',
                        cache:false,
                        success:function (b) {
                            if(b.result){
                                swal({
                                    title: "成功!!",
                                    text: b.msg,
                                    type: "success",
                                    confirmButtonText: "确定",
                                    closeOnConfirm: false
                                }, function () {
                                    window.location.reload();
                                });
                            }else {
                                swal({
                                    title: "失败!!",
                                    text: b.msg,
                                    type: "warning",
                                    confirmButtonText: "确定",
                                    closeOnConfirm: false
                                }, function () {
                                    window.location.reload();
                                });
                            }
                        },
                        error:function (b) {
                            swal({
                                title: "失败!!",
                                text: b.msg,
                                type: "warning",
                                confirmButtonText: "确定",
                                closeOnConfirm: false
                            }, function () {
                                window.location.reload();
                            });
                        }
                    })
                } else {
                    swal({
                        title: "已取消!!",
                        text: "您已取消了操作",
                        type: "error",
                        confirmButtonText: "确定",
                        closeOnConfirm: false
                    }, function () {
                        window.location.reload();
                    });
                }
            })
        }
    }
</script>
</html>