<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>操作员添加</title>
    <meta name="keywords" content="科研项目管理">
    <meta name="description" content="科研项目管理">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE">
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/jquery/jquery-1.10.2.min.js"></script>
</head>
    <body>
    <div class="wrapper">
        <span id="config" class="ui-icon ui-state-default ui-icon-config"></span>
        <div class="mod-toolbar-top mr0 cf dn" id="toolTop"></div>
        <div class="bills cf">
            <div class="con-header">
                <div class="row-item" style="text-align: center ">
                    <h1>操作员添加</h1>
                </div>
                <div style="height: 30px; background-color: slategray"></div>
                <div style="height: 30px;"></div>
                <form action="<%=path%>/operator/add" method="post" id="form">
                    <div style="height: 400px; border: 1px black solid;">
                        <table align="center" style="margin-top: 50px; height: 300px;" border="0" cellspacing="0" width="600px">
                            <tr>
                                <td>请输入操作员昵称：</td>
                                <td>
                                    <input type="text" name="name" id="name" onblur="add(this.value)">
                                </td>
                            </tr>
                            <tr>
                                <td>请输入操作员电话：</td>
                                <td>
                                    <input type="text" name="phone" id="phone">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right">
                                    <input type="button" value="确定" onclick="send()">
                                </td>
                            </tr>
                        </table>
                    </div>
                </form>
                <div style="height: 30px;"></div>
                <div style="height: 30px; background-color: slategray"></div>
            </div>
        </div>
    </div>
    </body>
<script>
    function add(value) {
        var name = document.getElementById("name");
        $.ajax({
            type: "POST",
            url: "<%=path%>/operator/judgmentAccount?name=" + value,
            dataType: "json",
            success: function (data) {
                if (data.result) {
                } else {
                    alert("昵称重复,请重新输入");
                    name.value = "";
                }
            }
        });
    }
    function send() {
        var name = document.getElementById("name");
        var phone = document.getElementById("phone");
        if(phone.value == ""){
            alert("操作员联系电话不可为空");
            phone.focus();
        } else if (name.value == ""){
            alert("操作员昵称不可为空");
            name.focus();
        } else {
            $("form").submit();
        }
    }
</script>
</html>