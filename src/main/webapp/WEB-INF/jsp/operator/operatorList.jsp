<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <title>操作员列表</title>
    <style>
        #table-3 thead, #table-3 tr {
            border-top-width: 1px;
            border-top-style: solid;
            border-top-color: rgb(235, 242, 224);
        }
        #table-3 {
            border-bottom-width: 1px;
            border-bottom-style: solid;
            border-bottom-color: rgb(235, 242, 224);
        }

        /* Padding and font style */
        #table-3 td, #table-3 th {
            padding: 5px 10px;
            font-size: 12px;
            font-family: Verdana;
            /*color: rgb(149, 170, 109);*/
        }

        /* Alternating background colors */
        #table-3 tr:nth-child(even) {
            background: #eaf5ff;
        }
        #table-3 tr:nth-child(odd) {
            background: #c7e5ff;
        }
    </style>
    <script src="<%=path%>/static/hplus/js/jquery.min.js?v=2.1.4"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/jquery/jquery-1.10.2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('button[name="edit"]').click(function () {
                var id = $(this).attr("aid");
                window.location.href = "<%=path%>/operator/operatorEditPage?id=" + id;
            });

            $('button[name=delete]').click(function () {
                var id = $(this).attr("aid");
                result = confirm("是否确定删除");
                if (result == true) {
                    $.ajax({
                        type: "POST",
                        url: "<%=path%>/operator/delete?id=" + id,
                        dataType: "json",
                        success: function (data) {
                            if (data.result) {
                                alert(data.msg);
                                window.location.reload();
                            }
                        }
                    });
                }
            })
        })
    </script>
</head>
    <body class="top-navigation full-height-layout gray-bg">
        <div class="wrapper" style="height: 100%" id="body">
            <div class="mod-toolbar-top mr0 cf dn" id="toolTop"></div>
            <div class="bills cf" style="height: 100%">
                <div class="con-header" style="width: 100%">
                    <div class="row-item" style="text-align: center">
                        <h1 style="font-size: 23px">唐山军荣铝业有限公司业务员管理</h1>
                    </div>
                    <div class="row">
                        <div class="col-sm-6" style="width: 100%; height: 95%;">
                            <div class="tab-content">
                                <div id="tab-1" class="tab-pane active">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            <table id="table-3" style="width: 100%; border: 1px solid black;" border="1">
                                                <thead>
                                                    <tr>
                                                        <th style="display: none">ID</th>
                                                        <th>操作员昵称</th>
                                                        <th>操作员电话</th>
                                                        <th>操作</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:forEach items="${list}" var="list">
                                                        <tr>
                                                            <td style="display: none">${list.id}</td>
                                                            <td align="center">${list.name}</td>
                                                            <td align="center">${list.mobile}</td>
                                                            <c:if test="${sessionScope.aid == 1}">
                                                                <td align="center">
                                                                    <button name="edit" aid="${list.id}" onclick="">
                                                                        修改
                                                                    </button>
                                                                    &nbsp;&nbsp;&nbsp;
                                                                    <button name="delete" aid="${list.id}" onclick="">
                                                                        删除
                                                                    </button>
                                                                </td>
                                                            </c:if>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
