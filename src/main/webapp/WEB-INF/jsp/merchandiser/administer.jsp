<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <script></script>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>业务员提成管理</title>
    <meta name="keywords" content="科研项目管理">
    <meta name="description" content="科研项目管理">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE">
</head>
<body>
    <div id="page-wrapper" class="gray-bg" style="height: 90%; min-height: auto;">
        <div class="row border-bottom white-bg">
            <!-- 页面顶部¨ -->
            <%@ include file="../admin/top.jsp"%>
        </div>
        <div class="row J_mainContent" id="iframe1" style="height: 700px">
            <iframe src="<%=path%>/merchandiser/info"></iframe>
        </div>
    </div>
</body>
</html>