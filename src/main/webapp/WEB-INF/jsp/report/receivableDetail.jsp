<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>详情单</title>
    <link rel="shortcut icon" href="<%=path%>static/hplus/favicon.ico">
    <link href="<%=path%>/static/hplus/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/animate.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="<%=path%>/statics2/saas/scm/app2_release/css/common.css?ver=20150522" rel="stylesheet" type="text/css">
    <link href="<%=path%>/statics2/saas/scm/app2_release/css/green/ui.min.css?ver=20150522" rel="stylesheet">
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/jquery/jquery-1.10.2.min.js"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/json3.min.js"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/common.js?ver=20150522"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/grid.js?ver=20150522"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/plugins.js?ver=20150522"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/plugins/jquery.dialog.js?self=true&ver=20150522"></script>
    <link href="<%=path%>/statics2/css/base.css" rel="stylesheet" type="text/css">
    <link href="<%=path%>/statics2/css/green/default.css?ver=20150522" rel="stylesheet" type="text/css" id="defaultFile">
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/tabs.js?ver=20150522"></script>
    <script src="<%=path%>/static/hplus/js/jquery.min.js?v=2.1.4"></script>
    <script src="<%=path%>/static/hplus/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="<%=path%>/static/hplus/js/content.js?v=1.0.0"></script>
    <script src="<%=path%>/static/hplus/js/plugins/flot/jquery.flot.js"></script>
    <script src="<%=path%>/static/hplus/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="<%=path%>/static/hplus/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="<%=path%>/static/hplus/js/plugins/chartJs/Chart.min.js"></script>
    <script src="<%=path%>/static/hplus/js/plugins/peity/jquery.peity.min.js"></script>
    <script src="<%=path%>/static/hplus/js/demo/peity-demo.js"></script>
</head>
<body class="top-navigation full-height-layout gray-bg" >
    <div id="wrapper" style="background-color: transparent;" >
        <div class="row border-bottom white-bg" style="height: 10%">
            <!-- 页面顶部¨ -->
            <%@ include file="../admin/top.jsp"%>
        </div>
        <div class="row" style="height: 80%">
            <div class="col-sm-6" style="width: 100%; height: 100%;">
                <div class="tab-content">
                    <div id="tab-1" class="tab-pane active">
                        <div class="panel panel-primary">
                            <div class="panel-body" style="height: 100%;">
                                <c:if test="${sessionScope.aid == 1}">
                                    <iframe class="J_iframe" name="mainFrame" style="width: 100%;height: 800px"
                                            src="http://101.200.203.98:7080/WebReport/ReportServer?reportlet=铝瓶客户回款明细001.cpt">
                                    </iframe>
                                </c:if>
                                <c:if test="${sessionScope.aid != 1}">
                                    <iframe class="J_iframe" name="mainFrame" style="width: 100%;height: 800px"
                                            src="http://101.200.203.98:7080/WebReport/ReportServer?reportlet=铝瓶客户回款明细002.cpt&saleMan=${sessionScope.aid}">
                                    </iframe>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
