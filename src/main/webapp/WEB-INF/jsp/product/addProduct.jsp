<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <title>Title</title>
</head>
<body>
<script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/jquery/jquery-1.10.2.min.js"></script>
    <div style="height: 30px;"></div>
    <div style="height: 30px; background-color: slategray"></div>
    <div style="height: 30px;"></div>
    <div>
        <form action="<%=path%>/settings/addProduct" method="post" id="form">
            <table align="center" style="width: 50%; height: 50%;">
                <tr>
                    <td align="right" width="50%">商品名称：</td>
                    <td>
                        <input type="text" id="newProduct" style="width: 140px">
                        <select name="productName" id="productName" onchange="show_sub(this.options[this.options.selectedIndex].value)">
                            <option value="" selected = selected>添加新商品</option>
                            <c:forEach items="${list}" var="product">
                                <option value="${product.dataName}">${product.dataName}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td align="right" width="50%">商品规格：</td>
                    <td>
                        <input type="text" name="specifications" id="specifications" style=" width: 140px">
                        <span style="color: #6a7791">示例（150*150*150）</span>
                    </td>
                </tr>
                <tr>
                    <td align="right" width="50%">商品容积：</td>
                    <td>
                        <input type="text" name="bulk" id="bulk" style="width: 140px">
                        <input type="radio" name="unit" value="升">升
                        <input type="radio" name="unit" value="毫升" checked>毫升
                    </td>
                </tr>
                <tr>
                    <td align="right" width="50%">商品特征：</td>
                    <td>
                        <input type="text" name="feature" id="feature" style="width: 140px">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        <input type="button" onclick="send()" value="确认添加">
                    </td>
                </tr>
            </table>
        </form>
    </div>
    <div style="height: 30px;"></div>
    <div style="height: 30px; background-color: slategray"></div>
    <div style="height: 30px;"></div>
</body>
<script>
    function send () {
        var productNos = "";
        var newProduct = document.getElementById("newProduct");
        var specifications = document.getElementById("specifications");
        var bulk = document.getElementById("bulk");
        var feature = document.getElementById("feature");
        if(newProduct.value == ""){
            alert("请选择要增加的商品");
        } else if (specifications.value == ""){
            alert("请输入商品规格")
            specifications.focus();
        } else if (bulk.value == ""){
            alert("请输入商品容积");
            bulk.focus();
        }else if (feature.value == ""){
            alert("请输入商品特征");
            feature.focus();
        } else {
            var formParam = $("#form").serialize();
            var newProduct = $("#newProduct").val();
            $.ajax({
                type:'post',
                url:'<%=path%>/product/addProduct?newProduct='+newProduct,
                data:formParam,
                cache:false,
                dataType:'json',
                success:function(data){
                    if (data.result){
                        alert(data.msg);
                        window.location.reload();
                    } else {
                        alert(data.msg)
                    }
                },
                error:function (data) {
                    if (data.result){
                        alert(data.msg);
                        window.location.reload();
                    } else {
                        alert(data.msg);
                    }
                }
            });
        }
    }
    function show_sub(v){
        var newProduct = document.getElementById("newProduct");
        if (v != ""){
            $("#newProduct").attr("disabled",true);
            $("#newProduct").val(v);
        } else {
            $("#newProduct").attr("disabled",false);
            $("#newProduct").val(v);
        }
    }
</script>
</html>