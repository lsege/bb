<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <link href="<%=path%>/statics2/saas/scm/app2_release/css/common.css?ver=20150522" rel="stylesheet" type="text/css">
    <link href="<%=path%>/statics2/saas/scm/app2_release/css/green/ui.min.css?ver=20150522" rel="stylesheet">
    <link href="<%=path%>/statics2/css/green/bills.css?ver=20150522" rel="stylesheet" type="text/css">
    <link href="<%=path%>/static/hplus/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/animate.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/bootstrap.min.css?v=4.1.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus//css/bootstrap.min.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/style.css?v=4.1.0" rel="stylesheet">
    <script src="<%=path%>/static/hplus/js/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/jquery/jquery-1.10.2.min.js"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/json3.min.js"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/common.js?ver=20150522"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/grid.js?ver=20150522"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/plugins.js?ver=20150522"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/plugins/jquery.dialog.js?self=true&ver=20150522"></script>
    <script src="<%=path%>/static/hplus/js/bootstrap.min.js"></script>
    <style type="text/css">
        th{
            text-align: center;
        }
    </style>
    <style>
        #table-3 thead, #table-3 tr {
            border-top-width: 1px;
            border-top-style: solid;
            border-top-color: rgb(235, 242, 224);
        }
        #table-3 {
            border-bottom-width: 1px;
            border-bottom-style: solid;
            border-bottom-color: rgb(235, 242, 224);
        }

        /* Padding and font style */
        #table-3 td, #table-3 th {
            padding: 5px 10px;
            font-size: 12px;
            font-family: Verdana;
            /*color: rgb(149, 170, 109);*/
        }

        /* Alternating background colors */
        #table-3 tr:nth-child(even) {
            background: rgb(230, 238, 214)
        }
        #table-3 tr:nth-child(odd) {
            background: #FFF
        }
    </style>
</head>
<body>
    <div class="wrapper" style="height: 100%" id="body">
        <div class="bills cf" style="height: 100%">
            <div class="con-header" style="width: 100%">
                <div class="row-item" style="text-align: center">
                    <h1 style="font-size: 23px">唐山军荣铝业有限公司销货单管理</h1>
                </div>
                <div class="row">
                    <div class="col-sm-6" style="width: 100%; height: 95%;">
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel panel-primary">
                                    <div class="panel-body">
                                        <table id="table-3" style="width: 100%" border="1">
                                            <thead>
                                                <tr>
                                                    <th style="display: none">ID</th>
                                                    <th>编号</th>
                                                    <th>商品名称</th>
                                                    <th>商品规格</th>
                                                    <th>商品描述</th>
                                                    <th>操作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <c:forEach items="${list}" var="list" varStatus="count">
                                                <tr>
                                                    <td align="center">${list.index}</td>
                                                    <td style="display: none" align="center">${list.id}</td>
                                                    <td align="center">${list.name}</td>
                                                    <td align="center">${list.qual}</td>
                                                    <td align="center">${list.dataName}</td>
                                                    <td>
                                                        <button name="edit" aid="${list.id}" type="button" class="btn btn-primary">修改</button>
                                                        <button name="delete" aid="${list.id}" type="button" class="btn btn-default">删除</button>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                        <div style="font-size: 16px; width: 95%; margin-top: 20px">
                                            <div class="left" style="float: left; width: 75%">共${total}条记录</div>
                                            <div class="right" style="float: left;">
                                                <div class="btn-group">
                                                    <button class="btn btn-white" onclick="change(1)">首页</button>
                                                    <button type="button" class="btn btn-white" onclick="change(${currentPage-1})" style="height: 35px">
                                                        <i class="fa fa-chevron-left"></i>
                                                    </button>
                                                    <button class="btn btn-white" onclick="change(${currentPage-1})">${currentPage-1}</button>
                                                    <button class="btn btn-white  active" onclick="change(${currentPage})">${currentPage}</button>
                                                    <button class="btn btn-white" onclick="change(${currentPage+1})">${currentPage+1}</button>
                                                    <button type="button" class="btn btn-white" onclick="change(${currentPage+1})" style="height: 35px">
                                                        <i class="fa fa-chevron-right"></i>
                                                    </button>
                                                    <button class="btn btn-white" onclick="change(${pageCount})">尾页</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script>
    /*分页方法*/
    function change(page) {
        window.location = "<%=path%>/product/productList?page="+page;
    }
    $(document).ready(function () {
        /*修改方法*/
        $('button[name=edit]').click(function(){
            var id=$(this).attr("aid");
            window.location.href="<%=path%>/product/productEditPage?id="+id;
        });

        $('[name=delete]').click(function(){
            var aId=$(this).attr("aid");
            swal({
                title: "您确定要删除这条信息吗",
                text: "删除后将无法恢复，请谨慎操作！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "是的，我要删除！",
                cancelButtonText: "让我再考虑一下…",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url:"<%=path%>/product/deleteProduct?id="+aId,
                        dataType:'json',
                        cache:false,
                        success:function (b) {
                            if(b.result){
                                swal({
                                    title: "成功!!",
                                    text: b.msg,
                                    type: "success",
                                    confirmButtonText: "确定",
                                    closeOnConfirm: false
                                }, function () {
                                    window.location.reload();
                                });
                            }else {
                                swal({
                                    title: "失败!!",
                                    text: b.msg,
                                    type: "warning",
                                    confirmButtonText: "确定",
                                    closeOnConfirm: false
                                }, function () {
                                    change(1);
                                });
                            }

                        },
                        error:function (b) {
                            swal({
                                title: "失败!!",
                                text: b.msg,
                                type: "warning",
                                confirmButtonText: "确定",
                                closeOnConfirm: false
                            }, function () {
                                change(1);
                            });
                        }
                    })

                } else {
                    swal({
                        title: "已取消!!",
                        text: "您已取消了操作",
                        type: "error",
                        confirmButtonText: "确定",
                        closeOnConfirm: false
                    }, function () {
                        change(1);
                    });
                }
            })
        });
    })
</script>
</html>