<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <title>Title</title>
    <link rel="shortcut icon" href="<%=path%>/static/hplus/favicon.ico">
    <link href="<%=path%>/static/hplus/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <!-- jqgrid-->
    <link href="<%=path%>/static/hplus/css/plugins/jqgrid/ui.jqgrid.css?0820" rel="stylesheet">

    <link href="<%=path%>/static/hplus/css/animate.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/js/plugins/layer/laydate/need/laydate.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

    <link href="<%=path%>/static/hplus/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <script src="<%=path%>/static/hplus/js/plugins/sweetalert/content.min.js?v=1.0.0"></script>
    <script src="<%=path%>/static/hplus/js/plugins/sweetalert/sweetalert.min.js"></script>
</head>
<body>
<div class="wrapper">
    <div class="row">
        <div class="col-sm-6" style="width: 100%; height: 95%;">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#tab-1" aria-expanded="false">客户添加</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="tab-1" class="tab-pane active">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                客户添加
                            </div>
                            <div class="panel-body">
                                <div id="myModal_contract" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div>
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <form class="form-horizontal m-t" id="commentForm2">
                                                    <div class="form-group">
                                                        <table style="width: 70%" align="center">
                                                            <tr>
                                                                <td>
                                                                    <label class="col-sm-5 control-label">客户名称：</label>
                                                                    <div class="col-sm-6" style="width: 360px">
                                                                        <div class="input-group">
                                                                            <input id="canyu" name="customerName" type="text" class="form-control" required="" aria-required="true" value="${userName}" style="width: 360px">
                                                                            <input type="hidden" id="canyuId" value="${userId}">
                                                                            <div class="input-group-btn">
                                                                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                                                                </ul>
                                                                            </div>
                                                                            <input id="yxyh" type="hidden">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <label class="col-sm-5 control-label">客户联系方式：</label>
                                                                    <div class="col-sm-6" style="width: 360px">
                                                                        <div class="input-group">
                                                                            <input id="customerPhone" name="customerPhone" type="text" class="form-control"style="width: 360px">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <label class="col-sm-5 control-label">客户地址：</label>
                                                                    <div class="col-sm-6" style="width: 360px">
                                                                        <div class="input-group">
                                                                            <input id="customerAddress" name="customerAddress" type="text" class="form-control"style="width: 360px">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <label class="col-sm-5 control-label">联系人：</label>
                                                                    <div class="col-sm-6" style="width: 360px">
                                                                        <div class="input-group">
                                                                            <input id="customerPeople" name="customerPeople" type="text" class="form-control"style="width: 360px">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <label class="col-sm-5 control-label">联系人手机号：</label>
                                                                    <div class="col-sm-6" style="width: 360px">
                                                                        <div class="input-group">
                                                                            <input id="peoplePhone" name="peoplePhone" type="text" class="form-control"style="width: 360px">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <label class="col-sm-5 control-label">添加业务员：</label>
                                                                    <div class="col-sm-6" style="width: 360px">
                                                                        <div class="input-group">
                                                                            <input id="salesMan" name="salesMan" type="text" class="form-control" style="width: 360px" value="${list.get(0).name}" >
                                                                            <select name="productName" id="productName" onchange="show_sub(this.options[this.options.selectedIndex].value)">
                                                                                <c:forEach items="${list}" var="list">
                                                                                    <option value="${list.name}">${list.name}</option>
                                                                                </c:forEach>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <label class="col-sm-5 control-label">是否为老客户：</label>
                                                                    <div class="col-sm-6" style="width: 360px">
                                                                        <div class="input-group">
                                                                            <input name="stateCustomer" type="radio" >是
                                                                            <input name="stateCustomer" type="radio" checked >否
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">
                                                                    <button class="btn btn-primary " type="button" onclick="send()" style="margin-right: 15%"><i class="fa fa-check"></i>&nbsp;提交</button>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    function send () {
        var customerName = document.getElementById("canyu");
        var customerPhone = document.getElementById("customerPhone");
        var customerAddress = document.getElementById("customerAddress");
        var customerPeople = document.getElementById("customerPeople");
        var peoplePhone = document.getElementById("peoplePhone");
        var stateCustomer = document.getElementsByName("stateCustomer");
        if(customerName.value == ""){
            alert("请输入客户名称");
            customerName.focus();
        } else {
            $.ajax({
                type: "POST",
                url:"/ssm/customer/addCustomer?customerName="+customerName.value+"&customerPhone="+customerPhone.value+"&customerAddress="+customerAddress.value+"&customerPeople="+customerPeople.value+"&peoplePhone="+peoplePhone.value+"&stateCustomer="+stateCustomer.value+"&salesMan="+salesMan.value,
                dataType:'json',
                cache:false,
                success:function (b) {
                    if(b.result){
                        swal({
                            title: "成功!!",
                            text: b.msg,
                            type: "success",
                            confirmButtonText: "确定",
                            closeOnConfirm: false
                        }, function () {
                            top.location=self.location.href = "<%=path%>/index/customerListPage";
                        });
                    }else {
                        swal({
                            title: "失败!!",
                            text: b.msg,
                            type: "warning",
                            confirmButtonText: "确定",
                            closeOnConfirm: false
                        }, function () {
                            window.location.reload();
                        });
                    }

                },
                error:function (b) {
                    swal({
                        title: "失败!!",
                        text: b.msg,
                        type: "warning",
                        confirmButtonText: "确定",
                        closeOnConfirm: false
                    }, function () {
                        window.location.reload();
                    });
                }
            })
        };
    }
    function show_sub(v){
        var salesMan = document.getElementById("salesMan");
        if (v != ""){
            $("#salesMan").attr("disabled",true);
            salesMan.value = v;
        } else {
            $("#salesMan").attr("disabled",false);
            salesMan.value = "";
        }
    }
</script>
<!-- 全局js -->

<script src="<%=path%>/static/hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="<%=path%>/static/hplus/js/bootstrap.min.js?v=3.3.6"></script>

<script src="<%=path%>/static/hplus/js/plugins/toastr/toastr.min.js"></script>
<script src="<%=path%>/static/hplus/js/plugins/dropzone/dropzone.js"></script>
<!-- Peity -->
<script src="<%=path%>/static/hplus/js/plugins/peity/jquery.peity.min.js"></script>

<!-- jqGrid -->
<script src="<%=path%>/static/hplus/js/plugins/jqgrid/i18n/grid.locale-cn.js?0820"></script>
<script src="<%=path%>/static/hplus/js/plugins/jqgrid/jquery.jqGrid.min.js?0820"></script>
<!-- 自定义js -->
<%--<script src="/<%=path%>static/hplus/js/content.js?v=1.0.0"></script>--%>
<!-- Toastr script -->
<script src="<%=path%>/static/hplus/js/plugins/toastr/toastr.min.js"></script>
<%--<script src="static/hplus/js/plugins/validate/jquery.validate.min.js"></script>
<script src="static/hplus/js/plugins/validate/messages_zh.min.js"></script>--%>
<!-- layerDate plugin javascript -->
<script src="<%=path%>/static/hplus/js/plugins/layer/laydate/laydate.js"></script>
<script src="<%=path%>/static/js/jquery.printarea.js"></script>
<!-- Page-Level Scripts -->
<!-- tips -->
<script src="<%=path%>/static/js/jquery.tips.js"></script>
<!-- auto search -->
<script src="<%=path%>/static/js/map.js"></script>
<script src="<%=path%>/static/hplus/js/plugins/suggest/bootstrap-suggest.min.js"></script>

<script>
    $(function(){
        var vdata = new Map();
        var storage = new Map();
        $('#canyu').blur(function(event){
            var inp = $(this).val();
            if(vdata.keys.length!=0){
                var da = vdata.get(inp);
                console.log(vdata);
                if(da != undefined && da != 'undefined'){
                    console.log(da);
                    var ul = $('#content');

                    if(ul.children().length>=10){
                        alert('最多10人');
                    }else {
                        var st = storage.get(inp);
                        if(st == undefined || st == 'undefined'){
                            storage.put(inp,da);
                            $("#canyuId").val(da);
                        }else {
                        }
                    }
                    $("#yxyh").val("有效");
                }else {
                    $("#yxyh").val("无效");
                }
            }else {
                $("#yxyh").val("无效");
            }
        });

        $('li[name="litag"]').on('click',function(){
            var htm = $(this).children().html();
            storage.remove(htm);
            $(this).remove();
        });
        var proSuggest = $("#canyu").bsSuggest({
            indexId: 1, //data.value 的第几个数据，作为input输入框的内容
            indexKey: 0, //data.value 的第几个数据，作为input输入框的内容
            allowNoKeyword: false, //是否允许无关键字时请求数据
            multiWord: true, //以分隔符号分割的多关键字支持
            separator: ",", //多关键字支持时的分隔符，默认为空格
            getDataMethod: "url", //获取数据的方式，总是从 URL 获取
            effectiveFieldsAlias: {
              name: "客户名称"
            },
            showHeader: true,
            url: '<%=path%>/customer/loadCustomerName?str=',
            /*优先从url ajax 请求 json 帮助数据，注意最后一个参数为关键字请求参数*/
            /*如果从 url 获取数据，并且需要跨域，则该参数必须设置*/
            processData: function (json) { // url 获取数据时，对数据的处理，作为 getData 的回调函数
                var i, len, data = {
                    value: []
                };
                if (!json || !json.result || json.result.length == 0) {
                    return false;
                }
                len = json.result.length;
                for (i = 0; i < len; i++) {
                    data.value.push({
                        "name": json.result[i][1],
                    });
                    vdata.put(json.result[i][1]);
                }
                return data;
            }
        });
    })

</script>
</html>
