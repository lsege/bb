<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <title>Title</title>
</head>
<body>
<script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/jquery/jquery-1.10.2.min.js"></script>
<div style="height: 30px;"></div>
<div style="height: 30px; background-color: slategray"></div>
<div style="height: 30px;"></div>
<div>
    <form action="<%=path%>/customer/editCustomer" method="post" id="form">
        <table align="center" style="width: 50%; height: 50%;">
            <tr>
                <td align="right" width="50%">客户名称</td>
                <td>
                    <input type="hidden" name="id" value="${customer.customerId}">
                    <input type="text" name="customerName" id="customerName" value="${customer.customerName}">
                </td>
            </tr>
            <tr>
                <td align="right">客户联系方式</td>
                <td>
                    <input type="text" name="customerPhone" id="customerPhone" value="${customer.customerPhone}">
                </td>
            </tr>
            <tr>
                <td align="right">客户地址</td>
                <td>
                    <input type="text" name="customerAddress" id="customerAddress" value="${customer.cusstomerAddress}">
                </td>
            </tr>
            <tr>
                <td align="right">联系人</td>
                <td>
                    <input type="text" name="customerPeople" id="customerPeople" value="${customer.customerPeople}">
                </td>
            </tr>
            <tr>
                <td align="right">联系人手机号</td>
                <td>
                    <input type="text" name="peoplePhone" id="peoplePhone" value="${customer.peoplePhone}">
                </td>
            </tr>
            <tr>
                <td align="right">请选择业务员</td>
                <td>
                    <input type="text" name="salesMan" id="salesMan" style="width: 140px">
                    <select name="productName" id="productName" onchange="show_sub(this.options[this.options.selectedIndex].value)">
                        <option value="" selected = selected>添加新业务员</option>
                        <c:forEach items="${list}" var="list">
                            <c:if test="${list.name == customer.salesMan}">
                                <option value="${list.name}" selected>${list.name}</option>
                            </c:if>
                            <c:if test="${list.name != customer.salesMan}">
                                <option value="${list.name}">${list.name}</option>
                            </c:if>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td align="right">是否为老客户</td>
                <td>
                    <c:if test="${customer.stateCustomer == 1}">
                        <input type="radio" name="stateCustomer" value="2">是
                        <input type="radio" name="stateCustomer" value="1" checked>否
                    </c:if>
                    <c:if test="${customer.stateCustomer == 2}">
                        <input type="radio" name="stateCustomer" value="2" checked>是
                        <input type="radio" name="stateCustomer" value="1">否
                    </c:if>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <input type="button" onclick="send()" value="确认修改">
                </td>
            </tr>
        </table>
    </form>
</div>
<div style="height: 30px;"></div>
<div style="height: 30px; background-color: slategray"></div>
<div style="height: 30px;"></div>
</body>
<script>
    function send () {
        var customerName = document.getElementById("customerName");
        var customerPhone = document.getElementById("customerPhone");
        var customerAddress = document.getElementById("customerAddress");
        var customerPeople = document.getElementById("customerPeople");
        var peoplePhone = document.getElementById("peoplePhone");
        var stateCustomer = document.getElementsByName("stateCustomer");
        if(customerName.value == ""){
            alert("请输入客户名称");
            customerName.focus();
        } else {
            $("form").submit();
        }
    }
    function show_sub(v){
        var salesMan = document.getElementById("salesMan");
        if (v != ""){
            $("#salesMan").attr("disabled",true);
            salesMan.value = v;
        } else {
            $("#salesMan").attr("disabled",false);
            salesMan.value = "";
        }
    }
</script>
</html>