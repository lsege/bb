<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="../top.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script type="text/javascript">
        var DOMAIN = document.domain;
        var WDURL = "";
        var SCHEME = "green";
        try {
            document.domain = '<%=path%>';
        } catch (e) {
        }
        //ctrl+F5 增加版本号来清空iframe的缓存的
        $(document).keydown(function (event) {
            /* Act on the event */
            if (event.keyCode === 116 && event.ctrlKey) {
                var defaultPage = Public.getDefaultPage();
                var href = defaultPage.location.href.split('?')[0] + '?';
                var params = Public.urlParam();
                params['version'] = Date.parse((new Date()));
                for (i in params) {
                    if (i && typeof i != 'function') {
                        href += i + '=' + params[i] + '&';
                    }
                }
                defaultPage.location.href = href;
                event.preventDefault();
            }
        });
    </script>

    <link href="<%=path%>/static/css/green/bills.css?ver=20150522" rel="stylesheet" type="text/css">
    <style>
        #barCodeInsert {
            margin-left: 10px;
            font-weight: 100;
            font-size: 12px;
            color: #fff;
            background-color: #B1B1B1;
            padding: 0 5px;
            border-radius: 2px;
            line-height: 19px;
            height: 20px;
            display: inline-block;
        }

        #barCodeInsert.active {
            background-color: #23B317;
        }

    </style>
</head>
<body>
<input type="hidden" value="<%=path%>" id="hid">
<div class="wrapper">
    <span id="config" class="ui-icon ui-state-default ui-icon-config"></span>
    <div class="mod-toolbar-top mr0 cf dn" id="toolTop"></div>
    <div class="bills cf">
        <div class="con-header">
            <dl class="cf">
                <dd class="pct25" hidden>
                    <label hidden>ID</label>
                    <input type="text"  class="input-txt ui-input" autocomplete="off" id="customer" value="${nmgb}" hidden>
                </dd>
                <dd class="pct20" hidden>
                    <label>付款日期：</label>
                    <input type="text" id="date" class="ui-input ui-datepicker-input" value="2015-06-08">
                </dd>
                <dd id="11" class="pct25" >
                    <label>单据编号:</label>
                    <input id="billNo" type="text" class="input-txt ui-input" autocomplete="off" value="${num}">
                </dd>
                <dd id="" class="pct25" hidden>
                    <label>销货单编号:</label>
                    <input id="selectNo" type="text" class="input-txt ui-input" autocomplete="off" value="${nmgb}">
                </dd>
            </dl>
        </div>
        <div class="grid-wrap">
            <table id="grid">
            </table>
            <div id="page"></div>
        </div>
        <div class="con-footer cf">
            <ul id="amountArea" class="cf">
                <li>
                    <label> 制表人：</label>
                    <input type="text" id="make_man"  class="input-txt ui-input" autocomplete="off">
                </li>
                <li>
                    <label> 制表日期：</label>
                    <input type="text" id="make_date" class="ui-input ui-datepicker-input" value="2015-06-08">
                </li>
                <li>
                    <label> 填表人：</label>
                    <input type="text" id="input_man"  class="input-txt ui-input" autocomplete="off">
                </li>
                <li>
                    <label> 填表日期：</label>
                    <input type="text" id="input_date" class="ui-input ui-datepicker-input" autocomplete="off" >
                </li>
                <li>
                    <label> 复核人：</label>
                    <input type="text" id="reviewer_man"  class="input-txt ui-input" autocomplete="off">
                </li>
                <li>
                    <label> 复核日期：</label>
                    <input type="text" id="reviewer_date" class="ui-input ui-datepicker-input" autocomplete="off" >
                </li>
            </ul>
        </div>
        <div class="cf" id="bottomField">
            <div class="fr" id="toolBottom"></div>
        </div>
        <div id="mark"></div>
    </div>

    <div id="initCombo" class="dn">
        <input type="text" class="textbox goodsAuto" name="goods" autocomplete="off">
        <input type="text" class="textbox storageAuto" name="storage" autocomplete="off">
        <input type="text" class="textbox unitAuto" name="unit" autocomplete="off">
        <input type="text" class="textbox batchAuto" name="batch" autocomplete="off">
        <input type="text" class="textbox dateAuto" name="date" autocomplete="off">
        <input type="text" class="textbox priceAuto" name="price" autocomplete="off">
    </div>
    <div id="storageBox" class="shadow target_box dn">
    </div>
</div>
<script>
        window.history.pushState('forward', null, '/index/index5?type=3');
</script>
<script src="<%=path%>/static/product/a.js"></script>
<script src="<%=path%>/static/sales/sales_input.js"></script>
<script src="<%=path%>/static/hplus/js/plugins/suggest/bootstrap-suggest.min.js"></script>
<script src="<%=path%>/static/hplus/js/plugins/layer/laydate/laydate.js"></script>
<link href="<%=path%>/statics2/saas/scm/app2_release/css/common.css?ver=20150522" rel="stylesheet" type="text/css">
<link href="<%=path%>/statics2/saas/scm/app2_release/css/green/ui.min.css?ver=20150522" rel="stylesheet">
<script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/jquery/jquery-1.10.2.min.js"></script>
<script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/json3.min.js"></script>
<script src="<%=path%>/statics2/saas/scm/app2_release/js/common/common1.js?ver=20150522"></script>
<script src="<%=path%>/statics2/saas/scm/app2_release/js/common/grid.js?ver=20150522"></script>
<script src="<%=path%>/statics2/saas/scm/app2_release/js/common/plugins.js?ver=20150522"></script>
<script src="<%=path%>/statics2/saas/scm/app2_release/js/common/plugins/jquery.dialog.js?self=true&ver=20150522"></script>
<script src="<%=path%>/static/hplus/js/plugins/layer/laydate/laydate.js"></script>
<script>
    //外部js调用
    laydate({
        elem: '#make_date', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
        event: 'focus' //响应事件。如果没有传入event，则按照默认的click
    });
    laydate({
        elem: '#input_date', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
        event: 'focus' //响应事件。如果没有传入event，则按照默认的click
    });
    laydate({
        elem: '#reviewer_date', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
        event: 'focus' //响应事件。如果没有传入event，则按照默认的click
    });

    //日期范围限制
    var start = {
        elem: '#start',
        format: 'YYYY/MM/DD hh:mm:ss',
        min: laydate.now(), //设定最小日期为当前日期
        max: '2099-06-16 23:59:59', //最大日期
        istime: true,
        istoday: false,
        choose: function (datas) {
            end.min = datas; //开始日选好后，重置结束日的最小日期
            end.start = datas //将结束日的初始值设定为开始日
        }
    };
    var end = {
        elem: '#end',
        format: 'YYYY/MM/DD hh:mm:ss',
        min: laydate.now(),
        max: '2099-06-16 23:59:59',
        istime: true,
        istoday: false,
        choose: function (datas) {
            start.max = datas; //结束日选好后，重置开始日的最大日期
        }
    };
    laydate(start);
    laydate(end);
</script>
</body>
</html>




