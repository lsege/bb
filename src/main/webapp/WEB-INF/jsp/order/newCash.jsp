<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>销售业绩列表</title>
    <meta name="keywords" content="科研项目管理">
    <meta name="description" content="科研项目管理">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE">
    <link href="<%=path%>/statics2/saas/scm/app2_release/css/common.css?ver=20150522" rel="stylesheet" type="text/css">
    <link href="<%=path%>/statics2/saas/scm/app2_release/css/green/ui.min.css?ver=20150522" rel="stylesheet">
    <link href="<%=path%>/statics2/css/green/bills.css?ver=20150522" rel="stylesheet" type="text/css">
    <link href="<%=path%>/static/hplus/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/animate.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/style.css?v=4.1.0" rel="stylesheet">
    <link href="<%=path%>/statics2/css/green/bills.css?ver=20150522" rel="stylesheet" type="text/css">
    <link href="<%=path%>/static/hplus/css/bootstrap.min.css?v=4.1.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/jquery/jquery-1.10.2.min.js"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/json3.min.js"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/common.js?ver=20150522"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/plugins.js?ver=20150522"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/plugins/jquery.dialog.js?self=true&ver=20150522"></script>
    <script src="<%=path%>/static/hplus/js/plugins/layer/laydate/laydate.js"></script>
    <script src="<%=path%>/static/sales/sales_input.js"></script>
    <script src="<%=path%>/static/hplus/js/plugins/suggest/bootstrap-suggest.min.js"></script>
    <script src="<%=path%>/static/hplus/js/bootstrap.min.js"></script>

    <script src="<%=path%>/static/hplus/js/plugins/toastr/toastr.min.js"></script>

    <link href="<%=path%>/static/hplus/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <script src="<%=path%>/static/hplus/js/plugins/sweetalert/content.min.js?v=1.0.0"></script>
    <script src="<%=path%>/static/hplus/js/plugins/sweetalert/sweetalert.min.js"></script>
</head>
</head>
<body>
<div class="wrapper">
    <div class="row">
        <div class="col-sm-6" style="width: 100%; height: 95%;">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#tab-1" aria-expanded="false">现金回款单</a>
                    </li>
                    <li class="">
                        <a data-toggle="tab" href="#tab-2" aria-expanded="true">承兑回款单</a>
                    </li>
                    <li class="">
                        <a data-toggle="tab" href="#tab-3" aria-expanded="false">电汇回款单</a>
                    </li>
                    <li class="">
                        <a data-toggle="tab" href="#tab-4" aria-expanded="false">转账回款单</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="tab-1" class="tab-pane active">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                现金回款单
                            </div>
                            <div class="panel-body">
                                <form id="form4">
                                    <div style="height: 550px; border: 1px black solid;">
                                        <table style="width:100%; height: 100%">
                                            <tr>
                                                <td hidden>
                                                    <input type="text" name="documentType4" value="4" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <label>付款日期：</label>
                                                    <input type="text" id="payData4" class="ui-input ui-datepicker-input" value="" name="payData4" style="width: 130px">
                                                </td>
                                                <td align="center">
                                                    <!-管理员登陆显示选择销售人员->
                                                    <c:if test="${sessionScope.level == 0}">
                                                        <label>业务员:</label>
                                                        <span class="ui-combo-wrap">
                                                            <input type="text" name="salesMan4" class="input-txt" autocomplete="off" value="" data-ref="date" id="salesMan4" style="width: 130px;">
                                                            <a id="chooseSalesMan4"><i class="ui-icon-ellipsis"></i></a>
                                                        </span>
                                                    </c:if>
                                                    <!-业务员登陆隐藏选择销售人员，使用当前用户ID->
                                                    <c:if test="${sessionScope.level == 1}">
                                                        <span class="ui-combo-wrap">
                                                            <input type="text" class="input-txt" autocomplete="off" value="${sessionScope.names}" name="salesMan4" style="width: 120px;" hidden>
                                                        </span>
                                                    </c:if>
                                                </td>
                                                <td align="center">
                                                    <label>单据编号：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="billNo4" class="input-txt" autocomplete="off" value="${num}" data-ref="date" id="billNo4" style="width: 130px">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <label>付款客户：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="goodsName4" class="input-txt" autocomplete="off" value="" data-ref="date" id="goodsName4" style="width: 130px;">
                                                        <a id="chooseCustomer4"><i class="ui-icon-ellipsis"></i></a>
                                                    </span>
                                                </td>
                                                <td></td>
                                                <td align="center">
                                                    <label>核对单据：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="bill" class="input-txt" autocomplete="off" value="" data-ref="date" id="bill" style="width: 130px;">
                                                        <a id="chooseBill4"><i class="ui-icon-ellipsis"></i></a>
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>所选单据信息：</label>
                                                    <table align="center" border="1" id="table4">
                                                        <tr>
                                                            <td width="120px" align="center">单据编号</td><td width="120px" align="center">需付金额</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>付款金额：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="price4" class="input-txt" autocomplete="off" value="" data-ref="date" id="price4" style="width: 130px;">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>联系方式：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="goodsContact4" class="input-txt" autocomplete="off" value="" data-ref="date" id="goodsContact4" style="width: 130px;">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>备注信息：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="note4" class="input-txt" autocomplete="off" value="" data-ref="date" id="note" style="width: 130px;">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <label>经手人：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="handlers4" class="input-txt" autocomplete="off" value="" data-ref="date" id="handlers" style="width: 130px;">
                                                    </span>
                                                </td>
                                                <td></td>
                                                <td align="center">
                                                    <label>录入人：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="enterOne4" class="input-txt" autocomplete="off" value="" data-ref="date" id="enterOne" style="width: 130px;">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" align="right">
                                                    <button class="btn btn-primary " type="button" onclick="send4()" style="margin-right: 15%"><i class="fa fa-check"></i>&nbsp;提交</button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div id="tab-2" class="tab-pane">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                承兑回款单
                            </div>
                            <div class="panel-body">
                                <form id="form1">
                                    <div style="height: 550px; border: 1px black solid;">
                                        <table style="width:100%; height: 100%">
                                            <tr>
                                                <td hidden>
                                                    <input type="text" name="documentType1" value="1" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <label>收票日期：</label>
                                                    <input type="text" id="payData1" class="ui-input ui-datepicker-input" value="" name="payData1" style="width: 130px">
                                                </td>
                                                <td align="center">
                                                    <!-管理员登陆显示选择销售人员->
                                                    <c:if test="${sessionScope.level == 0}">
                                                        <label>业务员:</label>
                                                        <span class="ui-combo-wrap">
                                                            <input type="text" name="salesMan1" class="input-txt" autocomplete="off" value="" data-ref="date" id="salesMan1" style="width: 130px;">
                                                            <a id="chooseSalesMan1"><i class="ui-icon-ellipsis"></i></a>
                                                        </span>
                                                    </c:if>
                                                    <!-业务员登陆隐藏选择销售人员，使用当前用户ID->
                                                    <c:if test="${sessionScope.level == 1}">
                                                        <span class="ui-combo-wrap">
                                                            <input type="text" class="input-txt" autocomplete="off" value="${sessionScope.names}" name="salesMan1" style="width: 120px;" hidden>
                                                        </span>
                                                    </c:if>
                                                </td>
                                                <td align="center">
                                                    <label>单据编号：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="billNo1" class="input-txt" autocomplete="off" value="${num}" data-ref="date" id="billNo1" style="width: 130px">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <label>付款客户：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="goodsName1" class="input-txt" autocomplete="off" value="" data-ref="date" id="goodsName1" style="width: 130px;">
                                                        <a id="chooseCustomer1"><i class="ui-icon-ellipsis"></i></a>
                                                    </span>
                                                </td>
                                                <td></td>
                                                <td align="center">
                                                    <label>核对单据：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="bill" class="input-txt" autocomplete="off" value="" data-ref="date"style="width: 130px;">
                                                        <a id="chooseBill1"><i class="ui-icon-ellipsis"></i></a>
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>所选单据信息：</label>
                                                    <table align="center" border="1" id="table1">
                                                        <tr>
                                                            <td width="120px" align="center">单据编号</td><td width="120px" align="center">需付金额</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>付款金额：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="price1" class="input-txt" autocomplete="off" value="" data-ref="date" id="price1" style="width: 130px;">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>联系方式：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="goodsContact1" class="input-txt" autocomplete="off" value="" data-ref="date" id="goodsContact1" style="width: 130px;">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>寄票单位：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="sendUnit1" class="input-txt" autocomplete="off" value="" data-ref="date" id="sendUnit1" style="width: 130px;">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>转下单位：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="toUnit1" class="input-txt" autocomplete="off" value="" data-ref="date" id="toUnit1" style="width: 130px;">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>承兑票号：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="chengduiNum1" class="input-txt" autocomplete="off" value="" data-ref="date" id="chengduiNum1" style="width: 130px;">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>承兑到款日：</label>
                                                    <input type="text" id="chengduiDate1" class="ui-input ui-datepicker-input" value="" name="chengduiDate1" style="width: 130px">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>付款银行：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="chengduiBang1" class="input-txt" autocomplete="off" value="" data-ref="date" id="chengduiBang" style="width: 130px;">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>备注信息：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="note1" class="input-txt" autocomplete="off" value="" data-ref="date" id="note1" style="width: 130px;">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <label>经手人：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="handlers1" class="input-txt" autocomplete="off" value="" data-ref="date" id="handlers1" style="width: 130px;">
                                                    </span>
                                                </td>
                                                <td></td>
                                                <td align="center">
                                                    <label>录入人：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="enterOne1" class="input-txt" autocomplete="off" value="" data-ref="date" id="enterOne1" style="width: 130px;">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" align="right">
                                                    <button class="btn btn-primary " type="button" onclick="send1()" style="margin-right: 15%"><i class="fa fa-check"></i>&nbsp;提交</button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div id="tab-3" class="tab-pane">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                电汇回款单
                            </div>
                            <div class="panel-body">
                                <form id="form2">
                                    <div style="height: 550px; border: 1px black solid;">
                                        <table style="width:100%; height: 100%">
                                            <tr>
                                                <td hidden>
                                                    <input type="text" name="documentType2" value="2" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <label>汇款日期：</label>
                                                    <input type="text" id="payData2" class="ui-input ui-datepicker-input" value="" name="payData2" style="width: 130px">
                                                </td>
                                                <td align="center">
                                                    <!-管理员登陆显示选择销售人员->
                                                    <c:if test="${sessionScope.level == 0}">
                                                        <label>业务员:</label>
                                                        <span class="ui-combo-wrap">
                                                            <input type="text" name="salesMan2" class="input-txt" autocomplete="off" value="" data-ref="date" id="salesMan2" style="width: 130px;">
                                                            <a id="chooseSalesMan2"><i class="ui-icon-ellipsis"></i></a>
                                                        </span>
                                                    </c:if>
                                                    <!-业务员登陆隐藏选择销售人员，使用当前用户ID->
                                                    <c:if test="${sessionScope.level == 1}">
                                                        <span class="ui-combo-wrap">
                                                            <input type="text" class="input-txt" autocomplete="off" value="${sessionScope.names}" name="salesMan2" style="width: 120px;" hidden>
                                                        </span>
                                                    </c:if>
                                                </td>
                                                <td align="center">
                                                    <label>单据编号：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="billNo2" class="input-txt" autocomplete="off" value="${num}" data-ref="date" id="billNo2" style="width: 130px">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <label>付款客户：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="goodsName2" class="input-txt" autocomplete="off" value="" data-ref="date" id="goodsName2" style="width: 130px;">
                                                        <a id="chooseCustomer2"><i class="ui-icon-ellipsis"></i></a>
                                                    </span>
                                                </td>
                                                <td></td>
                                                <td align="center">
                                                    <label>核对单据：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="bill" class="input-txt" autocomplete="off" value="" data-ref="date"style="width: 130px;">
                                                        <a id="chooseBill2"><i class="ui-icon-ellipsis"></i></a>
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>所选单据信息：</label>
                                                    <table align="center" border="1" id="table2">
                                                        <tr>
                                                            <td width="120px" align="center">单据编号</td><td width="120px" align="center">需付金额</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>付款金额：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="price2" class="input-txt" autocomplete="off" value="" data-ref="date" id="price2" style="width: 130px;">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>银行名称：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="dianhuiBankName2" class="input-txt" autocomplete="off" value="" data-ref="date" id="dianhuiBankName2" style="width: 130px;">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>付款账号：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="dianhuiPayAccount2" class="input-txt" autocomplete="off" value="" data-ref="date" id="dianhuiPayAccount2" style="width: 130px;">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>收款账号：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="dianhuiCollectionAccount2" class="input-txt" autocomplete="off" value="" data-ref="date" id="dianhuiCollectionAccount2" style="width: 130px;">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>联系方式：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="goodsContact2" class="input-txt" autocomplete="off" value="" data-ref="date" id="goodsContact2" style="width: 130px;">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>备注信息：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="note2" class="input-txt" autocomplete="off" value="" data-ref="date" id="note2" style="width: 130px;">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>电汇内容：</label>
                                                    <textarea type="text" name="dianhuiContext2" class="input-txt" autocomplete="off" value="" data-ref="date" id="dianhuiContext2" style="width: 130px;"></textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <label>经手人：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="handlers2" class="input-txt" autocomplete="off" value="" data-ref="date" id="handlers2" style="width: 130px;">
                                                    </span>
                                                </td>
                                                <td></td>
                                                <td align="center">
                                                    <label>录入人：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="enterOne2" class="input-txt" autocomplete="off" value="" data-ref="date" id="enterOne2" style="width: 130px;">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" align="right">
                                                    <button class="btn btn-primary " type="button" onclick="send2()" style="margin-right: 15%"><i class="fa fa-check"></i>&nbsp;提交</button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div id="tab-4" class="tab-pane">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                转账回款单
                            </div>
                            <div class="panel-body">
                                <form id="form3">
                                    <div style="height: 550px; border: 1px black solid;">
                                        <table style="width:100%; height: 100%">
                                            <tr>
                                                <td hidden>
                                                    <input type="text" name="documentType3" value="3" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <label>汇款日期：</label>
                                                    <input type="text" id="payData3" class="ui-input ui-datepicker-input" value="" name="payData3" style="width: 130px">
                                                </td>
                                                <td align="center">
                                                    <!-管理员登陆显示选择销售人员->
                                                    <c:if test="${sessionScope.level == 0}">
                                                        <label>业务员:</label>
                                                        <span class="ui-combo-wrap">
                                                            <input type="text" name="salesMan3" class="input-txt" autocomplete="off" value="" data-ref="date" id="salesMan3" style="width: 130px;">
                                                            <a id="chooseSalesMan3"><i class="ui-icon-ellipsis"></i></a>
                                                        </span>
                                                    </c:if>
                                                    <!-业务员登陆隐藏选择销售人员，使用当前用户ID->
                                                    <c:if test="${sessionScope.level == 1}">
                                                        <span class="ui-combo-wrap">
                                                            <input type="text" class="input-txt" autocomplete="off" value="${sessionScope.names}" name="salesMan3" style="width: 120px;" hidden>
                                                        </span>
                                                    </c:if>
                                                </td>
                                                <td align="center">
                                                    <label>单据编号：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="billNo3" class="input-txt" autocomplete="off" value="${num}" data-ref="date" id="billNo3" style="width: 130px">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <label>付款客户：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="goodsName3" class="input-txt" autocomplete="off" value="" data-ref="date" id="goodsName3" style="width: 130px;">
                                                        <a id="chooseCustomer3"><i class="ui-icon-ellipsis"></i></a>
                                                    </span>
                                                </td>
                                                <td></td>
                                                <td align="center">
                                                    <label>核对单据：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="bill" class="input-txt" autocomplete="off" value="" data-ref="date"style="width: 130px;">
                                                        <a id="chooseBill3"><i class="ui-icon-ellipsis"></i></a>
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>所选单据信息：</label>
                                                    <table align="center" border="1" id="table3">
                                                        <tr>
                                                            <td width="120px" align="center">单据编号</td><td width="120px" align="center">需付金额</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>付款金额：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="price3" class="input-txt" autocomplete="off" value="" data-ref="date" id="price3" style="width: 130px;">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>付款人账号：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="zhuanzhangPayAccount3" class="input-txt" autocomplete="off" value="" data-ref="date" id="zhuanzhangPayAccount3" style="width: 130px;">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>收款人账号：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="zhuanzhangCollectionAccount3" class="input-txt" autocomplete="off" value="" data-ref="date" id="zhuanzhangCollectionAccount3" style="width: 130px;">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>联系方式：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="goodsContact3" class="input-txt" autocomplete="off" value="" data-ref="date" id="goodsContact3" style="width: 130px;">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="3">
                                                    <label>备注信息：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="note3" class="input-txt" autocomplete="off" value="" data-ref="date" id="note3" style="width: 130px;">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <label>经手人：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="handlers3" class="input-txt" autocomplete="off" value="" data-ref="date" id="handlers3" style="width: 130px;">
                                                    </span>
                                                </td>
                                                <td></td>
                                                <td align="center">
                                                    <label>录入人：</label>
                                                    <span class="ui-combo-wrap">
                                                        <input type="text" name="enterOne3" class="input-txt" autocomplete="off" value="" data-ref="date" id="enterOne3" style="width: 130px;">
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" align="right">
                                                    <button class="btn btn-primary " type="button" onclick="send3()" style="margin-right: 15%"><i class="fa fa-check"></i>&nbsp;提交</button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--客户列表1-->
<div class="modal inmodal fade" id="customer_modal1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <label>选择客户</label>
                </div>
                <div style="margin-top: 10px">
                    <label>
                        <input id="selectCustomerName1" type="text" placeholder="请输入客户名称" oninput="selectCustomerByName1(this.value)">
                    </label>
                </div>
                <div class="modal-body" style="overflow:scroll; height:400px;" id="customerTable1">
                    <table style="width: 100%; border: 1px solid #b1b1b1" border="1">
                        <c:forEach items="${customerRows}" var="customers">
                            <tr style="height: 50px">
                                <td align="center"><input type="radio" value="${customers.name1}" name="selectCustomer1" id="${customers.name1}"></td>
                                <td align="center" ondblclick="dbClickCustomer('${customers.name1}')">${customers.name1}</td>
                                <td align="center"><input type="radio" value="${customers.name2}" name="selectCustomer1" id="${customers.name2}"></td>
                                <td align="center" ondblclick="dbClickCustomer('${customers.name2}')">${customers.name2}</td>
                                <td align="center"><input type="radio" value="${customers.name3}" name="selectCustomer1" id="${customers.name3}"></td>
                                <td align="center" ondblclick="dbClickCustomer('${customers.name3}')">${customers.name3}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="modal-footer">
                    <div style="float: left; width: 25%">
                        <h4>共${size}家客户</h4>
                    </div>
                    <div style="float: left; width: 50%" id="page1">
                        <input type="button" value="首页" onclick="customerPaging1(1)">
                        <input type="button" value="上一页" onclick="customerPaging1(1)">
                        <input type="button" value="1" onclick="customerPaging1(1)" style="background-color:deepskyblue; width: 20px">
                        <input type="button" value="2" onclick="customerPaging1(2)" style="width: 20px">
                        <input type="button" value="3" onclick="customerPaging1(3)" style="width: 20px">
                        <input type="button" value="下一页" onclick="customerPaging1(${currentPage+1})">
                        <input type="button" value="尾页" onclick="customerPaging1(${pageCount})">
                    </div>
                    <div style="float: left; width: 25%">
                        <button id="close_customer1" type="button" class="btn btn-white" data-dismiss="modal">取消</button>
                        <button class="btn btn-primary" type="submit" id="customer_end1" >确定</button>
                    </div>
                    <div style="float: none"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--客户列表2-->
<div class="modal inmodal fade" id="customer_modal2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <label>选择客户</label>
                </div>
                <div style="margin-top: 10px">
                    <label>
                        <input id="selectCustomerName2" type="text" placeholder="请输入客户名称" oninput="selectCustomerByName2(this.value)">
                    </label>
                </div>
                <div class="modal-body" style="overflow:scroll; height:400px;" id="customerTable2">
                    <table style="width: 100%; border: 1px solid #b1b1b1" border="1">
                        <c:forEach items="${customerRows}" var="customers">
                            <tr style="height: 50px">
                                <td align="center"><input type="radio" value="${customers.name1}" name="selectCustomer2" id="${customers.name1}"></td>
                                <td align="center" ondblclick="dbClickCustomer('${customers.name1}')">${customers.name1}</td>
                                <td align="center"><input type="radio" value="${customers.name2}" name="selectCustomer2" id="${customers.name2}"></td>
                                <td align="center" ondblclick="dbClickCustomer('${customers.name2}')">${customers.name2}</td>
                                <td align="center"><input type="radio" value="${customers.name3}" name="selectCustomer2" id="${customers.name3}"></td>
                                <td align="center" ondblclick="dbClickCustomer('${customers.name3}')">${customers.name3}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="modal-footer">
                    <div style="float: left; width: 25%">
                        <h4>共${size}家客户</h4>
                    </div>
                    <div style="float: left; width: 50%" id="page2">
                        <input type="button" value="首页" onclick="customerPaging2(1)">
                        <input type="button" value="上一页" onclick="customerPaging2(1)">
                        <input type="button" value="1" onclick="customerPaging2(1)" style="background-color:deepskyblue; width: 20px">
                        <input type="button" value="2" onclick="customerPaging2(2)" style="width: 20px">
                        <input type="button" value="3" onclick="customerPaging2(3)" style="width: 20px">
                        <input type="button" value="下一页" onclick="customerPaging2(${currentPage+1})">
                        <input type="button" value="尾页" onclick="customerPaging2(${pageCount})">
                    </div>
                    <div style="float: left; width: 25%">
                        <button id="close_customer2" type="button" class="btn btn-white" data-dismiss="modal">取消</button>
                        <button class="btn btn-primary" type="submit" id="customer_end2" >确定</button>
                    </div>
                    <div style="float: none"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--客户列表3-->
<div class="modal inmodal fade" id="customer_modal3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <label>选择客户</label>
                </div>
                <div style="margin-top: 10px">
                    <label>
                        <input id="selectCustomerName3" type="text" placeholder="请输入客户名称" oninput="selectCustomerByName3(this.value)">
                    </label>
                </div>
                <div class="modal-body" style="overflow:scroll; height:400px;" id="customerTable3">
                    <table style="width: 100%; border: 1px solid #b1b1b1" border="1">
                        <c:forEach items="${customerRows}" var="customers">
                            <tr style="height: 50px">
                                <td align="center"><input type="radio" value="${customers.name1}" name="selectCustomer3" id="${customers.name1}"></td>
                                <td align="center" ondblclick="dbClickCustomer('${customers.name1}')">${customers.name1}</td>
                                <td align="center"><input type="radio" value="${customers.name2}" name="selectCustomer3" id="${customers.name2}"></td>
                                <td align="center" ondblclick="dbClickCustomer('${customers.name2}')">${customers.name2}</td>
                                <td align="center"><input type="radio" value="${customers.name3}" name="selectCustomer3" id="${customers.name3}"></td>
                                <td align="center" ondblclick="dbClickCustomer('${customers.name3}')">${customers.name3}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="modal-footer">
                    <div style="float: left; width: 25%">
                        <h4>共${size}家客户</h4>
                    </div>
                    <div style="float: left; width: 50%" id="page3">
                        <input type="button" value="首页" onclick="customerPaging3(1)">
                        <input type="button" value="上一页" onclick="customerPaging3(1)">
                        <input type="button" value="1" onclick="customerPaging3(1)" style="background-color:deepskyblue; width: 20px">
                        <input type="button" value="2" onclick="customerPaging3(2)" style="width: 20px">
                        <input type="button" value="3" onclick="customerPaging3(3)" style="width: 20px">
                        <input type="button" value="下一页" onclick="customerPaging3(${currentPage+1})">
                        <input type="button" value="尾页" onclick="customerPaging3(${pageCount})">
                    </div>
                    <div style="float: left; width: 25%">
                        <button id="close_customer3" type="button" class="btn btn-white" data-dismiss="modal">取消</button>
                        <button class="btn btn-primary" type="submit" id="customer_end3" >确定</button>
                    </div>
                    <div style="float: none"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--客户列表4-->
<div class="modal inmodal fade" id="customer_modal4" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <label>选择客户</label>
                </div>
                <div style="margin-top: 10px">
                    <label>
                        <input id="selectCustomerName4" type="text" placeholder="请输入客户名称" oninput="selectCustomerByName4(this.value)">
                    </label>
                </div>
                <div class="modal-body" style="overflow:scroll; height:400px;" id="customerTable4">
                    <table style="width: 100%; border: 1px solid #b1b1b1" border="1">
                        <c:forEach items="${customerRows}" var="customers">
                            <tr style="height: 50px">
                                <td align="center"><input type="radio" value="${customers.name1}" name="selectCustomer3" id="${customers.name1}"></td>
                                <td align="center" ondblclick="dbClickCustomer('${customers.name1}')">${customers.name1}</td>
                                <td align="center"><input type="radio" value="${customers.name2}" name="selectCustomer3" id="${customers.name2}"></td>
                                <td align="center" ondblclick="dbClickCustomer('${customers.name2}')">${customers.name2}</td>
                                <td align="center"><input type="radio" value="${customers.name3}" name="selectCustomer3" id="${customers.name3}"></td>
                                <td align="center" ondblclick="dbClickCustomer('${customers.name3}')">${customers.name3}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="modal-footer">
                    <div style="float: left; width: 25%">
                        <h4>共${size}家客户</h4>
                    </div>
                    <div style="float: left; width: 50%" id="page4">
                        <input type="button" value="首页" onclick="customerPaging4(1)">
                        <input type="button" value="上一页" onclick="customerPaging4(1)">
                        <input type="button" value="1" onclick="customerPaging4(1)" style="background-color:deepskyblue; width: 20px">
                        <input type="button" value="2" onclick="customerPaging4(2)" style="width: 20px">
                        <input type="button" value="3" onclick="customerPaging4(3)" style="width: 20px">
                        <input type="button" value="下一页" onclick="customerPaging4(${currentPage+1})">
                        <input type="button" value="尾页" onclick="customerPaging4(${pageCount})">
                    </div>
                    <div style="float: left; width: 25%">
                        <button id="close_customer4" type="button" class="btn btn-white" data-dismiss="modal">取消</button>
                        <button class="btn btn-primary" type="submit" id="customer_end4" >确定</button>
                    </div>
                    <div style="float: none"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--单据列表1-->
<div class="modal inmodal fade" id="document_modal1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <label>选择销货单</label>
                </div>
                <div class="modal-body" style="overflow:scroll; height:400px;">
                    <h1 id="errpr1" align="center"></h1>
                    <table style="width: 50%; border: 1px solid #b1b1b1" border="1" id="documentNo1" align="center">
                    </table>
                </div>
                <div class="modal-footer">
                    <button id="close_document1" type="button" class="btn btn-white" data-dismiss="modal">取消</button>
                    <button class="btn btn-primary" type="submit" id="end1" >确定</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--单据列表2-->
<div class="modal inmodal fade" id="document_modal2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <label>选择销货单</label>
                </div>
                <div class="modal-body" style="overflow:scroll; height:400px;">
                    <h1 id="errpr2" align="center"></h1>
                    <table style="width: 50%; border: 1px solid #b1b1b1" border="1" id="documentNo2" align="center">
                    </table>
                </div>
                <div class="modal-footer">
                    <button id="close_document2" type="button" class="btn btn-white" data-dismiss="modal">取消</button>
                    <button class="btn btn-primary" type="submit" id="end2" >确定</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--单据列表3-->
<div class="modal inmodal fade" id="document_modal3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <label>选择销货单</label>
                </div>
                <div class="modal-body" style="overflow:scroll; height:400px;">
                    <h1 id="errpr3" align="center"></h1>
                    <table style="width: 50%; border: 1px solid #b1b1b1" border="1" id="documentNo3" align="center">
                    </table>
                </div>
                <div class="modal-footer">
                    <button id="close_document3" type="button" class="btn btn-white" data-dismiss="modal">取消</button>
                    <button class="btn btn-primary" type="submit" id="end3" >确定</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--单据列表4-->
<div class="modal inmodal fade" id="document_modal4" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <label>选择销货单</label>
                </div>
                <div class="modal-body" style="overflow:scroll; height:400px;">
                    <h1 id="errpr4" align="center"></h1>
                    <table style="width: 50%; border: 1px solid #b1b1b1" border="1" id="documentNo4" align="center">
                    </table>
                </div>
                <div class="modal-footer">
                    <button id="close_document4" type="button" class="btn btn-white" data-dismiss="modal">取消</button>
                    <button class="btn btn-primary" type="submit" id="end4" >确定</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!--销售员列表-->
<div class="modal inmodal fade" id="salesMan_modal1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <label>选择销售员</label>
                </div>
                <div class="modal-body" style="overflow:scroll; height:400px;">
                    <table style="width: 100%; border: 1px solid #b1b1b1" border="1">
                        <c:forEach items="${salesMan}" var="salesMan">
                            <tr style="height: 50px">
                                <td align="center"><input type="radio" value="${salesMan.name}" id="${salesMan.name}" name="selectSalesMan1"></td>
                                <td align="center" ondblclick="shuangji1('${salesMan.name}')">${salesMan.name}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="modal-footer">
                    <button id="close_salesMan1" type="button" class="btn btn-white" data-dismiss="modal">取消</button>
                    <button class="btn btn-primary" type="submit" id="end_SalesMan1" >确定</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--销售员列表-->
<div class="modal inmodal fade" id="salesMan_modal2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <label>选择销售员</label>
                </div>
                <div class="modal-body" style="overflow:scroll; height:400px;">
                    <table style="width: 100%; border: 1px solid #b1b1b1" border="1">
                        <c:forEach items="${salesMan}" var="salesMan">
                            <tr style="height: 50px">
                                <td align="center"><input type="radio" value="${salesMan.name}" id="${salesMan.name}" name="selectSalesMan2"></td>
                                <td align="center" ondblclick="shuangji1('${salesMan.name}')">${salesMan.name}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="modal-footer">
                    <button id="close_salesMan2" type="button" class="btn btn-white" data-dismiss="modal">取消</button>
                    <button class="btn btn-primary" type="submit" id="end_SalesMan2" >确定</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--销售员列表-->
<div class="modal inmodal fade" id="salesMan_modal3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <label>选择销售员</label>
                </div>
                <div class="modal-body" style="overflow:scroll; height:400px;">
                    <table style="width: 100%; border: 1px solid #b1b1b1" border="1">
                        <c:forEach items="${salesMan}" var="salesMan">
                            <tr style="height: 50px">
                                <td align="center"><input type="radio" value="${salesMan.name}" id="${salesMan.name}" name="selectSalesMan3"></td>
                                <td align="center" ondblclick="shuangji1('${salesMan.name}')">${salesMan.name}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="modal-footer">
                    <button id="close_salesMan3" type="button" class="btn btn-white" data-dismiss="modal">取消</button>
                    <button class="btn btn-primary" type="submit" id="end_SalesMan3" >确定</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--销售员列表-->
<div class="modal inmodal fade" id="salesMan_modal4" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <label>选择销售员</label>
                </div>
                <div class="modal-body" style="overflow:scroll; height:400px;">
                    <table style="width: 100%; border: 1px solid #b1b1b1" border="1">
                        <c:forEach items="${salesMan}" var="salesMan">
                            <tr style="height: 50px">
                                <td align="center"><input type="radio" value="${salesMan.name}" id="${salesMan.name}" name="selectSalesMan4"></td>
                                <td align="center" ondblclick="shuangji1('${salesMan.name}')">${salesMan.name}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="modal-footer">
                    <button id="close_salesMan4" type="button" class="btn btn-white" data-dismiss="modal">取消</button>
                    <button class="btn btn-primary" type="submit" id="end_SalesMan4" >确定</button>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<!--选择日期方法1-->
<script>
    //外部js调用
    laydate({
        elem: '#payData1', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
        event: 'focus' //响应事件。如果没有传入event，则按照默认的click
    });
    laydate({
        elem: '#chengduiDate1', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
        event: 'focus' //响应事件。如果没有传入event，则按照默认的click
    });
</script>
<!--选择日期方法2-->
<script>
    //外部js调用
    laydate({
        elem: '#payData2', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
        event: 'focus' //响应事件。如果没有传入event，则按照默认的click
    });
</script>
<!--选择日期方法3-->
<script>
    //外部js调用
    laydate({
        elem: '#payData3', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
        event: 'focus' //响应事件。如果没有传入event，则按照默认的click
    });
</script>
<!--选择日期方法4-->
<script>
    //外部js调用
    laydate({
        elem: '#payData4', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
        event: 'focus' //响应事件。如果没有传入event，则按照默认的click
    });
</script>

<!--选择客户方法1-->
<script>
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "progressBar": false,
        "positionClass": "toast-top-center",
        "onclick": null,
        "showDuration": "400",
        "hideDuration": "1000",
        "timeOut": "2500",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    /*所选客户*/
    var customerName = "";
    /*打开客户面板*/
    $("a[id='chooseCustomer1']").click(function () {
        $("#customer_modal1").modal();
    });
    /*选择客户*/
    $("input[name='selectCustomer1']").click(function () {
        customerName = $(this).val();
    });
    /*确定所选客户*/
    $("button[id='customer_end1']").click(function () {
        var name = document.getElementById("goodsName1");
        name.value = customerName;
        queryCustomerArrery(customerName);
        $("#close_customer1").click();
        customerPaging1(1);
        customerName = "";
    });
    function selectCustomer(value) {
        customerName = value;
    }
    function dbClickCustomer(value) {
        customerName = value;
        $("#customer_end1").click();
    }
</script>
<!--选择客户方法2-->
<script>
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "progressBar": false,
        "positionClass": "toast-top-center",
        "onclick": null,
        "showDuration": "400",
        "hideDuration": "1000",
        "timeOut": "2500",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    /*所选客户*/
    var customerName = "";
    /*打开客户面板*/
    $("a[id='chooseCustomer2']").click(function () {
        $("#customer_modal2").modal();
    });
    /*选择客户*/
    $("input[name='selectCustomer2']").click(function () {
        customerName = $(this).val();
    });
    /*确定所选客户*/
    $("button[id='customer_end2']").click(function () {
        var name = document.getElementById("goodsName2");
        name.value = customerName;
        queryCustomerArrery(customerName);
        $("#close_customer2").click();
        customerPaging2(1);
        customerName = "";
    });
    function selectCustomer(value) {
        customerName = value;
    }
    function dbClickCustomer(value) {
        customerName = value;
        $("#customer_end2").click();
    }
</script>
<!--选择客户方法3-->
<script>
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "progressBar": false,
        "positionClass": "toast-top-center",
        "onclick": null,
        "showDuration": "400",
        "hideDuration": "1000",
        "timeOut": "2500",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    /*所选客户*/
    var customerName = "";
    /*打开客户面板*/
    $("a[id='chooseCustomer3']").click(function () {
        $("#customer_modal3").modal();
    });
    /*选择客户*/
    $("input[name='selectCustomer3']").click(function () {
        customerName = $(this).val();
    });
    /*确定所选客户*/
    $("button[id='customer_end3']").click(function () {
        var name = document.getElementById("goodsName3");
        name.value = customerName;
        queryCustomerArrery(customerName);
        customerPaging3(1);
        $("#close_customer3").click();
        customerPaging3(1);
        customerName = "";
    });
    function selectCustomer(value) {
        customerName = value;
    }
    function dbClickCustomer(value) {
        customerName = value;
        $("#customer_end3").click();
    }
</script>
<!--选择客户方法4-->
<script>
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "progressBar": false,
        "positionClass": "toast-top-center",
        "onclick": null,
        "showDuration": "400",
        "hideDuration": "1000",
        "timeOut": "2500",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    /*所选客户*/
    var customerName = "";
    /*打开客户面板*/
    $("a[id='chooseCustomer4']").click(function () {
        $("#customer_modal4").modal();
    });
    /*选择客户*/
    $("input[name='selectCustomer4']").click(function () {
        customerName = $(this).val();
    });
    /*确定所选客户*/
    $("button[id='customer_end4']").click(function () {
        var name = document.getElementById("goodsName4");
        name.value = customerName;
        queryCustomerArrery(customerName);
        $("#close_customer4").click();
        customerPaging1(1);
        customerName = "";

    });
    function selectCustomer(value) {
        customerName = value;
    }
    function dbClickCustomer(value) {
        customerName = value;
        $("#customer_end4").click();
    }
</script>

<!--选择销售人员方法1-->
<script>
    /*销售人员*/
    var salesMan = "";
    /*打开销售人员面板*/
    $("#chooseSalesMan1").click(function () {
        $("#salesMan_modal1").modal();
    });
    /*选择客户*/
    $("input[name='selectSalesMan1']").click(function () {
        salesMan = $(this).val();
    });
    /*确定所选销售员*/
    $("button[id='end_SalesMan1']").click(function () {
        var name = document.getElementById("salesMan1");
        name.value = salesMan;
        salesMan = "";
        $("#close_salesMan1").click();
    })
</script>
<!--选择销售人员方法2-->
<script>
    /*销售人员*/
    var salesMan = "";
    /*打开销售人员面板*/
    $("#chooseSalesMan2").click(function () {
        $("#salesMan_modal2").modal();
    });
    /*选择客户*/
    $("input[name='selectSalesMan2']").click(function () {
        salesMan = $(this).val();
    });
    /*确定所选销售员*/
    $("button[id='end_SalesMan2']").click(function () {
        var name = document.getElementById("salesMan2");
        name.value = salesMan;
        salesMan = "";
        $("#close_salesMan2").click();
    })
</script>
<!--选择销售人员方法3-->
<script>
    /*销售人员*/
    var salesMan = "";
    /*打开销售人员面板*/
    $("#chooseSalesMan3").click(function () {
        $("#salesMan_modal3").modal();
    });
    /*选择客户*/
    $("input[name='selectSalesMan3']").click(function () {
        salesMan = $(this).val();
    });
    /*确定所选销售员*/
    $("button[id='end_SalesMan3']").click(function () {
        var name = document.getElementById("salesMan3");
        name.value = salesMan;
        customer = "";
        $("#close_salesMan3").click();
    })
</script>
<!--选择销售人员方法4-->
<script>
    /*销售人员*/
    var salesMan = "";
    /*打开销售人员面板*/
    $("#chooseSalesMan4").click(function () {
        $("#salesMan_modal4").modal();
    });
    /*选择客户*/
    $("input[name='selectSalesMan4']").click(function () {
        salesMan = $(this).val();
    });
    /*确定所选销售员*/
    $("button[id='end_SalesMan4']").click(function () {
        var name = document.getElementById("salesMan4");
        name.value = salesMan;
        customer = "";
        $("#close_salesMan4").click();
    })
</script>

<!--读取销货单据信息1-->
<script>
    $("#chooseBill1").click(function () {
        var customer = $("#goodsName1").val();
        if (customer == "" || customer == null){
            $("#document_modal1").modal();
            $("#errpr1").text("请先选择客户");
        } else {
            $.ajax({
                type:"POST",
                url:"<%=path%>/order/querySalesOrderByCustomer?customer="+customer,
                dataType:"json",
                success:function(e){
                    if (e.result) {
                        $("#errpr1").text(e.msg);
                        var table = "<tr><td>操作</td><td align='center'>单据编号</td><td>需付金额</td></tr>";
                        for (var i=0; i<e.t.size; i++){
                            var info =""+
                                "<tr><td>" +
                                "<input align='center' type='checkbox' name='chooseBillNo1' value='"+e.t.orders[i].documentNumber+"@"+e.t.orders[i].money+"' id='"+i+"' aValue='"+e.t.orders[i].money+"'>" +
                                "</td><td align='center'>"+e.t.orders[i].documentNumber+"</td><td align='center'>"+e.t.orders[i].money+"</td></tr>";
                            $("#documentNo1").append(table+info);
                            $("#document_modal1").modal();
                        }
                    } else {
                        $("#errpr1").text(e.msg);
                        $("#document_modal1").modal();
                    }
                }
            });
        }
    });
</script>
<!--读取销货单据信息2-->
<script>
    $("#chooseBill2").click(function () {
        var customer = $("#goodsName2").val();
        if (customer == "" || customer == null){
            $("#document_modal2").modal();
            $("#errpr2").text("请先选择客户");
        } else {
            $.ajax({
                type:"POST",
                url:"<%=path%>/order/querySalesOrderByCustomer?customer="+customer,
                dataType:"json",
                success:function(e){
                    if (e.result) {
                        $("#errpr2").text(e.msg);
                        var table = "<tr><td>操作</td><td align='center'>单据编号</td><td>需付金额</td></tr>";
                        for (var i=0; i<e.t.size; i++){
                            var info =""+
                                "<tr><td>" +
                                "<input align='center' type='checkbox' name='chooseBillNo2' value='"+e.t.orders[i].documentNumber+"@"+e.t.orders[i].money+"' id='"+i+"' aValue='"+e.t.orders[i].money+"'>" +
                                "</td><td align='center'>"+e.t.orders[i].documentNumber+"</td><td align='center'>"+e.t.orders[i].money+"</td></tr>";
                            $("#documentNo2").append(table+info);
                            $("#document_modal2").modal();
                        }
                    } else {
                        $("#errpr2").text(e.msg);
                        $("#document_modal2").modal();
                    }
                }
            });
        }
    });
</script>
<!--读取销货单据信息3-->
<script>
    $("#chooseBill3").click(function () {
        var customer = $("#goodsName3").val();
        if (customer == "" || customer == null){
            $("#document_modal3").modal();
            $("#errpr3").text("请先选择客户");
        } else {
            $.ajax({
                type:"POST",
                url:"<%=path%>/order/querySalesOrderByCustomer?customer="+customer,
                dataType:"json",
                success:function(e){
                    if (e.result) {
                        $("#errpr3").text(e.msg);
                        var table = "<tr><td>操作</td><td align='center'>单据编号</td><td>需付金额</td></tr>";
                        for (var i=0; i<e.t.size; i++){
                            var info =""+
                                "<tr><td>" +
                                "<input align='center' type='checkbox' name='chooseBillNo3' value='"+e.t.orders[i].documentNumber+"@"+e.t.orders[i].money+"' id='"+i+"' aValue='"+e.t.orders[i].money+"'>" +
                                "</td><td align='center'>"+e.t.orders[i].documentNumber+"</td><td align='center'>"+e.t.orders[i].money+"</td></tr>";
                            $("#documentNo3").append(table+info);
                            $("#document_modal3").modal();
                        }
                    } else {
                        $("#errpr3").text(e.msg);
                        $("#document_modal3").modal();
                    }
                }
            });
        }
    });
</script>
<!--读取销货单据信息4-->
<script>
    $("#chooseBill4").click(function () {
        var customer = $("#goodsName4").val();
        if (customer == "" || customer == null){
            $("#document_modal4").modal();
            $("#errpr4").text("请先选择客户");
        } else {
            $.ajax({
                type:"POST",
                url:"<%=path%>/order/querySalesOrderByCustomer?customer="+customer,
                dataType:"json",
                success:function(e){
                    if (e.result) {
                        $("#errpr4").text(e.msg);
                        var table = "<tr><td>操作</td><td align='center'>单据编号</td><td>需付金额</td></tr>";
                        for (var i=0; i<e.t.size; i++){
                            var info =""+
                                "<tr><td>" +
                                "<input align='center' type='checkbox' name='chooseBillNo4' value='"+e.t.orders[i].documentNumber+"@"+e.t.orders[i].money+"' id='"+i+"' aValue='"+e.t.orders[i].money+"'>" +
                                "</td><td align='center'>"+e.t.orders[i].documentNumber+"</td><td align='center'>"+e.t.orders[i].money+"</td></tr>";
                            $("#documentNo4").append(table+info);
                            $("#document_modal4").modal();
                        }
                    } else {
                        $("#errpr4").text(e.msg);
                        $("#document_modal4").modal();
                    }
                }
            });
        }
    });
</script>

<!--选择销货单1-->
<script>
    var billNos = new Array;
    var moneys = new Array;
    $("#end1").click(function () {
        //获取所选信息
        var chooseBills = document.getElementsByName("chooseBillNo1");
        for (var i=0; i<chooseBills.length; i++){
            if (chooseBills[i].checked) {
                billNos.push(chooseBills[i].value.split("@")[0]);
                moneys.push(chooseBills[i].value.split("@")[1])
            }
        }
        //关闭面板
        $("#close_document1").click();
        var tr = "";
        for(var i=0; i<billNos.length; i++){
            tr += ""+
                "<tr><td><input type='text'value='"+billNos[i]+"' name='billNos1'></td><td>"+moneys[i]+"</td></tr>";
        }
        $("#table1").append(tr);
        billNos.splice(0,billNos.length);
        $("#documentNo1").html("");
        billNos = [];
        moneys = [];
    })
</script>
<!--选择销货单2-->
<script>
    var billNos = new Array;
    var moneys = new Array;
    $("#end2").click(function () {
        //获取所选信息
        var chooseBills = document.getElementsByName("chooseBillNo2");
        for (var i=0; i<chooseBills.length; i++){
            if (chooseBills[i].checked) {
                billNos.push(chooseBills[i].value.split("@")[0]);
                moneys.push(chooseBills[i].value.split("@")[1])
            }
        }
        //关闭面板
        $("#close_document2").click();
        var tr = "";
        for(var i=0; i<billNos.length; i++){
            tr += ""+
                "<tr><td><input type='text'value='"+billNos[i]+"' name='billNos2'></td><td>"+moneys[i]+"</td></tr>";
        }
        $("#table2").append(tr);
        billNos.splice(0,billNos.length);
        $("#documentNo2").html("");
        billNos = [];
        moneys = [];
    })
</script>
<!--选择销货单3-->
<script>
    var billNos = new Array;
    var moneys = new Array;
    $("#end3").click(function () {
        //获取所选信息
        var chooseBills = document.getElementsByName("chooseBillNo3");
        for (var i=0; i<chooseBills.length; i++){
            if (chooseBills[i].checked) {
                billNos.push(chooseBills[i].value.split("@")[0]);
                moneys.push(chooseBills[i].value.split("@")[1])
            }
        }
        //关闭面板
        $("#close_document3").click();
        var tr = "";
        for(var i=0; i<billNos.length; i++){
            tr += ""+
                "<tr><td><input type='text'value='"+billNos[i]+"' name='billNos3'></td><td>"+moneys[i]+"</td></tr>";
        }
        $("#table3").append(tr);
        billNos.splice(0,billNos.length);
        $("#documentNo3").html("");
        billNos = [];
        moneys = [];
    })
</script>
<!--选择销货单4-->
<script>
    var billNos = new Array;
    var moneys = new Array;
    $("#end4").click(function () {
        //获取所选信息
        var chooseBills = document.getElementsByName("chooseBillNo4");
        for (var i=0; i<chooseBills.length; i++){
            if (chooseBills[i].checked) {
                billNos.push(chooseBills[i].value.split("@")[0]);
                moneys.push(chooseBills[i].value.split("@")[1])
            }
        }
        //关闭面板
        $("#close_document4").click();
        var tr = "";
        for(var i=0; i<billNos.length; i++){
            tr += ""+
                "<tr><td><input type='text'value='"+billNos[i]+"' name='billNos4'></td><td>"+moneys[i]+"</td></tr>";
        }
        $("#table4").append(tr);
        billNos.splice(0,billNos.length);
        $("#documentNo4").html("");
        billNos = [];
        moneys = [];
    })
</script>

<!--提交非空验证1(承兑)-->
<script>
    function send1 () {
        var billNo = document.getElementById("billNo1");
        var price = document.getElementById("price1");
        var goodsName = document.getElementById("goodsName1");
        var salesMan = document.getElementsByName("salesMan1");
        if (billNo.value == "") {
            toastr.error("单据编号不能为空", '操作提示');
            billNo.focus();
        } else if (isNaN(price.value) || price.value<0){
            toastr.error("付款金额必须为大于0的数字", '操作提示');
        } else if (goodsName.value == ""){
            toastr.error("付款客户不能为空", '操作提示');
            goodsName.focus();
        } else if (salesMan.value == ""){
            toastr.error("业务员不可为空，请选择", '操作提示');
            goodsName.focus();
        } else {
            $.ajax({
                type:"POST",
                url:"<%=path%>/order/queryBillNo?billNo="+billNo.value,
                dataType:"json",
                success:function(e){
                    if (e.result){
                        $.ajax({
                            type:"POST",
                            url:"<%=path%>/order/addCashOrder1",
                            data: $('#form1').serialize(),
                            dataType:"json",
                            success:function(result){
                                if (result.result){
                                    swal({
                                        title: "成功!!",
                                        text: result.msg,
                                        type: "success",
                                        confirmButtonText: "确定",
                                        closeOnConfirm: false
                                    }, function () {
                                        top.location="<%=path%>/index/toReceivableList";
                                    });
                                } else {
                                    swal({
                                        title: "失败!!",
                                        text: result.msg,
                                        type: "warning",
                                        confirmButtonText: "确定",
                                        closeOnConfirm: false
                                    }, function () {
                                        window.location.reload();
                                    });
                                }
                            }
                        });
                    } else {
                        swal({
                            title: "失败!!",
                            text: e.msg,
                            type: "warning",
                            confirmButtonText: "确定",
                            closeOnConfirm: false
                        }, function () {
                            window.location.reload();
                        });
                    }
                }
            });
        }
    }
</script>
<!--提交非空验证2(电汇)-->
<script>
    function send2 () {
        var billNo = document.getElementById("billNo2");
        var price = document.getElementById("price2");
        var goodsName = document.getElementById("goodsName2");
        var salesMan = document.getElementsByName("salesMan2");
        if (billNo.value == "") {
            toastr.error("单据编号不能为空", '操作提示');
            billNo.focus();
        } else if (isNaN(price.value) || price.value<0){
            toastr.error("付款金额必须为大于0的数字", '操作提示');
        } else if (goodsName.value == ""){
            toastr.error("付款客户不能为空", '操作提示');
            goodsName.focus();
        } else if (salesMan.value == ""){
            toastr.error("业务员不可为空，请选择", '操作提示');
            goodsName.focus();
        } else {
            $.ajax({
                type:"POST",
                url:"<%=path%>/order/queryBillNo?billNo="+billNo.value,
                dataType:"json",
                success:function(e){
                    if (e.result){
                        $.ajax({
                            type:"POST",
                            url:"<%=path%>/order/addCashOrder2",
                            data: $('#form2').serialize(),
                            dataType:"json",
                            success:function(result){
                                if (result.result){
                                    swal({
                                        title: "成功!!",
                                        text: result.msg,
                                        type: "success",
                                        confirmButtonText: "确定",
                                        closeOnConfirm: false
                                    }, function () {
                                        top.location="<%=path%>/index/toReceivableList";
                                    });
                                } else {
                                    swal({
                                        title: "失败!!",
                                        text: result.msg,
                                        type: "warning",
                                        confirmButtonText: "确定",
                                        closeOnConfirm: false
                                    }, function () {
                                        window.location.reload();
                                    });
                                }
                            }
                        });
                    } else {
                        swal({
                            title: "失败!!",
                            text: e.msg,
                            type: "warning",
                            confirmButtonText: "确定",
                            closeOnConfirm: false
                        }, function () {
                            window.location.reload();
                        });
                    }
                }
            });
        }
    }
</script>
<!--提交非空验证3(转账)-->
<script>
    function send3 () {
        var billNo = document.getElementById("billNo4");
        var price = document.getElementById("price4");
        var goodsName = document.getElementById("goodsName4");
        var salesMan = document.getElementsByName("salesMan4");
        if (billNo.value == "") {
            toastr.error("单据编号不能为空", '操作提示');
            billNo.focus();
        } else if (isNaN(price.value) || price.value<0){
            toastr.error("付款金额必须为大于0的数字", '操作提示');
        } else if (goodsName.value == ""){
            toastr.error("付款客户不能为空", '操作提示');
            goodsName.focus();
        } else if (salesMan.value == ""){
            toastr.error("业务员不可为空，请选择", '操作提示');
            goodsName.focus();
        } else {
            $.ajax({
                type:"POST",
                url:"<%=path%>/order/queryBillNo?billNo="+billNo.value,
                dataType:"json",
                success:function(e){
                    if (e.result){
                        $.ajax({
                            type:"POST",
                            url:"<%=path%>/order/addCashOrder3",
                            data: $('#form3').serialize(),
                            dataType:"json",
                            success:function(result){
                                if (result.result){
                                    swal({
                                        title: "成功!!",
                                        text: result.msg,
                                        type: "success",
                                        confirmButtonText: "确定",
                                        closeOnConfirm: false
                                    }, function () {
                                        top.location="<%=path%>/index/toReceivableList";
                                    });
                                } else {
                                    swal({
                                        title: "失败!!",
                                        text: result.msg,
                                        type: "warning",
                                        confirmButtonText: "确定",
                                        closeOnConfirm: false
                                    }, function () {
                                        window.location.reload();
                                    });
                                }
                            }
                        });
                    } else {
                        swal({
                            title: "失败!!",
                            text: e.msg,
                            type: "warning",
                            confirmButtonText: "确定",
                            closeOnConfirm: false
                        }, function () {
                            window.location.reload();
                        });
                    }
                }
            });
        }
    }
</script>
<!--提交非空验证4(现金)-->
<script>
    function send4 () {
        var billNo = document.getElementById("billNo4");
        var price = document.getElementById("price4");
        var goodsName = document.getElementById("goodsName4");
        var salesMan = document.getElementsByName("salesMan4");
        if (billNo.value == "") {
            toastr.error("单据编号不能为空", '操作提示');
            billNo.focus();
        } else if (isNaN(price.value) || price.value<0){
            toastr.error("付款金额必须为大于0的数字", '操作提示');
        } else if (goodsName.value == ""){
            toastr.error("付款客户不能为空", '操作提示');
            goodsName.focus();
        } else if (salesMan.value == ""){
            toastr.error("业务员不可为空，请选择", '操作提示');
            goodsName.focus();
        } else {
            $.ajax({
                type:"POST",
                url:"<%=path%>/order/queryBillNo?billNo="+billNo.value,
                dataType:"json",
                success:function(e){
                    if (e.result){
                        $.ajax({
                            type:"POST",
                            url:"<%=path%>/order/addCashOrder4",
                            data: $('#form4').serialize(),
                            dataType:"json",
                            success:function(result){
                                if (result.result){
                                    swal({
                                        title: "成功!!",
                                        text: result.msg,
                                        type: "success",
                                        confirmButtonText: "确定",
                                        closeOnConfirm: false
                                    }, function () {
                                        top.location="<%=path%>/index/toReceivableList";
                                    });
                                } else {
                                    swal({
                                        title: "失败!!",
                                        text: result.msg,
                                        type: "warning",
                                        confirmButtonText: "确定",
                                        closeOnConfirm: false
                                    }, function () {
                                        window.location.reload();
                                    });
                                }
                            }
                        });
                    } else {
                        swal({
                            title: "失败!!",
                            text: e.msg,
                            type: "warning",
                            confirmButtonText: "确定",
                            closeOnConfirm: false
                        }, function () {
                            window.location.reload();
                        });
                    }
                }
            });
        }
    }
</script>

<!--取消按钮清空表格1-->
<script>
    $("#close_document1").click(function () {
        $("#documentNo1").html("");
    })
</script>
<!--取消按钮清空表格2-->
<script>
    $("#close_document2").click(function () {
        $("#documentNo2").html("");
    })
</script>
<!--取消按钮清空表格3-->
<script>
    $("#close_document3").click(function () {
        $("#documentNo3").html("");
    })
</script>
<!--取消按钮清空表格4-->
<script>
    $("#close_document4").click(function () {
        $("#documentNo4").html("");
    })
</script>

<!--双击-->
<script>
    //销售员双击
    function shuangji1(value) {
        var radio  = document.getElementById(value);
        radio.click();
        $("#end_SalesMan").click();
    }
</script>
<!--模糊查询客户-->
<script>
    function selectCustomerByName1(value) {
        $.ajax({
            type:"POST",
            url:"<%=path%>/sales/selectCustomerByName?name="+value,
            dataType:"json",
            success:function(e){
                if (e.result){
                    var table = "" +
                        "<table style='width: 100%; border: 1px solid #b1b1b1' border='1'>";
                    for (var i=0; i<e.t.customerRows.length; i++){
                        var newTable = "" +
                            "<tr style='height: 50px'>"+
                            "<td align='center'><input type='radio' value='"+e.t.customerRows[i].name1+"' name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name1+"'></td>"+
                            "<td align='center' ondblclick='dbClickCustomer(this)'>"+e.t.customerRows[i].name1+"</td>"+
                            "<td align='center'><input type='radio' value="+e.t.customerRows[i].name2+" name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name2+"'></td>"+
                            "<td align='center' ondblclick='dbClickCustomer(this)'>"+e.t.customerRows[i].name2+"</td>"+
                            "<td align='center'><input type='radio' value="+e.t.customerRows[i].name3+" name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name3+"'></td>"+
                            "<td align='center' ondblclick='dbClickCustomer(this)'>"+e.t.customerRows[i].name3+"</td>"+
                            "</tr>";
                        table = table+newTable;
                    }
                    table+="</table>";
                    var page = "" +
                        "<input type='button' value='首页' onclick='customerPaging1(1)'>"+
                        "<input type='button' value='上一页' onclick='customerPaging1("+(e.t.currentPage-1)+")'>"+
                        "<input type='button' value='"+(e.t.currentPage-1)+"' onclick='customerPaging1("+(e.t.currentPage-1)+")' style='width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage)+"' onclick='customerPaging1("+(e.t.currentPage)+")' style='background-color:deepskyblue; width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage+1)+"' onclick='customerPaging1("+(e.t.currentPage+1)+")' style='width: 20px'>"+
                        "<input type='button' value='下一页' onclick='customerPaging1("+(e.t.currentPage+1)+")'>"+
                        "<input type='button' value='尾页' onclick='customerPaging1("+e.t.pageCount+")'>"
                    $("#customerTable1").html(table);
                    $("#page1").html(page);
                } else {

                }
            }
        });
    }
    function selectCustomerByName2(value) {
        $.ajax({
            type:"POST",
            url:"<%=path%>/sales/selectCustomerByName?name="+value,
            dataType:"json",
            success:function(e){
                if (e.result){
                    var table = "" +
                        "<table style='width: 100%; border: 1px solid #b1b1b1' border='1'>";
                    for (var i=0; i<e.t.customerRows.length; i++){
                        var newTable = "" +
                            "<tr style='height: 50px'>"+
                            "<td align='center'><input type='radio' value='"+e.t.customerRows[i].name1+"' name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name1+"'></td>"+
                            "<td align='center' ondblclick='dbClickCustomer(this)'>"+e.t.customerRows[i].name1+"</td>"+
                            "<td align='center'><input type='radio' value="+e.t.customerRows[i].name2+" name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name2+"'></td>"+
                            "<td align='center' ondblclick='dbClickCustomer(this)'>"+e.t.customerRows[i].name2+"</td>"+
                            "<td align='center'><input type='radio' value="+e.t.customerRows[i].name3+" name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name3+"'></td>"+
                            "<td align='center' ondblclick='dbClickCustomer(this)'>"+e.t.customerRows[i].name3+"</td>"+
                            "</tr>";
                        table = table+newTable;
                    }
                    table+="</table>";
                    var page = "" +
                        "<input type='button' value='首页' onclick='customerPaging2(1)'>"+
                        "<input type='button' value='上一页' onclick='customerPaging2("+(e.t.currentPage-1)+")'>"+
                        "<input type='button' value='"+(e.t.currentPage-1)+"' onclick='customerPaging2("+(e.t.currentPage-1)+")' style='width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage)+"' onclick='customerPaging2("+(e.t.currentPage)+")' style='background-color:deepskyblue; width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage+1)+"' onclick='customerPaging2("+(e.t.currentPage+1)+")' style='width: 20px'>"+
                        "<input type='button' value='下一页' onclick='customerPaging2("+(e.t.currentPage+1)+")'>"+
                        "<input type='button' value='尾页' onclick='customerPaging2("+e.t.pageCount+")'>"
                    $("#customerTable2").html(table);
                    $("#page2").html(page);
                } else {

                }
            }
        });
    }
    function selectCustomerByName3(value) {
        $.ajax({
            type:"POST",
            url:"<%=path%>/sales/selectCustomerByName?name="+value,
            dataType:"json",
            success:function(e){
                if (e.result){
                    var table = "" +
                        "<table style='width: 100%; border: 1px solid #b1b1b1' border='1'>";
                    for (var i=0; i<e.t.customerRows.length; i++){
                        var newTable = "" +
                            "<tr style='height: 50px'>"+
                            "<td align='center'><input type='radio' value='"+e.t.customerRows[i].name1+"' name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name1+"'></td>"+
                            "<td align='center' ondblclick='dbClickCustomer(this)'>"+e.t.customerRows[i].name1+"</td>"+
                            "<td align='center'><input type='radio' value="+e.t.customerRows[i].name2+" name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name2+"'></td>"+
                            "<td align='center' ondblclick='dbClickCustomer(this)'>"+e.t.customerRows[i].name2+"</td>"+
                            "<td align='center'><input type='radio' value="+e.t.customerRows[i].name3+" name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name3+"'></td>"+
                            "<td align='center' ondblclick='dbClickCustomer(this)'>"+e.t.customerRows[i].name3+"</td>"+
                            "</tr>";
                        table = table+newTable;
                    }
                    table+="</table>";
                    var page = "" +
                        "<input type='button' value='首页' onclick='customerPaging3(1)'>"+
                        "<input type='button' value='上一页' onclick='customerPaging3("+(e.t.currentPage-1)+")'>"+
                        "<input type='button' value='"+(e.t.currentPage-1)+"' onclick='customerPaging3("+(e.t.currentPage-1)+")' style='width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage)+"' onclick='customerPaging3("+(e.t.currentPage)+")' style='background-color:deepskyblue; width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage+1)+"' onclick='customerPaging3("+(e.t.currentPage+1)+")' style='width: 20px'>"+
                        "<input type='button' value='下一页' onclick='customerPaging3("+(e.t.currentPage+1)+")'>"+
                        "<input type='button' value='尾页' onclick='customerPaging3("+e.t.pageCount+")'>"
                    $("#customerTable3").html(table);
                    $("#page3").html(page);
                } else {

                }
            }
        });
    }
    function selectCustomerByName4(value) {
        $.ajax({
            type:"POST",
            url:"<%=path%>/sales/selectCustomerByName?name="+value,
            dataType:"json",
            success:function(e){
                if (e.result){
                    var table = "" +
                        "<table style='width: 100%; border: 1px solid #b1b1b1' border='1'>";
                    for (var i=0; i<e.t.customerRows.length; i++){
                        var newTable = "" +
                            "<tr style='height: 50px'>"+
                            "<td align='center'><input type='radio' value='"+e.t.customerRows[i].name1+"' name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name1+"'></td>"+
                            "<td align='center' ondblclick='dbClickCustomer(this)'>"+e.t.customerRows[i].name1+"</td>"+
                            "<td align='center'><input type='radio' value="+e.t.customerRows[i].name2+" name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name2+"'></td>"+
                            "<td align='center' ondblclick='dbClickCustomer(this)'>"+e.t.customerRows[i].name2+"</td>"+
                            "<td align='center'><input type='radio' value="+e.t.customerRows[i].name3+" name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name3+"'></td>"+
                            "<td align='center' ondblclick='dbClickCustomer(this)'>"+e.t.customerRows[i].name3+"</td>"+
                            "</tr>";
                        table = table+newTable;
                    }
                    table+="</table>";
                    var page = "" +
                        "<input type='button' value='首页' onclick='customerPaging4(1)'>"+
                        "<input type='button' value='上一页' onclick='customerPaging4("+(e.t.currentPage-1)+")'>"+
                        "<input type='button' value='"+(e.t.currentPage-1)+"' onclick='customerPaging4("+(e.t.currentPage-1)+")' style='width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage)+"' onclick='customerPaging4("+(e.t.currentPage)+")' style='background-color:deepskyblue; width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage+1)+"' onclick='customerPaging4("+(e.t.currentPage+1)+")' style='width: 20px'>"+
                        "<input type='button' value='下一页' onclick='customerPaging4("+(e.t.currentPage+1)+")'>"+
                        "<input type='button' value='尾页' onclick='customerPaging4("+e.t.pageCount+")'>"
                    $("#customerTable4").html(table);
                    $("#page4").html(page);
                } else {

                }
            }
        });
    }
</script>
<!--客户分页-->
<script>
    function customerPaging1(page) {
        var name = document.getElementById("selectCustomerName1").value;
        $.ajax({
            type:"POST",
            url:"<%=path%>/sales/selectCustomerByName?page="+page+"&name="+name,
            dataType:"json",
            success:function(e){
                if (e.result){
                    var table = "" +
                        "<table style='width: 100%; border: 1px solid #b1b1b1' border='1'>";
                    for (var i=0; i<e.t.customerRows.length; i++){
                        var newTable = "" +
                            "<tr style='height: 50px'>"+
                            "<td align='center'><input type='radio' value='"+e.t.customerRows[i].name1+"' name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name1+"'></td>"+
                            "<td align='center' ondblclick='dbClickCustomer(\""+e.t.customerRows[i].name1+"\")'>"+e.t.customerRows[i].name1+"</td>"+
                            "<td align='center'><input type='radio' value="+e.t.customerRows[i].name2+" name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name2+"'></td>"+
                            "<td align='center' ondblclick='dbClickCustomer(\""+e.t.customerRows[i].name2+"\")'>"+e.t.customerRows[i].name2+"</td>"+
                            "<td align='center'><input type='radio' value="+e.t.customerRows[i].name3+" name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name3+"'></td>"+
                            "<td align='center' ondblclick='dbClickCustomer(\""+e.t.customerRows[i].name3+"\")'>"+e.t.customerRows[i].name3+"</td>"+
                            "</tr>";
                        table = table+newTable;
                    }
                    table+="</table>";
                    var page = "" +
                        "<input type='button' value='首页' onclick='customerPaging1(1)'>"+
                        "<input type='button' value='上一页' onclick='customerPaging1("+(e.t.currentPage-1)+")'>"+
                        "<input type='button' value='"+(e.t.currentPage-1)+"' onclick='customerPaging1("+(e.t.currentPage-1)+")' style='width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage)+"' onclick='customerPaging1("+(e.t.currentPage)+")' style='background-color:deepskyblue; width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage+1)+"' onclick='customerPaging1("+(e.t.currentPage+1)+")' style='width: 20px'>"+
                        "<input type='button' value='下一页' onclick='customerPaging1("+(e.t.currentPage+1)+")'>"+
                        "<input type='button' value='尾页' onclick='customerPaging1("+e.t.pageCount+")'>"
                    $("#customerTable1").html(table);
                    $("#page1").html(page);
                } else {
                }
            }
        });
    }
    function customerPaging2(page) {
        var name = document.getElementById("selectCustomerName2").value;
        $.ajax({
            type:"POST",
            url:"<%=path%>/sales/selectCustomerByName?page="+page+"&name="+name,
            dataType:"json",
            success:function(e){
                if (e.result){
                    var table = "" +
                        "<table style='width: 100%; border: 1px solid #b1b1b1' border='1'>";
                    for (var i=0; i<e.t.customerRows.length; i++){
                        var newTable = "" +
                            "<tr style='height: 50px'>"+
                            "<td align='center'><input type='radio' value='"+e.t.customerRows[i].name1+"' name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name1+"'></td>"+
                            "<td align='center' ondblclick='dbClickCustomer(\""+e.t.customerRows[i].name1+"\")'>"+e.t.customerRows[i].name1+"</td>"+
                            "<td align='center'><input type='radio' value="+e.t.customerRows[i].name2+" name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name2+"'></td>"+
                            "<td align='center' ondblclick='dbClickCustomer(\""+e.t.customerRows[i].name2+"\")'>"+e.t.customerRows[i].name2+"</td>"+
                            "<td align='center'><input type='radio' value="+e.t.customerRows[i].name3+" name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name3+"'></td>"+
                            "<td align='center' ondblclick='dbClickCustomer(\""+e.t.customerRows[i].name3+"\")'>"+e.t.customerRows[i].name3+"</td>"+
                            "</tr>";
                        table = table+newTable;
                    }
                    table+="</table>";
                    var page = "" +
                        "<input type='button' value='首页' onclick='customerPaging2(1)'>"+
                        "<input type='button' value='上一页' onclick='customerPaging2("+(e.t.currentPage-1)+")'>"+
                        "<input type='button' value='"+(e.t.currentPage-1)+"' onclick='customerPaging2("+(e.t.currentPage-1)+")' style='width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage)+"' onclick='customerPaging2("+(e.t.currentPage)+")' style='background-color:deepskyblue; width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage+1)+"' onclick='customerPaging2("+(e.t.currentPage+1)+")' style='width: 20px'>"+
                        "<input type='button' value='下一页' onclick='customerPaging2("+(e.t.currentPage+1)+")'>"+
                        "<input type='button' value='尾页' onclick='customerPaging2("+e.t.pageCount+")'>"
                    $("#customerTable2").html(table);
                    $("#page2").html(page);
                } else {
                }
            }
        });
    }
    function customerPaging3(page) {
        var name = document.getElementById("selectCustomerName3").value;
        $.ajax({
            type:"POST",
            url:"<%=path%>/sales/selectCustomerByName?page="+page+"&name="+name,
            dataType:"json",
            success:function(e){
                if (e.result){
                    var table = "" +
                        "<table style='width: 100%; border: 1px solid #b1b1b1' border='1'>";
                    for (var i=0; i<e.t.customerRows.length; i++){
                        var newTable = "" +
                            "<tr style='height: 50px'>"+
                            "<td align='center'><input type='radio' value='"+e.t.customerRows[i].name1+"' name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name1+"'></td>"+
                            "<td align='center' ondblclick='dbClickCustomer(\""+e.t.customerRows[i].name1+"\")'>"+e.t.customerRows[i].name1+"</td>"+
                            "<td align='center'><input type='radio' value="+e.t.customerRows[i].name2+" name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name2+"'></td>"+
                            "<td align='center' ondblclick='dbClickCustomer(\""+e.t.customerRows[i].name2+"\")'>"+e.t.customerRows[i].name2+"</td>"+
                            "<td align='center'><input type='radio' value="+e.t.customerRows[i].name3+" name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name3+"'></td>"+
                            "<td align='center' ondblclick='dbClickCustomer(\""+e.t.customerRows[i].name3+"\")'>"+e.t.customerRows[i].name3+"</td>"+
                            "</tr>";
                        table = table+newTable;
                    }
                    table+="</table>";
                    var page = "" +
                        "<input type='button' value='首页' onclick='customerPaging3(1)'>"+
                        "<input type='button' value='上一页' onclick='customerPaging3("+(e.t.currentPage-1)+")'>"+
                        "<input type='button' value='"+(e.t.currentPage-1)+"' onclick='customerPaging3("+(e.t.currentPage-1)+")' style='width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage)+"' onclick='customerPaging3("+(e.t.currentPage)+")' style='background-color:deepskyblue; width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage+1)+"' onclick='customerPaging3("+(e.t.currentPage+1)+")' style='width: 20px'>"+
                        "<input type='button' value='下一页' onclick='customerPaging3("+(e.t.currentPage+1)+")'>"+
                        "<input type='button' value='尾页' onclick='customerPaging3("+e.t.pageCount+")'>"
                    $("#customerTable3").html(table);
                    $("#page3").html(page);
                } else {
                }
            }
        });
    }
    function customerPaging4(page) {
        var name = document.getElementById("selectCustomerName4").value;
        $.ajax({
            type:"POST",
            url:"<%=path%>/sales/selectCustomerByName?page="+page+"&name="+name,
            dataType:"json",
            success:function(e){
                if (e.result){
                    var table = "" +
                        "<table style='width: 100%; border: 1px solid #b1b1b1' border='1'>";
                    for (var i=0; i<e.t.customerRows.length; i++){
                        var newTable = "" +
                            "<tr style='height: 50px'>"+
                            "<td align='center'><input type='radio' value='"+e.t.customerRows[i].name1+"' name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name1+"'></td>"+
                            "<td align='center' ondblclick='dbClickCustomer(\""+e.t.customerRows[i].name1+"\")'>"+e.t.customerRows[i].name1+"</td>"+
                            "<td align='center'><input type='radio' value="+e.t.customerRows[i].name2+" name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name2+"'></td>"+
                            "<td align='center' ondblclick='dbClickCustomer(\""+e.t.customerRows[i].name2+"\")'>"+e.t.customerRows[i].name2+"</td>"+
                            "<td align='center'><input type='radio' value="+e.t.customerRows[i].name3+" name='selectCustomer' onfocus='selectCustomer(this.value)' id='"+e.t.customerRows[i].name3+"'></td>"+
                            "<td align='center' ondblclick='dbClickCustomer(\""+e.t.customerRows[i].name3+"\")'>"+e.t.customerRows[i].name3+"</td>"+
                            "</tr>";
                        table = table+newTable;
                    }
                    table+="</table>";
                    var page = "" +
                        "<input type='button' value='首页' onclick='customerPaging4(1)'>"+
                        "<input type='button' value='上一页' onclick='customerPaging4("+(e.t.currentPage-1)+")'>"+
                        "<input type='button' value='"+(e.t.currentPage-1)+"' onclick='customerPaging4("+(e.t.currentPage-1)+")' style='width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage)+"' onclick='customerPaging4("+(e.t.currentPage)+")' style='background-color:deepskyblue; width: 20px'>"+
                        "<input type='button' value='"+(e.t.currentPage+1)+"' onclick='customerPaging4("+(e.t.currentPage+1)+")' style='width: 20px'>"+
                        "<input type='button' value='下一页' onclick='customerPaging4("+(e.t.currentPage+1)+")'>"+
                        "<input type='button' value='尾页' onclick='customerPaging4("+e.t.pageCount+")'>"
                    $("#customerTable4").html(table);
                    $("#page4").html(page);
                } else {
                }
            }
        });
    }
</script>
<!--查看当前客户是否有未付清的销货单-->
<script>
    function queryCustomerArrery(val) {
        $.ajax({
            type:"POST",
            url:"<%=path%>/order/querySalesOrderByCustomer?customer="+val,
            dataType:"json",
            success:function (e) {
                if (e.result){
                    //有单据
                    toastr.success(e.msg,"操作提示");
                } else {
                    //无单据
                    toastr.error(e.msg,"操作提示");
                }
            }
        })
    }
</script>
</html>