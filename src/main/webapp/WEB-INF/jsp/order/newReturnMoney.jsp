<%--
  Created by IntelliJ IDEA.
  User: xuzhongyao
  Date: 2017/1/17
  Time: 上午10:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>退款单录入</title>
    <meta name="keywords" content="科研项目管理">
    <meta name="description" content="科研项目管理">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE">
    <link href="<%=path%>/statics2/saas/scm/app2_release/css/common.css?ver=20150522" rel="stylesheet" type="text/css">
    <link href="<%=path%>/statics2/saas/scm/app2_release/css/green/ui.min.css?ver=20150522" rel="stylesheet">
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/jquery/jquery-1.10.2.min.js"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/json3.min.js"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/common.js?ver=20150522"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/grid.js?ver=20150522"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/plugins.js?ver=20150522"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/plugins/jquery.dialog.js?self=true&ver=20150522"></script>
    <script src="<%=path%>/static/hplus/js/plugins/layer/laydate/laydate.js"></script>
    <link href="<%=path%>/statics2/css/green/bills.css?ver=20150522" rel="stylesheet" type="text/css">
    <link href="<%=path%>/static/hplus/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/animate.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/style.css?v=4.1.0" rel="stylesheet">
</head>
<link href="<%=path%>/statics2/css/green/bills.css?ver=20150522" rel="stylesheet" type="text/css">
<style>
    #barCodeInsert{margin-left: 10px;font-weight: 100;font-size: 12px;color: #fff;background-color: #B1B1B1;padding: 0 5px;border-radius: 2px;line-height: 19px;height: 20px;display: inline-block;}
    #barCodeInsert.active{background-color: #23B317;}
</style>
</head>
<body>
<div class="wrapper">
    <span id="config" class="ui-icon ui-state-default ui-icon-config"></span>
    <div class="mod-toolbar-top mr0 cf dn" id="toolTop"></div>
    <div class="bills cf">
        <div class="con-header">
            <div class="row-item" style="text-align: center ">
                <h1 style="color: red">唐山军荣铝业有限公司退款单</h1>
            </div>
            <div style="height: 30px;"></div>
            <div style="height: 30px; background-color: slategray"></div>
            <div style="height: 30px;"></div>
            <form action="<%=path%>/operation/addReturnMoney" method="post" id="form">
                <div style="height: 500px; border: 1px black solid;">
                    <table align="center" style="margin-top: 50px; height: 300px; border-color: red" border="1" cellspacing="0">
                        <tr>
                            <td>
                                <table align="center" style="height: 100%">
                                    <tr style="height: 60px">
                                        <td><h2 style="color: red">创建时间：</h2></td>
                                        <td>
                                            <input type="text" id="date" class="ui-input ui-datepicker-input" autocomplete="off" name="creataData" style="border-color: red;" border="1">
                                        </td>
                                        <td width="400"></td>
                                        <td><h2 style="color: red">单据编号：</h2></td>
                                        <td>
                                            <input type="text" height="60" style="border-left: 0; border-top: 0; border-right: 0; border-bottom: 1px red solid;" name="billNo" id="billNo" value="${num}">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" >
                                            <table align="center" style="width: 95%; height: 95%">
                                                <tr>
                                                    <td align="right"><h2 style="color: red">退款客户名称：</h2></td>
                                                    <td>
                                                        <input type="text" height="60" style="border-left: 0; border-top: 0; border-right: 0; border-bottom: 1px red solid;" name="returnCustomer" id="returnCustomer">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right"><h2 style="color: red">退款客户联系方式：</h2></td>
                                                    <td>
                                                        <input type="text" height="60" style="border-left: 0; border-top: 0; border-right: 0; border-bottom: 1px red solid;" name="returnPhone" id="returnPhone">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right"><h2 style="color: red">退款金额 (元)：</h2></td>
                                                    <td>
                                                        <input type="text" height="60" style="border-left: 0; border-top: 0; border-right: 0; border-bottom: 1px red solid;" name="returnMoney" id="returnMoney">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right"><h2 style="color: red">退款方式：</h2></td>
                                                    <td>
                                                        <input type="radio" value="1" name="returnType" checked="checked"><span style="color: red">现金</span>
                                                        <input type="radio" value="2" name="returnType" id="returnType"><span style="color: red">电汇</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right"><h2 style="color: red">退款账户：</h2></td>
                                                    <td>
                                                        <%--<textarea height="60" style="border-left: 0; width: 170px; border-top: 0; border-right: 0; border-bottom: 1px black solid;" id="note" name="note"></textarea>--%>
                                                        <input type="text" height="60" style="border-left: 0; border-top: 0; border-right: 0; border-bottom: 1px red solid;" name="customerAccount" id="customerAccount">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right"><h2 style="color: red">到款时间：</h2></td>
                                                    <td>
                                                        <%--<textarea height="60" style="border-left: 0; width: 170px; border-top: 0; border-right: 0; border-bottom: 1px black solid;" id="note" name="note"></textarea>--%>
                                                        <input type="text" height="60" style="border-left: 0; border-top: 0; border-right: 0; border-bottom: 1px red solid;" id="endData" name="endData">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right"><h2 style="color: red">备注：</h2></td>
                                                    <td>
                                                        <textarea height="60" style="border-left: 0; width: 170px; border-top: 0; border-right: 0; border-bottom: 1px red solid;" id="note" name="note"></textarea>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr style="height: 60px">
                                        <td><h2 style="color: red">经手人</h2></td>
                                        <td>
                                            <input type="text" height="60" style="border-left: 0; border-top: 0; border-right: 0; border-bottom: 1px red solid;" id="reviewer" name="handlers">
                                        </td>
                                        <td width="400"></td>
                                        <td><h2 style="color: red">录入人</h2></td>
                                        <td>
                                            <input type="text" height="60" style="border-left: 0; border-top: 0; border-right: 0; border-bottom: 1px red solid;" id="input_man" name="enterOne">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <input type="button" value="确认" style="background-color: #ffffff; color: red; width: 60px; height: 30px" onclick="send()">
                            </td>
                        </tr>
                    </table>
                </div>
            </form>
            <div class="cf" id="bottomField">
                <div class="fr" id="toolBottom"></div>
            </div>
            <div style="height: 30px;"></div>
            <div style="height: 30px; background-color: slategray"></div>
            <div style="height: 30px;"></div>
            <div id="initCombo" class="dn">
                <input type="text" class="textbox goodsAuto" name="goods" autocomplete="off">
                <input type="text" class="textbox storageAuto" name="storage" autocomplete="off">
                <input type="text" class="textbox unitAuto" name="unit" autocomplete="off">
                <input type="text" class="textbox batchAuto" name="batch" autocomplete="off">
                <input type="text" class="textbox dateAuto" name="date" autocomplete="off">
                <input type="text" class="textbox priceAuto" name="price" autocomplete="off">
            </div>
            <div id="storageBox" class="shadow target_box dn">
            </div>
        </div>
        <script>
            var WDURL = "";
            var SCHEME= "green";
            try{
                document.domain = 'http://127.0.0.1/erpv2/';
            }catch(e){
            }
            //ctrl+F5 增加版本号来清空iframe的缓存的
            $(document).keydown(function(event) {
                /* Act on the event */
                if(event.keyCode === 116 && event.ctrlKey){
                    var defaultPage = Public.getDefaultPage();
                    var href = defaultPage.location.href.split('?')[0] + '?';
                    var params = Public.urlParam();
                    params['version'] = Date.parse((new Date()));
                    for(i in params){
                        if(i && typeof i != 'function'){
                            href += i + '=' + params[i] + '&';
                        }
                    }
                    defaultPage.location.href = href;
                    event.preventDefault();
                }
            });
        </script>
        <script src="<%=path%>/statics2/js/dist/cashBill.js?ver=20150522"></script>
        <script src="<%=path%>/static/sales/sales_input.js"></script>
        <script src="<%=path%>/static/hplus/js/plugins/suggest/bootstrap-suggest.min.js"></script>
</body>
<script>
    //外部js调用
    laydate({
        elem: '#date', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
        event: 'focus' //响应事件。如果没有传入event，则按照默认的click
    });
    //日期范围限制
    var start = {
        elem: '#start',
        format: 'YYYY/MM/DD',
        min: laydate.now(), //设定最小日期为当前日期
        max: '2099-06-16', //最大日期
        istime: true,
        istoday: false,
        choose: function (datas) {
            end.min = datas; //开始日选好后，重置结束日的最小日期
            end.start = datas //将结束日的初始值设定为开始日
        }
    };
    var end = {
        elem: '#end',
        format: 'YYYY/MM/DD',
        min: laydate.now(),
        max: '2099-06-16',
        istime: true,
        istoday: false,
        choose: function (datas) {
            start.max = datas; //结束日选好后，重置开始日的最大日期
        }
    };
    laydate(start);
    laydate(end);
</script>
<script>
    function send () {
        alert("方法进入");
        var date = document.getElementById("date");
        var billNo = document.getElementById("billNo");
        var returnCustomer = document.getElementById("returnCustomer");
        var returnMoney = document.getElementById("returnMoney");
        var returnPhone = document.getElementById("returnPhone");
        var returnType = document.getElementById("returnType");
        var customerAccount = document.getElementById("customerAccount");
        var endData = document.getElementById("endData");
        var note = document.getElementById("note");
        var reviewer = document.getElementById("reviewer");
        var input_man = document.getElementById("input_man");
        if(date.value == ""){
            alert("创建时间不能为空")
            date.focus();
        } else if (billNo.value == "") {
            alert("单据编号不能为空")
            billNo.focus();
        } else if (returnCustomer.value == "") {
            alert("退款客户名称不能为空")
            returnCustomer.focus();
        } else if (returnPhone.value == "") {
            alert("客户联系方式")
            returnPhone.focus();
        } else if (returnMoney.value == "") {
            alert("退款金额不能为空")
            returnMoney.focus();
        } else if (returnType.checked){
            if (customerAccount.value == ""){
                alert("客户账户不可为空");
                customerAccount.focus();
            }
        } else if (endData.value == ""){
            alert("到款日期不可为空");
            endData.focus();
        } else if (note.value == "") {
            alert("备注信息不可为空");
            note.focus();
        } else if (reviewer.value == "") {
            alert("经手人不可为空");
            reviewer.focus();
        } else if (input_man.value == "") {
            alert("录入人不可为空");
            input_man.focus();
        } else {
            $("form").submit();
            top.location="<%=path%>/index/index2";
        }
    }
</script>
</html>