<%--
  Created by IntelliJ IDEA.
  User: xuzhongyao
  Date: 2017/1/17
  Time: 上午10:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>销售业绩列表</title>
    <meta name="keywords" content="科研项目管理">
    <meta name="description" content="科研项目管理">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE">
    <link href="<%=path%>/statics2/saas/scm/app2_release/css/common.css?ver=20150522" rel="stylesheet" type="text/css">
    <link href="<%=path%>/statics2/saas/scm/app2_release/css/green/ui.min.css?ver=20150522" rel="stylesheet">
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/jquery/jquery-1.10.2.min.js"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/json3.min.js"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/common.js?ver=20150522"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/grid.js?ver=20150522"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/plugins.js?ver=20150522"></script>
    <script src="<%=path%>/statics2/saas/scm/app2_release/js/common/plugins/jquery.dialog.js?self=true&ver=20150522"></script>
    <link href="<%=path%>/statics2/css/green/bills.css?ver=20150522" rel="stylesheet" type="text/css">
    <link href="<%=path%>/static/hplus/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/animate.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/style.css?v=4.1.0" rel="stylesheet">

    <link href="<%=path%>statics2/css/green/bills.css?ver=20150522" rel="stylesheet" type="text/css">
    <style>
        #barCodeInsert{margin-left: 10px;font-weight: 100;font-size: 12px;color: #fff;background-color: #B1B1B1;padding: 0 5px;border-radius: 2px;line-height: 19px;height: 20px;display: inline-block;}
        #barCodeInsert.active{background-color: #23B317;}
        table{
            border: 4px;
        }
        h2{
            text-align: center;
        }
    </style>
</head>
<body>
<div>
    <h1>
        产品目录
    </h1>
    <h2>
        药用铝瓶目录
    </h2>
    <div style="padding: 20px">
        <table>
            <c:forEach items="${a}" var="c">
                <tr>
                    <c:forEach items="${c}" var="c">
                        <td>
                <span style="color: green">
                        ${c.productType}
                </span>
                        </td>
                        <td>
                            <img src="<%=path%>static/hplus/img/a4.jpg" style="width: 20px;height: 15px">
                        </td>
                    </c:forEach>
                </tr>
            </c:forEach>
        </table>
        <span style="color: red">
         备注：001好的  004次的  00A差的 007花片
    006黄的 008重的好的 088重的次的  002轻的
    003水印 009夹灰15L 033挂具15L 005口紧不好的
    010口松不好的
    </span>
    </div>
    <h2>
        封口钳目录
    </h2>
    <div style="padding: 20px">
        <table>
            <c:forEach items="${b}" var="c">
                <tr>
                    <c:forEach items="${c}" var="c">
                        <td>
                <span style="color: green">
                        ${c.productType}
                </span>
                        </td>
                        <td>
                            <img src="<%=path%>static/hplus/img/a4.jpg" style="width: 20px;height: 15px">
                        </td>
                    </c:forEach>
                </tr>
            </c:forEach>
        </table>
        <span style="color: red">
         备注：001好的  004次的  00A差的 007花片
    006黄的 008重的好的 088重的次的  002轻的
    003水印 009夹灰15L 033挂具15L 005口紧不好的
    010口松不好的
    </span>
    </div>
    <h2>
        封盖机瓶目录
    </h2>
    <div style="padding: 20px">
        <table>
            <c:forEach items="${c}" var="c">
                <tr>
                    <c:forEach items="${c}" var="c">
                        <td>
                <span style="color: green">
                        ${c.productType}
                </span>
                        </td>
                        <td>
                            <img src="<%=path%>static/hplus/img/a4.jpg" style="width: 20px;height: 15px">
                        </td>
                    </c:forEach>
                </tr>
            </c:forEach>
        </table>
        <span style="color: red">
         备注：001好的  004次的  00A差的 007花片
    006黄的 008重的好的 088重的次的  002轻的
    003水印 009夹灰15L 033挂具15L 005口紧不好的
    010口松不好的
    </span>
    </div>
</div>
<div>

</div>



</body>
</html>
