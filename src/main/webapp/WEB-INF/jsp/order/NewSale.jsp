<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <script></script>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>销售业绩列表</title>
    <meta name="keywords" content="科研项目管理">
    <meta name="description" content="科研项目管理">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE">
    <link href="<%=path%>/statics2/saas/scm/app2_release/css/common.css?ver=20150522" rel="stylesheet" type="text/css">
    <link href="<%=path%>/statics2/saas/scm/app2_release/css/green/ui.min.css?ver=20150522" rel="stylesheet">
    <link href="<%=path%>/statics2/css/green/bills.css?ver=20150522" rel="stylesheet" type="text/css">
    <link href="<%=path%>/static/hplus/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/animate.css" rel="stylesheet">
    <link href="<%=path%>/static/hplus/css/style.css?v=4.1.0" rel="stylesheet">
</head>
<body>
<input type="hidden" value="<%=path%>" id="hid">
<div class="wrapper" style="height: 100%" id="body">
    <span id="config" class="ui-icon ui-state-default ui-icon-config"></span>
    <div class="mod-toolbar-top mr0 cf dn" id="toolTop"></div>
    <div class="bills cf">
        <div class="con-header">
            <div class="row-item" style="text-align: center">
                <h1 style="font-size: 23px">唐山军荣铝业有限公司${name}</h1>
            </div>
            <dl class="cf">
                <dd class="pct25">
                    <label>客户：</label>
                    <span class="ui-combo-wrap" id="customer">
                     <input type="text" name="" class="input-txt" autocomplete="off" value="" data-ref="date">
                     <i class="ui-icon-ellipsis"></i></span>
                </dd>
                <dd id="identifier" class="pct20 tc">
                    <label>销售人员:</label>
                    <span class="ui-combo-wrap" id="sales">
                          <input type="text" class="input-txt" autocomplete="off"><i class="trigger"></i></span>
                </dd>
                <dd id="11" class="pct20">
                    <label>单据编号:</label>
                    <input id="number" type="text" class="input-txt ui-input" autocomplete="off" value="${num}">
                </dd>
            </dl>
        </div>
        <div class="grid-wrap">
            <table id="grid">
            </table>
            <div id="page"></div>
        </div>
        <div class="con-footer cf">
            <div class="mb10" hidden>
                <textarea type="text" id="note" class="ui-input ui-input-ph">暂无备注信息</textarea>
            </div>
            <ul id="amountArea" class="cf">

                <li>
                    <label>日期:</label>
                    <input type="text" id="payDate" class="ui-input ui-datepicker-input" value="2015-06-08">
                </li>
                <li>
                    <label>录入人员：</label>
                    <input type="text" id="inputMan" class="input-txt ui-input" autocomplete="off">
                </li>
                <li>
                    <label>复核人员：</label>
                    <input type="text" id="reviewerMan" class="input-txt ui-input" autocomplete="off">
                </li>
            </ul>
            <ul id="amountArea2" class="cf">
                <li style="padding-top: 5px;">
                    <label>是否电汇：</label>
                    <input type="radio" name="whetherElectint" class="regular_customer"  value="1">是&nbsp&nbsp&nbsp
                    <input type="radio" name="whetherElectint" class="regular_customer" checked value="0">否
                </li>
                <li style="padding-top: 5px;">
                    <label>是否老客户：</label>
                    <input type="radio" name="regular_customer" class="regular_customer"  value="1">是&nbsp&nbsp&nbsp
                    <input type="radio" name="regular_customer" class="regular_customer" checked value="0">否
                </li>
            </ul>
        </div>
        <div class="cf" id="bottomField">
            <div class="fr" id="toolBottom"></div>
        </div>
        <div id="mark"></div>
    </div>
    <div id="initCombo" class="dn">
        <input type="text" class="textbox goodsAuto" name="goods" autocomplete="off">
        <input type="text" class="textbox storageAuto" name="storage" autocomplete="off">
        <input type="text" class="textbox unitAuto" name="unit" autocomplete="off">
        <input type="text" class="textbox batchAuto" name="batch" autocomplete="off">
        <input type="text" class="textbox dateAuto" name="date" autocomplete="off">
        <input type="text" class="textbox priceAuto" name="price" autocomplete="off">
    </div>
    <div id="storageBox" class="shadow target_box dn">
    </div>
</div>
<script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/jquery/jquery-1.10.2.min.js"></script>
<script src="<%=path%>/statics2/saas/scm/app2_release/js/common/libs/json3.min.js"></script>
<script src="<%=path%>/statics2/saas/scm/app2_release/js/common/common.js?ver=20150522"></script>
<script src="<%=path%>/statics2/saas/scm/app2_release/js/common/grid.js?ver=20150522"></script>
<script src="<%=path%>/statics2/saas/scm/app2_release/js/common/plugins.js?ver=20150522"></script>
<script src="<%=path%>/statics2/saas/scm/app2_release/js/common/plugins/jquery.dialog.js?self=true&ver=20150522"></script>
<script src="<%=path%>/static/sales/sales.js"></script>
</body>
</html>
