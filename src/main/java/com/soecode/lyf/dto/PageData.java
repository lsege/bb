package com.soecode.lyf.dto;

import java.util.List;

/**
 * Created by 10091 on 2017-05-25.
 */
public class PageData<T> {
    private  int page;
    private  int records;
    private  int total;
    private  List<T> rows;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getRecords() {
        return records;
    }

    public void setRecords(int records) {
        this.records = records;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }
}
