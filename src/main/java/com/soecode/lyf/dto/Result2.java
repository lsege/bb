package com.soecode.lyf.dto;

/**
 * 封装json对象，所有返回结果都使用它
 */
public class Result2<T> {

    private boolean success;// 是否成功标志

    private T obj;// 成功时返回的数据

    private String msg;// 错误信息

    private PageData<T> data;
    private int status = 200;

    public Result2() {
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    // 成功时的构造器
    public Result2(boolean success, T data) {
        this.success = success;
        this.obj = data;
    }

    // 成功时的构造器
    public Result2(boolean success,PageData<T> data) {
        this.success = success;
        this.data = data;
    }

    // 错误时的构造器
    public Result2(boolean success, String error) {
        this.success = success;
        this.msg = error;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public T getObj() {
        return obj;
    }

    public void setObj(T obj) {
        this.obj = obj;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "JsonResult [success=" + success + ", data=" + data + ", error=" + msg + "]";
    }


    public PageData<T> getData() {
        return data;
    }

    public void setData(PageData<T> data) {
        this.data = data;
    }
}
