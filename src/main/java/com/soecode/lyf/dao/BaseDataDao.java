package com.soecode.lyf.dao;

import com.soecode.lyf.entity.ReturnOrder;
import com.soecode.lyf.entity.SalesMan;
import com.soecode.lyf.entity.vo.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by 10091 on 2017-05-25.
 */
public interface BaseDataDao {
    /**
     * 查询所有商品
     *
     * @return
     */
    List<StorageVO> queryStorage();
    /**
     * 查询所有商品
     *
     * @return
     */
    List<ProductVO> queryAll();

    /**
     * 查询所有商品订单
     *
     * @return
     */
    List<ProductVO> queryAll2();

    /**
     * 查询销售人员
     * @return
     */
    List<SalesMan> selectSales();
    /**
     * 查询客户
     * @return
     */
    List<CustomerVO> selectCustomer();
    /**
     * 查询客户
     * @param id
     * @return
     */
    int selectCustomerById(int id);

    /**
     * 查询订单
     * @return
     */
    List<SalesVO> queryOrders(QueryVO vo);

    int queryOrdersCount(QueryVO vo);

    List<Sales2Vo> queryOrders3(QueryVO vo);
    int queryOrders3Count(QueryVO vo);


    /**
     * 查询订单
     * @return
     */
    PostData queryOrders2(@Param("id")Integer id);
    PostData queryOrders4(@Param("id")Integer id);

    List<EntrieData> queryGoodsInfo(@Param("id") Integer id);
    /**
     * 查询订单根据单号
     * @return
     */
    PostData queryOrdersByNo(@Param("billNo")String billNo);

    PostData queryReturnOrderByNo(@Param("billNo") String billNo);
    /**
     * 查询订单
     * @return
     */
    List<EntrieData> queryOrdersDetail(@Param("pinId")String pinId);

    /**
     * 删除订单根据单号
     * @param billNo
     * @return
     */
    int deleteOrderInfo(@Param("billNo")String billNo);

    void deleteReturnOrder(@Param("billNo") String billNo);

    ProductVO queryData(@Param("id") String id);
}
