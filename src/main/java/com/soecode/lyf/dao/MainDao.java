package com.soecode.lyf.dao;

import com.soecode.lyf.entity.User;
import org.apache.ibatis.annotations.Param;

/**
 * Created by Administrator on 2018/1/9.
 */
public interface MainDao {

    User selectUser(@Param("name")String name, @Param("pwd")String pwd);
}
