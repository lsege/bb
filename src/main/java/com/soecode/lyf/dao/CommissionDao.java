package com.soecode.lyf.dao;

import com.soecode.lyf.entity.SalesManCommission;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Administrator on 2017/10/9.
 */
public interface CommissionDao {

    List<SalesManCommission> queryAllCommission();

    void editCommission(@Param("id") Integer id,@Param("commission") Double commission);

    List<SalesManCommission> queryAllCommissionByUser(@Param("userId") String userId);
}
