package com.soecode.lyf.dao;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 2017/10/19.
 */
public interface ReturnOrderDao {

    void addReturn(@Param("documentNumber") String documentNumber,@Param("customerName") String customerName,@Param("productName") String productName,@Param("count") String count,
                   @Param("bak") String bak,@Param("inputMan") String inputMan,@Param("reviewerMan") String reviewerMan,@Param("createDate") String createDate,@Param("salesmanName") String salesmanName);
}
