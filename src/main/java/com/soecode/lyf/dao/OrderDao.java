package com.soecode.lyf.dao;

import com.soecode.lyf.entity.*;
import com.soecode.lyf.entity.vo.MoneyAccept;
import com.soecode.lyf.entity.vo.OrderList;
import com.soecode.lyf.util.PageData;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by lsd on 2017-03-16.
 */
public interface OrderDao {
    /**查询所有商品*/
    List<ProductBase> queryAllProduct();

    /**分页查询所有商品*/
    List<ProductBase> queryAllProductByPage(@Param("startRow") Integer startRow,@Param("pageSize") Integer pageSize);

    /**查询所有客户*/
    List<Customer> queryAllCustomer();

    /**分页查询所有客户*/
    List<Customer> queryAllCustomerByPage(@Param("start") Integer startRow,@Param("pageSize") Integer pageSize);

    /**查询所有销售人员*/
    List<SalesMan> queeeryAllSalesMan();

    /**分页查询所有销售人员*/
    List<SalesMan> querySalesManByPage(@Param("start") Integer startRow,@Param("pageSize") Integer pageSize);

    /**根据单号查询销货单*/
    List<SalesOrders> queryOrderByNumber(@Param("number") String documentNumber);

    /**查询客户ID*/
    String queryCustomerIdByName(@Param("customerName") String customerName);

    /**查询销售ID*/
    String querySalesManId(@Param("name") String salesmanName);

    /**保存销货单信息*/
    void addSalesOrder(@Param("customerId") String customerId,@Param("createDate") String createDate,@Param("salesManId") String salesManId,@Param("documentNumber") String documentNumber,
                       @Param("reviewOfficer") String reviewOfficer,@Param("entryPersonnel") String entryPersonnel,@Param("regularCustomer") String regularCustomer,@Param("salesMoney") String salesMoney,
                       @Param("dueDate1") String dueDate1,@Param("dueDate2") String dueDate2,@Param("dueDate3") String dueDate3,@Param("dueDate4") String dueDate4,
                       @Param("payDate") String payDate,@Param("whetherElectint") String whetherElectint,@Param("pushMoney") String pushMoney,
                       @Param("pSum") String pSum,@Param("wSum") String wSum);

    /**查询商品ID*/
    String queryProductId(@Param("name") String s);

    /**保存销货单销售信息*/
    void addPaymentDetails(@Param("documentNumber") String documentNumber,@Param("bProductId") String bProductId,@Param("bCount") String bCount,@Param("bPrice") String bPrice,
                           @Param("bMoney") String bMoney,@Param("bBak") String bBak,@Param("wProductId") String wProductId,@Param("wCount") String wCount,@Param("wPrice") String wPrice,@Param("wMoney") String wMoney);

    /**保存客户欠款信息*/
    void addCustomerArrears(@Param("customer_id") String customerId,@Param("document_number") String documentNumber,@Param("require_money") Double requireMoney);





















    void saveOrder(@Param("billNo") String billNo,@Param("customerName")String contactName,@Param("customerId")String contactId,
                   @Param("apronNum")String totalQty,@Param("apronPrice")String totalAmount,@Param("totalPushMonery")String totalPushMoney,
                   @Param("reviewerMan") String reviewerMan,@Param("inputMan") String inputMan,@Param("dateIssuance") String payDate,
                   @Param("creationDate") Date data1,@Param("delStatus") int b,@Param("orderType") int c,@Param("payDate") String payDate1,@Param("bak") String description,
                   @Param("whetherElectint") int d,@Param("whether") int e);


    /**
     *
     * @param bill_num
     * @param make_from_name
     * @param full_from_name
     * @param review_name
     * @param make_from_date
     * @param full_from_date
     * @param review_date
     */
    void saveProduct( @Param("bill_num")String bill_num,  @Param("make_from_name")String make_from_name,
                      @Param("full_from_name")String full_from_name,@Param("review_name")String review_name,
                      @Param("make_from_date")String make_from_date,  @Param("full_from_date")String full_from_date,@Param("review_date")String review_date);
    /**
     * @param goods
     * @param specificationModel
     * @param qty
     * @param price
     * @param amount
     * @param descrip
     * @param criate
     * @param item_type
     * @param pin_ID
     */
    void saveOrderDetail(@Param("goods")String goods,@Param("specificationModel")String specificationModel,
                     @Param("qty")String qty,@Param("price")String price,@Param("amount")String amount,@Param("descrip")String descrip,
                     @Param("criate")String criate, @Param("item_type")String item_type, @Param("pin_ID")String pin_ID, @Param("storageId")String storageId, @Param("pushMoney")String pushMoney);

    void saveOrderBill(@Param("bill_num")String bill_num,@Param("product_id")String product_id,
                         @Param("num1")String num1,@Param("sales_amount")String sales_amount);

    SalesOrders selectIdByNumber(String documentNumber);

    List<SalesOrders> selectList(@Param("firstResult")Integer firstResult,@Param("maxResults")Integer maxResults,@Param("keyword")String keyword);

    List<SalesOrders> getTotal(@Param("keyword")String keyword);

    User selectUser(@Param("name")String name,@Param("pwd")String pwd);

    ArrayList findByCustomer(@Param("customer")String customer);

    List<Product> selectProduct(@Param("corres") Integer corres);

    void saveSalesman(@Param("name")String name,@Param("phone")String phone);

    void saveSaleCustomer(@Param("name1")String name1,@Param("phone1")String phone1,@Param("address")String address,
                          @Param("people")String people,@Param("wheh")String wheh);

    void saveUser(@Param("name")String name,@Param("pwd")String pwd);

    List<MoneyAccept> queryAllOrder();

    List<MoneyAccept> queryAllOrderByPage(@Param("startRow") int startRow,@Param("pageSize") int pageSize);

    OrderList queryOrderById(@Param("id") Long id);

    String queryOrderNumById(@Param("id") Long id);
    void deleteAllHeadPayment(@Param("listNum") String listNum);
    void deleteMoneyAccept(@Param("id") Long id);

    void saveReturnOrder(@Param("returnData") String returnData,@Param("orderNo") String orderNo,@Param("customerName") String customerName,
                         @Param("productName") String productName,@Param("productType") String productType,@Param("count") String count,
                         @Param("remark") String remark,@Param("returnReason") String returnReason,@Param("inputMan") String inputMan,@Param("auditorMan") String auditorMan);

    void saveReturnMoney(ReturnMoney returnMoney);

    void saveReturn(@Param("customerName") String contactName,@Param("no") String billNo,@Param("inputMan") String inputMan,@Param("endMan") String reviewerMan,@Param("endData") String payDate,@Param("note") String description,@Param("goodsName") String goods,@Param("count") String count);

    void editReturnOrder(@Param("id")Integer id,@Param("customerName") String contactName,@Param("no") String billNo,@Param("inputMan") String inputMan,
                         @Param("endMan") String reviewerMan,@Param("endData") String payDate,@Param("note") String description,
                         @Param("goodsName") String goods,@Param("count") String count);

    ReturnMoney queryReturnOrderById(@Param("id") Long id);

    void deleteReturnOrder(@Param("id") String id);

    String querySalesOrder(@Param("id") Long id);
    void deletePaymentDetails(@Param("listNum") String listNum);
    void deleteSalesOrders(@Param("id") String id);



    List<CustomerArrears> querySalesOrderByCustomer(@Param("customer") String customer);

    List<CustomerArrears> queryAllCustomerArrearsByCustomerId(@Param("customerId") String customerId);

    String querySurplusMoneyByCustomerId(@Param("customerId") String customerId);

    void cancelSalesOrder(@Param("id") Long id,@Param("alreadyMoney") String alreadyMoney,@Param("isToComplete") String isToComplete,@Param("date1") String date1);

    void surplusMoney(@Param("customerId") String customerId,@Param("money") String money);

    void addCheckPayment(@Param("moneyNumber") String moneyNumber,@Param("salesNumber") String salesNumber);

    void editSalesOrder(@Param("documentNumber") String documentNumber,@Param("data") String data);

    CustomerArrears queryCustomerArrearsByNumber(@Param("number") String number);

    List<CustomerArrears> queryArrearsByName(@Param("customer") String customer);

    List<AllHead> queryAllHeads(@Param("billNo") String billNo);

    List<SalesMan> queryAllSalesMan();


    List<Test> queryTest();

    List<MoneyAccept> queryAllOrdersById(@Param("userId") String userId);

    List<MoneyAccept> queryAllOrdersByPage(@Param("startRow") int startRow, @Param("pageSize") int pageSize, @Param("userId") String userId);

    Double queryArrears(@Param("s") String s);
}



























