package com.soecode.lyf.dao;

import com.soecode.lyf.entity.Customer;
import com.soecode.lyf.entity.SalesMan;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/8/7.
 */
public interface CustomerDao {
    List<Customer> queryAllCustomerByName(@Param("customer_name") String customer_name);
    void addCustomer(@Param("customer_name") String customer_name, @Param("customer_phone") String customer_phone, @Param("customer_address") String customer_address,
                     @Param("customer_people") String customer_people, @Param("people_phone") String people_phone, @Param("state_customer") int state_customer,
                     @Param("man") String salesManId);

    List<Customer> queryAllCustomer();

    List<Customer> queryAllCustomerByPage(@Param("startRow") int startRow,@Param("pageSize") int pageSize);

    void delete(@Param("id") String id);

    Customer queryCustomerById(@Param("id") String id);

    void editCustomer(@Param("id") String id,@Param("name") String customerName,@Param("phone") String customerPhone,
                      @Param("address") String customerAddress,@Param("people") String customerPeople,@Param("peoplePhone") String peoplePhone,
                      @Param("stateCustomer") String stateCustomer);

    List<SalesMan> queryAllSalesMan();

    SalesMan querySalesMan(@Param("salesMan") String salesMan);
    void addSalesMan(@Param("salesMan") String salesMan);

    List<Customer> loadCustomerName(@Param("str") String s);
}
