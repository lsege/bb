package com.soecode.lyf.dao;

import com.soecode.lyf.entity.DataBase;
import com.soecode.lyf.entity.Product;
import com.soecode.lyf.entity.vo.Products;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Administrator on 2017/8/7.
 */
public interface ProductDao {

    String queryDictByName(@Param("name") String name);

    String queryDictByUnit(@Param("unit") String unit);

    String queryDictByFeature(@Param("feature") String feature);

    String queryDictMaxByFeature();

    void addDictByFeature(@Param("feature") String feature,@Param("max") String max);

    String queryDict(@Param("dictFeature") String dictFeature);

    void addProduct(@Param("productType") String dictName,@Param("productVol") String bulk,@Param("volUnit") String dictUnit,@Param("volQual") String size,@Param("productTrait") String productTrait,@Param("nameQual") String nameQual);

    List<DataBase> queryAllDataName();

    DataBase queryDataBaseByDataName(@Param("dataName") String dataName);

    int queryMaxDataValue();

    void saveDataBase(@Param("detail") String detail,@Param("dataType") String dataType,@Param("dataName") String dataName,@Param("dataValue") String dataValue);

    List<Products> queryAllProduct();

    List<Products> queryAllProductByPage(@Param("startRow") int startRow,@Param("pageSize") int pageSize);

    void delete(@Param("id") String id);
    Products queryProductById(@Param("id") String id);

    void editProduct(@Param("id") String id,@Param("productType") String dictName,@Param("productVol") String bulk,@Param("volUnit") String dictUnit,@Param("volQual") String size,@Param("productTrait") String productTrait,@Param("nameQual") String nameQual);




    String queryProductIdByName(@Param("name") String newProduct);

    String queryUnitIdByName(@Param("unit") String unit);

    String queryFeatureIdByName(@Param("feature") String feature);

    String queryIdByName(@Param("name") String productName);
}
