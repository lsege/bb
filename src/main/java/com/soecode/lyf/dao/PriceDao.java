package com.soecode.lyf.dao;

import com.soecode.lyf.entity.AllHead;
import org.apache.ibatis.annotations.Param;

/**
 * Created by Administrator on 2017/3/20.
 */
public interface PriceDao {

    void savePrice(@Param("list_num")String list_num,@Param("customrt_name")String customrt_name,@Param("money")String money,
                   @Param("customer_phone")String customer_phone,@Param("num_chengdui")String num_chengdui,@Param("date_chengdui")String date_chengdui,
                   @Param("return_type") String return_type,@Param("chengdui_sendUnit")String chengdui_sendUnit,@Param("chengdui_toUnit")String chengdui_toUnit,
                   @Param("chengdui_bang")String chengdui_bang,@Param("customer_account")String customer_account,@Param("dianhuiBankName") String dianhuiBankName,
                   @Param("dianhuiPayAccount") String dianhuiPayAccount,@Param("dianhuiCollectionAccount") String dianhuiCollectionAccount,
                   @Param("dianhuiContext") String dianhuiContext,@Param("zhuanzhangPayAccount") String zhuanzhangPayAccount,@Param("zhuanzhangCollectionAccount") String zhuanzhangCollectionAccount,
                   @Param("note") String note);

    /**
     *
     * @param payment_date
     * @param document_number
     * @param input_man
     * @param brokerage
     * @param document_type
     * @param creation_date
     */
    void saveAllHead(@Param("payment_date")String payment_date,@Param("document_number")String document_number,@Param("input_man")String input_man,
                     @Param("brokerage")String brokerage,@Param("document_type")String document_type,@Param("creation_date")String creation_date,@Param("salesMan") String salesMan);

    AllHead selectIdByNumber(String documentNumber);

    Long queryIdByNumber(@Param("num") String num);

    void editCash(@Param("id") Long id,@Param("payment_date") String payment_date,@Param("document_number") String document_number,@Param("input_man") String input_man,@Param("brokerage") String brokerage,@Param("document_type") String document_type,@Param("creation_date") String creation_date);
    void editCashPrice(@Param("id")Long id,@Param("list_num")String list_num,@Param("customrt_name")String customrt_name,@Param("money")String money,
                       @Param("customer_phone")String customer_phone,@Param("note") String note);

    void editAcceptPrice(@Param("id") Long id,@Param("listNum") String listNum,@Param("customrtName") String customrtName,@Param("money") String money,@Param("customerPhone") String customerPhone,@Param("numChengdui") String numChengdui,@Param("dateChengdui") String dateChengdui,@Param("note") String note);

    void saveReturnMoneyAllHead(@Param("document_number") String document_number,@Param("input_man") String input_man,@Param("brokerage") String brokerage,@Param("document_type") String document_type,@Param("creation_date") String creation_date);
    void saveReturnMoneyPrice(@Param("list_num") String list_num,@Param("customrt_name") String customrt_name,@Param("money") String money,
                              @Param("customer_phone") String customer_phone,@Param("note") String note,@Param("end_data") String end_data,
                              @Param("return_type") String return_type,@Param("customer_account") String customer_account);

    void editReturnMoney(@Param("creation_date") String creation_date,@Param("document_number") String document_number,@Param("input_man") String input_man,@Param("brokerage") String brokerage,@Param("document_type") String document_type);
    void editReturnMoneyPrice(@Param("id") Long uId,@Param("document_number") String document_number,@Param("payment_customer_name") String payment_customer_name,
                              @Param("acceptance_amount") String acceptance_amount,@Param("customer_contact") String customer_contact,@Param("return_type") String return_type,
                              @Param("customer_account") String customer_account,@Param("end_data") String end_data,@Param("note") String note);

    String querySalesManId(@Param("salesMan") String salesMan);
}
