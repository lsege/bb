package com.soecode.lyf.dao;

import com.soecode.lyf.entity.OperatorVo;
import com.soecode.lyf.entity.SalesMan;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Administrator on 2017/8/21.
 */
public interface OperatorDao {

    SalesMan queryOperatorByAccount(@Param("account") String account);
    String md5Account(@Param("account") String account);
    String md5Password(@Param("password") String password);
    void saveOperator(@Param("name") String name,@Param("phone") String phone,@Param("state") String state);

    void deleteOperatorById(@Param("id") Long id);

    SalesMan queryUserInfo(@Param("id") Long id);
    void editOperator(@Param("id") Long id,@Param("name") String name,@Param("phone") String phone);

    List<SalesMan> queryAllUsers();

    void editUser(@Param("userId") Long uId,@Param("name") String name);

    void editUserPassord(@Param("userId") Long uId,@Param("name") String name,@Param("password") String password);
}
