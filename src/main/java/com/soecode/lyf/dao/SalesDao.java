package com.soecode.lyf.dao;

import com.soecode.lyf.entity.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Administrator on 2017/8/24.
 */
public interface SalesDao {

    List<ProductBase> queryAllProduct();
    List<ProductBase> queryAllProductByPage(@Param("startRow") int startRow,@Param("pageSize") int pageSize);

    List<Customer> queryAllCustomer();

    List<Customer> queryAllCustomerByPage(@Param("start") int start,@Param("pageSize") int pageSize);

    List<SalesMan> queeeryAllSalesMan();

    List<SalesMan> querySalesManByPage(@Param("start") int start,@Param("size") int pageSize);

    List<Payment> queryPaymentByNumber(@Param("no") String no);

    List<SalesOrders> queryOrderByNumber(@Param("number") String number);

    void addSalesOrder(@Param("customerId") String customerId,@Param("createDate") String createDate,@Param("salesManId") String salesManId,@Param("documentNumber") String documentNumber,
                       @Param("reviewOfficer") String reviewOfficer,@Param("entryPersonnel") String entryPersonnel,@Param("regularCustomer") String regularCustomer,@Param("salesMoney") String salesMoney,
                       @Param("dueDate1") String dueDate1,@Param("dueDate2") String dueDate2,@Param("dueDate3") String dueDate3,@Param("dueDate4") String dueDate4,
                       @Param("payDate") String payDate,@Param("whetherElectint") String whetherElectint,@Param("pushMoney") String pushMoney,
                       @Param("pSum") String pSum,@Param("wSum") String wSum);

    String queryProductId(@Param("name") String name);

    void addPaymentDetails(@Param("documentNumber") String documentNumber,@Param("bProductId") String bProductId,@Param("bCount") String bCount,@Param("bPrice") String bPrice,
                           @Param("bMoney") String bMoney,@Param("bBak") String bBak,@Param("wProductId") String wProductId,@Param("wCount") String wCount,@Param("wPrice") String wPrice,@Param("wMoney") String wMoney);

    SalesOrders querySalesOrderById(@Param("id") String id);

    void deleteArrears(@Param("billNo") String billNo);

    void deletePaymentDetails(@Param("no") String no);

    void deleteSalesOrder(@Param("id") String id);

    void editSalesOrder(@Param("id") String id,@Param("customerId") String customerId,@Param("createDate") String createDate,@Param("salesManId") String salesManId,@Param("documentNumber") String documentNumber,
                        @Param("reviewOfficer") String reviewOfficer,@Param("entryPersonnel") String entryPersonnel,@Param("regularCustomer") String regularCustomer,@Param("salesMoney") String salesMoney,
                        @Param("dueDate1") String dueDate1,@Param("dueDate2") String dueDate2,@Param("dueDate3") String dueDate3,@Param("dueDate4") String dueDate4,
                        @Param("payDate") String payDate,@Param("whetherElectint") String whetherElectint,@Param("pushMoney") String pushMoney,
                        @Param("pSum") String pSum,@Param("wSum") String wSum);


    void addSalesMan(@Param("name") String name);

    String querySalesManId(@Param("name") String name);

    String queryCustomerIdByName(@Param("name") String name);

    List<SalesOrders> queryAllSalesOrders();

    List<SalesOrders> queryAllSalesOrdersByPage(@Param("start") int start,@Param("size") int size);

    String queryNoById(@Param("id") String id);

    void addCustomerArrears(@Param("customer_id") String customerId,@Param("document_number") String documentNumber,@Param("require_money") Double requireMoney);

    String queryAMByNumber(@Param("documentNumber") String document);

    void deleteCustomerArrears(@Param("documentNumber") String document);

    void addCustomerArrearsS(@Param("customer_id") String customerId,@Param("document_number") String documentNumber,@Param("require_money") Double requireMoney,@Param("alreadyMoney") Double alreadyMoney);

    List<SalesOrders> queryAllSalesOrdersByPageAndId(@Param("startRow") int startRow,@Param("pageSize") int pageSize,@Param("id") String id);

    List<Customer> querCustomerByName(@Param("name") String name);

    List<Customer> queryAllCustomerByNamePage(@Param("startRow") int startRow,@Param("pageSize") int pageSize,@Param("name") String customerName);

    List<ProductBase> queryAllProductByName(@Param("name") String productName);

    List<ProductBase> queryAllProductByNamePage(@Param("startRow") int startRow,@Param("pageSize") int pageSize,@Param("name") String name);

    void deleMoneyAccept(@Param("documentNumber") String documentNumber);

    String queryMoneyOrder(@Param("documentNumber") String documentNumber);

    void deleteAllHeadPayment(@Param("moneyOrder") String moneyOrder);

    void deleteCheckPayment(@Param("documentNumber") String documentNumber);
}
