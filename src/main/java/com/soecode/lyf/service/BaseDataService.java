package com.soecode.lyf.service;

import com.soecode.lyf.entity.ReturnOrder;
import com.soecode.lyf.entity.SalesMan;
import com.soecode.lyf.entity.vo.*;

import java.util.List;

/**
 * Created by 10091 on 2017-05-25.
 */
public interface BaseDataService {
    List<StorageVO> queryStorage();


    List<ProductVO> queryAll();

    List<ProductVO> queryAll2();

    List<SalesMan> selectSales();

    List<CustomerVO> selectCustomer();

    int selectCustomerById(int id);

    void saveSales(PostData data);
    void saveReturn(PostData data);

    void saveSales2(PostData data);

    void saveSalesman(String name,String phone);

    void saveSaleCustomer(String name1,String phone1,String address,String people,String wheh);
    void saveUser(String name,String pwd);
    /**
     * 查询订单
     * @return
     */
    List<SalesVO> queryOrders(QueryVO vo);
    int queryOrdersCount(QueryVO vo);

    List<Sales2Vo> queryOrders2(QueryVO vo);
    int queryOrders2Count(QueryVO vo);

    PostData selectOrderDetail(int id);
    PostData selectOrderDetai2(int id);

    /**
     * 更新订单
     * @param data
     */
    Integer updateSales(PostData data);
    Integer editReturnOrder(PostData data);



    ProductVO queryData(String id);
}
