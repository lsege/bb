package com.soecode.lyf.service;

/**
 * Created by Administrator on 2017/10/19.
 */
public interface ReturnOrderService {
    void addReturn(String documentNumber, String customerName, String productName, String count, String bak, String inputMan, String reviewerMan, String createDate, String salesmanName);
}
