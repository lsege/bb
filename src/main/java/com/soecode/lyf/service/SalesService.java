package com.soecode.lyf.service;

import com.soecode.lyf.entity.*;
import com.soecode.lyf.entity.vo.JsonVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Administrator on 2017/8/24.
 */
public interface SalesService {

    List<ProductBase> queryAllProduct();

    List<ProductBase> queryAllProductByPage(int startRow,int pageSize);

    List<Customer> queryAllCustomer();

    List<Customer> queryAllCustomerByPage(int start,int pageSize);

    List<SalesMan> queeeryAllSalesMan();

    List<SalesMan> querySalesManByPage(int start, int pageSize);





    String queryCustomerIdByName(String name);

    String querySalesManId(String name);

    List<SalesOrders> queryOrderByNumber(String number);

    List<Payment> queryPaymentByNumber(String no);

    void addSalesOrder(String customerId,String createDate,String salesManId,String documentNumber,String reviewOfficer,String entryPersonnel,
                       String regularCustomer,String salesMoney,String dueDate1,String dueDate2,String dueDate3,String dueDate4,String payDate,String whetherElectint,String pushMoney,String pSum,String wSum);

    String queryProductId(String name);

    void addPaymentDetails(String documentNumber,String bProductId,String bCount,String bPrice, String bMoney, String bBak, String wProductId,String wCount, String wPrice, String wMoney);

    void addCustomerArrears(String customerId,String documentNumber,String pSum,String wSum);

    void editCustomerArrears(String customerId,String documentNumber,String pSum,String wSum);

    SalesOrders querySalesOrderById(String id);

    void deleteArrears(String billNo);

    void delete(String id);

    void editSalesOrder(String id,String customerId,String createDate,String salesManId,String documentNumber,String reviewOfficer,String entryPersonnel,
                        String regularCustomer,String salesMoney,String dueDate1,String dueDate2,String dueDate3,String dueDate4,String payDate,String whetherElectint,String pushMoney,String pSum,String wSum);

    void deletePaymentDetails(String documentNumber);

    List<SalesOrders> queryAllSalesOrders();

    List<SalesOrders> queryAllSalesOrdersByPage(int start,int size);

    List<SalesOrders> queryAllSalesOrdersByPageAndId(int startRow, int pageSize, String id);

    List<Customer> querCustomerByName(String name);

    List<Customer> queryAllCustomerByNamePage(int startRow, int pageSize, String name);

    List<ProductBase> queryAllProductByName(String name);

    List<ProductBase> queryAllProductByNamePage(int startRow, int pageSize, String name);

    void deleteSalesOrders(Long id,String no);

    void deleteMoneyAccept(String documentNumber);
}
