package com.soecode.lyf.service;

import com.soecode.lyf.entity.*;
import com.soecode.lyf.entity.vo.MoneyAccept;
import com.soecode.lyf.entity.vo.OrderList;
import com.soecode.lyf.util.PageData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lsd on 2017-03-16.
 */
public interface OrderService {

    /**查询所有商品*/
    List<ProductBase> queryAllProduct();

    /**分页查询所有商品*/
    List<ProductBase> queryAllProductByPage(Integer startRow, Integer pageSize);

    /**查询所有客户*/
    List<Customer> queryAllCustomer();

    /**分页查询所有客户*/
    List<Customer> queryAllCustomerByPage(Integer startRow, Integer pageSize);

    /**查询所有销售人员*/
    List<SalesMan> queeeryAllSalesMan();

    /**分页查询所有销售人员*/
    List<SalesMan> querySalesManByPage(Integer startRow, Integer pageSize);

    /**根据单据编号查询销货单*/
    List<SalesOrders> queryOrderByNumber(String documentNumber);

    /**查询客户ID*/
    String queryCustomerIdByName(String customerName);

    /**查询销售ID*/
    String querySalesManId(String salesmanName);

    /**保存销货单信息*/
    void addSalesOrder(String customerId,String createDate,String salesManId,String documentNumber,String reviewOfficer,String entryPersonnel,
                       String regularCustomer,String salesMoney,String dueDate1,String dueDate2,String dueDate3,String dueDate4,String payDate,String whetherElectint,String pushMoney,String pSum,String wSum);

    /**查询商品ID*/
    String queryProductId(String s);

    /**保存销货单销售信息*/
    void addPaymentDetails(String documentNumber,String bProductId,String bCount,String bPrice, String bMoney, String bBak, String wProductId,String wCount, String wPrice, String wMoney);

    /**保存客户欠款信息*/
    void addCustomerArrears(String customerId,String documentNumber,String pSum,String wSum);















    /**
     * 销售单录入
     * @param type
     * @param custmerName
     * @param date
     * @param whether
     * @param input_man
     * @param reviewer
     * @param totalQty
     * @param totalAmount
     * @param number
     * @param del
     * @param criate
     */
    //存储列表数据
    void saveOrderList(Integer type,String custmerName,String date,String whether,String input_man,String reviewer,
                   String totalQty,String totalAmount,String number,String del,String criate,String totalPushMoney,String whetherDateint,String whetherElectint,String payDate);

    void saveProductList(String billNo,String sales_people,String time,String make_man,String make_date,String input_man,
                         String input_date,String reviewer_man,String reviewer_date,String numNo);

    /**
     * 销售数据录入
     * @param goods
     * @param specificationModel
     * @param qty
     * @param price
     * @param amount
     * @param descrip
     * @param criate
     * @param item_type
     * @param pin_ID
     */
    //存储买卖记录
    void saveOrderDetail(String goods, String specificationModel, String qty, String price,
                     String amount, String descrip,String criate,String item_type,String pin_ID,String storageId,String pushMoney);

    List<SalesOrders> selectList(Integer firstResult,Integer maxResults,String keyword);
    List<SalesOrders> getTotal(String keyword);
    User selectUser(String name,String pwd);
    //模糊查询顾客信息
    ArrayList selectByCustomer(String customer);

    List<Product> findList(Integer b);

    /*查询全部现金，承兑单*/
    List<MoneyAccept> queryAllOrder();
    List<MoneyAccept> queryAllOrderByPage(int startRow,int pageSize);


    OrderList queryAllOrderById(Long id);
    ReturnMoney queryReturnOrderById(Long id);

    void delete(String id);

    void deleteReturnOrder(String id);

    void saveReturnOrder(String returnData,String orderNo,String customerName,String productName,String productType,String count,String remark,String returnReason,String inputMan,String auditorMan);

    void saveReturnMoney(ReturnMoney returnMoney);

    void deleteSalesOrder(String id);



    List<CustomerArrears> querySalesOrderByCustomer(String customer);

    void cancelSalesOrder(String goodsName,String price);

    void cancelSalesOrderByNumber(String goodsName,String billNo,String price,String payDate);

    void addCheckPayment(String moneyNumber,String salesNumber);

    List<CustomerArrears> queryArrearsByName(String customer);

    void addMouny(String customer,String price);

    List<AllHead> queryAllHeads(String billNo);

    List<SalesMan> queryAllSalesMan();

    List<Test> queryTest();

    List<MoneyAccept> queryAllOrdersById(String userId);

    List<MoneyAccept> queryAllOrdersByPage(int startRow, int pageSize, String userId);


    Double queryArrears(String s);
}
