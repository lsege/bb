package com.soecode.lyf.service;

import com.soecode.lyf.entity.DataBase;
import com.soecode.lyf.entity.Product;
import com.soecode.lyf.entity.vo.Products;

import java.util.List;

/**
 * Created by Administrator on 2017/8/7.
 */

public interface ProductService {
    String queryDictByName(String productName);
    String queryDictByUnit(String unit);
    String queryDictByFeature(String feature);
    void addProduct(String dictName,String bulk,String dictUnit,String size,String productTrait,String nameQual);

    List<DataBase> queryAllDataName();

    DataBase queryDataBaseByDataName(String dataName);
    String queryMaxFromDataBase();
    void saveDataBase(DataBase dataBase);

    List<Products> queryAllProduct();
    List<Products> queryAllProductByPage(int startRow,int pageSize);

    void delete(String id);
    Products queryProductById(String id);

    void editProduct(String id,String dictName,String bulk,String dictUnit,String size,String productTrait,String nameQual);




    String queryProductIdByName(String newProduct);

    String queryUnitIdByName(String unit);

    String queryFeatureIdByName(String feature);

    String queryIdByName(String productName);
}
