package com.soecode.lyf.service;

import com.soecode.lyf.entity.SalesManCommission;

import java.util.List;

/**
 * Created by Administrator on 2017/10/9.
 */
public interface CommissionService {
    List<SalesManCommission> queryAllCommission();

    void editCommission(Integer id,Double commission);

    List<SalesManCommission> queryAllCommissionByUser(String userId);
}
