package com.soecode.lyf.service.impl;

import com.soecode.lyf.service.ReturnOrderService;
import com.soecode.lyf.dao.ReturnOrderDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 2017/10/19.
 */
@Service
public class ReturnOrderServiceImpl implements ReturnOrderService {

    @Autowired
    ReturnOrderDao returnOrderDao;

    @Override
    public void addReturn(String documentNumber, String customerName, String productName, String count, String bak, String inputMan, String reviewerMan, String createDate, String salesmanName) {
        returnOrderDao.addReturn(documentNumber,customerName,productName,count,bak,inputMan,reviewerMan,createDate,salesmanName);
    }
}
