package com.soecode.lyf.service.impl;

import com.soecode.lyf.dao.SalesDao;
import com.soecode.lyf.entity.*;
import com.soecode.lyf.entity.vo.JsonVo;
import com.soecode.lyf.service.SalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/8/24.
 */
@Service
public class SalesServiceImpl implements SalesService {

    @Autowired
    SalesDao salesDao;

    @Override
    public List<ProductBase> queryAllProduct() {
        return salesDao.queryAllProduct();
    }

    @Override
    public List<ProductBase> queryAllProductByPage(int startRow, int pageSize) {
        return salesDao.queryAllProductByPage(startRow,pageSize);
    }

    @Override
    public List<Customer> queryAllCustomer() {
        return salesDao.queryAllCustomer();
    }

    @Override
    public List<Customer> queryAllCustomerByPage(int start, int pageSize) {
        return salesDao.queryAllCustomerByPage(start,pageSize);
    }

    @Override
    public List<SalesMan> queeeryAllSalesMan() {
        return salesDao.queeeryAllSalesMan();
    }

    @Override
    public List<SalesMan> querySalesManByPage(int start, int pageSize) {
        return salesDao.querySalesManByPage(start,pageSize);
    }

    @Override
    public List<Payment> queryPaymentByNumber(String no) {
        return salesDao.queryPaymentByNumber(no);
    }

    @Override
    public List<SalesOrders> queryOrderByNumber(String number) {
        return salesDao.queryOrderByNumber(number);
    }

    @Override
    public void addSalesOrder(String customerId, String createDate, String salesManId, String documentNumber, String reviewOfficer, String entryPersonnel, String regularCustomer, String salesMoney,String dueDate1,String dueDate2,String dueDate3,String dueDate4, String payDate, String whetherElectint, String pushMoney,String pSum,String wSum) {
        salesDao.addSalesOrder(customerId,createDate,salesManId,documentNumber,reviewOfficer,entryPersonnel,regularCustomer,salesMoney,dueDate1,dueDate2,dueDate3,dueDate4,payDate,whetherElectint,pushMoney,pSum,wSum);
    }

    @Override
    public String queryProductId(String name) {
        return salesDao.queryProductId(name);
    }

    @Override
    public void addPaymentDetails(String documentNumber, String bProductId, String bCount, String bPrice, String bMoney, String bBak, String wProductId, String wCount, String wPrice, String wMoney) {
        salesDao.addPaymentDetails(documentNumber,bProductId,bCount,bPrice,bMoney,bBak,wProductId,wCount,wPrice,wMoney);
    }

    @Override
    public SalesOrders querySalesOrderById(String id) {
        return salesDao.querySalesOrderById(id);
    }

    @Override
    public void delete(String id) {
        String no  = salesDao.queryNoById(id);
        if (no != null && no != ""){
            salesDao.deletePaymentDetails(no);
        }
        salesDao.deleteSalesOrder(id);
    }

    @Override
    public void editSalesOrder(String id,String customerId, String createDate, String salesManId, String documentNumber, String reviewOfficer, String entryPersonnel, String regularCustomer, String salesMoney,String dueDate1,String dueDate2,String dueDate3,String dueDate4, String payDate, String whetherElectint, String pushMoney,String pSum,String wSum) {
        salesDao.editSalesOrder(id,customerId,createDate,salesManId,documentNumber,reviewOfficer,entryPersonnel,regularCustomer,salesMoney,dueDate1,dueDate2,dueDate3,dueDate4,payDate,whetherElectint,pushMoney,pSum,wSum);
    }

    @Override
    public void deletePaymentDetails(String documentNumber) {
        salesDao.deletePaymentDetails(documentNumber);
    }

    @Override
    public String queryCustomerIdByName(String name) {
        return salesDao.queryCustomerIdByName(name);
    }

    @Override
    public String querySalesManId(String name) {
        String id = salesDao.querySalesManId(name);
        if (id == null){
            salesDao.addSalesMan(name);
            return salesDao.querySalesManId(name);
        }
        return salesDao.querySalesManId(name);
    }

    @Override
    public void addCustomerArrears(String customerId, String documentNumber, String pSum, String wSum) {
        Double p = Double.parseDouble(pSum);
        Double w = Double.parseDouble(wSum);
        Double s = p+w;
        salesDao.addCustomerArrears(customerId,documentNumber,s);
    }

    @Override
    public void editCustomerArrears(String customerId,String documentNumber, String pSum, String wSum) {
        String alreadyMoney = salesDao.queryAMByNumber(documentNumber);
        Double am = Double.parseDouble(alreadyMoney);
        Double p = Double.parseDouble(pSum);
        Double w = Double.parseDouble(wSum);
        Double s = p+w;
        salesDao.deleteCustomerArrears(documentNumber);
        if (am != 0.0){
            salesDao.addCustomerArrears(customerId,documentNumber,s);
        } else {
            salesDao.addCustomerArrearsS(customerId,documentNumber,s,am);
        }
    }

    @Override
    public List<SalesOrders> queryAllSalesOrders() {
        return salesDao.queryAllSalesOrders();
    }

    @Override
    public List<SalesOrders> queryAllSalesOrdersByPage(int start, int size) {
        return salesDao.queryAllSalesOrdersByPage(start,size);
    }

    @Override
    public List<SalesOrders> queryAllSalesOrdersByPageAndId(int startRow, int pageSize, String id) {
        return salesDao.queryAllSalesOrdersByPageAndId(startRow,pageSize,id);
    }

    @Override
    public List<Customer> querCustomerByName(String name) {
        String customerName = "%"+name+"%";
        return salesDao.querCustomerByName(customerName);
    }

    @Override
    public List<Customer> queryAllCustomerByNamePage(int startRow, int pageSize, String name) {
        String customerName = "%"+name+"%";
        return salesDao.queryAllCustomerByNamePage(startRow,pageSize,customerName);
    }

    @Override
    public List<ProductBase> queryAllProductByName(String name) {
        String productName = "%"+name+"%";
        return salesDao.queryAllProductByName(productName);
    }

    @Override
    public List<ProductBase> queryAllProductByNamePage(int startRow, int pageSize, String name) {
        String productName = "%"+name+"%";
        return salesDao.queryAllProductByNamePage(startRow,pageSize,productName);
    }

    @Override
    public void deleteSalesOrders(Long id,String no) {
        salesDao.deletePaymentDetails(no);
        salesDao.deleteSalesOrder(id.toString());
    }

    @Override
    public void deleteMoneyAccept(String documentNumber) {
        String moneyOrder = salesDao.queryMoneyOrder(documentNumber);
        salesDao.deleMoneyAccept(moneyOrder);
        salesDao.deleteAllHeadPayment(moneyOrder);
        salesDao.deleteCheckPayment(documentNumber);
    }

    @Override
    public void deleteArrears(String billNo) {
        salesDao.deleteArrears(billNo);
    }
}
