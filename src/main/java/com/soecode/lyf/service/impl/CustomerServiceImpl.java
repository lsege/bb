package com.soecode.lyf.service.impl;

import com.soecode.lyf.dao.CustomerDao;
import com.soecode.lyf.entity.Customer;
import com.soecode.lyf.entity.SalesMan;
import com.soecode.lyf.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/8/7.
 */
@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerDao customerDao;

    @Override
    public List<Customer> queryAllCustomerByName(String customer_name) {
        return customerDao.queryAllCustomerByName(customer_name);
    }

    @Override
    public void addCustomer(String customer_name, String customer_phone, String customer_address, String customer_people, String people_phone, int state_customer, String salesMan) {
        //查询是否存在此业务员
        SalesMan salesMan1 = customerDao.querySalesMan(salesMan);
        if(salesMan1 == null){
            customerDao.addCustomer(customer_name,customer_phone,customer_address,customer_people,people_phone,state_customer,"");
        } else {
            customerDao.addCustomer(customer_name,customer_phone,customer_address,customer_people,people_phone,state_customer,salesMan);
        }
    }

    @Override
    public List<Customer> queryAllCustomer() {
        return customerDao.queryAllCustomer();
    }

    @Override
    public List<Customer> queryAllCustomerByPage(int startRow, int pageSize) {
        return customerDao.queryAllCustomerByPage(startRow,pageSize);
    }

    @Override
    public void delete(String id) {
        customerDao.delete(id);
    }

    @Override
    public Customer queryCustomerById(String id) {
        return customerDao.queryCustomerById(id);
    }

    @Override
    public void editCustomer(String id, String customerName, String customerPhone, String customerAddress, String customerPeople, String peoplePhone, String stateCustomer) {
        customerDao.editCustomer(id,customerName,customerPhone,customerAddress,customerPeople,peoplePhone,stateCustomer);
    }

    @Override
    public List<SalesMan> queryAllSalesMan() {
        return customerDao.queryAllSalesMan();
    }

    @Override
    public List<Customer> loadCustomerName(String s) {
        return customerDao.loadCustomerName(s);
    }
}
