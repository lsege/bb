package com.soecode.lyf.service.impl;

import com.soecode.lyf.dao.ProductDao;
import com.soecode.lyf.entity.DataBase;
import com.soecode.lyf.entity.Product;
import com.soecode.lyf.entity.vo.Products;
import com.soecode.lyf.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017/8/7.
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductDao productDao;

    @Override
    public String queryDictByName(String productName) {
        //查询字典
        return productDao.queryDictByName(productName);
    }

    @Override
    public String queryDictByUnit(String unit) {
        return productDao.queryDictByUnit(unit);
    }

    @Override
    public String queryDictByFeature(String feature) {
        //查询商品特征字典值
        String dFeature = productDao.queryDictByFeature(feature);
        if (dFeature == null){
            //无此字典生成字典
            String max = productDao.queryDictMaxByFeature();
            productDao.addDictByFeature(feature,max);
            dFeature = productDao.queryDictByFeature(feature);
        }
        return dFeature;
    }


    @Override
    public void addProduct(String dictName, String bulk, String dictUnit, String size, String productTrait, String nameQual) {
        productDao.addProduct(dictName,bulk,dictUnit,size,productTrait,nameQual);
    }

    @Override
    public List<DataBase> queryAllDataName() {
        return productDao.queryAllDataName();
    }

    @Override
    public DataBase queryDataBaseByDataName(String dataName) {
        return productDao.queryDataBaseByDataName(dataName);
    }

    @Override
    public String queryMaxFromDataBase() {
        int max = productDao.queryMaxDataValue();
        String newMax = (max+1)+"";
        return newMax;
    }

    @Override
    public void saveDataBase(DataBase dataBase) {
        productDao.saveDataBase(dataBase.getDetail(),dataBase.getDataType(),dataBase.getDataName(),dataBase.getDataValue());
    }

    @Override
    public List<Products> queryAllProduct() {
        return productDao.queryAllProduct();
    }

    @Override
    public List<Products> queryAllProductByPage(int startRow, int pageSize) {
        return productDao.queryAllProductByPage(startRow,pageSize);
    }

    @Override
    public void delete(String id) {
        productDao.delete(id);
    }

    @Override
    public Products queryProductById(String id) {
        return productDao.queryProductById(id);
    }

    @Override
    public void editProduct(String id, String dictName, String bulk, String dictUnit, String size, String productTrait, String nameQual) {
        productDao.editProduct(id,dictName,bulk,dictUnit,size,productTrait,nameQual);
    }




    @Override
    public String queryProductIdByName(String newProduct) {
        String id = productDao.queryProductIdByName(newProduct);
        if (id == null){
            //生成新字典
            productDao.saveDataBase("产品类型","1",newProduct,((productDao.queryMaxDataValue() + 1) + ""));
            id = productDao.queryProductIdByName(newProduct);
            return id;
        } else {
            return id;
        }
    }

    @Override
    public String queryUnitIdByName(String unit) {
        return productDao.queryUnitIdByName(unit);
    }

    @Override
    public String queryFeatureIdByName(String feature) {
        String id = productDao.queryFeatureIdByName(feature);
        if (id == null){
            //生成新字典
            String max = productDao.queryDictMaxByFeature();
            String max1 = (max + 1) +"";

            productDao.addDictByFeature(feature,max1);
            id = productDao.queryFeatureIdByName(feature);
            return id;
        } else {
            return id;
        }
    }

    @Override
    public String queryIdByName(String productName) {
        return productDao.queryIdByName(productName);
    }
}
