package com.soecode.lyf.service.impl;

import com.soecode.lyf.dao.PriceDao;
import com.soecode.lyf.entity.AllHead;
import com.soecode.lyf.service.BillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by lsd on 2017-03-20.
 */
@Service
public class BillServiceImpl implements BillService {
    // 注入Service依赖
    @Autowired
    PriceDao priceDao;

    /**
     *
     * @param payment_date
     * @param document_number
     * @param input_man
     * @param brokerage
     * @param document_type
     * @param creation_date
     */
    @Override
    public void saveCash(String payment_date,String document_number,String input_man,String brokerage,String document_type,String creation_date,String salesMan) {
        String salesManId = priceDao.querySalesManId(salesMan);
        priceDao.saveAllHead(payment_date,document_number,input_man,brokerage,document_type,creation_date,salesManId);
    }

    @Override
    public void saveCashPrice(String listNum,String customrtName,String money,String customerPhone,String numChengdui,String dateChengdui,String returnType,
                              String chengduiSendUnit,String chengduiToUnit,String chengduiBang,String customerAccount,String dianhuiBankName,
                              String dianhuiPayAccount,String dianhuiCollectionAccount,String dianhuiContext,String note,String zhuanzhangPayAccount,String zhuanzhangCollectionAccount) {
        priceDao.savePrice(listNum,customrtName,money,customerPhone,numChengdui,dateChengdui,returnType,chengduiSendUnit,chengduiToUnit,chengduiBang,customerAccount,dianhuiBankName,dianhuiPayAccount,dianhuiCollectionAccount,dianhuiContext,zhuanzhangPayAccount,zhuanzhangCollectionAccount,note);
    }

//    @Override
//    public void saveElectPrice(String pin_ID,String payment_customer_name,String cash_amount,String customer_contact,String note) {
//        priceDao.savePrice(pin_ID,payment_customer_name,cash_amount,customer_contact,"","",note);
//    }
//
//    @Override
//    public void saveAcceptPrice(String pin_ID,String payment_customer_name, String acceptance_amount, String date_acceptance,
//                                String acceptance_number,String customer_contact,String note) {
//        priceDao.savePrice(pin_ID,payment_customer_name,acceptance_amount,date_acceptance,acceptance_number,customer_contact,note);
//
//    }



    @Override
    public void editCash(String payment_date, String document_number, String input_man, String brokerage, String document_type, String creation_date) {
        Long id = priceDao.queryIdByNumber(document_number);
        priceDao.editCash(id,payment_date,document_number,input_man,brokerage,document_type,creation_date);
    }

    @Override
    public void editCashPrice(Long id,String listNum,String payment_customer_name, String cash_amount, String customer_contact, String note) {
        priceDao.editCashPrice(id,listNum,payment_customer_name,cash_amount,customer_contact,note);
    }

    @Override
    public void editAcceptPrice(Long id, String listNum, String customrtName, String money, String customerPhone, String numChengdui, String dateChengdui, String note) {
        priceDao.editAcceptPrice(id,listNum,customrtName,money,customerPhone,numChengdui,dateChengdui,note);
    }

    @Override
    public void saveReturnMoney(String document_number, String input_man, String brokerage, String document_type, String creation_date) {
        priceDao.saveReturnMoneyAllHead(document_number,input_man,brokerage,document_type,creation_date);
    }

    @Override
    public void saveReturnMoneyPrice(String list_num, String customrt_name, String money, String customer_phone, String note, String end_data, String return_type, String customer_account) {
        priceDao.saveReturnMoneyPrice(list_num,customrt_name,money,customer_phone,note,end_data,return_type,customer_account);
    }

    @Override
    public void editReturnMoney(String creation_date, String document_number, String input_man, String brokerage, String document_type) {
        priceDao.editReturnMoney(creation_date,document_number,input_man,brokerage,document_type);
    }

    @Override
    public void editReturnMoneyPrice(Long uId, String document_number, String payment_customer_name, String acceptance_amount, String customer_contact, String return_type, String customer_account, String end_data, String note) {
        priceDao.editReturnMoneyPrice(uId,document_number,payment_customer_name,acceptance_amount,customer_contact,return_type,customer_account,end_data,note);
    }
}
