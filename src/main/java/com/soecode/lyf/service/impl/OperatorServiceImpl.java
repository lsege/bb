package com.soecode.lyf.service.impl;

import com.soecode.lyf.dao.OperatorDao;
import com.soecode.lyf.entity.OperatorVo;
import com.soecode.lyf.entity.SalesMan;
import com.soecode.lyf.service.OperatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017/8/21.
 */
@Service
public class OperatorServiceImpl implements OperatorService {

    @Autowired
    OperatorDao operatorDao;

    @Override
    public SalesMan queryOperatorByAccount(String account) {
        return operatorDao.queryOperatorByAccount(account);
    }
    @Override
    public void addOperator(String name, String phone) {
        String state = "1";
        operatorDao.saveOperator(name,phone,state);
    }

    @Override
    public void deleteOperatorById(Long id) {
        operatorDao.deleteOperatorById(id);
    }

    @Override
    public SalesMan queryUserInfo(Long id) {
        return operatorDao.queryUserInfo(id);
    }
    @Override
    public void editOperator(Long id,String name, String phone) {
        operatorDao.editOperator(id,name,phone);
    }

    @Override
    public List<SalesMan> queryAllUsers() {
        return operatorDao.queryAllUsers();
    }

    @Override
    public void editUser(Long uId, String name) {
        operatorDao.editUser(uId,name);
    }

    @Override
    public void editUserPassord(Long uId, String name, String password) {
        operatorDao.editUserPassord(uId,name,password);
    }
}
