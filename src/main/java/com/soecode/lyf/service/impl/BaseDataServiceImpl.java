package com.soecode.lyf.service.impl;

import com.soecode.lyf.dao.BaseDataDao;
import com.soecode.lyf.dao.OrderDao;
import com.soecode.lyf.entity.ReturnOrder;
import com.soecode.lyf.entity.SalesMan;
import com.soecode.lyf.entity.vo.*;
import com.soecode.lyf.service.BaseDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by 10091 on 2017-05-25.
 */
@Service
public class BaseDataServiceImpl implements BaseDataService {
    @Autowired
    BaseDataDao baseDataDao;
    @Autowired
    OrderDao orderDao;

    /**
     * 查询仓库
     *
     * @return
     */
    public List<StorageVO> queryStorage(){
        return baseDataDao.queryStorage();
    }

    public List<ProductVO> queryAll() {
        return baseDataDao.queryAll();
    }
    public List<ProductVO> queryAll2() {
        return baseDataDao.queryAll2();
    }


    /**
     * 查询销售人员
     * @return
     */
    public List<SalesMan> selectSales(){
        return baseDataDao.selectSales();
    }

    @Override
    public List<CustomerVO> selectCustomer() {
        return baseDataDao.selectCustomer();
    }

    @Override
    public int selectCustomerById(int id) {
        return baseDataDao.selectCustomerById(id);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveSales(PostData data) {
        if(null != data){
            //查询销售人员ID
            Date data1 = new Date();
            orderDao.saveOrder(data.getBillNo(),data.getContactName(),data.getContactId(),data.getTotalQty(),data.getTotalAmount(),data.getTotalPushMoney(),data.getReviewerMan(),data.getInputMan(),data.getPayDate(),data1,1,1,data.getPayDate(),data.getEntries().get(0).getDescription(),1,1);
            if(null != data.getEntries()){
                for(EntrieData ls : data.getEntries()){
                    orderDao.saveOrderDetail(ls.getInvId().toString(),null,ls.getQty().toString(),ls.getPrice().toString(),ls.getAmount().toString(),ls.getDescription(),null,"1",data.getBillNo(),ls.getStorageId(),ls.getPushMoney().toString());
                }
            }
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveReturn(PostData data) {
        if(null != data){
            orderDao.saveReturn(data.getContactName(),data.getBillNo(),data.getInputMan(),data.getReviewerMan(),data.getPayDate(),data.getDescription(),data.getGoods(),data.getCount());
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Integer updateSales(PostData data) {
        Integer id= -1;
        if(null != data){
            //全部删除详细信息再插入
            baseDataDao.deleteOrderInfo(data.getBillNo());
            //执行插入
//            orderDao.saveOrder(1,data.getContactName(),data.getDate(),"1",data.getInputMan(),data.getReviewerMan(),data.getTotalQty().toString(),data.getTotalAmount().toString(),data.getBillNo(),"1",null,data.getTotalPushMoney(),data.getWhetherDateint(),data.getWhetherElectint(),data.getPayDate(),data.getContactId(),data.getSalesId());
            if(null != data.getEntries()){
                for(EntrieData ls : data.getEntries()){
                    orderDao.saveOrderDetail(ls.getInvId().toString(),null,ls.getQty().toString(),ls.getPrice().toString(),ls.getAmount().toString(),ls.getDescription(),null,"1",data.getBillNo(),ls.getStorageId(),ls.getPushMoney().toString());
                }
            }
            PostData _obj=baseDataDao.queryOrdersByNo(data.getBillNo());
            if(_obj != null){
                id= _obj.getId();
            }
        }
        return id;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Integer editReturnOrder(PostData data) {
        Integer id = -1;
        orderDao.editReturnOrder(data.getId(),data.getContactName(),data.getBillNo(),data.getInputMan(),data.getReviewerMan(),data.getPayDate(),data.getDescription(),data.getGoods(),data.getCount());
        PostData _obj = baseDataDao.queryReturnOrderByNo(data.getBillNo());

        List<EntrieData> eds = baseDataDao.queryGoodsInfo(_obj.getId());
        _obj.setEntries(eds);

        if(_obj != null){
            id = _obj.getId();
        }
        return id;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveSales2(PostData data) {
        if(null != data){
            orderDao.saveProduct(data.getBillNo(),"","","","2017-02-03","2017-02-03","2017-02-03");
            if(null != data.getEntries()){
                for(EntrieData ls : data.getEntries()){
                    orderDao.saveOrderBill(data.getBillNo(), String.valueOf(ls.getInvId()), String.valueOf(ls.getQty()), String.valueOf(ls.getAmount()));
                }
            }
        }
    }

    @Override
    public void saveSalesman(String name, String phone) {
        orderDao.saveSalesman(name,phone);
    }

    @Override
    public void saveSaleCustomer(String name1, String phone1, String address, String people, String wheh) {
        orderDao.saveSaleCustomer(name1,phone1,address,people,wheh);
    }

    @Override
    public void saveUser(String name, String pwd) {
        orderDao.saveUser(name,pwd);
    }

    @Override
    public List<SalesVO> queryOrders(QueryVO vo) {
        return baseDataDao.queryOrders(vo);
    }
    @Override
    public int queryOrdersCount(QueryVO vo) {
        return baseDataDao.queryOrdersCount(vo);
    }

    @Override
    public List<Sales2Vo> queryOrders2(QueryVO vo) {
        return baseDataDao.queryOrders3(vo);
    }

    @Override
    public int queryOrders2Count(QueryVO vo) {
        return baseDataDao.queryOrders3Count(vo);
    }

    /**
     * 查询订单详情
     * @param id
     * @return
     */
    @Override
    public PostData selectOrderDetail(int id){
        PostData pd=  baseDataDao.queryOrders2(id);
        List<EntrieData> eds= baseDataDao.queryOrdersDetail(pd.getBillNo());
        pd.setEntries(eds);
        return  pd;
    }
    @Override
    public PostData selectOrderDetai2(int id){
        PostData pd=  baseDataDao.queryOrders4(id);
        List<EntrieData> eds = baseDataDao.queryGoodsInfo(pd.getId());
        pd.setEntries(eds);
        return  pd;
    }


    @Override
    public ProductVO queryData(String id) {
        return baseDataDao.queryData(id);
    }
}
