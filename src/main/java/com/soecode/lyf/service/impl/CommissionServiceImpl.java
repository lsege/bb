package com.soecode.lyf.service.impl;

import com.soecode.lyf.dao.CommissionDao;
import com.soecode.lyf.entity.SalesManCommission;
import com.soecode.lyf.service.CommissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017/10/9.
 */
@Service
public class CommissionServiceImpl implements CommissionService {

    @Autowired
    CommissionDao commissionDao;

    @Override
    public List<SalesManCommission> queryAllCommission() {
        return commissionDao.queryAllCommission();
    }

    @Override
    public void editCommission(Integer id, Double commission) {
        commissionDao.editCommission(id,commission);
    }

    @Override
    public List<SalesManCommission> queryAllCommissionByUser(String userId) {
        return commissionDao.queryAllCommissionByUser(userId);
    }
}
