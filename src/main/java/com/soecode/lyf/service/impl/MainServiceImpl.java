package com.soecode.lyf.service.impl;

import com.soecode.lyf.dao.MainDao;
import com.soecode.lyf.entity.User;
import com.soecode.lyf.service.MainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 2018/1/9.
 */
@Service
public class MainServiceImpl implements MainService {

    @Autowired
    MainDao mainDao;

    @Override
    public User selectUser(String name, String pwd) {
        return mainDao.selectUser(name,pwd);
    }
}
