package com.soecode.lyf.service.impl;

import com.soecode.lyf.dao.OrderDao;
import com.soecode.lyf.entity.*;
import com.soecode.lyf.entity.vo.MoneyAccept;
import com.soecode.lyf.entity.vo.OrderList;
import com.soecode.lyf.service.OrderService;

import java.text.SimpleDateFormat;
import java.util.*;
import com.soecode.lyf.util.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lsd on 2017-03-16.
 */
@Service
public class OrderServiceImpl implements OrderService {
    // 注入Service依赖
    @Autowired
    OrderDao orderDao;

    /**查询所有商品*/
    @Override
    public List<ProductBase> queryAllProduct() {
        return orderDao.queryAllProduct();
    }

    /**分页查询所有商品*/
    @Override
    public List<ProductBase> queryAllProductByPage(Integer startRow, Integer pageSize) {
        return orderDao.queryAllProductByPage(startRow,pageSize);
    }

    /**查询所有客户*/
    @Override
    public List<Customer> queryAllCustomer() {
        return orderDao.queryAllCustomer();
    }

    /**分页查询所有客户*/
    @Override
    public List<Customer> queryAllCustomerByPage(Integer startRow, Integer pageSize) {
        return orderDao.queryAllCustomerByPage(startRow,pageSize);
    }

    /**查询所有销售人员*/
    @Override
    public List<SalesMan> queeeryAllSalesMan() {
        return orderDao.queeeryAllSalesMan();
    }

    /**分页查询所有销售人员*/
    @Override
    public List<SalesMan> querySalesManByPage(Integer startRow, Integer pageSize) {
        return orderDao.querySalesManByPage(startRow,pageSize);
    }

    /**根据单据编号查询销货单*/
    @Override
    public List<SalesOrders> queryOrderByNumber(String documentNumber) {
        return orderDao.queryOrderByNumber(documentNumber);
    }

    /**查询客户ID*/
    @Override
    public String queryCustomerIdByName(String customerName) {
        return orderDao.queryCustomerIdByName(customerName);
    }

    /**查询销售ID*/
    @Override
    public String querySalesManId(String salesmanName) {
        return orderDao.querySalesManId(salesmanName);
    }

    /**保存销货单信息*/
    @Override
    public void addSalesOrder(String customerId, String createDate, String salesManId, String documentNumber, String reviewOfficer, String entryPersonnel, String regularCustomer, String salesMoney,String dueDate1,String dueDate2,String dueDate3,String dueDate4, String payDate, String whetherElectint, String pushMoney,String pSum,String wSum) {
        orderDao.addSalesOrder(customerId,createDate,salesManId,documentNumber,reviewOfficer,entryPersonnel,regularCustomer,salesMoney,dueDate1,dueDate2,dueDate3,dueDate4,payDate,whetherElectint,pushMoney,pSum,wSum);
    }

    /**查询商品ID*/
    @Override
    public String queryProductId(String s) {
        return orderDao.queryProductId(s);
    }

    /**保存销货单销售信息*/
    @Override
    public void addPaymentDetails(String documentNumber, String bProductId, String bCount, String bPrice, String bMoney, String bBak, String wProductId, String wCount, String wPrice, String wMoney) {
        orderDao.addPaymentDetails(documentNumber,bProductId,bCount,bPrice,bMoney,bBak,wProductId,wCount,wPrice,wMoney);
    }

    /**保存客户欠款信息*/
    @Override
    public void addCustomerArrears(String customerId, String documentNumber, String pSum, String wSum) {
        Double p = Double.parseDouble(pSum);
        Double w = Double.parseDouble(wSum);
        Double s = p+w;
        orderDao.addCustomerArrears(customerId,documentNumber,s);
    }































    /**
     *
     * @param type
     * @param custmerName
     * @param date
     * @param whether
     * @param input_man
     * @param reviewer
     * @param totalQty
     * @param totalAmount
     * @param number
     * @param del
     * @param criate
     */
    @Override
    public void saveOrderList(Integer type,String custmerName,String date,String whether,String input_man,String reviewer,
                          String totalQty,String totalAmount,String number,String del,String criate,String totalPushMoney,String whetherDateint,String whetherElectint,String payDate) {
        System.out.println(type+"=="+custmerName+"=="+date+"=="+whether+"=="+input_man+"=="+reviewer+"=="+totalQty+"=="+totalAmount+"=="+number+"=="+del+"=="+criate);
        //orderDao.saveOrder(type,custmerName,date,whether,input_man,reviewer,totalQty,totalAmount,number,del,criate,totalPushMoney,whetherDateint,whetherElectint,payDate);
    }
    @Override
    public void saveProductList(String billNo, String sales_people, String time, String make_man, String make_date,
                                String input_man, String input_date, String reviewer_man, String reviewer_date, String numNo) {
//        orderDao.saveProduct(billNo,sales_people,time,"1",make_man,make_date,input_man,input_date,reviewer_man,reviewer_date,numNo);
    }

    /**
     *
     * @param goods
     * @param specificationModel
     * @param qty
     * @param price
     * @param amount
     * @param descrip
     * @param criate
     * @param item_type
     * @param pin_ID
     */
    @Override
    public void saveOrderDetail(String goods,String specificationModel,  String qty, String price,
                          String amount,String descrip,String criate,String item_type,String pin_ID,String storageId,String pushMoney) {
        orderDao.saveOrderDetail(goods,specificationModel,qty,price,amount,descrip,criate,item_type,pin_ID,storageId,pushMoney);
    }

    @Override
    public List<SalesOrders> selectList(Integer firstResult,Integer maxResults,String keyword) {
        return orderDao.selectList(firstResult,maxResults,keyword);
    }

    @Override
    public List<SalesOrders> getTotal(String keyword) {
        return orderDao.getTotal(keyword);
    }

    @Override
    public User selectUser(String name, String pwd) {
        return orderDao.selectUser(name,pwd);
    }

    @Override
    public ArrayList selectByCustomer(String customer) {
        return orderDao.findByCustomer(customer);
    }

    @Override
    public List<Product> findList(Integer id) {
        return orderDao.selectProduct(id);
    }

    @Override
    public List<MoneyAccept> queryAllOrder() {
        return orderDao.queryAllOrder();
    }

    @Override
    public List<MoneyAccept> queryAllOrderByPage(int startRow, int pageSize) {
        return orderDao.queryAllOrderByPage(startRow,pageSize);
    }

    @Override
    public OrderList queryAllOrderById(Long id) {
        return orderDao.queryOrderById(id);
    }

    @Override
    public ReturnMoney queryReturnOrderById(Long id) {
        return orderDao.queryReturnOrderById(id);
}

    @Override
    public void delete(String id) {
        Long uId = Long.parseLong(id);
        String listNum = orderDao.queryOrderNumById(uId);
        orderDao.deleteAllHeadPayment(listNum);
        orderDao.deleteMoneyAccept(uId);
    }

    @Override
    public void saveReturnOrder(String returnData, String orderNo, String customerName, String productName, String productType, String count, String remark, String returnReason, String inputMan, String auditorMan) {
        orderDao.saveReturnOrder(returnData,orderNo,customerName,productName,productType,count,remark,returnReason,inputMan,auditorMan);
    }

    @Override
    public void saveReturnMoney(ReturnMoney returnMoney) {
        orderDao.saveReturnMoney(returnMoney);
    }

    @Override
    public void deleteReturnOrder(String id) {
        orderDao.deleteReturnOrder(id);
    }

    @Override
    public void deleteSalesOrder(String id) {
        Long uId = Long.parseLong(id);
        String listNum = orderDao.querySalesOrder(uId);
        try {
            orderDao.deletePaymentDetails(listNum);
            orderDao.deleteSalesOrders(id);
        } catch (Exception e){
            e.printStackTrace();
        }
    }



    @Override
    public List<CustomerArrears> querySalesOrderByCustomer(String customer) {
        String customerId = orderDao.queryCustomerIdByName(customer);
        return orderDao.querySalesOrderByCustomer(customerId);
    }

    @Override
    public void cancelSalesOrder(String goodsName, String price) {
        //根据客户名查询客户ID
        String customerId = orderDao.queryCustomerIdByName(goodsName);
        //根据客户ID查询当前客户所有未付全的欠款单(根据发货日期排序)
        List<CustomerArrears> customerArrears = orderDao.queryAllCustomerArrearsByCustomerId(customerId);
        //查询客户钱包是否有余额
        Double surplusMoney = Double.parseDouble(orderDao.querySurplusMoneyByCustomerId(customerId));
        //此次核销金额
        Double money = Double.parseDouble(price);
        if (surplusMoney == null || surplusMoney == 0.0){
            //此客户无余额，使用金额进行核销
            for (int i=0; i<customerArrears.size(); i++){
                if (customerArrears.get(i).getAlreadyMoney().equals("0")){
                    //此单据还未付款
                    if (money >= Double.parseDouble(customerArrears.get(i).getRequireMoney())){
                        //金额足够核销此单据
                        String alreadyMoney = customerArrears.get(i).getRequireMoney();
                        String isToComplete = "1";
                        Date date = new Date(System.currentTimeMillis());
                        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
                        String date1 = sf.format(date);
                        orderDao.cancelSalesOrder(customerArrears.get(i).getId(),alreadyMoney,isToComplete,date1);
                        money = money - ((Double.parseDouble(customerArrears.get(i).getRequireMoney())) - (Double.parseDouble(customerArrears.get(i).getAlreadyMoney())));
                        orderDao.editSalesOrder(customerArrears.get(i).getDocumentNumber(),date1);
                    } else {
                        //金额不足核销此单据
                        String alreadyMoney = money+"";
                        String isToComplete = "0";
                        String date1 = "未付清";
                        orderDao.cancelSalesOrder(customerArrears.get(i).getId(),alreadyMoney,isToComplete,date1);
                        money = 0.0;
                        orderDao.editSalesOrder(customerArrears.get(i).getDocumentNumber(),date1);
                    }
                } else {
                    //此单据已付款，但未付清
                    if (money >= (Double.parseDouble(customerArrears.get(i).getRequireMoney()))-(Double.parseDouble(customerArrears.get(i).getAlreadyMoney()))){
                        //金额足够核销此单据
                        String alreadyMoney = customerArrears.get(i).getRequireMoney();
                        String isToComplete = "1";
                        Date date = new Date(System.currentTimeMillis());
                        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
                        String date1 = sf.format(date);
                        orderDao.cancelSalesOrder(customerArrears.get(i).getId(),alreadyMoney,isToComplete,date1);
                        money = money - ((Double.parseDouble(customerArrears.get(i).getRequireMoney())) - (Double.parseDouble(customerArrears.get(i).getAlreadyMoney())));
                        orderDao.editSalesOrder(customerArrears.get(i).getDocumentNumber(),date1);
                    } else {
                        //金额不足核销此单据
                        String alreadyMoney = money+(Double.parseDouble(customerArrears.get(i).getAlreadyMoney()))+"";
                        String isToComplete = "0";
                        String date1 = "未付清";
                        orderDao.cancelSalesOrder(customerArrears.get(i).getId(),alreadyMoney,isToComplete,date1);
                        money = 0.0;
                        orderDao.editSalesOrder(customerArrears.get(i).getDocumentNumber(),date1);
                    }
                }
            }
            if (money > 0){
                //存入客户钱包
                orderDao.surplusMoney(customerId,money+"");
            }
        } else {
            //客户有余额存在
            for (int i=0; i<customerArrears.size(); i++){
                if (customerArrears.get(i).getAlreadyMoney().equals("0")){
                    //此单据未付款
                    if (surplusMoney >= Double.parseDouble(customerArrears.get(i).getRequireMoney())){
                        //余额足够核销此单据
                        String alreadyMoney = customerArrears.get(i).getRequireMoney();
                        String isToComplete = "1";
                        Date date = new Date(System.currentTimeMillis());
                        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
                        String date1 = sf.format(date);
                        orderDao.cancelSalesOrder(customerArrears.get(i).getId(),alreadyMoney,isToComplete,date1);
                        surplusMoney = surplusMoney - ((Double.parseDouble(customerArrears.get(i).getRequireMoney())) - (Double.parseDouble(customerArrears.get(i).getAlreadyMoney())));
                        orderDao.editSalesOrder(customerArrears.get(i).getDocumentNumber(),date1);
                    } else {
                        //余额不足核销此单据
                        if (surplusMoney+money >= Double.parseDouble(customerArrears.get(i).getRequireMoney())){
                            //余额加金额足够核销单据
                            String alreadyMoney = customerArrears.get(i).getRequireMoney();
                            String isToComplete = "1";
                            Date date = new Date(System.currentTimeMillis());
                            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
                            String date1 = sf.format(date);
                            orderDao.cancelSalesOrder(customerArrears.get(i).getId(),alreadyMoney,isToComplete,date1);
                            orderDao.editSalesOrder(customerArrears.get(i).getDocumentNumber(),date1);
                            money = money - ((Double.parseDouble(customerArrears.get(i).getRequireMoney())) - (Double.parseDouble(customerArrears.get(i).getAlreadyMoney())) - surplusMoney);
                            //余额已用光，删除客户余额
                            surplusMoney = 0.0;
                            orderDao.surplusMoney(customerId,surplusMoney+"");
                        } else {
                            //余额加金额，不足核销此单据
                            String alreadyMoney = surplusMoney+money+"";
                            String isToComplete = "0";
                            String date1 = "未付清";
                            orderDao.cancelSalesOrder(customerArrears.get(i).getId(),alreadyMoney,isToComplete,date1);
                            //余额已用光，删除客户余额
                            surplusMoney = 0.0;
                            money = 0.0;
                            orderDao.surplusMoney(customerId,surplusMoney+"");
                            orderDao.editSalesOrder(customerArrears.get(i).getDocumentNumber(),date1);
                        }
                    }
                } else {
                    //此单据已付款但未付清
                    if (surplusMoney >= ((Double.parseDouble(customerArrears.get(i).getRequireMoney())) - (Double.parseDouble(customerArrears.get(i).getAlreadyMoney())))){
                        //余额足够核销此单据
                        String alreadyMoney = customerArrears.get(i).getRequireMoney();
                        String isToComplete = "1";
                        Date date = new Date(System.currentTimeMillis());
                        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
                        String date1 = sf.format(date);
                        orderDao.cancelSalesOrder(customerArrears.get(i).getId(),alreadyMoney,isToComplete,date1);
                        surplusMoney = surplusMoney - ((Double.parseDouble(customerArrears.get(i).getRequireMoney())) - (Double.parseDouble(customerArrears.get(i).getAlreadyMoney())));
                        orderDao.editSalesOrder(customerArrears.get(i).getDocumentNumber(),date1);
                    } else {
                        //余额不足核销此单据
                        if (surplusMoney+money >= Double.parseDouble(customerArrears.get(i).getRequireMoney())){
                            //余额加金额足够核销单据
                            String alreadyMoney = customerArrears.get(i).getRequireMoney();
                            String isToComplete = "1";
                            Date date = new Date(System.currentTimeMillis());
                            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
                            String date1 = sf.format(date);
                            orderDao.cancelSalesOrder(customerArrears.get(i).getId(),alreadyMoney,isToComplete,date1);
                            money = money - ((Double.parseDouble(customerArrears.get(i).getRequireMoney())) - (Double.parseDouble(customerArrears.get(i).getAlreadyMoney())) - surplusMoney);
                            //余额已用光，删除客户余额
                            surplusMoney = 0.0;
                            orderDao.surplusMoney(customerId,surplusMoney+"");
                            orderDao.editSalesOrder(customerArrears.get(i).getDocumentNumber(),date1);
                        } else {
                            //余额加金额，不足核销此单据
                            Double aMoney = Double.parseDouble(customerArrears.get(i).getAlreadyMoney());
                            String alreadyMoney = surplusMoney+money+aMoney+"";
                            String isToComplete = "0";
                            String date1 = "未付清";
                            orderDao.cancelSalesOrder(customerArrears.get(i).getId(),alreadyMoney,isToComplete,date1);
                            //余额已用光，删除客户余额
                            surplusMoney = 0.0;
                            money = 0.0;
                            orderDao.surplusMoney(customerId,surplusMoney+"");
                            orderDao.editSalesOrder(customerArrears.get(i).getDocumentNumber(),date1);
                        }
                    }
                }
            }
            if (surplusMoney > 0.0){
                //余额还有剩余，保存余额
                orderDao.surplusMoney(customerId,surplusMoney+"");
            }
            if (money > 0.0){
                Double surplusMoney2 = Double.parseDouble(orderDao.querySurplusMoneyByCustomerId(customerId));
                System.out.print(surplusMoney2);
                System.out.print(money+surplusMoney2);
                //金额还有剩余，保存余额
                orderDao.surplusMoney(customerId,(money+surplusMoney2)+"");
            }
        }
    }

    @Override
    public void cancelSalesOrderByNumber(String goodsName,String billNo, String price,String payDate) {
        //根据客户名查询客户ID
        String customerId = orderDao.queryCustomerIdByName(goodsName);
        CustomerArrears customerArrears = orderDao.queryCustomerArrearsByNumber(billNo);
        //用户余额
        Double surplusMoney = Double.parseDouble(orderDao.querySurplusMoneyByCustomerId(customerId));
        //此次核销金额
        Double money = Double.parseDouble(price);

        if (surplusMoney == null || surplusMoney == 0.0){
            //此客户无余额，使用金额进行核销
                if (customerArrears.getAlreadyMoney().equals("0")){
                    //此单据还未付款
                    if (money >= Double.parseDouble(customerArrears.getRequireMoney())){
                        //金额足够核销此单据
                        String alreadyMoney = customerArrears.getRequireMoney();
                        String isToComplete = "1";

                        orderDao.cancelSalesOrder(customerArrears.getId(),alreadyMoney,isToComplete,payDate);
                        money = money - ((Double.parseDouble(customerArrears.getRequireMoney())) - (Double.parseDouble(customerArrears.getAlreadyMoney())));
                        orderDao.editSalesOrder(customerArrears.getDocumentNumber(),payDate);
                    } else {
                        //金额不足核销此单据
                        String alreadyMoney = money+"";
                        String isToComplete = "0";
                        String date1 = "未付清";
                        orderDao.cancelSalesOrder(customerArrears.getId(),alreadyMoney,isToComplete,date1);
                        money = 0.0;
                        orderDao.editSalesOrder(customerArrears.getDocumentNumber(),date1);
                    }
                } else {
                    //此单据已付款，但未付清
                    if (money >= (Double.parseDouble(customerArrears.getRequireMoney()))-(Double.parseDouble(customerArrears.getAlreadyMoney()))){
                        //金额足够核销此单据
                        String alreadyMoney = customerArrears.getRequireMoney();
                        String isToComplete = "1";

                        orderDao.cancelSalesOrder(customerArrears.getId(),alreadyMoney,isToComplete,payDate);
                        money = money - ((Double.parseDouble(customerArrears.getRequireMoney())) - (Double.parseDouble(customerArrears.getAlreadyMoney())));
                        orderDao.editSalesOrder(customerArrears.getDocumentNumber(),payDate);
                    } else {
                        //金额不足核销此单据
                        String alreadyMoney = money+(Double.parseDouble(customerArrears.getAlreadyMoney()))+"";
                        String isToComplete = "0";
                        String date1 = "未付清";
                        orderDao.cancelSalesOrder(customerArrears.getId(),alreadyMoney,isToComplete,date1);
                        money = 0.0;
                        orderDao.editSalesOrder(customerArrears.getDocumentNumber(),date1);
                    }
                }
            if (money > 0){
                //存入客户钱包
                orderDao.surplusMoney(customerId,money+"");
            }
        } else {
            //客户有余额存在
                if (customerArrears.getAlreadyMoney().equals("0")){
                    //此单据未付款
                    if (surplusMoney >= Double.parseDouble(customerArrears.getRequireMoney())){
                        //余额足够核销此单据
                        String alreadyMoney = customerArrears.getRequireMoney();
                        String isToComplete = "1";

                        orderDao.cancelSalesOrder(customerArrears.getId(),alreadyMoney,isToComplete,payDate);
                        surplusMoney = surplusMoney - ((Double.parseDouble(customerArrears.getRequireMoney())) - (Double.parseDouble(customerArrears.getAlreadyMoney())));
                        orderDao.editSalesOrder(customerArrears.getDocumentNumber(),payDate);
                    } else {
                        //余额不足核销此单据
                        if (surplusMoney+money >= Double.parseDouble(customerArrears.getRequireMoney())){
                            //余额加金额足够核销单据
                            String alreadyMoney = customerArrears.getRequireMoney();
                            String isToComplete = "1";

                            orderDao.cancelSalesOrder(customerArrears.getId(),alreadyMoney,isToComplete,payDate);
                            orderDao.editSalesOrder(customerArrears.getDocumentNumber(),payDate);
                            money = money - ((Double.parseDouble(customerArrears.getRequireMoney())) - (Double.parseDouble(customerArrears.getAlreadyMoney())) - surplusMoney);
                            //余额已用光，删除客户余额
                            surplusMoney = 0.0;
                            orderDao.surplusMoney(customerId,surplusMoney+"");
                        } else {
                            //余额加金额，不足核销此单据
                            String alreadyMoney = surplusMoney+money+"";
                            String isToComplete = "0";
                            String date1 = "未付清";
                            orderDao.cancelSalesOrder(customerArrears.getId(),alreadyMoney,isToComplete,date1);
                            //余额已用光，删除客户余额
                            surplusMoney = 0.0;
                            money = 0.0;
                            orderDao.surplusMoney(customerId,surplusMoney+"");
                            orderDao.editSalesOrder(customerArrears.getDocumentNumber(),date1);
                        }
                    }
                } else {
                    //此单据已付款但未付清
                    if (surplusMoney >= ((Double.parseDouble(customerArrears.getRequireMoney())) - (Double.parseDouble(customerArrears.getAlreadyMoney())))){
                        //余额足够核销此单据
                        String alreadyMoney = customerArrears.getRequireMoney();
                        String isToComplete = "1";

                        orderDao.cancelSalesOrder(customerArrears.getId(),alreadyMoney,isToComplete,payDate);
                        surplusMoney = surplusMoney - ((Double.parseDouble(customerArrears.getRequireMoney())) - (Double.parseDouble(customerArrears.getAlreadyMoney())));
                        orderDao.editSalesOrder(customerArrears.getDocumentNumber(),payDate);
                    } else {
                        //余额不足核销此单据
                        if (surplusMoney+money >= Double.parseDouble(customerArrears.getRequireMoney())){
                            //余额加金额足够核销单据
                            String alreadyMoney = customerArrears.getRequireMoney();
                            String isToComplete = "1";

                            orderDao.cancelSalesOrder(customerArrears.getId(),alreadyMoney,isToComplete,payDate);
                            money = money - ((Double.parseDouble(customerArrears.getRequireMoney())) - (Double.parseDouble(customerArrears.getAlreadyMoney())) - surplusMoney);
                            //余额已用光，删除客户余额
                            surplusMoney = 0.0;
                            orderDao.surplusMoney(customerId,surplusMoney+"");
                            orderDao.editSalesOrder(customerArrears.getDocumentNumber(),payDate);
                        } else {
                            //余额加金额，不足核销此单据
                            Double aMoney = Double.parseDouble(customerArrears.getAlreadyMoney());
                            String alreadyMoney = surplusMoney+money+aMoney+"";
                            String isToComplete = "0";
                            String date1 = "未付清";
                            orderDao.cancelSalesOrder(customerArrears.getId(),alreadyMoney,isToComplete,date1);
                            //余额已用光，删除客户余额
                            surplusMoney = 0.0;
                            money = 0.0;
                            orderDao.surplusMoney(customerId,surplusMoney+"");
                            orderDao.editSalesOrder(customerArrears.getDocumentNumber(),date1);
                        }
                    }
                }

            if (surplusMoney > 0.0){
                //余额还有剩余，保存余额
                orderDao.surplusMoney(customerId,surplusMoney+"");
            }
            if (money > 0.0){
                Double surplusMoney2 = Double.parseDouble(orderDao.querySurplusMoneyByCustomerId(customerId));
                System.out.print(surplusMoney2);
                System.out.print(money+surplusMoney2);
                //金额还有剩余，保存余额
                orderDao.surplusMoney(customerId,(money+surplusMoney2)+"");
            }
        }
    }

    @Override
    public void addCheckPayment(String moneyNumber, String salesNumber) {
        orderDao.addCheckPayment(moneyNumber,salesNumber);
    }

    @Override
    public List<CustomerArrears> queryArrearsByName(String customer) {
        //查询客户ID
        String customerId = orderDao.queryCustomerIdByName(customer);
        return orderDao.queryArrearsByName(customerId);
    }

    @Override
    public void addMouny(String customer,String price) {
        //查询顾客Id
        String customerId = orderDao.queryCustomerIdByName(customer);
        //查询此顾客的余额
        Double surplusMoney = Double.parseDouble(orderDao.querySurplusMoneyByCustomerId(customerId));
        if (surplusMoney > 0.0){
            Double money = Double.parseDouble(price);
            orderDao.surplusMoney(customerId,(surplusMoney+money)+"");
        } else {
            Double money = Double.parseDouble(price);
            orderDao.surplusMoney(customerId,money+"");
        }
    }

    @Override
    public List<AllHead> queryAllHeads(String billNo) {
        return orderDao.queryAllHeads(billNo);
}

    @Override
    public List<SalesMan> queryAllSalesMan() {
        return orderDao.queryAllSalesMan();
    }


    @Override
    public List<Test> queryTest() {
        return orderDao.queryTest();
    }

    @Override
    public List<MoneyAccept> queryAllOrdersById(String userId) {
        return orderDao.queryAllOrdersById(userId);
    }

    @Override
    public List<MoneyAccept> queryAllOrdersByPage(int startRow, int pageSize, String userId) {
        return orderDao.queryAllOrdersByPage(startRow,pageSize,userId);
    }

    @Override
    public Double queryArrears(String s) {
        return orderDao.queryArrears(s);
    }
}
