package com.soecode.lyf.service;

import com.soecode.lyf.entity.OperatorVo;
import com.soecode.lyf.entity.SalesMan;

import java.util.List;

/**
 * Created by Administrator on 2017/8/21.
 */
public interface OperatorService {

    SalesMan queryOperatorByAccount(String account);
    void addOperator(String name,String phone);

    void deleteOperatorById(Long id);

    SalesMan queryUserInfo(Long id);

    void editOperator(Long id,String name,String phone);

    List<SalesMan> queryAllUsers();

    void editUser(Long uId, String name);

    void editUserPassord(Long uId, String name, String password);
}
