package com.soecode.lyf.service;

import com.soecode.lyf.entity.User;

/**
 * Created by Administrator on 2018/1/9.
 */
public interface MainService {

    User selectUser(String name, String pwd);
}
