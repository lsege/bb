package com.soecode.lyf.service;

import com.soecode.lyf.entity.AllHead;

/**
 * Created by lsd on 2017-03-20.
 */

public interface BillService {
    /**
     *
     * @param payment_date
     * @param document_number
     * @param input_man
     * @param brokerage
     * @param document_type
     * @param creation_date
     */
    void  saveCash(String payment_date,String document_number,String input_man,String brokerage,String document_type,String creation_date,String salesMan);

    void  saveCashPrice(String listNum,String customrtName,String money,String customerPhone,String numChengdui,String dateChengdui,String returnType,
                        String chengduiSendUnit,String chengduiToUnit,String chengduiBang,String customerAccount,String dianhuiBankName,
                        String chengduiPayAccount,String dianhuiCollectionAccount,String chengduiContext,String note,String zhuanzhangPayAccount,String zhuanzhangCollectionAccount);

    void editCash(String payment_date,String document_number,String input_man,String brokerage,String document_type,String creation_date);



    void editCashPrice(Long id,String listNum,String payment_customer_name,String cash_amount,String customer_contact,String note);
//    void  saveElectPrice(String pin_ID,String payment_customer_name,String cash_amount,String customer_contact,String note);
//    void  saveAcceptPrice(String pin_ID,String payment_customer_name, String acceptance_amount, String date_acceptance,
//                          String acceptance_number,String customer_contact,String note);

    void editAcceptPrice(Long id,String listNum,String customrtName,String money,String customerPhone,String numChengdui,String dateChengdui,String note);

    void saveReturnMoney(String document_number,String input_man,String brokerage,String document_type,String creation_date);
    void saveReturnMoneyPrice(String list_num,String customrt_name,String money,String customer_phone,String note,String end_data,String return_type,String customer_account);

    void editReturnMoney(String creation_date,String document_number,String input_man,String brokerage,String document_type);
    void editReturnMoneyPrice(Long uId,String document_number,String payment_customer_name,String acceptance_amount,String customer_contact,String return_type,String customer_account,String end_data,String note);
}
