package com.soecode.lyf.service;

import com.soecode.lyf.entity.Customer;
import com.soecode.lyf.entity.SalesMan;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/8/7.
 */
public interface CustomerService {
    List<Customer> queryAllCustomerByName(String customer_name);
    void addCustomer(String customer_name, String customer_phone, String customer_address, String customer_people, String people_phone, int state_customer, String salesMan);

    List<Customer> queryAllCustomer();
    List<Customer> queryAllCustomerByPage(int startRow,int pageSize);

    void delete(String id);

    Customer queryCustomerById(String id);

    void editCustomer(String id,String customerName,String customerPhone,String customerAddress,String customerPeople,String peoplePhone,String stateCustomer);

    List<SalesMan> queryAllSalesMan();

    List<Customer> loadCustomerName(String s);
}
