package com.soecode.lyf.entity;

/**
 * Created by lsd on 2017-03-14.
 */
public class SalesOrders {
    private Long id;
    private String customerName;
    private String createDate;
    private String salesmanId;
    private String salesmanName;
    private String documentNumber;
    private String inputMan;
    private String reviewerMan;
    private String regularCustomer;
    private String saleMoney;
    private String dueDate1;
    private String dueDate2;
    private String dueDate3;
    private String dueDate4;
    private String payDate;
    private String whetherExtended;
    private String pushMoney;
    private String pSum;
    private String wSum;
    private Integer index;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(String salesmanId) {
        this.salesmanId = salesmanId;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getInputMan() {
        return inputMan;
    }

    public void setInputMan(String inputMan) {
        this.inputMan = inputMan;
    }

    public String getReviewerMan() {
        return reviewerMan;
    }

    public void setReviewerMan(String reviewerMan) {
        this.reviewerMan = reviewerMan;
    }

    public String getRegularCustomer() {
        return regularCustomer;
    }

    public void setRegularCustomer(String regularCustomer) {
        this.regularCustomer = regularCustomer;
    }

    public String getSaleMoney() {
        return saleMoney;
    }

    public void setSaleMoney(String saleMoney) {
        this.saleMoney = saleMoney;
    }

    public String getDueDate1() {
        return dueDate1;
    }

    public void setDueDate1(String dueDate1) {
        this.dueDate1 = dueDate1;
    }

    public String getDueDate2() {
        return dueDate2;
    }

    public void setDueDate2(String dueDate2) {
        this.dueDate2 = dueDate2;
    }

    public String getDueDate3() {
        return dueDate3;
    }

    public void setDueDate3(String dueDate3) {
        this.dueDate3 = dueDate3;
    }

    public String getDueDate4() {
        return dueDate4;
    }

    public void setDueDate4(String dueDate4) {
        this.dueDate4 = dueDate4;
    }

    public String getPayDate() {
        return payDate;
    }

    public void setPayDate(String payDate) {
        this.payDate = payDate;
    }

    public String getWhetherExtended() {
        return whetherExtended;
    }

    public void setWhetherExtended(String whetherExtended) {
        this.whetherExtended = whetherExtended;
    }

    public String getPushMoney() {
        return pushMoney;
    }

    public void setPushMoney(String pushMoney) {
        this.pushMoney = pushMoney;
    }

    public String getpSum() {
        return pSum;
    }

    public void setpSum(String pSum) {
        this.pSum = pSum;
    }

    public String getwSum() {
        return wSum;
    }

    public void setwSum(String wSum) {
        this.wSum = wSum;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }
}
