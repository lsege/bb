package com.soecode.lyf.entity;

/**
 * Created by Administrator on 2017/9/6.
 */
public class CustomerArrears {
    private Long id;
    private String customerId;
    private String documentNumber;
    private String requireMoney;
    private String alreadyMoney;
    private String isToComplete;
    private String completionDate;
    private String money;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getRequireMoney() {
        return requireMoney;
    }

    public void setRequireMoney(String requireMoney) {
        this.requireMoney = requireMoney;
    }

    public String getAlreadyMoney() {
        return alreadyMoney;
    }

    public void setAlreadyMoney(String alreadyMoney) {
        this.alreadyMoney = alreadyMoney;
    }

    public String getIsToComplete() {
        return isToComplete;
    }

    public void setIsToComplete(String isToComplete) {
        this.isToComplete = isToComplete;
    }

    public String getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(String completionDate) {
        this.completionDate = completionDate;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }
}
