package com.soecode.lyf.entity;

import java.io.Serializable;

/**
 * 商品详情
 * Created by 10091 on 2017-05-25.
 */
public class ProductBase implements Serializable{

    /*
FieldTypeComment
idint(11) NOT NULL
product_type int(11) NULL
product_vol float NULL
vol_unit int(11) NULL
vol_qual varchar(100) NULL
product_trait int(11) NULL
name_qual varchar(100) NULL汇总名称
del_flag*/

    private String id;
    private String productType;
    private String nameQual;
    private int delFlag;
    private String volQual;
    private String dataValue;
    private Integer index;

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getNameQual() {
        return nameQual;
    }

    public void setNameQual(String nameQual) {
        this.nameQual = nameQual;
    }

    public int getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(int delFlag) {
        this.delFlag = delFlag;
    }

    public String getVolQual() {
        return volQual;
    }

    public void setVolQual(String volQual) {
        this.volQual = volQual;
    }

    public String getDataValue() {
        return dataValue;
    }

    public void setDataValue(String dataValue) {
        this.dataValue = dataValue;
    }
}
