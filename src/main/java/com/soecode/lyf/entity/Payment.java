package com.soecode.lyf.entity;

/**
 * Created by Administrator on 2017/8/24.
 */
public class Payment {
    private String id;
    private String orderNummber;
    private String bProduct;
    private String bCount;
    private String bPrice;
    private String bMoney;
    private String bBak;
    private String wProduct;
    private String wCount;
    private String wPrice;
    private String wMoney;
    private int index;

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    private String html;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderNummber() {
        return orderNummber;
    }

    public void setOrderNummber(String orderNummber) {
        this.orderNummber = orderNummber;
    }

    public String getbProduct() {
        return bProduct;
    }

    public void setbProduct(String bProduct) {
        this.bProduct = bProduct;
    }

    public String getbCount() {
        return bCount;
    }

    public void setbCount(String bCount) {
        this.bCount = bCount;
    }

    public String getbPrice() {
        return bPrice;
    }

    public void setbPrice(String bPrice) {
        this.bPrice = bPrice;
    }

    public String getbMoney() {
        return bMoney;
    }

    public void setbMoney(String bMoney) {
        this.bMoney = bMoney;
    }

    public String getbBak() {
        return bBak;
    }

    public void setbBak(String bBak) {
        this.bBak = bBak;
    }

    public String getwProduct() {
        return wProduct;
    }

    public void setwProduct(String wProduct) {
        this.wProduct = wProduct;
    }

    public String getwCount() {
        return wCount;
    }

    public void setwCount(String wCount) {
        this.wCount = wCount;
    }

    public String getwPrice() {
        return wPrice;
    }

    public void setwPrice(String wPrice) {
        this.wPrice = wPrice;
    }

    public String getwMoney() {
        return wMoney;
    }

    public void setwMoney(String wMoney) {
        this.wMoney = wMoney;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
