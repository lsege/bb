package com.soecode.lyf.entity;

/**
 * Created by Administrator on 2017/9/21.
 */
public class SalesManCommission {
    private String id;
    private String dataType;
    private String min;
    private String max;
    private String proportion;
    private String atype;
    private String commissionStandard;
    private String userName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public String getMax() {
        return max;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public String getProportion() {
        return proportion;
    }

    public void setProportion(String proportion) {
        this.proportion = proportion;
    }

    public String getAtype() {
        return atype;
    }

    public void setAtype(String atype) {
        this.atype = atype;
    }

    public String getCommissionStandard() {
        return commissionStandard;
    }

    public void setCommissionStandard(String commissionStandard) {
        this.commissionStandard = commissionStandard;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
