package com.soecode.lyf.entity;

/**
 * Created by Administrator on 2017/8/16.
 */
public class ReturnMoney {
    private int id;
    private String creataData;
    private String no;
    private String returnCustomer;
    private String returnPhone;
    private String returnMoney;
    private String CustomerAccount;
    private String endData;
    private String inputMan;
    private String auditorMan;
    private String noto;
    private String returnType;

    public String getCreataData() {
        return creataData;
    }

    public void setCreataData(String creataData) {
        this.creataData = creataData;
    }

    public String getReturnType() {
        return returnType;
    }

    public void setReturnType(String returnType) {
        this.returnType = returnType;
    }

    public String getNoto() {
        return noto;
    }

    public void setNoto(String noto) {
        this.noto = noto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreateData() {
        return creataData;
    }

    public void setCreateData(String creataData) {
        this.creataData = creataData;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getReturnCustomer() {
        return returnCustomer;
    }

    public void setReturnCustomer(String returnCustomer) {
        this.returnCustomer = returnCustomer;
    }

    public String getReturnPhone() {
        return returnPhone;
    }

    public void setReturnPhone(String returnPhone) {
        this.returnPhone = returnPhone;
    }

    public String getReturnMoney() {
        return returnMoney;
    }

    public void setReturnMoney(String returnMoney) {
        this.returnMoney = returnMoney;
    }

    public String getCustomerAccount() {
        return CustomerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        CustomerAccount = customerAccount;
    }

    public String getEndData() {
        return endData;
    }

    public void setEndData(String endData) {
        this.endData = endData;
    }

    public String getInputMan() {
        return inputMan;
    }

    public void setInputMan(String inputMan) {
        this.inputMan = inputMan;
    }

    public String getAuditorMan() {
        return auditorMan;
    }

    public void setAuditorMan(String auditorMan) {
        this.auditorMan = auditorMan;
    }
}
