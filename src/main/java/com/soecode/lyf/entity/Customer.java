package com.soecode.lyf.entity;

/**
 * Created by lsd on 2017-04-15.
 */
public class Customer {
    private String customerId;
    private String customerName;
    private String customerPhone;
    private String cusstomerAddress;
    private String customerPeople;
    private String peoplePhone;
    private String stateCustomer;
    private String salesMan;
    private String inData;

    private int index;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCusstomerAddress() {
        return cusstomerAddress;
    }

    public void setCusstomerAddress(String cusstomerAddress) {
        this.cusstomerAddress = cusstomerAddress;
    }

    public String getCustomerPeople() {
        return customerPeople;
    }

    public void setCustomerPeople(String customerPeople) {
        this.customerPeople = customerPeople;
    }

    public String getPeoplePhone() {
        return peoplePhone;
    }

    public void setPeoplePhone(String peoplePhone) {
        this.peoplePhone = peoplePhone;
    }

    public String getStateCustomer() {
        return stateCustomer;
    }

    public void setStateCustomer(String stateCustomer) {
        this.stateCustomer = stateCustomer;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getSalesMan() {
        return salesMan;
    }

    public void setSalesMan(String salesMan) {
        this.salesMan = salesMan;
    }

    public String getInData() {
        return inData;
    }

    public void setInData(String inData) {
        this.inData = inData;
    }
}
