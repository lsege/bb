package com.soecode.lyf.entity;

import java.util.List;

/**
 * Created by Administrator on 2018/1/11.
 */
public class SuggestResult {
    private List<String[]> result;
    private String tmall;

    public List<String[]> getResult() {
        return result;
    }

    public void setResult(List<String[]> result) {
        this.result = result;
    }

    public String getTmall() {
        return tmall;
    }

    public void setTmall(String tmall) {
        this.tmall = tmall;
    }
}
