package com.soecode.lyf.entity;

/**
 * Created by Administrator on 2017/9/14.
 */
public class Test {
    private String id;
    private String salesMan;
    private String customerName;
    private String bProduct;
    private String bCount;
    private String bSalesDate;
    private String bMoney;
    private String wProduct;
    private String wCount;
    private String wSalesDate;
    private String wMoney;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSalesMan() {
        return salesMan;
    }

    public void setSalesMan(String salesMan) {
        this.salesMan = salesMan;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getbProduct() {
        return bProduct;
    }

    public void setbProduct(String bProduct) {
        this.bProduct = bProduct;
    }

    public String getbCount() {
        return bCount;
    }

    public void setbCount(String bCount) {
        this.bCount = bCount;
    }

    public String getbSalesDate() {
        return bSalesDate;
    }

    public void setbSalesDate(String bSalesDate) {
        this.bSalesDate = bSalesDate;
    }

    public String getbMoney() {
        return bMoney;
    }

    public void setbMoney(String bMoney) {
        this.bMoney = bMoney;
    }

    public String getwProduct() {
        return wProduct;
    }

    public void setwProduct(String wProduct) {
        this.wProduct = wProduct;
    }

    public String getwCount() {
        return wCount;
    }

    public void setwCount(String wCount) {
        this.wCount = wCount;
    }

    public String getwSalesDate() {
        return wSalesDate;
    }

    public void setwSalesDate(String wSalesDate) {
        this.wSalesDate = wSalesDate;
    }

    public String getwMoney() {
        return wMoney;
    }

    public void setwMoney(String wMoney) {
        this.wMoney = wMoney;
    }
}
