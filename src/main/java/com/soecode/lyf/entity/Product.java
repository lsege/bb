package com.soecode.lyf.entity;

/**
 * Created by lsd on 2017-04-18.
 */
public class Product {
    private String productType;
    private Integer correspondingId;

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public Integer getCorrespondingId() {
        return correspondingId;
    }

    public void setCorrespondingId(Integer correspondingId) {
        this.correspondingId = correspondingId;
    }
}
