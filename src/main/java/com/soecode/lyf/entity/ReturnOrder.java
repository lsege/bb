package com.soecode.lyf.entity;

/**
 * Created by Administrator on 2017/8/15.
 */
public class ReturnOrder {
    private int id;
    private String billNo;
    private String comtactName;
    private String goods;
    private String count;
    private String description;
    private String inputMan;
    private String reviewerMan;
    private String payDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getComtactName() {
        return comtactName;
    }

    public void setComtactName(String comtactName) {
        this.comtactName = comtactName;
    }

    public String getGoods() {
        return goods;
    }

    public void setGoods(String goods) {
        this.goods = goods;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInputMan() {
        return inputMan;
    }

    public void setInputMan(String inputMan) {
        this.inputMan = inputMan;
    }

    public String getReviewerMan() {
        return reviewerMan;
    }

    public void setReviewerMan(String reviewerMan) {
        this.reviewerMan = reviewerMan;
    }

    public String getPayDate() {
        return payDate;
    }

    public void setPayDate(String payDate) {
        this.payDate = payDate;
    }
}
