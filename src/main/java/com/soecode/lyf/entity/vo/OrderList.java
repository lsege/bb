package com.soecode.lyf.entity.vo;

import java.sql.Date;

/**
 * Created by Administrator on 2017/7/26.
 */
public class OrderList {
    Long id;
    String listNum;
    String customrtName;
    String customerPhone;
    String numChengdui;
    Date dateChengdui;
    String note;
    String paymentDate;
    String inputMan;
    String brokerage;
    String creationDate;
    String documentType;
    String money;
    String endData;
    String returnType;
    String customerAccount;

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getListNum() {
        return listNum;
    }

    public void setListNum(String listNum) {
        this.listNum = listNum;
    }

    public String getCustomrtName() {
        return customrtName;
    }

    public void setCustomrtName(String customrtName) {
        this.customrtName = customrtName;
    }

    public String getNumChengdui() {
        return numChengdui;
    }

    public void setNumChengdui(String numChengdui) {
        this.numChengdui = numChengdui;
    }

    public Date getDateChengdui() {
        return dateChengdui;
    }

    public void setDateChengdui(Date dateChengdui) {
        this.dateChengdui = dateChengdui;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getInputMan() {
        return inputMan;
    }

    public void setInputMan(String inputMan) {
        this.inputMan = inputMan;
    }

    public String getBrokerage() {
        return brokerage;
    }

    public void setBrokerage(String brokerage) {
        this.brokerage = brokerage;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getEndData() {
        return endData;
    }

    public void setEndData(String endData) {
        this.endData = endData;
    }

    public String getReturnType() {
        return returnType;
    }

    public void setReturnType(String returnType) {
        this.returnType = returnType;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }
}
