package com.soecode.lyf.entity.vo;

/**
 * Created by 10091 on 2017-07-18.
 */
public class QueryVO {

    private String beginDate;
    private String endDate;
    private int rows;
    private int page;
    private String matchCon;

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getMatchCon() {
        return matchCon;
    }

    public void setMatchCon(String matchCon) {
        this.matchCon = matchCon;
    }
}
