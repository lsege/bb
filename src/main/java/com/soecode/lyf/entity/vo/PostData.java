package com.soecode.lyf.entity.vo;


import com.soecode.lyf.entity.ReturnOrder;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by 10091 on 2017-05-26.
 */
@JsonIgnoreProperties(ignoreUnknown = true)//忽略未知属性
public class PostData {
    private String contactId;
    private String  contactName;
    private Integer salesId;
    private String  date;
    private String  billNo;
    private String  inputMan;//录入人
    private String  reviewerMan;
    private String totalQty;//总数
    private String totalAmount;//总额
    private String totalPushMoney; //总运费
    private String whetherDateint ;//是否延期
    private String whetherElectint ;//是否电汇
    private String payDate ;//付款日期

    private String goods;
    private String count;

    private String description;//备注

    private String pinId;
    private Integer transType = 150601;
    private String status="edit";
    private Integer id;
    private Integer buId = 0;
    private Integer cLevel = 1;
    private Integer whether; //是否老客户




    public Integer getWhether() {
        return whether;
    }

    public void setWhether(Integer whether) {
        this.whether = whether;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBuId() {
        return buId;
    }

    public void setBuId(Integer buId) {
        this.buId = buId;
    }

    public Integer getcLevel() {
        return cLevel;
    }

    public void setcLevel(Integer cLevel) {
        this.cLevel = cLevel;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getTransType() {
        return transType;
    }

    public void setTransType(Integer transType) {
        this.transType = transType;
    }

    public String getPinId() {
        return pinId;
    }

    public void setPinId(String pinId) {
        this.pinId = pinId;
    }

    public String getTotalPushMoney() {
        return totalPushMoney;
    }

    public void setTotalPushMoney(String totalPushMoney) {
        this.totalPushMoney = totalPushMoney;
    }

    public String getWhetherDateint() {
        return whetherDateint;
    }

    public void setWhetherDateint(String whetherDateint) {
        this.whetherDateint = whetherDateint;
    }

    public String getWhetherElectint() {
        return whetherElectint;
    }

    public void setWhetherElectint(String whetherElectint) {
        this.whetherElectint = whetherElectint;
    }

    public String getPayDate() {
        return payDate;
    }

    public void setPayDate(String payDate) {
        this.payDate = payDate;
    }

    public String getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(String totalQty) {
        this.totalQty = totalQty;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    List<EntrieData> entries;
    List<ReturnOrder> orders;

    public List<ReturnOrder> getOrders() {
        return orders;
    }

    public void setOrders(List<ReturnOrder> orders) {
        this.orders = orders;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public Integer getSalesId() {
        return salesId;
    }

    public void setSalesId(Integer salesId) {
        this.salesId = salesId;
    }



    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getInputMan() {
        return inputMan;
    }

    public void setInputMan(String inputMan) {
        this.inputMan = inputMan;
    }

    public String getReviewerMan() {
        return reviewerMan;
    }

    public void setReviewerMan(String reviewerMan) {
        this.reviewerMan = reviewerMan;
    }

    public List<EntrieData> getEntries() {
        return entries;
    }

    public void setEntries(List<EntrieData> entries) {
        this.entries = entries;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGoods() {
        return goods;
    }

    public void setGoods(String goods) {
        this.goods = goods;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
