package com.soecode.lyf.entity.vo;

/**
 * Created by Administrator on 2017/8/23.
 */
public class Products {
    private String id;
    private String name;
    private String qual;
    private String dataName;
    private String volQual;
    private String volUnit;
    private String productVol;

    private int index;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQual() {
        return qual;
    }

    public void setQual(String qual) {
        this.qual = qual;
    }

    public String getDataName() {
        return dataName;
    }

    public void setDataName(String dataName) {
        this.dataName = dataName;
    }

    public String getVolQual() {
        return volQual;
    }

    public void setVolQual(String volQual) {
        this.volQual = volQual;
    }

    public String getVolUnit() {
        return volUnit;
    }

    public void setVolUnit(String volUnit) {
        this.volUnit = volUnit;
    }

    public String getProductVol() {
        return productVol;
    }

    public void setProductVol(String productVol) {
        this.productVol = productVol;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
