package com.soecode.lyf.entity.vo;

/**
 * Created by 10091 on 2017-07-11.
 */
public class StorageVO {


    /**
     * address :
     * delete : false
     * allowNeg : false
     * deptId : 0
     * empId : 0
     * groupx : null
     * id : 1
     * locationNo : 1
     * name : 仓库1
     * phone : null
     * type : 0
     */
    private int id;
    private String name;
    private String locationNo;
    private int disable;
    private boolean allowNeg;
    private int deptId;
    private int empId;
    private String groupx;
    private String phone;
    private int type;
    private String address;
    private boolean isDelete;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocationNo() {
        return locationNo;
    }

    public void setLocationNo(String locationNo) {
        this.locationNo = locationNo;
    }

    public int getDisable() {
        return disable;
    }

    public void setDisable(int disable) {
        this.disable = disable;
    }

    public boolean isAllowNeg() {
        return allowNeg;
    }

    public void setAllowNeg(boolean allowNeg) {
        this.allowNeg = allowNeg;
    }

    public int getDeptId() {
        return deptId;
    }

    public void setDeptId(int deptId) {
        this.deptId = deptId;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getGroupx() {
        return groupx;
    }

    public void setGroupx(String groupx) {
        this.groupx = groupx;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }
}
