package com.soecode.lyf.entity.vo;

/**
 * Created by 10091 on 2017-07-12.
 */
public class SalesVO {

    /**
     * hxStateCode : 0
     * checkName :
     * checked : 0
     * salesId : 0 --
     * salesName : null --
     * billDate : 2017-07-12 --
     * billStatus : 0
     * totalQty : 1
     * id : 2
     * amount : 20   --
     * billStatusName : 未出库
     * transType : 150601
     * rpAmount : 0
     * contactName : 1111 --
     * description :
     * billNo : XS201707121618087 --
     * totalAmount : 20
     * userName : 模板俱乐部
     * transTypeName : 销货
     */

    private int hxStateCode;
    private String checkName;
    private int checked;
    private int salesId;
    private String salesName;
    private String billDate;
    private String billStatus;
    private int totalQty;
    private int id;
    private int amount;
    private String billStatusName;
    private int transType;
    private int rpAmount;
    private String contactName;
    private String description;
    private String billNo;
    private int totalAmount;
    private String userName;
    private String transTypeName;

    private float totalPushMonery;
    private String payDate;
    private String dateIssuance;
    private int whether;
    private int whetherDateint;
    private int whetherElectint;

    public float getTotalPushMonery() {
        return totalPushMonery;
    }

    public void setTotalPushMonery(float totalPushMonery) {
        this.totalPushMonery = totalPushMonery;
    }

    public String getPayDate() {
        return payDate;
    }

    public void setPayDate(String payDate) {
        this.payDate = payDate;
    }

    public String getDateIssuance() {
        return dateIssuance;
    }

    public void setDateIssuance(String dateIssuance) {
        this.dateIssuance = dateIssuance;
    }

    public int getWhether() {
        return whether;
    }

    public void setWhether(int whether) {
        this.whether = whether;
    }

    public int getWhetherDateint() {
        return whetherDateint;
    }

    public void setWhetherDateint(int whetherDateint) {
        this.whetherDateint = whetherDateint;
    }

    public int getWhetherElectint() {
        return whetherElectint;
    }

    public void setWhetherElectint(int whetherElectint) {
        this.whetherElectint = whetherElectint;
    }

    public int getHxStateCode() {
        return hxStateCode;
    }

    public void setHxStateCode(int hxStateCode) {
        this.hxStateCode = hxStateCode;
    }

    public String getCheckName() {
        return checkName;
    }

    public void setCheckName(String checkName) {
        this.checkName = checkName;
    }

    public int getChecked() {
        return checked;
    }

    public void setChecked(int checked) {
        this.checked = checked;
    }

    public int getSalesId() {
        return salesId;
    }

    public void setSalesId(int salesId) {
        this.salesId = salesId;
    }

    public String getSalesName() {
        return salesName;
    }

    public void setSalesName(String salesName) {
        this.salesName = salesName;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(String billStatus) {
        this.billStatus = billStatus;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getBillStatusName() {
        return billStatusName;
    }

    public void setBillStatusName(String billStatusName) {
        this.billStatusName = billStatusName;
    }

    public int getTransType() {
        return transType;
    }

    public void setTransType(int transType) {
        this.transType = transType;
    }

    public int getRpAmount() {
        return rpAmount;
    }

    public void setRpAmount(int rpAmount) {
        this.rpAmount = rpAmount;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTransTypeName() {
        return transTypeName;
    }

    public void setTransTypeName(String transTypeName) {
        this.transTypeName = transTypeName;
    }
}
