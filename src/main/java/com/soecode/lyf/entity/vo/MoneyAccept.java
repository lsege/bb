package com.soecode.lyf.entity.vo;

/**
 * Created by Administrator on 2018/1/8.
 */
public class MoneyAccept {
    private String id;
    private String listNum;
    private String customrtName;
    private String money;
    private String customerPhone;
    private String numChengdui;
    private String dateChengdui;
    private String returnType;
    private String chengduiSendUnit;
    private String chengduiToUnit;
    private String chengduiBang;
    private String customerAccount;
    private String dianhuiBankName;
    private String dianhuiPayAccount;
    private String dianhuiCollectionAccount;
    private String dianhuiContext;
    private String zhuanzhangPayAccount;
    private String zhuanzhangCollectionAccount;
    private String note;
    private String documentType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getListNum() {
        return listNum;
    }

    public void setListNum(String listNum) {
        this.listNum = listNum;
    }

    public String getCustomrtName() {
        return customrtName;
    }

    public void setCustomrtName(String customrtName) {
        this.customrtName = customrtName;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getNumChengdui() {
        return numChengdui;
    }

    public void setNumChengdui(String numChengdui) {
        this.numChengdui = numChengdui;
    }

    public String getDateChengdui() {
        return dateChengdui;
    }

    public void setDateChengdui(String dateChengdui) {
        this.dateChengdui = dateChengdui;
    }

    public String getReturnType() {
        return returnType;
    }

    public void setReturnType(String returnType) {
        this.returnType = returnType;
    }

    public String getChengduiSendUnit() {
        return chengduiSendUnit;
    }

    public void setChengduiSendUnit(String chengduiSendUnit) {
        this.chengduiSendUnit = chengduiSendUnit;
    }

    public String getChengduiToUnit() {
        return chengduiToUnit;
    }

    public void setChengduiToUnit(String chengduiToUnit) {
        this.chengduiToUnit = chengduiToUnit;
    }

    public String getChengduiBang() {
        return chengduiBang;
    }

    public void setChengduiBang(String chengduiBang) {
        this.chengduiBang = chengduiBang;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public String getDianhuiBankName() {
        return dianhuiBankName;
    }

    public void setDianhuiBankName(String dianhuiBankName) {
        this.dianhuiBankName = dianhuiBankName;
    }

    public String getDianhuiPayAccount() {
        return dianhuiPayAccount;
    }

    public void setDianhuiPayAccount(String dianhuiPayAccount) {
        this.dianhuiPayAccount = dianhuiPayAccount;
    }

    public String getDianhuiCollectionAccount() {
        return dianhuiCollectionAccount;
    }

    public void setDianhuiCollectionAccount(String dianhuiCollectionAccount) {
        this.dianhuiCollectionAccount = dianhuiCollectionAccount;
    }

    public String getDianhuiContext() {
        return dianhuiContext;
    }

    public void setDianhuiContext(String dianhuiContext) {
        this.dianhuiContext = dianhuiContext;
    }

    public String getZhuanzhangPayAccount() {
        return zhuanzhangPayAccount;
    }

    public void setZhuanzhangPayAccount(String zhuanzhangPayAccount) {
        this.zhuanzhangPayAccount = zhuanzhangPayAccount;
    }

    public String getZhuanzhangCollectionAccount() {
        return zhuanzhangCollectionAccount;
    }

    public void setZhuanzhangCollectionAccount(String zhuanzhangCollectionAccount) {
        this.zhuanzhangCollectionAccount = zhuanzhangCollectionAccount;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }
}
