package com.soecode.lyf.entity.vo;

/**
 * Created by Administrator on 2017/8/17.
 */
public class Sales2Vo {
    private int id;
    private String orderNo;
    private String customerName;
    private String productName;
    private String count;
    private String remark;
    private String inputMan;
    private String auditorMan;
    private String returnData;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getInputMan() {
        return inputMan;
    }

    public void setInputMan(String inputMan) {
        this.inputMan = inputMan;
    }

    public String getAuditorMan() {
        return auditorMan;
    }

    public void setAuditorMan(String auditorMan) {
        this.auditorMan = auditorMan;
    }

    public String getReturnData() {
        return returnData;
    }

    public void setReturnData(String returnData) {
        this.returnData = returnData;
    }
}
