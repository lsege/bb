package com.soecode.lyf.entity.vo;

/**
 * 商品信息
 * Created by 10091 on 2017-05-26.
 */
public class
EntrieData {
    private Integer invId;
    private Float qty;
    private Float price;
    private Float amount;
    private String description;
    private String storageId;
    private Float pushMoney;

    private String goods;
    private String locationName;
    private String spec;

    private int id;
    private String billNo;
    private String comtactName;

    private String count;

    private String inputMan;
    private String reviewerMan;
    private String payDate;

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getGoods() {
        return goods;
    }

    public void setGoods(String goods) {
        this.goods = goods;
    }

    public Float getPushMoney() {
        return pushMoney;
    }

    public void setPushMoney(Float pushMoney) {
        this.pushMoney = pushMoney;
    }

    public String getStorageId() {
        return storageId;
    }

    public void setStorageId(String storageId) {
        this.storageId = storageId;
    }

    public Integer getInvId() {
        return invId;
    }

    public void setInvId(Integer invId) {
        this.invId = invId;
    }

    public Float getQty() {
        return qty;
    }

    public void setQty(Float qty) {
        this.qty = qty;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getComtactName() {
        return comtactName;
    }

    public void setComtactName(String comtactName) {
        this.comtactName = comtactName;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getInputMan() {
        return inputMan;
    }

    public void setInputMan(String inputMan) {
        this.inputMan = inputMan;
    }

    public String getReviewerMan() {
        return reviewerMan;
    }

    public void setReviewerMan(String reviewerMan) {
        this.reviewerMan = reviewerMan;
    }

    public String getPayDate() {
        return payDate;
    }

    public void setPayDate(String payDate) {
        this.payDate = payDate;
    }
}
