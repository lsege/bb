package com.soecode.lyf.web;

import com.soecode.lyf.entity.OperatorVo;
import com.soecode.lyf.entity.SalesMan;
import com.soecode.lyf.entity.vo.JsonVo;
import com.soecode.lyf.service.OperatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Administrator on 2017/8/21.
 */
@Controller
@RequestMapping("/operator")
public class OperatorController {

    @Autowired
    OperatorService operatorService;

    /**
     * 读取业务员列表数据
     * @return
     */
    @RequestMapping("/operatorList")
    public ModelAndView operatorList(HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            mv.setViewName("login");
        } else {
            List<SalesMan> list = operatorService.queryAllUsers();
            mv.addObject("list",list);
            mv.setViewName("operator/operatorList");
        }
        return mv;
    }

    /**
     * 查询数据并跳转到修改页面
     * @param id
     * @param request
     * @return
     */
    @RequestMapping("/operatorEditPage")
    public ModelAndView operatorEditPage(String id,HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
             mv.setViewName("login");
        } else {
            Long uId = Long.parseLong(id);
            //查询操作员数据，将数据带到修改页面
            SalesMan salesMan = operatorService.queryUserInfo(uId);
            if (salesMan != null){
                mv.addObject("salesMan",salesMan);
            }
            mv.setViewName("operator/editOperator");
        }
        return mv;
    }

    /**
     * 修改操作员数据
     * @param id
     * @param name
     * @param phone
     * @param request
     * @return
     */
    @RequestMapping("/editOperator")
    @ResponseBody
    public JsonVo editOperator(String id,String name,String phone,HttpServletRequest request,String password){
        JsonVo json = new JsonVo();
        if (request.getSession().getAttribute("aid") == null){
           json.setResult(false);
           json.setMsg("无权修改");
        } else {
            try {
                if (password.equals("")){
                    //未修改密码
                    Long uId = Long.parseLong(id);
                    operatorService.editUser(uId,name);
                    operatorService.editOperator(uId,name,phone);
                } else {
                    Long uId = Long.parseLong(id);
                    //账号加密
                    //密码加密
                    //（账号+密码）+加密
                    operatorService.editUserPassord(uId,name,password);
                    operatorService.editOperator(uId,name,phone);
                }
            } catch (Exception e){
                e.printStackTrace();
                json.setResult(false);
                json.setMsg("修改失败");
            }
            json.setResult(true);
            json.setMsg("修改成功");
        }
        return json;
    }

    /**
     * 删除操作员信息
     * @param id
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public JsonVo delete(String id,HttpServletRequest request){
        JsonVo json = new JsonVo();
        if (request.getSession().getAttribute("aid") == null){
            json.setResult(false);
            json.setMsg("请登陆");
        } else {
            Long uId = Long.parseLong(id);
            try {
                operatorService.deleteOperatorById(uId);
                json.setResult(true);
                json.setMsg("删除成功");
            } catch (Exception e){
                json.setResult(false);
                json.setMsg("删除失败");
                e.printStackTrace();
            }
        }
        return json;
    }

}
