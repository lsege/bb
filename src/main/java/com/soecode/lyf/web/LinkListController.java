package com.soecode.lyf.web;

import com.soecode.lyf.entity.*;
import com.soecode.lyf.entity.vo.JsonVo;
import com.soecode.lyf.entity.vo.MoneyAccept;
import com.soecode.lyf.entity.vo.OrderList;
import com.soecode.lyf.entity.vo.Products;
import com.soecode.lyf.service.*;
import com.soecode.lyf.web.base.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by lsd on 2017-04-17.
 */
@Controller
@RequestMapping("/index")
public class LinkListController extends BaseController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private OrderService orderService;

    @Autowired
    private ProductService productService;

    @Autowired
    private BillService billService;

    @Autowired
    CustomerService customerService;

    @Autowired
    SalesService salesService;

    /**
     * 跳转到首页（头部标签+页面IFrame）
     * @param request
     * @return
     */
    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public ModelAndView home(HttpServletRequest request) {
        ModelAndView mv = new ModelAndView();
        if(request.getSession().getAttribute("aid") == null){
            //未登录返回首页
            mv.setViewName("login");
        } else {
            mv.setViewName("admin/home");
        }
        return mv;
    }

    /**
     * 销货单列表页面
     * @param request
     * @return
     */
    @RequestMapping(value = "/salesOrdersPage", method = RequestMethod.GET)
    public ModelAndView salesOrdersPage(HttpServletRequest request) {
        ModelAndView mv = new ModelAndView();
        if(request.getSession().getAttribute("aid") == null){
            //未登录返回首页
            mv.setViewName("login");
        } else {
            mv.setViewName("admin/salesOrdersPage");
        }
        return mv;
    }

    /**
     * 请求销货单列表数据
     * @param page
     * @param request
     * @return
     */
    @RequestMapping("/salesOrdersList")
    public ModelAndView salesOrdersList(String page,HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            //未登录返回首页
            mv.setViewName("login");
        } else {
            String id = request.getSession().getAttribute("aid").toString();
            //每页显示的条数
            int pageSize = 10;
            List<SalesOrders> list = salesService.queryAllSalesOrders();
            //总数据量
            Long total = 0L;
            if (id.equals("1")){
                total = Long.valueOf(list.size()+"");
            } else {
                for (int i=0; i<list.size(); i++){
                    if (list.get(i).getSalesmanId().equals(id)){
                        total +=1;
                    }
                }
            }
            mv.addObject("total", total);
            //总页数
            int pageTimes;
            if(list.size()%pageSize == 0)
            {
                pageTimes = list.size()/pageSize;
            }else {
                pageTimes = list.size()/pageSize + 1;
            }
            mv.addObject("pageCount", pageTimes);
            //页面初始的时候page没有值
            if (page == null || page == ""){
                page = "1";
            }
            //每页开始的第几条记录
            int startRow = (Integer.parseInt(page)-1) * pageSize;
            if (id.equals("1")){
                list = salesService.queryAllSalesOrdersByPage(startRow, pageSize);
                for (int i=0; i<list.size(); i++){
                    list.get(i).setIndex((Integer.parseInt(page)-1)*10+1+i);
                }
            } else {
                list = salesService.queryAllSalesOrdersByPageAndId(startRow, pageSize,id);
                for (int i=0; i<list.size(); i++){
                    list.get(i).setIndex((Integer.parseInt(page)-1)*10+1+i);
                }
            }
            mv.addObject("currentPage", Integer.parseInt(page));
            mv.addObject("list", list);
            mv.setViewName("sales/salesOrdersList");
        }
        return mv;
    }

    /**
     * 回款录入页面
     * @param request
     * @return
     */
    @RequestMapping(value = "/toReceivableEntry", method = RequestMethod.GET)
    public ModelAndView toReceivableEntry(HttpServletRequest request) {
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            mv.setViewName("login");
        } else {
            mv.setViewName("sales/receivableEntryPage");
        }
        return mv;
    }

    /**
     * 请求回款录入数据
     * @param page
     * @param request
     * @return
     */
    @RequestMapping(value = "/addCashOrder", method = RequestMethod.GET)
    public ModelAndView addCashOrder(String page,HttpServletRequest request) {
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            mv.setViewName("login");
        } else {
            if (page == null || page == ""){
                page = "1";
            }
            int pageSize = 21;
            int startRow = (Integer.parseInt(page)-1) * pageSize;

            //查询客户列表
            List<Customer> list = salesService.queryAllCustomer();
            List<Customer> customers = salesService.queryAllCustomerByPage(startRow,pageSize);
            List<CustomerRow> customerRows = new ArrayList<CustomerRow>();
            if (customers.size()%3 == 0){
                for (int i=0; i<customers.size(); i+=3){
                    CustomerRow customerRow = new CustomerRow();
                    customerRow.setName1(customers.get(i).getCustomerName());
                    customerRow.setName2(customers.get(i+1).getCustomerName());
                    customerRow.setName3(customers.get(i+2).getCustomerName());
                    customerRows.add(customerRow);
                }
            }
            if (customers.size()%3 == 1){
                for (int i=0; i<customers.size(); i+=3){
                    CustomerRow customerRow = new CustomerRow();
                    if (i == customers.size() -1){
                        customerRow.setName1(customers.get(i).getCustomerName());
                        customerRow.setName2("");
                        customerRow.setName3("");
                    } else {
                        customerRow.setName1(customers.get(i).getCustomerName());
                        customerRow.setName2(customers.get(i+1).getCustomerName());
                        customerRow.setName3(customers.get(i+2).getCustomerName());
                    }
                    customerRows.add(customerRow);
                }
            }
            if (customers.size()%3 == 2){
                for (int i=0; i<customers.size(); i+=3){
                    CustomerRow customerRow = new CustomerRow();
                    if (i == customers.size() -1){
                        customerRow.setName1(customers.get(i).getCustomerName());
                        customerRow.setName2(customers.get(i+1).getCustomerName());
                        customerRow.setName3("");
                    } else {
                        customerRow.setName1(customers.get(i).getCustomerName());
                        customerRow.setName2(customers.get(i+1).getCustomerName());
                        customerRow.setName3(customers.get(i+2).getCustomerName());
                    }
                    customerRows.add(customerRow);
                }
            }

            for (int i=0; i<customers.size(); i++){
                customers.get(i).setIndex((Integer.parseInt(page)-1)*10+1+i);
            }

            //查询业务员列表
            List<SalesMan> salesMan = orderService.queryAllSalesMan();
            SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddhhmmss");
            Date data2 = new Date();
            String number = sf.format(data2);
            mv.addObject("num",number);//单据编号
            mv.addObject("salesMan",salesMan);//销售列表
            mv.addObject("size",list.size());//客户数量
            mv.addObject("customerRows",customerRows);//客户列表
            mv.addObject("currentPage",Integer.parseInt(page));//当前页码
            mv.setViewName("order/newCash");
        }
        return mv;
    }

    /**
     * 跳转到回款单据列表
     * @param page
     * @param request
     * @return
     */
    @RequestMapping(value = "/toReceivableList", method = RequestMethod.GET)
    public ModelAndView toReceivableList(String page, HttpServletRequest request) {
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            //未登录
            mv.setViewName("login");
        } else {
            String userId = request.getSession().getAttribute("aid").toString();
            //每页显示的条数
            int pageSize = 10;
            List<MoneyAccept> list = new ArrayList<MoneyAccept>();
            if (userId.equals("1")){
                list = orderService.queryAllOrder();
            } else {
                list = orderService.queryAllOrdersById(userId);
            }
            //总数据量
            mv.addObject("total", list.size());
            //总页数
            int pageTimes;
            if(list.size()%pageSize == 0)
            {
                pageTimes = list.size()/pageSize;
            }else {
                pageTimes = list.size()/pageSize + 1;
            }
            mv.addObject("pageCount", pageTimes);
            //页面初始的时候page没有值
            if(null == page || page.equals("0")) {
                page = "1";
            }
            //每页开始的第几条记录
            int startRow = (Integer.parseInt(page)-1) * pageSize;
            if (userId.equals("1")){
                list = this.orderService.queryAllOrderByPage(startRow, pageSize);
            } else {
                list = orderService.queryAllOrdersByPage(startRow,pageSize,userId);
            }
            mv.addObject("currentPage", Integer.parseInt(page));
            mv.addObject("list", list);
            mv.setViewName("sales/ReceivableListPage");
        }
        return mv;
    }

    /**
     * 铝瓶销售明细报表
     * @param request
     * @return
     */
    @RequestMapping("/salesDetail")
    public ModelAndView salesDetail(HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            //未登录
            mv.setViewName("login");
        } else {
            mv.setViewName("report/salesDetail");
        }
        return mv;
    }

    /**
     * 铝瓶销售汇总报表
     * @param request
     * @return
     */
    @RequestMapping("/salesSummary")
    public ModelAndView salesSummary(HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            //未登录
            mv.setViewName("login");
        } else {
            mv.setViewName("report/salesSummary");
        }
        return mv;
    }

    /**
     * 客户采购明细报表
     * @param request
     * @return
     */
    @RequestMapping("/procurementDetail")
    public ModelAndView procurementDetail(HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            //未登录
            mv.setViewName("login");
        } else {
            mv.setViewName("report/procurementDetail");
        }
        return mv;
    }

    /**
     * 客户采购汇总报表
     * @param request
     * @return
     */
    @RequestMapping("/procurementSummary")
    public ModelAndView procurementSummary(HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            //未登录
            mv.setViewName("login");
        } else {
            mv.setViewName("report/procurementSummary");
        }
        return mv;
    }

    /**
     * 客户回款明细报表
     * @param request
     * @return
     */
    @RequestMapping("/receivableDetail")
    public ModelAndView receivableDetail(HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            //未登录
            mv.setViewName("login");
        } else {
            mv.setViewName("report/receivableDetail");
        }
        return mv;
    }

    /**
     * 客户回款汇总报表
     * @param request
     * @return
     */
    @RequestMapping("/receivableSummary")
    public ModelAndView receivableSummary(HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            //未登录
            mv.setViewName("login");
        } else {
            mv.setViewName("report/receivableSummary");
        }
        return mv;
    }

    /**
     * 业务提成明细报表
     * @param request
     * @return
     */
    @RequestMapping("/commissionDetail")
    public ModelAndView commissionDetail(HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            //未登录
            mv.setViewName("login");
        } else {
            mv.setViewName("report/commissionDetail");
        }
        return mv;
    }

    /**
     * 业务提成报表
     * @param request
     * @return
     */
    @RequestMapping("/commissionSummary")
    public ModelAndView commissionSummary(HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            //未登录
            mv.setViewName("login");
        } else {
            mv.setViewName("report/commissionSummary");
        }
        return mv;
    }

    /**
     * 跳转到客户目录页面
     * @return
     */
    @RequestMapping("/customerListPage")
    public ModelAndView customerListPage(HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            mv.setViewName("login");
        } else {
            mv.setViewName("customer/customerListPage");
        }
        return mv;
    }

    /**
     * 跳转到商品目录页面
     * @param request
     * @return
     */
    @RequestMapping("/productListPage")
    public ModelAndView productListPage(HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            //未登录
            mv.setViewName("login");
        } else {
            mv.setViewName("product/productListPage");
        }
        return mv;
    }

    /**
     * 跳转到客户添加页面
     * @param request
     * @return
     */
    @RequestMapping("/addCustomerPage")
    public ModelAndView addCustomerPage(HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            mv.setViewName("login");
        } else {
            mv.setViewName("customer/addCustomerPage");
        }
        return mv;
    }

    /**
     * 跳转到商品添加页面
     * @param request
     * @return
     */
    @RequestMapping("/addProductPage")
    public ModelAndView addProductPage(HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            mv.setViewName("login");
        } else {
            mv.setViewName("product/addProductPage");
        }
        return mv;
    }

    /**
     * 跳转到业务员列表页面
     * @param request
     * @return
     */
    @RequestMapping("/operatorAdminPage")
    public ModelAndView operatorAdminPage(HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            mv.setViewName("login");
        } else {
            mv.setViewName("operator/operatorListPage");
        }
        return mv;
    }

    /**
     * 跳转到业务员提成比例管理页面
     * @param request
     * @return
     */
    @RequestMapping("/commissionPage")
    public ModelAndView commissionPage(HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            mv.setViewName("login");
        } else {
            mv.setViewName("commission/salesManCommissionPage");
        }
        return mv;
    }

    /**
     * 退出登录
     * @param request
     * @return
     */
    @RequestMapping("/exit")
    public ModelAndView exit(HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        HttpSession session = request.getSession();
        session.removeAttribute("aid");
        session.removeAttribute("level");
        session.removeAttribute("names");
        mv.setViewName("login");
        return mv;
    }





















    @RequestMapping(value = "/index", method = RequestMethod.GET)
    private ModelAndView index(String id,String pwd) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("admin/index?id="+id);
        return mv;
    }

    @RequestMapping(value = "/index11", method = RequestMethod.GET)
    private ModelAndView index11(String id,String returnData,String orderNo,String productName,String auditorMan,
                                 String count,String customerName,String inputMan,String remark) {
        ModelAndView mv = new ModelAndView();
        if(null != id){
            mv.addObject("id",id);
        }
        mv.setViewName("admin/index11");
        return mv;
    }

    @RequestMapping("/index9")
    public ModelAndView index9(String type,String id){
        ModelAndView mv = new ModelAndView();
        mv.addObject("type",type);
        mv.setViewName("admin/index9");
        return mv;
    }





    @RequestMapping(value = "/query", method = RequestMethod.GET)
    @ResponseBody
    private JsonVo query(){
        JsonVo json = new JsonVo();
        List<MoneyAccept> list =  orderService.queryAllOrder();
        json.setT(list);
        return json;
    }
    @RequestMapping(value = "/index3", method = RequestMethod.GET)
    private ModelAndView index3(String name,String pwd) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("admin/index3");
        return mv;
    }

    @RequestMapping(value = "/index5")
    private ModelAndView index5(String type) {
        ModelAndView mv = new ModelAndView();
        mv.addObject("type",type);
        mv.setViewName("admin/index5");
        return mv;
    }
    @RequestMapping(value = "/index6", method = RequestMethod.GET)
    private ModelAndView index6(String type) {
        ModelAndView mv = new ModelAndView();
        mv.addObject("type",type);
        mv.setViewName("admin/index6");
        return mv;
    }

    @RequestMapping("/index10")
    public ModelAndView index10(String type){
        ModelAndView mv = new ModelAndView();
        mv.addObject("type",type);
        mv.setViewName("admin/index10");
        return mv;
    }


    @RequestMapping(value = "/saleList", method = RequestMethod.GET)
    private ModelAndView saleList(String type) {
        ModelAndView mv = new ModelAndView();
        mv.addObject("type",type);
        mv.setViewName("order/saleList");
        return mv;
    }


    @RequestMapping("/saleList2")
    public ModelAndView saleList2(String type){
        ModelAndView mv = new ModelAndView();
        mv.addObject("type",type);
        mv.setViewName("order/saleList2");
        return mv;
    }

    //进入产品销售
    @RequestMapping(value = "/productSales")
    private ModelAndView list2(String type) {
        String name = null;

        ModelAndView mv = new ModelAndView();
        mv.addObject("name",name);
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddhhmmss");
        Date data2 = new Date();
        String number = sf.format(data2);
        mv.addObject("num",number);
        if(type.equals("1")){
            name = "封盖机";
            mv.setViewName("order/NewSale2");
        }else if(type.equals("2")){
            name = "封口钳";
            mv.setViewName("order/NewSale2");
        }else if(type.equals("3")){
            name = "铝瓶";
            mv.setViewName("order/NewSale2");
        }

        return mv;
    }
    //进入销货单
    @RequestMapping(value = "/saleput", method = RequestMethod.GET)
    private ModelAndView list() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("order/list");
        return mv;
    }

    @RequestMapping(value = "/saleput2", method = RequestMethod.GET)
    private ModelAndView saleput2(String type,String id) {
        String name = null;
        if(type.equals("1")){
            name = "销货单";
        }else {
            name = "退货单";
        }
        ModelAndView mv = new ModelAndView();
        mv.addObject("name",name);
        if(id != null){
            mv.addObject("id",id);
        }
        mv.setViewName("order/list2");
        return mv;
    }

    //进入承兑付款单
    @RequestMapping(value = "/accept", method = RequestMethod.GET)
    private ModelAndView accept(String nmgb) {
        ModelAndView mv = new ModelAndView();
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddhhmmss");
        Date data2 = new Date();
        String number = sf.format(data2);
        System.out.println(number);
        mv.addObject("nmgb",nmgb);
        mv.addObject("num",number);
        mv.setViewName("order/newAccept");
        return mv;
    }
    //进入现金付款单

    //进入电汇付款单
    @RequestMapping(value = "/elect", method = RequestMethod.GET)
    private ModelAndView elect(String nmgb) {
        ModelAndView mv = new ModelAndView();
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddhhmmss");
        Date data2 = new Date();
        String number = sf.format(data2);
        mv.addObject("nmgb",nmgb);
        mv.addObject("num",number);
        mv.setViewName("order/newElect");
        return mv;
    }
    @RequestMapping(value="/list")
    public ModelAndView AAlist(){
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("order/allList");
        return mv;
    }
    @RequestMapping(value = "/report", method = RequestMethod.GET)
    private ModelAndView report(String da) {
        ModelAndView mv = new ModelAndView();
        if(da.equals("11")){
            mv.setViewName("admin/report_0");
        }else if(da.equals("12")){
           mv.setViewName("admin/report_1");
        }else if(da.equals("13")){
           mv.setViewName("admin/report_2");
        }else if(da.equals("14")){
           mv.setViewName("admin/report_3");
        }else if(da.equals("15")){
           mv.setViewName("admin/report_4");
        }else if(da.equals("16")){
           mv.setViewName("admin/report_5");
        }else if(da.equals("17")){
           mv.setViewName("admin/report_6");
        }else if(da.equals("18")){
           mv.setViewName("admin/report_7");
        }else if(da.equals("21")){
            //读取客户列表
            List<Customer> customers = orderService.queryAllCustomer();
            mv.addObject("customers",customers);
            mv.setViewName("admin/report_8");
        }else if(da.equals("22")){
            List<Customer> customers = orderService.queryAllCustomer();
            mv.addObject("customers",customers);
            mv.setViewName("admin/report_9");
        }else if(da.equals("23")){
            mv.setViewName("admin/report_10");
        }else if(da.equals("24")){
            mv.setViewName("admin/report_11");
        }else if(da.equals("31")){
            mv.setViewName("admin/report_12");
        }else if(da.equals("32")){
            mv.setViewName("admin/report_13");
        }else if(da.equals("33")){
            mv.setViewName("admin/report_14");
        }else if(da.equals("34")){
            mv.setViewName("admin/report_15");
        }else if(da.equals("41")){
            mv.setViewName("admin/report_16");
        }else if(da.equals("42")){
            //读取客户列表
            List<Customer> customers = orderService.queryAllCustomer();
            mv.addObject("customers",customers);
            mv.setViewName("admin/report_17");
        }else if(da.equals("51")){
            mv.setViewName("admin/report_18");
        }else if(da.equals("53")){
            mv.setViewName("admin/report_19");
        } else if(da.equals("61")){
            mv.setViewName("admin/report_20");
        }else if(da.equals("62")){
            mv.setViewName("admin/report_21");
        }else if(da.equals("63")){
            mv.setViewName("admin/report_22");
        }else if(da.equals("64")){
            mv.setViewName("admin/report_23");
        }else if(da.equals("65")){
            mv.setViewName("admin/report_24");
        }else if (da.equals("66")){
            mv.setViewName("admin/report_25");
        }
        return mv;
    }



    @RequestMapping("/returnList")
    public ModelAndView returnList(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("admin/salesOrdersPage");
        return mv;
    }
    @RequestMapping("/addReturnList")
    public ModelAndView addReturnList(){
        ModelAndView mv = new ModelAndView();
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddhhmmss");
        Date data2 = new Date();
        String number = sf.format(data2);
        mv.addObject("num",number);
        mv.setViewName("order/addReturnList");
        return mv;
    }
    @RequestMapping("/index8")
    public ModelAndView returnMoney(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("admin/index8");
        return mv;
    }
    @RequestMapping("/newReturnMoney")
    public ModelAndView newReturnMoney(){
        ModelAndView mv = new ModelAndView();
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddhhmmss");
        Date date = new Date();
        String number = sf.format(date);
        mv.addObject("num",number);
        mv.setViewName("order/newReturnMoney");
        return mv;
    }

    @RequestMapping("/addReturns")
    public ModelAndView addReturns(){
        ModelAndView mv = new ModelAndView();
        String name = "退货单";
        mv.addObject("name",name);
        mv.setViewName("order/addReturns");
        return mv;
    }

    @RequestMapping("/addOperator")
    public ModelAndView addOperator(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("admin/addOperator");
        return mv;
    }
    @RequestMapping("/goAddOperator")
    public ModelAndView goAddOperator(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("operator/addOperator");
        return mv;
    }














    @RequestMapping("/returnOrder")
    public ModelAndView returnOrder(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("return/returnOrders");
        return mv;
    }
}
