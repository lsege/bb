package com.soecode.lyf.web;

import com.soecode.lyf.entity.DataBase;
import com.soecode.lyf.entity.vo.JsonVo;
import com.soecode.lyf.entity.vo.Products;
import com.soecode.lyf.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Administrator on 2017/8/23.
 */
@Controller
@RequestMapping("/product")
public class ProductController {

    @Autowired
    ProductService productService;

    /**
     * 读取商品列表数据
     * @param page
     * @param request
     * @return
     */
    @RequestMapping("/productList")
    public ModelAndView productList(String page, HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            //未登录
            mv.setViewName("login");
        } else {
            //每页显示的条数
            int pageSize = 10;
            List<Products> list = productService.queryAllProduct();
            //总数据量
            mv.addObject("total", list.size());
            //总页数
            int pageTimes;
            if (list.size() % pageSize == 0) {
                pageTimes = list.size() / pageSize;
            } else {
                pageTimes = list.size() / pageSize + 1;
            }
            mv.addObject("pageCount", pageTimes);
            //页面初始的时候page没有值
            if (null == page || page.equals("0")) {
                page = "1";
            }
            //每页开始的第几条记录
            int startRow = (Integer.parseInt(page) - 1) * pageSize;
            list = productService.queryAllProductByPage(startRow, pageSize);
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setIndex((Integer.parseInt(page) - 1) * 10 + 1 + i);
            }
            mv.addObject("currentPage", Integer.parseInt(page));
            mv.addObject("list", list);
            mv.setViewName("product/productList");
        }
        return mv;
    }

    /**
     * 查询商品数据，并跳转到修改页面
     * @param id
     * @param request
     * @return
     */
    @RequestMapping("/productEditPage")
    public ModelAndView productEditPage(String id,HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            mv.setViewName("login");
        } else {
            Products products = productService.queryProductById(id);
            List<DataBase> list = productService.queryAllDataName();
            mv.addObject("list",list);
            mv.addObject("products",products);
            mv.setViewName("product/editProduct");
        }
        return mv;
    }

    /**
     * 修改商品数据
     * @param id
     * @param newProduct
     * @param specifications
     * @param bulk
     * @param unit
     * @param feature
     * @return
     */
    @RequestMapping("/editProduct")
    public ModelAndView editProduct(String id,String newProduct,String specifications,String bulk, String unit,String feature,HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            mv.setViewName("login");
        } else {
            //查询当前商品名是否已经存在
            DataBase dataBase = productService.queryDataBaseByDataName(newProduct);
            if (dataBase == null){
                //保存新商品名称字典
                DataBase dataBase1 = new DataBase();
                dataBase1.setDetail("产品类型");
                dataBase1.setDataType("1");
                dataBase1.setDataName(newProduct);
                dataBase1.setDataValue(productService.queryMaxFromDataBase());
                //保存字典
                productService.saveDataBase(dataBase1);
                //获取商品字典
                String dictName = productService.queryDictByName(newProduct);
                //查询所选容积单位字典值
                String dictUnit = productService.queryDictByUnit(unit);
                //查询商品特征字典值
                String dictFeature = productService.queryDictByFeature(feature);
                //生成容积单位
                String specificationsss = "";
                String[] sizes = specifications.split("\\*");
                for (int i = 0; i<sizes.length; i++){
                    if (sizes[i].length() == 3){
                        specificationsss += sizes[i];
                    } else if (sizes[i].length() == 2){
                        specificationsss = specificationsss+"0"+sizes[i];
                    }
                }
                String size = bulk+unit+specificationsss+dictFeature;
                try {
                    //进行商品的修改
                    productService.editProduct(id,dictName,bulk,dictUnit,specifications,dictFeature,size);
                } catch (Exception e){
                    mv.addObject("error","商品添加失败");
                    e.printStackTrace();
                    mv.setViewName("product/editProduct");
                }
            } else {
                //添加为已有商品，获取字典值
                //获取商品字典
                String dictName = productService.queryDictByName(newProduct);
                //查询所选容积单位字典值
                String dictUnit = productService.queryDictByUnit(unit);
                //查询商品特征字典值
                String dictFeature = productService.queryDictByFeature(feature);
                //切割容积
                String specificationsss = "";
                String[] sizes = specifications.split("\\*");
                for (int i = 0; i<sizes.length; i++){
                    if (sizes[i].length() == 3){
                        specificationsss += sizes[i];
                    } else if (sizes[i].length() == 2){
                        specificationsss = specificationsss+"0"+sizes[i];
                    }

                }
                //生成容积单位
                String size = bulk+unit+specificationsss+dictFeature;
                try {
                    //进行商品修改
                    productService.editProduct(id,dictName,bulk,dictUnit,specifications,dictFeature,size);
                } catch (Exception e){
                    mv.addObject("error","商品添加失败");
                    e.printStackTrace();
                    mv.setViewName("product/editProduct");
                }
            }
            mv.setViewName("/product/productListPage");
        }
        return mv;
    }

    /**
     * 删除商品信息
     * @param id
     * @param request
     * @return
     */
    @RequestMapping("/deleteProduct")
    @ResponseBody
    public JsonVo deleteProduct(String id,HttpServletRequest request){
        JsonVo json = new JsonVo();
        if (request.getSession().getAttribute("aid") == null){
            //未登录
            json.setResult(false);
            json.setMsg("请登陆");
        } else {
            try {
                productService.delete(id);
                json.setResult(true);
                json.setMsg("商品删除成功");
            } catch (Exception e){
                e.printStackTrace();
                json.setResult(false);
                json.setMsg("商品删除失败");
            }
        }
        return json;
    }

    /**
     * 查询数据，并跳转到客户添加页面
     * @param request
     * @return
     */
    @RequestMapping("/queryAddProductInfo")
    public ModelAndView queryAddProductInfo(HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            mv.setViewName("login");
        } else {
            //查询已有商品名称
            List<DataBase> list = productService.queryAllDataName();
            mv.addObject("list",list);
            mv.setViewName("product/addProduct");
        }
        return mv;
    }

    /**
     * 保存新商品信息
     * @param newProduct
     * @param specifications
     * @param bulk
     * @param unit
     * @param feature
     * @param request
     * @return
     */
    @RequestMapping("/addProduct")
    @ResponseBody
    public JsonVo addProduct(String newProduct, String specifications, String bulk, String unit, String feature,HttpServletRequest request){
        JsonVo json = new JsonVo();
        if (request.getSession().getAttribute("aid") == null){
            json.setResult(false);
            json.setMsg("请先登陆");
        } else {
            try {
                //商品类型处理
                String productNameId = productService.queryProductIdByName(newProduct);
                //商品乘积处理
                String specificName = "";
                String[] specific = specifications.split("\\*");
                for (int i = 0; i <specific.length; i++){
                    if (specific[i].length() == 3){
                        specificName += specific[i];
                    } else if (specific[i].length() == 2){
                        specificName = specificName+"0"+specific[i];
                    }
                }
                //商品容积处理

                //商品单位处理
                String unitId = productService.queryUnitIdByName(unit);
                //商品特征处理
                String featureId = productService.queryFeatureIdByName(feature);
                //组合商品名称
                String productName = bulk+unit+specificName+featureId;
                //查看商品是否重复
                String id = productService.queryIdByName(productName);
                if (id == null){
                    //商品添加
                    productService.addProduct(productNameId,bulk,unitId,specifications,featureId,productName);
                    json.setResult(true);
                    json.setMsg("添加成功");
                } else {
                    json.setResult(false);
                    json.setMsg("商品已存在");
                }
            } catch (Exception e){
                e.printStackTrace();
                json.setResult(false);
                json.setMsg("添加失败");
            }
        }
        return json;
    }

}
