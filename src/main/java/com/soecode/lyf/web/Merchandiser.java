package com.soecode.lyf.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Administrator on 2017/9/21.
 */
@Controller
@RequestMapping("/merchandiser")
public class Merchandiser {

    @RequestMapping("/administer")
    public ModelAndView administer(){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("merchandiser/administer");
        return mv;
    }

    @RequestMapping("/info")
    public ModelAndView info(){
        ModelAndView mv = new ModelAndView();
        //查询业务员于业务员相关比例
        mv.setViewName("merchandiser/info");
        return mv;
    }
}