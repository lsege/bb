package com.soecode.lyf.web;

import com.soecode.lyf.entity.*;
import com.soecode.lyf.entity.vo.JsonVo;
import com.soecode.lyf.service.BillService;
import com.soecode.lyf.service.OrderService;
import com.soecode.lyf.service.ReturnOrderService;
import com.soecode.lyf.service.SalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by lsd on 2017-06-01.
 */
@Controller
@RequestMapping("/returnsOrder")
public class ReturnsOrderController {

    @Autowired
    SalesService salesService;

    @Autowired
    ReturnOrderService returnOrderService;

    @RequestMapping("/goAddReturns")
    public ModelAndView goAddReturns(String page){
        ModelAndView mv = new ModelAndView();

        //显示商品列表
        List<Row> rows = new ArrayList<Row>();
        //查询商品列表
        int pageSize = 21;
        List<ProductBase> productBases = salesService.queryAllProduct();
        //总数据量
        mv.addObject("total", productBases.size());
        //总页数
        int pageTimes;
        if(productBases.size()%pageSize == 0)
        {
            pageTimes = productBases.size()/pageSize;
        }else {
            pageTimes = productBases.size()/pageSize + 1;
        }
        mv.addObject("pageCount", pageTimes);
        //页面初始的时候page没有值
        if (page == null || page == ""){
            page = "1";
        }
        //每页开始的第几条记录
        int startRow = (Integer.parseInt(page)-1) * pageSize;
        productBases = salesService.queryAllProductByPage(startRow, pageSize);
        //商品分组
        if (productBases.size()%3 == 0){
            for (int i=0; i<productBases.size()-2; i+=3){
                Row row = new Row();
                row.setId1(productBases.get(i).getId());
                row.setName1(productBases.get(i).getNameQual());
                row.setType1(productBases.get(i).getProductType());

                row.setId2(productBases.get(i+1).getId());
                row.setName2(productBases.get(i+1).getNameQual());
                row.setType2(productBases.get(i+1).getProductType());

                row.setId3(productBases.get(i+2).getId());
                row.setName3(productBases.get(i+2).getNameQual());
                row.setType3(productBases.get(i+2).getProductType());
                rows.add(row);
            }
        }
        if (productBases.size()%3 == 1){
            for (int i=0; i<productBases.size()-2; i+=3){
                Row row = new Row();
                row.setId1(productBases.get(i).getId());
                row.setName1(productBases.get(i).getNameQual());
                row.setType1(productBases.get(i).getProductType());

                row.setId2(productBases.get(i+1).getId());
                row.setName2(productBases.get(i+1).getNameQual());
                row.setType2(productBases.get(i+1).getProductType());

                row.setId3(productBases.get(i+2).getId());
                row.setName3(productBases.get(i+2).getNameQual());
                row.setType3(productBases.get(i+2).getProductType());

                if (i == productBases.size() -3){
                    row.setId1(productBases.get(i).getId());
                    row.setName1(productBases.get(i).getNameQual());
                    row.setType1(productBases.get(i).getProductType());
                }
                rows.add(row);
            }
        }
        if (productBases.size()%3 == 2){
            for (int i=0; i<productBases.size()-2; i+=3){
                Row row = new Row();
                row.setId1(productBases.get(i).getId());
                row.setName1(productBases.get(i).getNameQual());
                row.setType1(productBases.get(i).getProductType());

                row.setId2(productBases.get(i+1).getId());
                row.setName2(productBases.get(i+1).getNameQual());
                row.setType2(productBases.get(i+1).getProductType());

                row.setId3(productBases.get(i+2).getId());
                row.setName3(productBases.get(i+2).getNameQual());
                row.setType3(productBases.get(i+2).getProductType());
                if (i == productBases.size() -4){
                    row.setId1(productBases.get(i).getId());
                    row.setName1(productBases.get(i).getNameQual());
                    row.setType1(productBases.get(i).getProductType());
                }
                rows.add(row);
            }
        }
        //查询客户列表
        List<Customer> list = salesService.queryAllCustomer();
        List<Customer> customers = salesService.queryAllCustomerByPage(startRow,pageSize);
        List<CustomerRow> customerRows = new ArrayList<CustomerRow>();
        if (customers.size()%3 == 0){
            for (int i=0; i<customers.size(); i+=3){
                CustomerRow customerRow = new CustomerRow();
                customerRow.setName1(customers.get(i).getCustomerName());
                customerRow.setName2(customers.get(i+1).getCustomerName());
                customerRow.setName3(customers.get(i+2).getCustomerName());
                customerRows.add(customerRow);
            }
        }
        if (customers.size()%3 == 1){
            for (int i=0; i<customers.size(); i+=3){
                CustomerRow customerRow = new CustomerRow();
                if (i == customers.size() -1){
                    customerRow.setName1(customers.get(i).getCustomerName());
                    customerRow.setName2("");
                    customerRow.setName3("");
                } else {
                    customerRow.setName1(customers.get(i).getCustomerName());
                    customerRow.setName2(customers.get(i+1).getCustomerName());
                    customerRow.setName3(customers.get(i+2).getCustomerName());
                }
                customerRows.add(customerRow);
            }
        }
        if (customers.size()%3 == 2){
            for (int i=0; i<customers.size(); i+=3){
                CustomerRow customerRow = new CustomerRow();
                if (i == customers.size() -1){
                    customerRow.setName1(customers.get(i).getCustomerName());
                    customerRow.setName2(customers.get(i+1).getCustomerName());
                    customerRow.setName3("");
                } else {
                    customerRow.setName1(customers.get(i).getCustomerName());
                    customerRow.setName2(customers.get(i+1).getCustomerName());
                    customerRow.setName3(customers.get(i+2).getCustomerName());
                }
                customerRows.add(customerRow);
            }
        }

        for (int i=0; i<customers.size(); i++){
            customers.get(i).setIndex((Integer.parseInt(page)-1)*10+1+i);
        }


        /*查询销售人员列表*/
        List<SalesMan> salesMan = salesService.queeeryAllSalesMan();
        List<SalesMan> salesMEN = salesService.querySalesManByPage(startRow,pageSize);
        List<SaleManRow> saleManRows = new ArrayList<SaleManRow>();
        //销售人员分组
        if (salesMEN.size()%3 == 0){
            for (int i=0; i<salesMEN.size(); i+=3){
                SaleManRow saleManRow = new SaleManRow();
                saleManRow.setName1(salesMEN.get(i).getName());
                saleManRow.setName2(salesMEN.get(i+1).getName());
                saleManRow.setName3(salesMEN.get(i+2).getName());
                saleManRows.add(saleManRow);
            }
        }
        if (salesMEN.size()%3 == 1){
            for (int i=0; i<salesMEN.size(); i+=3){
                SaleManRow saleManRow = new SaleManRow();
                if (i == salesMEN.size() -1){
                    saleManRow.setName1(salesMEN.get(i).getName());
                    saleManRow.setName2("");
                    saleManRow.setName3("");
                } else {
                    saleManRow.setName1(salesMEN.get(i).getName());
                    saleManRow.setName2(salesMEN.get(i+1).getName());
                    saleManRow.setName3(salesMEN.get(i+2).getName());

                }
                saleManRows.add(saleManRow);
            }
        }
        if (salesMEN.size()%3 == 2){
            for (int i=0; i<salesMEN.size(); i+=3){
                SaleManRow saleManRow = new SaleManRow();
                if (i == salesMEN.size() -2){
                    saleManRow.setName1(salesMEN.get(i).getName());
                    saleManRow.setName2(salesMEN.get(i+1).getName());
                    saleManRow.setName3("");
                } else {
                    saleManRow.setName1(salesMEN.get(i).getName());
                    saleManRow.setName2(salesMEN.get(i+1).getName());
                    saleManRow.setName3(salesMEN.get(i+2).getName());

                }
                saleManRows.add(saleManRow);
            }
        }




        /*生成单据编号*/
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddhhmmss");
        Date data2 = new Date();
        String number = sf.format(data2);
        mv.addObject("saleSize",salesMan.size());
        mv.addObject("size",list.size());
        mv.addObject("number",number);
        mv.addObject("saleManRows",saleManRows);
        mv.addObject("customerRows",customerRows);
        mv.addObject("rows",rows);
        mv.addObject("currentPage",Integer.parseInt(page));

        mv.setViewName("return/addReturnsOrder");
        return mv;
    }

    @RequestMapping("/addReyurnOrder")
    @ResponseBody
    public JsonVo addReyurnOrder(String[] data1,String[] data2,String[] data3){
        JsonVo json = new JsonVo();

        String customerName;
        String [] customerNames = data1[0].split("=");
        customerName = customerNames[1];
        String [] createDates = data1[1].split("=");
        String createDate;
        createDate = createDates[1];
        String salesmanName;
        String [] salesmanNames = data1[2].split("=");
        salesmanName = salesmanNames[1];
        String documentNumber;
        String [] documentNumbers = data1[3].split("=");
        documentNumber = documentNumbers[1];

        String inputMan;
        try {
            inputMan = (data3[0].split("="))[1];
        } catch (Exception e){
            inputMan = "";
        }
        String reviewerMan;
        try {
            reviewerMan = (data3[1].split("="))[1];
        } catch (Exception e){
            reviewerMan = "";
        }

        //多组表格数据
        String [] productNames =  new String[data2.length/9];
        String [] counts = new String[data2.length/9];
        String [] baks = new String[data2.length/9];
        //第一行数据
        try {
            productNames[0] = (data2[0].split("="))[1];
        } catch (Exception e){
            productNames[0] = "";
        }
        try {
            counts[0] = (data2[1].split("="))[1];
        } catch (Exception e){
            counts[0] = "";
        }
        try{
            baks[0] = (data2[2].split("="))[1];
        }catch (Exception e){
            baks[0] = "";
        }


        try {
            String productId = salesService.queryProductId(productNames[0]);
            String salesManId = salesService.querySalesManId(salesmanName);
            String customerId = salesService.queryCustomerIdByName(customerName);
            returnOrderService.addReturn(documentNumber,customerId,productId,counts[0],baks[0],inputMan,reviewerMan,createDate,salesManId);
            json.setResult(true);
            json.setMsg("保存成功");
        } catch (Exception e){
            e.printStackTrace();
            json.setResult(false);
            json.setMsg("保存失败");
        }

        return json;
    }
}
