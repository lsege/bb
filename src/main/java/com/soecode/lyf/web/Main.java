package com.soecode.lyf.web;

import com.soecode.lyf.entity.User;
import com.soecode.lyf.entity.vo.JsonVo;
import com.soecode.lyf.service.BillService;
import com.soecode.lyf.service.MainService;
import com.soecode.lyf.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by lsd on 2017-06-01.
 */
@Controller
@RequestMapping("/main")
public class Main {

    @Autowired
    MainService mainService;

    /**
     * 参数加密
     * @param inStr
     * @return
     */
    @RequestMapping("/encryption")
    @ResponseBody
    public static JsonVo encryption(String inStr) {
        JsonVo json = new JsonVo();
        char[] a = inStr.toCharArray();
        for (int i = 0; i < a.length; i++) {
            a[i] = (char) (a[i] ^ 't');
        }
        String s = new String(a);
        json.setT(s);
        json.setResult(true);
        return json;
    }

    /**
     * 用户登陆 设置session
     * @param str
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/home", method = RequestMethod.GET)
    private JsonVo home(String str, HttpServletRequest request){
        //获取Session
        HttpSession session = request.getSession();
        JsonVo jsonVo = new JsonVo();
        try{
            String decode = JM(str);
            String name =decode.substring(0,32);
            String pwd = decode.substring(32);
            User b = mainService.selectUser(name,pwd);
            if(name.equals(b.getUsername())&&pwd.equals(b.getPassword())){
                session.setAttribute("aid",b.getId());
                session.setAttribute("level",b.getLevel());
                session.setAttribute("names",b.getNames());
                session.setMaxInactiveInterval(60*30);
                jsonVo.setResult(true);
            }
        }catch (Exception e){
            e.printStackTrace();
            jsonVo.setResult(false);
        }
        return jsonVo;
    }

    /**
     * 进入首页
     * @param request
     * @return
     */
    @RequestMapping("/index")
    public ModelAndView aa (HttpServletRequest request){
        ModelAndView modelAndView = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            //未登录，返回登陆页
            modelAndView.setViewName("login");
        } else {
            modelAndView.setViewName("admin/index");
        }
        return modelAndView;
    }

    /**
     * 参数解密
     * @param inStr
     * @return
     */
    public static String JM(String inStr) {
        char[] a = inStr.toCharArray();
        for (int i = 0; i < a.length; i++) {
            a[i] = (char) (a[i] ^ 't');
        }
        String k = new String(a);
        return k;
    }
}
