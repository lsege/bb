package com.soecode.lyf.web;

import com.soecode.lyf.entity.Product;
import com.soecode.lyf.service.OrderService;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lsd on 2017-04-18.
 */
@Controller
@RequestMapping("/add")
public class NewAddController {
    @Autowired
    OrderService orderService;
    @ResponseBody
    @RequestMapping(value = "/selectList")
    public ModelAndView findProduct(){
        //铝瓶
        List<Product> mlist =  orderService.findList(1);
        //封口钳
        List<Product> nlist =  orderService.findList(1);
        //封盖机
        List<Product> blist =  orderService.findList(1);
        List<List<Product>> listm= new ArrayList<List<Product>>();
        //获取被拆分的数组个数
        int size = 6;
        int arrm = mlist.size()%size==0?mlist.size()/size:mlist.size()/size+1;
        for(int i=0;i<arrm;i++) {
            List<Product>  sub = new ArrayList<Product>();
            //把指定索引数据放入到list中
            for(int j=i*size;j<=size*(i+1)-1;j++) {
                if(j<=mlist.size()-1) {
                    sub.add(mlist.get(j));
                }
            }
            listm.add(sub);
        }
        List<List<Product>> listn = new ArrayList<List<Product>>();
        //获取被拆分的数组个数
        int arrn = nlist.size()%size==0?nlist.size()/size:nlist.size()/size+1;
        for(int i=0;i<arrn;i++) {
            List<Product>  sub = new ArrayList<Product>();
            //把指定索引数据放入到list中
            for(int j=i*size;j<=size*(i+1)-1;j++) {
                if(j<=nlist.size()-1) {
                    sub.add(nlist.get(j));
                }
            }
            listn.add(sub);
        }
        List<List<Product>> listb = new ArrayList<List<Product>>();
        //获取被拆分的数组个数
        int arrb = blist.size()%size==0?blist.size()/size:blist.size()/size+1;
        for(int i=0;i<arrb;i++) {
            List<Product>  sub = new ArrayList<Product>();
            //把指定索引数据放入到list中
            for(int j=i*size;j<=size*(i+1)-1;j++) {
                if(j<=blist.size()-1) {
                    sub.add(blist.get(j));
                }
            }
            listb.add(sub);
        }
        ModelAndView mv = new ModelAndView();
        mv.setViewName("order/throughPath");
        mv.addObject("a",listm);
        mv.addObject("b",listn);
        mv.addObject("c",listb);
        return mv;
    }
}
