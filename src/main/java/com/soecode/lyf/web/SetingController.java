package com.soecode.lyf.web;

import com.soecode.lyf.entity.Customer;
import com.soecode.lyf.entity.vo.JsonVo;
import com.soecode.lyf.service.CustomerService;
import com.soecode.lyf.service.ProductService;
import com.soecode.lyf.web.base.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 选择查询
 * Created by 10091 on 2017-05-25.
 */
@Controller
@RequestMapping("/settings")
public class SetingController extends BaseController {

    @Autowired
    CustomerService customerService;

    @Autowired
    ProductService productService;

    @RequestMapping(value="/select_customer")
    public ModelAndView select_customer(){
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("set/selectCustomer");
        return mv;
    }


    @RequestMapping(value="/goods_batch")
    public ModelAndView goods_batch(){
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("set/selectGoods");
        return mv;
    }
    @RequestMapping(value="/goods_batch2")
    public ModelAndView goods_batch2(){
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("set/selectGoods2");
        return mv;
    }

    @RequestMapping(value="/add_sales")
    public ModelAndView addsales(){
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("set/addsales");
        return mv;
    }


}
