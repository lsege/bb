package com.soecode.lyf.web;

import com.soecode.lyf.dto.Result;
import com.soecode.lyf.entity.*;
import com.soecode.lyf.entity.vo.JsonVo;
import com.soecode.lyf.entity.vo.MoneyAccept;
import com.soecode.lyf.entity.vo.OrderList;
import com.soecode.lyf.service.BillService;
import com.soecode.lyf.service.OrderService;
import com.soecode.lyf.service.SalesService;
import com.soecode.lyf.web.base.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by lsd on 2017-03-13.
 */
@Controller
@RequestMapping("/order")
public class OrderController extends BaseController{

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private OrderService orderService;

    @Autowired
    private BillService billService;

    @Autowired
    SalesService salesService;

    /**
     * 请求首页（销货单）页面，并读取数据
     * @param page
     * @param request
     * @return
     */
    @RequestMapping(value="/goAddSalesOrder")
    public ModelAndView addSalesOrder(String page,HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            mv.setViewName("login");
        } else {
            //显示商品列表
            List<Row> rows = new ArrayList<Row>();
            //查询商品列表
            int pageSize = 21;
            List<ProductBase> productBases = salesService.queryAllProduct();
            //总数据量
            mv.addObject("total", productBases.size());
            //总页数
            int pageTimes;
            if(productBases.size()%pageSize == 0)
            {
                pageTimes = productBases.size()/pageSize;
            }else {
                pageTimes = productBases.size()/pageSize + 1;
            }
            mv.addObject("pageCount", pageTimes);
            //页面初始的时候page没有值
            if (page == null || page == ""){
                page = "1";
            }
            //每页开始的第几条记录
            int startRow = (Integer.parseInt(page)-1) * pageSize;
            productBases = salesService.queryAllProductByPage(startRow, pageSize);
            //商品分组
            if (productBases.size()%3 == 0){
                for (int i=0; i<productBases.size()-2; i+=3){
                    Row row = new Row();
                    row.setId1(productBases.get(i).getId());
                    row.setName1(productBases.get(i).getNameQual());
                    row.setType1(productBases.get(i).getProductType());

                    row.setId2(productBases.get(i+1).getId());
                    row.setName2(productBases.get(i+1).getNameQual());
                    row.setType2(productBases.get(i+1).getProductType());

                    row.setId3(productBases.get(i+2).getId());
                    row.setName3(productBases.get(i+2).getNameQual());
                    row.setType3(productBases.get(i+2).getProductType());
                    rows.add(row);
                }
            }
            if (productBases.size()%3 == 1){
                for (int i=0; i<productBases.size()-2; i+=3){
                    Row row = new Row();
                    row.setId1(productBases.get(i).getId());
                    row.setName1(productBases.get(i).getNameQual());
                    row.setType1(productBases.get(i).getProductType());

                    row.setId2(productBases.get(i+1).getId());
                    row.setName2(productBases.get(i+1).getNameQual());
                    row.setType2(productBases.get(i+1).getProductType());

                    row.setId3(productBases.get(i+2).getId());
                    row.setName3(productBases.get(i+2).getNameQual());
                    row.setType3(productBases.get(i+2).getProductType());

                    if (i == productBases.size() -3){
                        row.setId1(productBases.get(i).getId());
                        row.setName1(productBases.get(i).getNameQual());
                        row.setType1(productBases.get(i).getProductType());
                    }
                    rows.add(row);
                }
            }
            if (productBases.size()%3 == 2){
                for (int i=0; i<productBases.size()-2; i+=3){
                    Row row = new Row();
                    row.setId1(productBases.get(i).getId());
                    row.setName1(productBases.get(i).getNameQual());
                    row.setType1(productBases.get(i).getProductType());

                    row.setId2(productBases.get(i+1).getId());
                    row.setName2(productBases.get(i+1).getNameQual());
                    row.setType2(productBases.get(i+1).getProductType());

                    row.setId3(productBases.get(i+2).getId());
                    row.setName3(productBases.get(i+2).getNameQual());
                    row.setType3(productBases.get(i+2).getProductType());
                    if (i == productBases.size() -4){
                        row.setId1(productBases.get(i).getId());
                        row.setName1(productBases.get(i).getNameQual());
                        row.setType1(productBases.get(i).getProductType());
                    }
                    rows.add(row);
                }
            }
            //查询客户列表
            List<Customer> list = salesService.queryAllCustomer();
            List<Customer> customers = salesService.queryAllCustomerByPage(startRow,pageSize);
            List<CustomerRow> customerRows = new ArrayList<CustomerRow>();
            if (customers.size()%3 == 0){
                for (int i=0; i<customers.size(); i+=3){
                    CustomerRow customerRow = new CustomerRow();
                    customerRow.setName1(customers.get(i).getCustomerName());
                    customerRow.setName2(customers.get(i+1).getCustomerName());
                    customerRow.setName3(customers.get(i+2).getCustomerName());
                    customerRows.add(customerRow);
                }
            }
            if (customers.size()%3 == 1){
                for (int i=0; i<customers.size(); i+=3){
                    CustomerRow customerRow = new CustomerRow();
                    if (i == customers.size() -1){
                        customerRow.setName1(customers.get(i).getCustomerName());
                        customerRow.setName2("");
                        customerRow.setName3("");
                    } else {
                        customerRow.setName1(customers.get(i).getCustomerName());
                        customerRow.setName2(customers.get(i+1).getCustomerName());
                        customerRow.setName3(customers.get(i+2).getCustomerName());
                    }
                    customerRows.add(customerRow);
                }
            }
            if (customers.size()%3 == 2){
                for (int i=0; i<customers.size(); i+=3){
                    CustomerRow customerRow = new CustomerRow();
                    if (i == customers.size() -1){
                        customerRow.setName1(customers.get(i).getCustomerName());
                        customerRow.setName2(customers.get(i+1).getCustomerName());
                        customerRow.setName3("");
                    } else {
                        customerRow.setName1(customers.get(i).getCustomerName());
                        customerRow.setName2(customers.get(i+1).getCustomerName());
                        customerRow.setName3(customers.get(i+2).getCustomerName());
                    }
                    customerRows.add(customerRow);
                }
            }

            for (int i=0; i<customers.size(); i++){
                customers.get(i).setIndex((Integer.parseInt(page)-1)*10+1+i);
            }


        /*查询销售人员列表*/
            List<SalesMan> salesMan = salesService.queeeryAllSalesMan();
            List<SalesMan> salesMEN = salesService.querySalesManByPage(startRow,pageSize);
            List<SaleManRow> saleManRows = new ArrayList<SaleManRow>();
            //销售人员分组
            if (salesMEN.size()%3 == 0){
                for (int i=0; i<salesMEN.size(); i+=3){
                    SaleManRow saleManRow = new SaleManRow();
                    saleManRow.setName1(salesMEN.get(i).getName());
                    saleManRow.setName2(salesMEN.get(i+1).getName());
                    saleManRow.setName3(salesMEN.get(i+2).getName());
                    saleManRows.add(saleManRow);
                }
            }
            if (salesMEN.size()%3 == 1){
                for (int i=0; i<salesMEN.size(); i+=3){
                    SaleManRow saleManRow = new SaleManRow();
                    if (i == salesMEN.size() -1){
                        saleManRow.setName1(salesMEN.get(i).getName());
                        saleManRow.setName2("");
                        saleManRow.setName3("");
                    } else {
                        saleManRow.setName1(salesMEN.get(i).getName());
                        saleManRow.setName2(salesMEN.get(i+1).getName());
                        saleManRow.setName3(salesMEN.get(i+2).getName());

                    }
                    saleManRows.add(saleManRow);
                }
            }
            if (salesMEN.size()%3 == 2){
                for (int i=0; i<salesMEN.size(); i+=3){
                    SaleManRow saleManRow = new SaleManRow();
                    if (i == salesMEN.size() -2){
                        saleManRow.setName1(salesMEN.get(i).getName());
                        saleManRow.setName2(salesMEN.get(i+1).getName());
                        saleManRow.setName3("");
                    } else {
                        saleManRow.setName1(salesMEN.get(i).getName());
                        saleManRow.setName2(salesMEN.get(i+1).getName());
                        saleManRow.setName3(salesMEN.get(i+2).getName());

                    }
                    saleManRows.add(saleManRow);
                }
            }
        /*生成单据编号*/
            SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddhhmmss");
            Date data2 = new Date();
            String number = sf.format(data2);
            mv.addObject("saleSize",salesMan.size());//销售人员数量
            mv.addObject("size",list.size());//客户列表
            mv.addObject("number",number);//单据编号
            mv.addObject("saleManRows",saleManRows);//销售分组数据
            mv.addObject("customerRows",customerRows);//客户分组数据
            mv.addObject("rows",rows);//商品分组数据
            mv.addObject("currentPage",Integer.parseInt(page));//当前页码
            mv.setViewName("sales/addSalesOrder");
        }
        return mv;
    }

    /**
     * 根据客户查询是否有需要付款的单据
     * @param customer
     * @return
     */
    @RequestMapping("/querySalesOrderByCustomer")
    @ResponseBody
    public JsonVo querySalesOrderByCustomer(String customer,HttpServletRequest request){
        JsonVo json = new JsonVo();
        if (request.getSession().getAttribute("aid") == null){
            json.setResult(false);
            json.setMsg("请先登陆");
        } else {
            //查询此客户下的单据编号
            List<CustomerArrears> orders = orderService.querySalesOrderByCustomer(customer);
            Map<String,Object> map = new HashMap<String, Object>();
            if (orders.size() != 0){
                map.put("size",orders.size());
                map.put("orders",orders);
                json.setMsg("此客户有需付款的销货单据");
                json.setT(map);
                json.setResult(true);
            } else {
                json.setMsg("此客户无需付款的销货单据");
                json.setResult(false);
            }
        }
        return json;
    }

    /**
     * 查询现金回款单单据编号是否存在
     * @param billNo
     * @return
     */
    @RequestMapping("/queryBillNo")
    @ResponseBody
    public JsonVo queryBillNo(String billNo){
        JsonVo jsonVo = new JsonVo();
        List<AllHead> allHeads = orderService.queryAllHeads(billNo);
        if (allHeads.size() > 0){
            jsonVo.setResult(false);
            jsonVo.setMsg("单据编号已经存在");
        } else {
            jsonVo.setResult(true);
        }
        return jsonVo;
    }

    /**
     * 回款单保存1（承兑回款单）
     * @param request
     * @param documentType1
     * @param payData1
     * @param salesMan1
     * @param billNo1
     * @param goodsName1
     * @param billNos1
     * @param price1
     * @param goodsContact1
     * @param note1
     * @param handlers1
     * @param enterOne1
     * @return
     */
    @RequestMapping(value="/addCashOrder1",method = RequestMethod.POST)
    @ResponseBody
    public JsonVo addCashOrder1(HttpServletRequest request,String documentType1,String payData1,String salesMan1,String billNo1,String goodsName1,String[] billNos1,String price1,String goodsContact1,String note1,String handlers1,String enterOne1,String sendUnit1,String toUnit1,String chengduiNum1,String chengduiDate1,String chengduiBang1){
        JsonVo json = new JsonVo();
        if (request.getSession().getAttribute("aid") == null){
            //未登录
            json.setResult(false);
            json.setMsg("尚未登陆");
        } else {
            try {
                String payment_date = payData1;
                String document_number = billNo1;
                String input_man = enterOne1;
                String brokerage = handlers1;
                String document_type = documentType1;
                String payment_customer_name = goodsName1;
                String cash_amount = price1;
                String customer_contact = goodsContact1;
                Date date1 = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String creation_date = sdf.format(date1);//创建时间

                //保存单据信息
                billService.saveCash(payment_date,document_number,input_man,brokerage,document_type,creation_date,salesMan1);
                billService.saveCashPrice(document_number,payment_customer_name,cash_amount,customer_contact,chengduiNum1,chengduiDate1,document_type,sendUnit1,toUnit1,chengduiBang1,"","","","","",note1,"","");
                //根据客户Id查询客户的欠款记录
                List<CustomerArrears> arrears = orderService.queryArrearsByName(goodsName1);
                if (arrears.size()>0){
                    if (billNos1 == null){

                    } else {
                        for (int i=0; i<billNos1.length; i++){
                            orderService.cancelSalesOrderByNumber(goodsName1,billNos1[i],price1,payData1);
                            orderService.addCheckPayment(document_number,billNos1[i]);
                        }
                    }
                } else {
                    //此客户无需付款单据，保存余额
                    orderService.addMouny(goodsName1,price1);
                }
                json.setResult(true);
                json.setMsg("承兑回款单保存成功");
            } catch (Exception e){
                json.setResult(false);
                json.setMsg("承兑回款单保存失败");
                e.printStackTrace();
            }
        }
        return json;
    }

    /**
     * 回款单保存(电汇回款单)
     * @param request
     * @param documentType2
     * @param payData2
     * @param salesMan2
     * @param billNo2
     * @param goodsName2
     * @param billNos2
     * @param price2
     * @param dianhuiBankName2
     * @param dianhuiPayAccount2
     * @param dianhuiCollectionAccount2
     * @param goodsContact2
     * @param note2
     * @param dianhuiContext2
     * @param handlers2
     * @param enterOne2
     * @return
     */
    @RequestMapping(value="/addCashOrder2",method = RequestMethod.POST)
    @ResponseBody
    public JsonVo addCashOrder2(HttpServletRequest request,String documentType2,String payData2,String salesMan2,String billNo2,String goodsName2,String[] billNos2,String price2,String dianhuiBankName2,String dianhuiPayAccount2,String dianhuiCollectionAccount2,String goodsContact2,String note2,String dianhuiContext2,String handlers2,String enterOne2){
        JsonVo json = new JsonVo();
        if (request.getSession().getAttribute("aid") == null){
            //未登录
            json.setResult(false);
            json.setMsg("尚未登陆");
        } else {
            try {
                String payment_date = payData2;
                String document_number = billNo2;
                String input_man = enterOne2;
                String brokerage = handlers2;
                String document_type = documentType2;
                String payment_customer_name = goodsName2;
                String cash_amount = price2;
                String customer_contact = goodsContact2;
                Date date1 = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String creation_date = sdf.format(date1);//创建时间

                //保存单据信息
                billService.saveCash(payment_date,document_number,input_man,brokerage,document_type,creation_date,salesMan2);
                billService.saveCashPrice(document_number,payment_customer_name,cash_amount,customer_contact,"","",document_type,"","","","",dianhuiBankName2,dianhuiPayAccount2,dianhuiCollectionAccount2,dianhuiContext2,note2,"","");
                //根据客户Id查询客户的欠款记录
                List<CustomerArrears> arrears = orderService.queryArrearsByName(goodsName2);
                if (arrears.size()>0){
                    if (billNos2 == null){

                    } else {
                        for (int i=0; i<billNos2.length; i++){
                            orderService.cancelSalesOrderByNumber(goodsName2,billNos2[i],price2,payData2);
                            orderService.addCheckPayment(document_number,billNos2[i]);
                        }
                    }
                } else {
                    //此客户无需付款单据，保存余额
                    orderService.addMouny(goodsName2,price2);
                }
                json.setResult(true);
                json.setMsg("电汇回款单保存成功");
            } catch (Exception e){
                json.setResult(false);
                json.setMsg("电汇回款单保存失败");
                e.printStackTrace();
            }
        }
        return json;
    }

    /**
     * 回款单保存(转账)
     * @param request
     * @param documentType3
     * @param payData3
     * @param salesMan3
     * @param billNo3
     * @param goodsName3
     * @param billNos3
     * @param price3
     * @param zhuanzhangPayAccount3
     * @param zhuanzhangCollectionAccount3
     * @param goodsContact3
     * @param note3
     * @param handlers2
     * @param enterOne2
     * @return
     */
    @RequestMapping(value="/addCashOrder3",method = RequestMethod.POST)
    @ResponseBody
    public JsonVo addCashOrder3(HttpServletRequest request,String documentType3,String payData3,String salesMan3,String billNo3,String goodsName3,String[] billNos3,String price3,String zhuanzhangPayAccount3,String zhuanzhangCollectionAccount3,String goodsContact3,String note3,String handlers2,String enterOne2){
        JsonVo json = new JsonVo();
        if (request.getSession().getAttribute("aid") == null){
            //未登录
            json.setResult(false);
            json.setMsg("尚未登陆");
        } else {
            try {
                String payment_date = payData3;
                String document_number = billNo3;
                String input_man = enterOne2;
                String brokerage = handlers2;
                String document_type = documentType3;
                String payment_customer_name = goodsName3;
                String cash_amount = price3;
                String customer_contact = goodsContact3;
                Date date1 = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String creation_date = sdf.format(date1);//创建时间

                //保存单据信息
                billService.saveCash(payment_date,document_number,input_man,brokerage,document_type,creation_date,salesMan3);
                billService.saveCashPrice(document_number,payment_customer_name,cash_amount,customer_contact,"","",document_type,"","","","","","","","",note3,zhuanzhangPayAccount3,zhuanzhangCollectionAccount3);
                //根据客户Id查询客户的欠款记录
                List<CustomerArrears> arrears = orderService.queryArrearsByName(goodsName3);
                if (arrears.size()>0){
                    if (billNos3 == null){

                    } else {
                        for (int i=0; i<billNos3.length; i++){
                            orderService.cancelSalesOrderByNumber(goodsName3,billNos3[i],price3,payData3);
                            orderService.addCheckPayment(document_number,billNos3[i]);
                        }
                    }
                } else {
                    //此客户无需付款单据，保存余额
                    orderService.addMouny(goodsName3,price3);
                }
                json.setResult(true);
                json.setMsg("电汇回款单保存成功");
            } catch (Exception e){
                json.setResult(false);
                json.setMsg("电汇回款单保存失败");
                e.printStackTrace();
            }
        }
        return json;
    }


    /**
     * 回款单保存4（现金回款单）
     * @param request
     * @param documentType4
     * @param payData4
     * @param salesMan4
     * @param billNo4
     * @param goodsName4
     * @param billNos4
     * @param price4
     * @param goodsContact4
     * @param note4
     * @param handlers4
     * @param enterOne4
     * @return
     */
    @RequestMapping(value="/addCashOrder4",method = RequestMethod.POST)
    @ResponseBody
    public JsonVo addCashOrder4(HttpServletRequest request,String documentType4,String payData4,String salesMan4,String billNo4,String goodsName4,String[] billNos4,String price4,String goodsContact4,String note4,String handlers4,String enterOne4){
        JsonVo json = new JsonVo();
        if (request.getSession().getAttribute("aid") == null){
            //未登录
            json.setResult(false);
            json.setMsg("尚未登陆");
        } else {
            try {
                String payment_date = payData4;
                String document_number = billNo4;
                String input_man = enterOne4;
                String brokerage = handlers4;
                String document_type = documentType4;
                String payment_customer_name = goodsName4;
                String cash_amount = price4;
                String customer_contact = goodsContact4;
                Date date1 = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String creation_date = sdf.format(date1);//创建时间

                Double money = 0.0;
                for (int i=0; i<billNos4.length; i++){
                    Double arrears = orderService.queryArrears(billNos4[i]);
                    money += arrears;
                }
                if (money < Double.parseDouble(price4)){
                    json.setResult(false);
                    json.setMsg("回款单金额不可大于销货单合计金额");
                    return json;
                }


                //保存单据信息
                billService.saveCash(payment_date,document_number,input_man,brokerage,document_type,creation_date,salesMan4);
                billService.saveCashPrice(document_number,payment_customer_name,cash_amount,customer_contact,"","",document_type,"","","","","","","","",note4,"","");
                //根据客户Id查询客户的欠款记录
                List<CustomerArrears> arrears = orderService.queryArrearsByName(goodsName4);
                if (arrears.size()>0){
                    if (billNos4 == null){

                    } else {
                        for (int i=0; i<billNos4.length; i++){
                            orderService.cancelSalesOrderByNumber(goodsName4,billNos4[i],price4,payData4);
                            orderService.addCheckPayment(document_number,billNos4[i]);
                        }
                    }
                } else {
                    //此客户无需付款单据，保存余额
                    orderService.addMouny(goodsName4,price4);
                }
                json.setResult(true);
                json.setMsg("回款单保存成功");
            } catch (Exception e){
                json.setResult(false);
                json.setMsg("回款单保存失败");
                e.printStackTrace();
            }
        }
        return json;
    }

    /**
     * 修改现金回款单页面
     * @param id
     * @param type
     * @return
     */
    @RequestMapping(value = "/editOrderPage", method = RequestMethod.GET)
    public ModelAndView editOrderPage(String id,String type,HttpServletRequest request) {
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            //未登录
            mv.setViewName("login");
        } else {
            if (type.equals("1")){
                //承兑修改
                OrderList list = orderService.queryAllOrderById(Long.parseLong(id));
                mv.setViewName("admin/edit2");
                mv.addObject("list", list);
            } else if (type.equals("2")){
                //电汇修改
            } else if (type.equals("3")){
                //转账修改
            } else if (type.equals("4")){
                //现金修改
                OrderList list = orderService.queryAllOrderById(Long.parseLong(id));
                mv.setViewName("admin/edit1");
                mv.addObject("list", list);
            }
        }
        return mv;
    }

    /**
     * 修改现金回款单据
     * @param id
     * @param createData
     * @param billNo
     * @param returnCustomer
     * @param returnPhone
     * @param returnMoney
     * @param returnType
     * @param customerAccount
     * @param endData
     * @param handlers
     * @param enterOne
     * @param note
     * @return
     */
    @RequestMapping("/editReceivableInfo")
    public ModelAndView editReceivableInfo(String id,String createData,String billNo,String returnCustomer,String returnPhone,String returnMoney,String returnType,String customerAccount,String endData,String handlers,String enterOne,String note,HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            //未登录
            mv.setViewName("login");
        } else {
            String create_data = createData;
            String document_number = billNo;
            String input_man = handlers;
            String brokerage = enterOne;
            String document_type = "1";
            Date date1 = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String creation_date = sdf.format(date1);//创建时间
            billService.editReturnMoney(creation_date, document_number, input_man, brokerage, document_type);
            String payment_customer_name = returnCustomer;
            String acceptance_amount = returnMoney;
            String customer_contact = returnPhone;
            String return_type = returnType;
            String customer_account = customerAccount;
            String end_data = endData;
            Long uId = Long.parseLong(id);
            billService.editReturnMoneyPrice(uId, document_number, payment_customer_name, acceptance_amount, customer_contact, return_type, customer_account, end_data, note);
            //每页显示的条数
            int pageSize = 10;
            List<MoneyAccept> list = orderService.queryAllOrder();
            //总数据量
            mv.addObject("total", list.size());
            //总页数
            int pageTimes;
            if (list.size() % pageSize == 0) {
                pageTimes = list.size() / pageSize;
            } else {
                pageTimes = list.size() / pageSize + 1;
            }
            mv.addObject("pageCount", pageTimes);
            //页面初始的时候page没有值
            String page = "1";
            //每页开始的第几条记录
            int startRow = (Integer.parseInt(page) - 1) * pageSize;
            list = this.orderService.queryAllOrderByPage(startRow, pageSize);
            mv.addObject("currentPage", Integer.parseInt(page));
            mv.addObject("list", list);
            mv.setViewName("sales/ReceivableListPage");
        }
        return mv;
    }

    /**
     * 删除回款单
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value = "/deleteReceivableInfo",method = RequestMethod.POST)
    @ResponseBody
    public JsonVo deleteReceivableInfo(String id,HttpServletRequest request){
        JsonVo json = new JsonVo();
        if (request.getSession().getAttribute("aid") == null){
            //未登录
            json.setResult(false);
            json.setMsg("请先登陆");
        } else {
            try{
                orderService.delete(id);
                json.setResult(true);
                json.setMsg("回款单删除成功");
            }catch (Exception e){
                json.setResult(false);
                json.setMsg("回款单删除失败");
            }
        }
        return json;
    }






















    @ResponseBody
    @RequestMapping(value = "/selectList")
    public JqGridPageView<SalesOrders> selectList(HttpServletRequest request,String keyword)throws Exception{
        JqGridPageView<SalesOrders> json = new JqGridPageView<SalesOrders>();
        System.out.println(keyword);
        Integer page = Integer.valueOf(request.getParameter("page"));
        Integer rows = Integer.valueOf(request.getParameter("rows"));
        Integer firstResult = (page-1)*rows;
        Integer maxResults = page*rows;
        List<SalesOrders> salesOrders = orderService.selectList(firstResult,maxResults,keyword);
        List<SalesOrders> bb=orderService.getTotal(keyword);
        int total = bb.size();
        json.setRows(salesOrders);
        json.setMaxResults(rows);
        json.setRecords(total);
        System.out.println(json);
        return json;
    }




    @RequestMapping(value="/goAddSale2")
    public ModelAndView goAddSale2(String name,String id,String spec){
        ModelAndView mv = this.getModelAndView();
        mv.setViewName("order/NewSale3");
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddhhmmss");
        Date data1 = new Date();
        String number = sf.format(data1);
        System.out.println(number);
        mv.addObject("name",name);
        mv.addObject("num",number);
        mv.addObject("id",id);
        mv.addObject("spec",spec);
        return mv;
    }

    @RequestMapping(value="/addNew",method = RequestMethod.POST)
    public ModelAndView addNew(HttpServletRequest request,String postData){
        String[] dapost = postData.split(":",1);
        JSONArray array = JSONArray.fromObject(dapost);
        JSONObject object = (JSONObject) array.get(0);
        String type = object.getString("order_type");//单据类型
        String custmerName = object.getString("custmerName");//客户
        String date = object.getString("date");//发货时间
        String number = object.getString("billNo");//编号
        String whether = object.getString("regular_customer");/*是否是老客户*/
        String input_man = object.getString("input_man");//录入人
        String reviewer = object.getString("reviewer");//审核人
        String totalQty = object.getString("totalQty");//总数目
        String totalAmount = object.getString("totalAmount");//总金额
        String totalPushMoney = object.getString("totalPushMoney");//总运费
        String whetherDateint = object.getString("whetherDateint");//是否延期
        String whetherElectint = object.getString("whetherElectint");//是否电汇
        String payDate = object.getString("payDate");//付款日期
        Date date1 = new Date();
        int numNo ;
        if(type.equals("销货单")){
            numNo = 1;
        }else {
            numNo = 2;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String criate = sdf.format(date1);//创建时间
        String del = String.valueOf(1);
        orderService.saveOrderList(numNo,custmerName,date,whether,input_man,reviewer,totalQty,totalAmount,number,del,criate,totalPushMoney,whetherDateint,whetherElectint,payDate);
        String entries = object.getString("entries");
        JSONArray json = JSONArray.fromObject(entries);
        for(int i=0;i<json.size();i++){
            JSONObject obj = (JSONObject) json.get(i);
            String goods = obj.getString("goods");
            String specificationModel = obj.getString("specificationModel");
            String qty = obj.getString("qty");
            String price = obj.getString("price");
            String amount = obj.getString("amount");
            String pushMoney = obj.getString("pushMoney");
            String descrip = obj.getString("description");
            String storageId = obj.getString("storageId");
            String item_type = String.valueOf(1);
            orderService.saveOrderDetail(goods,specificationModel,qty,price,amount,descrip,criate,item_type,number,storageId,pushMoney);
        }
        ModelAndView mv = new ModelAndView();
        mv.setViewName("redirect:/index/saleput");
        return mv;
    }

    //修改薪金单
    @RequestMapping("/editCash")
    public ModelAndView editCash(HttpServletRequest request,String id,String paymentDate,String listNum,String customrtName,String money,String customerPhone,String note,String brokerages,String inputMan){
        String payment_date = paymentDate;
        String list_num = listNum;
        String input_man = inputMan;
        String brokerage = brokerages;
        String document_type = "4";
        String customrt_name = customrtName;
        String cash_amount = money;
        String customer_contact = customerPhone;
        Date date1 = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String creation_date = sdf.format(date1);//创建时间
        billService.editCash(payment_date,list_num,input_man,brokerage,document_type,creation_date);
        Long uId = Long.parseLong(id);
        billService.editCashPrice(uId,list_num,customrt_name,cash_amount,customer_contact,note);


        ModelAndView modelAndView = new ModelAndView();
        //每页显示的条数
        int pageSize = 10;
        List<MoneyAccept> list = orderService.queryAllOrder();
        //总数据量
        modelAndView.addObject("total", list.size());
        //总页数
        int pageTimes;
        if(list.size()%pageSize == 0)
        {
            pageTimes = list.size()/pageSize;
        }else {
            pageTimes = list.size()/pageSize + 1;
        }
        modelAndView.addObject("pageCount", pageTimes);
        //页面初始的时候page没有值
        String page = "1";
        //每页开始的第几条记录
        int startRow = (Integer.parseInt(page)-1) * pageSize;
        list = this.orderService.queryAllOrderByPage(startRow, pageSize);
        modelAndView.addObject("currentPage", Integer.parseInt(page));
        modelAndView.addObject("list", list);
        modelAndView.setViewName("admin/index2");
        return modelAndView;
    }

    //添加回款单(电汇)
//    @RequestMapping(value="/addElect",method = RequestMethod.POST)
//    public ModelAndView addElect(HttpServletRequest request,String postData){
//        String[] dapost = postData.split(":",1);
//        JSONArray array = JSONArray.fromObject(dapost);
//        JSONObject object = (JSONObject) array.get(0);
//        String payment_date = object.getString("date");//发货时间
//        String document_number = object.getString("billNo");//编号
//        String input_man = object.getString("input_man");//录入人
//        String brokerage = object.getString("reviewer");//审核人
//        String document_type = "6";//票据类型（电汇）
//        String entries = object.getString("entries");
//        Date date1 = new Date();
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        String creation_date = sdf.format(date1);//创建时间
//        String note = "";
//        billService.saveCash(payment_date,document_number,input_man,brokerage,document_type,creation_date);
//        JSONArray json = JSONArray.fromObject(entries);
//        for(int i=0;i<json.size();i++){
//            JSONObject obj = (JSONObject) json.get(i);
//            String payment_customer_name = obj.getString("goods");
//            String cash_amount = obj.getString("price");
//            String customer_contact = obj.getString("description");
//            billService.saveElectPrice(document_number,payment_customer_name,cash_amount,customer_contact,note);
//        }
//        ModelAndView mv = new ModelAndView();
//        mv.setViewName("redirect:/bill/saleput");
//        return mv;
//    }
    //添加回款单(承兑)
//    @RequestMapping(value="/addAccept",method = RequestMethod.POST)
//    public ModelAndView addAccept(HttpServletRequest request,String payData,String billNo,String DTicketNo,String goodsName,String price,String bankName,String drawerData,String endData,String goodsContact,String note,String handlers,String enterOne){
//        String payment_date = payData;
//        String document_number = billNo;
//        String input_man = enterOne;
//        String brokerage = handlers;
//        String document_type = "5";
//        Date date1 = new Date();
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        String creation_date = sdf.format(date1);//创建时间
//        billService.saveCash(payment_date,document_number,input_man,brokerage,document_type,creation_date);
//        String payment_customer_name = goodsName;
//        String acceptance_amount = price;
//        String customer_contact = goodsContact;
//        String acceptance_number = DTicketNo;
//        String date_acceptance = endData;
//        billService.saveAcceptPrice(document_number,payment_customer_name,acceptance_amount,customer_contact,acceptance_number,date_acceptance,note);
//        ModelAndView mv = new ModelAndView();
//        mv.setViewName("admin/index2");
//        return mv;
//    }

    /*修改承兑*/
    @RequestMapping("/editAccept")
    public ModelAndView editAccept(HttpServletRequest request,String id,String paymentDate,String listNum,String numChengdui,String customrtName,String money,String creationDate,String dateChengdui,String customerPhone,String note,String brokerages,String inputMan){
        String payment_date = paymentDate;
        String document_number = listNum;
        String input_man = inputMan;
        String brokerage = brokerages;
        String document_type = "5";
        Date date1 = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String creation_date = sdf.format(date1);//创建时间
        billService.editCash(payment_date,document_number,input_man,brokerage,document_type,creation_date);
        String payment_customer_name = customrtName;
        String acceptance_amount = money;
        String customer_contact = customerPhone;
        String acceptance_number = numChengdui;
        String date_acceptance = dateChengdui;
        Long uId = Long.parseLong(id);
        billService.editAcceptPrice(uId,document_number,payment_customer_name,acceptance_amount,customer_contact,acceptance_number,date_acceptance,note);

        ModelAndView modelAndView = new ModelAndView();
        //每页显示的条数
        int pageSize = 10;
        List<MoneyAccept> list = orderService.queryAllOrder();
        //总数据量
        modelAndView.addObject("total", list.size());
        //总页数
        int pageTimes;
        if(list.size()%pageSize == 0)
        {
            pageTimes = list.size()/pageSize;
        }else {
            pageTimes = list.size()/pageSize + 1;
        }
        modelAndView.addObject("pageCount", pageTimes);
        //页面初始的时候page没有值
        String page = "1";
        //每页开始的第几条记录
        int startRow = (Integer.parseInt(page)-1) * pageSize;
        list = this.orderService.queryAllOrderByPage(startRow, pageSize);
        modelAndView.addObject("currentPage", Integer.parseInt(page));
        modelAndView.addObject("list", list);
        modelAndView.setViewName("admin/index2");
        return modelAndView;
    }

    //查询数据库里存在的公司（模糊查询）
    @ResponseBody
    @RequestMapping(value="/findByCustomer")
    public JSONArray selectCustomer(String customer){
        List<Customer> avlue = orderService.selectByCustomer("");
        System.out.println(avlue);
        //根据模糊查询，查询多个数据；将数据通过json传送
        JSONArray json2 = new JSONArray();
        JSONObject json = new JSONObject();
        for(Customer jb:avlue){
            json.put("ID",jb.getCustomerId());
            json.put("name",jb.getCustomerName());
            json.put("people",jb.getCustomerPeople());
            json2.add(json);
        }
        System.out.println(json2);
        return json2;
    }
    @RequestMapping(value="/addMachine",method = RequestMethod.POST)
    public ModelAndView addBottle(HttpServletRequest request,String postData){
        String[] dapost = postData.split(":",1);
        JSONArray array = JSONArray.fromObject(dapost);
        JSONObject object = (JSONObject) array.get(0);
        String billNo = object.getString("billNo");
        String sales_people = object.getString("sales_people");
        String time = object.getString("time");
        String make_man = object.getString("make_man");
        String make_date = object.getString("make_date");
        String input_man = object.getString("input_man");
        String input_date = object.getString("input_date");
        String reviewer_man = object.getString("reviewer_man");
        String reviewer_date = object.getString("reviewer_date");
        String sales_type = object.getString("sales_type");
        String entries = object.getString("entries");
        JSONArray json = JSONArray.fromObject(entries);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = new Date();
        String criate = sdf.format(date1);//创建时间
        int numNo ;
        if(sales_type.equals("bottle")){
            numNo = 1;
            for(int i=0;i<json.size();i++){
                JSONObject obj = (JSONObject) json.get(i);
                String goods = obj.getString("goods");
                String specificationModel = obj.getString("specificationModel");
                String qty = obj.getString("qty");
                String price = obj.getString("price");
                String amount = obj.getString("amount");
                String descrip = obj.getString("description");
                String storageId = obj.getString("storageId");
                String pushMoney = obj.getString("pushMoney");
                String item_type = String.valueOf(1);
                orderService.saveOrderDetail(goods,specificationModel,qty,price,amount,descrip,criate,item_type,billNo,storageId,pushMoney);
            }
        }else if(sales_type.equals("bottle")){
            numNo = 2;
        }else{
            numNo = 3;
        }

        String del = String.valueOf(1);
        orderService.saveProductList(billNo,sales_people,time,make_man,make_date,input_man,input_date,reviewer_man,
                reviewer_date, String.valueOf(numNo));
        ModelAndView mv = new ModelAndView();
        mv.setViewName("redirect:/index/saleput");
        return mv;
    }
    @RequestMapping("/addReturnOrder")
    public ModelAndView addReturnOrder(String returnData,String orderNo,String customerName,String productName,String productType,String count,String remark,String returnReason,String inputMan,String auditorMan){
        try {
            orderService.saveReturnOrder(returnData,orderNo,customerName,productName,productType,count,remark,returnReason,inputMan,auditorMan);
        } catch (Exception e){
            e.printStackTrace();
        }
        ModelAndView mv = new ModelAndView();
        mv.setViewName("admin/home");
        return mv;
    }
    /*添加退款单*/
    @RequestMapping("/addReturnMoney")
    public ModelAndView addReturnMoney(String createData,String billNo,String returnCustomer,String returnPhone,String returnMoney,String returnType,String customerAccount,String endData,String handlers,String enterOne,String note){

        String creation_date = createData;//创建时间
        String list_num = billNo;//单据编号
        String customrt_name = returnCustomer;//客户名称
        String customer_phone = returnPhone;//客户联系方式
        String money = returnMoney;//金额
        String return_type = returnType;//退款类型（现金，电汇）
        String customer_account = customerAccount;//客户账户
        String end_data = endData;//到款时间
        String brokerage = handlers;//经手人
        String input_man = enterOne;//录入人
        String document_type = "1";

        try {
            billService.saveReturnMoney(list_num,input_man,brokerage,document_type,creation_date);
            billService.saveReturnMoneyPrice(list_num,customrt_name,money,customer_phone,note,end_data,return_type,customer_account);
        } catch (Exception e){
            e.printStackTrace();
        }

        ModelAndView mv = new ModelAndView();
//        try {
//            orderService.saveReturnMoney(returnMoney);
//        } catch (Exception e){
//            e.printStackTrace();
//        }
        mv.setViewName("admin/index2");
        return mv;
    }
    @RequestMapping("/goAddReturns")
    public ModelAndView goAddReturns(String name,String id){
        ModelAndView mv = this.getModelAndView();
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddhhmmss");
        Date data1 = new Date();
        String number = sf.format(data1);
        mv.addObject("name",name);
        mv.addObject("num",number);
        mv.addObject("id",id);
        mv.setViewName("order/newReturns");
        return mv;
    }
    /*修改方法*/

    @RequestMapping(value="/deleteReturnOrder")
    @ResponseBody
    public Result<String> deleteReturnOrder(String id){
        try {
            orderService.deleteReturnOrder(id);
        } catch (Exception e){
            e.printStackTrace();
        }
        Result result = new Result();
        result.setSuccess(true);
        result.setError("删除成功");
        return result;
    }

    @RequestMapping("/deleteSalesOrder")
    @ResponseBody
    public Result<String> deleteSalesOrder(String id){
        try {
            orderService.deleteSalesOrder(id);
        } catch (Exception e){
            e.printStackTrace();
        }
        Result result = new Result();
        result.setSuccess(true);
        result.setError("删除成功");
        return result;
    }



}
