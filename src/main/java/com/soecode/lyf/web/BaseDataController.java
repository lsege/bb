package com.soecode.lyf.web;

import com.soecode.lyf.dto.PageData;
import com.soecode.lyf.dto.Result;
import com.soecode.lyf.dto.Result2;
import com.soecode.lyf.entity.SalesMan;
import com.soecode.lyf.entity.vo.*;
import com.soecode.lyf.service.BaseDataService;
import com.soecode.lyf.util.Md5;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by 10091 on 2017-05-25.
 */
@Controller
@RequestMapping("/basedata")
public class BaseDataController {

    @Autowired
    BaseDataService baseDataService;

    /**
     * 查询商品
     * @return
     */
    @RequestMapping(value="/invlocation")
    @ResponseBody
    public Result2<ProductVO> invlocation(){

        List<StorageVO> lists= baseDataService.queryStorage();
        PageData<StorageVO> datas = new PageData<StorageVO>();
        datas.setRows(lists);
        datas.setPage(1);
        datas.setRecords(20);
        datas.setTotal(1);

        return new Result2(true,datas);
    }
    /**
     * 查询商品
     * @return
     */
    @RequestMapping(value="/inventory")
    @ResponseBody
    public Result2 inventory(){
        List<ProductVO> lists= baseDataService.queryAll();
        PageData<ProductVO> datas = new PageData<ProductVO>();
        datas.setRows(lists);
        datas.setPage(1);
        datas.setRecords(20);
        datas.setTotal(1);
        return new Result2(true,datas);
    }

    /**
     * 查询商品销售信息
     * @return
     */
    @RequestMapping(value="/inventory2")
    @ResponseBody
    public Result2<ProductVO> inventory2(){
        List<ProductVO> lists= baseDataService.queryAll2();
        PageData<ProductVO> datas = new PageData<ProductVO>();
        datas.setRows(lists);
        datas.setPage(1);
        datas.setRecords(20);
        datas.setTotal(1);
        return new Result2(true,datas);
    }
    @RequestMapping("/inventory3")
    public ModelAndView send(String[] ids){
        ModelAndView mv = new ModelAndView();
        List<ProductVO> list = new ArrayList<ProductVO>();
        for (int i=0; i<ids.length; i++){
            ProductVO vo = baseDataService.queryData(ids[i]);
            list.add(vo);
        }
        mv.addObject(list);
        mv.setViewName("order/NewSale3");
        return mv;
    }

    @RequestMapping("/send")
    public Result2<ProductVO> send(String[] id,String[] name,String[] spec){
        List<ProductVO> list = new ArrayList<ProductVO>();
        for (int i = 0; i<id.length; i++){
            ProductVO vo = new ProductVO();
            vo.setId(Integer.parseInt(id[i]));
            vo.setName(name[i]);
            vo.setSpec(spec[i]);
            list.add(vo);
        }
        PageData<ProductVO> datas = new PageData<ProductVO>();
        datas.setRows(list);
        datas.setPage(1);
        datas.setRecords(20);
        datas.setTotal(1);
        return new Result2(true,datas);
    }

    /**
     * 保存订单商品
     * @return
     */
    @RequestMapping(value="/addNew",consumes = "application/json")
    @ResponseBody
    public Result<String> addNew(@RequestBody PostData postData ){
        baseDataService.saveSales(postData);
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddhhmmss");
        Date data1 = new Date();
        String number = sf.format(data1);
        Result<String> result = new Result<String>();
        result.setData(number);
        return result;
    }
    @RequestMapping(value="/addNewReturn",consumes = "application/json")
    @ResponseBody
    public Result<String> addNewReturn(@RequestBody PostData postData ){
        baseDataService.saveReturn(postData);
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddhhmmss");
        Date data1 = new Date();
        String number = sf.format(data1);
        Result<String> result = new Result<String>();
        result.setData(number);
        return result;
    }



    /**
     * 更新订单
     * @return
     */
    @RequestMapping(value="/updateInvSa",consumes = "application/json")
    @ResponseBody
    public Result<String> updateInvSa(@RequestBody PostData postData ){
        Integer id = baseDataService.updateSales(postData);
        return new Result(true,id);
    }

    @RequestMapping(value="/updateInvSa2",consumes = "application/json")
    @ResponseBody
    public Result<String> edit(@RequestBody PostData postData){
        Integer id = baseDataService.editReturnOrder(postData);
        return new Result(true,id);
    }

    @RequestMapping(value="/addNew2",consumes = "application/json")
    @ResponseBody
    public Result2<String> addNew2(@RequestBody PostData postData ){
        baseDataService.saveSales2(postData);
        return new Result2(true,"ok");
    }
    /**
     * 查询客户是否存在
     * @return
     */
    @RequestMapping(value="/findNearSaEmp")
    @ResponseBody
    public Result2<String> findNearSaEmp(@PathVariable("id") int id){

        int num=baseDataService.selectCustomerById(id);
        return new Result2(true,"success");
    }
    /**
     * 查询销售
     * @return
     */
    @RequestMapping(value="/employee")
    @ResponseBody
    public Result2<SalesMan> employee(){

        List<SalesMan> list=baseDataService.selectSales();
        return new Result2(true,list);
    }

    /**
     * 查询销售
     * @return
     */
    @RequestMapping(value="/contact")
    @ResponseBody
    public Result2<SalesMan> contact(){

        List<CustomerVO> list=baseDataService.selectCustomer();
        PageData<CustomerVO> datas = new PageData<CustomerVO>();
        datas.setRows(list);
        datas.setPage(1);
        datas.setRecords(20);
        datas.setTotal(1);
        return new Result2(true,datas);
    }

    /**
     * 添加业务员
     * @return
     */
    @RequestMapping(value="/addSalebb")
    @ResponseBody
    public boolean addSales(String name ,String phone){
        boolean b = true;
        try {
            baseDataService.saveSalesman(name,phone);
        }catch (Exception e){
            b=false;
        }
        return b;
    }
    /**
     * 添加顾客
     * @return
     */
    @RequestMapping(value="/addCustomer")
    @ResponseBody
    public boolean addCustomer(String name1 ,String phone1,String address,String people,String wheh){
        boolean b = true;
        try {
            baseDataService.saveSaleCustomer(name1,phone1,address,people,wheh);
        }catch (Exception e){
            b=false;
        }
        return b;
    }
    /**
     * 添加管理
     * @return
     */
    @RequestMapping(value="/addUser")
    @ResponseBody
    public boolean addUser(String username ,String userpwd){
        boolean b = true;
        try {
            baseDataService.saveUser(Md5.string2MD5(username),Md5.string2MD5(userpwd));
        }catch (Exception e){
            b=false;
        }
        return b;
    }


    /**
     * 查询商品销售信息
     * @return
     */
    @RequestMapping(value="/queryOrders")
    @ResponseBody
    public Result2<SalesVO> queryOrders(QueryVO qv){
        Result2 result2 =new Result2() ;
        if(null != qv){
            result2.setSuccess(true);
            List<SalesVO> list=baseDataService.queryOrders(qv);
            int count= baseDataService.queryOrdersCount(qv);
            PageData<SalesVO> datas = new PageData<SalesVO>();

            datas.setRows(list);
            datas.setPage(qv.getPage());
            datas.setRecords(count);
            datas.setTotal(count /qv.getRows() +1);

            result2.setData(datas);
        }else{
            result2.setSuccess(false);
            result2.setStatus(100);
        }

        return result2;
    }
    @RequestMapping("/queryOrders2")
    @ResponseBody
    public Result2<Sales2Vo> queryOrders2(QueryVO qv){
        Result2 result2 = new Result2();

        if(null != qv){
            result2.setSuccess(true);
            List<Sales2Vo> list = baseDataService.queryOrders2(qv);//查询退货单数据
            int count = baseDataService.queryOrders2Count(qv);//查询退货单总数
            PageData<Sales2Vo> data = new PageData<Sales2Vo>();
            data.setRows(list);
            data.setPage(qv.getPage());
            data.setRecords(count);
            data.setTotal(count /qv.getRows() +1);
            result2.setData(data);
        }else{
            result2.setSuccess(false);
            result2.setStatus(100);
        }
        return result2;
    }



    /**
     * 查询销售
     * @return
     */
    @RequestMapping(value="/selectOrderDetail")
    @ResponseBody
    public Result<SalesMan> selectOrderDetail(Integer id,String type){
            PostData pd=baseDataService.selectOrderDetail(id);
            return new Result(true,pd);

    }
    @RequestMapping(value="/selectOrderDetai2")
    @ResponseBody
    public Result<SalesMan> selectOrderDetai2(Integer id,String type){
        PostData pd=baseDataService.selectOrderDetai2(id);
        return new Result(true,pd);

    }
}
