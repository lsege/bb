package com.soecode.lyf.web;

import com.mysql.jdbc.StringUtils;
import com.soecode.lyf.dto.Result;
import com.soecode.lyf.entity.*;
import com.soecode.lyf.entity.vo.JsonVo;
import com.soecode.lyf.entity.vo.PostData;
import com.soecode.lyf.entity.vo.Products;
import com.soecode.lyf.service.SalesService;
import com.soecode.lyf.web.base.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Administrator on 2017/8/24.
 */
@Controller
@RequestMapping("/sales")
public class SalesController extends BaseController {

    @Autowired
    SalesService salesService;

    /**
     * 商品分页（模糊查询）
     * @param page
     * @param name
     * @return
     */
    @RequestMapping("/selectProductByName")
    @ResponseBody
    public JsonVo selectProductByName(String page,String name){
        int pageSize = 21;

        JsonVo jsonVo = new JsonVo();
        List<Row> rows = new ArrayList<Row>();
        List<ProductBase> list = salesService.queryAllProductByName(name);
        //总数据量
        Map<String,Object> map = new HashMap<String, Object>();
        map.put("total", list.size());
        //总页数
        int pageTimes;
        if(list.size()%pageSize == 0)
        {
            pageTimes = list.size()/pageSize;
        }else {
            pageTimes = list.size()/pageSize + 1;
        }
        map.put("pageCount",pageTimes);
        //页面初始的时候page没有值
        if (page == null || page == ""){
            page = "1";
        }
        //每页开始的第几条记录
        int startRow = (Integer.parseInt(page)-1) * pageSize;

        list = salesService.queryAllProductByNamePage(startRow, pageSize,name);
        //商品分组
        if (list.size()%3 == 0){
            for (int i=0; i<list.size()-2; i+=3){
                Row row = new Row();
                row.setId1(list.get(i).getId());
                row.setName1(list.get(i).getNameQual());
                row.setType1(list.get(i).getProductType());

                row.setId2(list.get(i+1).getId());
                row.setName2(list.get(i+1).getNameQual());
                row.setType2(list.get(i+1).getProductType());

                row.setId3(list.get(i+2).getId());
                row.setName3(list.get(i+2).getNameQual());
                row.setType3(list.get(i+2).getProductType());
                rows.add(row);
            }
        }
        if (list.size()%3 == 1){
            for (int i=0; i<list.size()-2; i+=3){
                Row row = new Row();
                row.setId1(list.get(i).getId());
                row.setName1(list.get(i).getNameQual());
                row.setType1(list.get(i).getProductType());

                row.setId2(list.get(i+1).getId());
                row.setName2(list.get(i+1).getNameQual());
                row.setType2(list.get(i+1).getProductType());

                row.setId3(list.get(i+2).getId());
                row.setName3(list.get(i+2).getNameQual());
                row.setType3(list.get(i+2).getProductType());

                if (i == list.size() -3){
                    row.setId1(list.get(i).getId());
                    row.setName1(list.get(i).getNameQual());
                    row.setType1(list.get(i).getProductType());
                }
                rows.add(row);
            }
        }
        if (list.size()%3 == 2){
            for (int i=0; i<list.size()-2; i+=3){
                Row row = new Row();
                row.setId1(list.get(i).getId());
                row.setName1(list.get(i).getNameQual());
                row.setType1(list.get(i).getProductType());

                row.setId2(list.get(i+1).getId());
                row.setName2(list.get(i+1).getNameQual());
                row.setType2(list.get(i+1).getProductType());

                row.setId3(list.get(i+2).getId());
                row.setName3(list.get(i+2).getNameQual());
                row.setType3(list.get(i+2).getProductType());
                if (i == list.size() -4){
                    row.setId1(list.get(i).getId());
                    row.setName1(list.get(i).getNameQual());
                    row.setType1(list.get(i).getProductType());
                }
                rows.add(row);
            }
        }

        for (int i=0; i<list.size(); i++){
            list.get(i).setIndex((Integer.parseInt(page)-1)*10+1+i);
        }
        HttpSession session = getRequest().getSession();
        session.setAttribute("currentPage",Integer.parseInt(page));
        map.put("currentPage",Integer.parseInt(page));
        map.put("row",rows);
        jsonVo.setT(map);
        jsonVo.setResult(true);
        jsonVo.setMsg("获取分页成功");
        return jsonVo;
    }

    /**
     * 客户分页（模糊查询）
     * @param page
     * @param name
     * @return
     */
    @RequestMapping("/selectCustomerByName")
    @ResponseBody
    public JsonVo selectCustomerByName(String page,String name){
        if (page == null || page == ""){
            page = "1";
        }
        int pageSize = 21;

        JsonVo jsonVo = new JsonVo();
        List<CustomerRow> customerRows = new ArrayList<CustomerRow>();
        List<Customer> list = salesService.querCustomerByName(name);

        Map<String,Object> map = new HashMap<String, Object>();
        //页面初始的时候page没有值
        if (page == null || page == ""){
            page = "1";
        }
        //每页开始的第几条记录
        int startRow = (Integer.parseInt(page)-1) * pageSize;

        Integer pageCount = (list.size() / 21);

        list = salesService.queryAllCustomerByNamePage(startRow, pageSize,name);
        //商品分组

        if (list.size() %3 == 0){
            for (int i=0; i<list.size(); i+=3){
                CustomerRow customer = new CustomerRow();
                customer.setName1(list.get(i).getCustomerName());
                customer.setName2(list.get(i+1).getCustomerName());
                customer.setName3(list.get(i+2).getCustomerName());
                customerRows.add(customer);
            }
        }
        if (list.size()%3 == 1){
            for (int i=0; i<list.size(); i+=3){
                CustomerRow customerRow1 = new CustomerRow();
                if (list.size() -1 == i){
                    customerRow1.setName1(list.get(i).getCustomerName());
                    customerRow1.setName2("");
                    customerRow1.setName3("");
                } else {
                    customerRow1.setName1(list.get(i).getCustomerName());
                    customerRow1.setName2(list.get(i+1).getCustomerName());
                    customerRow1.setName3(list.get(i+2).getCustomerName());
                }
                customerRows.add(customerRow1);
            }
        }
        if (list.size()%3 == 2){
            for (int i=0; i<list.size(); i+=3){
                CustomerRow customerRow = new CustomerRow();
                if (i == list.size() -2){
                    customerRow.setName1(list.get(i).getCustomerName());
                    customerRow.setName2(list.get(i+1).getCustomerName());
                    customerRow.setName3("");
                } else {
                    customerRow.setName1(list.get(i).getCustomerName());
                    customerRow.setName2(list.get(i+1).getCustomerName());
                    customerRow.setName3(list.get(i+2).getCustomerName());
                }
                customerRows.add(customerRow);
            }
        }

        for (int i=0; i<list.size(); i++){
            list.get(i).setIndex((Integer.parseInt(page)-1)*10+1+i);
        }
        //总数据量
        map.put("total", list.size());
        //总页数
        int pageTimes;
        if(list.size()%pageSize == 0)
        {
            pageTimes = list.size()/pageSize;
        }else {
            pageTimes = list.size()/pageSize + 1;
        }
        map.put("pageCount",pageTimes);

        map.put("currentPage",Integer.parseInt(page));

        map.put("pageCount",pageCount);

        map.put("customerRows",customerRows);
        jsonVo.setT(map);
        jsonVo.setResult(true);
        jsonVo.setMsg("获取分页成功");
        return jsonVo;
    }

    /**
     * 销售分页
     * @param page
     * @return
     */
    @RequestMapping("/paginationSalesMan")
    @ResponseBody
    public JsonVo paginationSalesMan(String page){
        JsonVo jsonVo = new JsonVo();
        List<SaleManRow> saleManRows = new ArrayList<SaleManRow>();
        int pageSize = 21;
        List<SalesMan> list = salesService.queeeryAllSalesMan();
        //总数据量
        Map<String,Object> map = new HashMap<String, Object>();
        map.put("total", list.size());
        //总页数
        int pageTimes;
        if(list.size()%pageSize == 0)
        {
            pageTimes = list.size()/pageSize;
        }else {
            pageTimes = list.size()/pageSize + 1;
        }
        map.put("pageCount",pageTimes);
        //页面初始的时候page没有值
        if (page == null || page == ""){
            page = "1";
        }
        //每页开始的第几条记录
        int startRow = (Integer.parseInt(page)-1) * pageSize;

        list = salesService.querySalesManByPage(startRow, pageSize);
        //商品分组
        if (list.size()%3 == 0){
            for (int i=0; i<list.size(); i+=3){
                SaleManRow saleManRow = new SaleManRow();
                saleManRow.setName1(list.get(i).getName());
                saleManRow.setName2(list.get(i+1).getName());
                saleManRow.setName3(list.get(i+2).getName());
                saleManRows.add(saleManRow);
            }
        }
        if (list.size()%3 == 1){
            for (int i=0; i<list.size(); i+=3){
                SaleManRow saleManRow = new SaleManRow();
                if (i == list.size() -1){
                    saleManRow.setName1(list.get(i).getName());
                    saleManRow.setName2("");
                    saleManRow.setName3("");
                } else {
                    saleManRow.setName1(list.get(i).getName());
                    saleManRow.setName2(list.get(i+1).getName());
                    saleManRow.setName3(list.get(i+2).getName());

                }
                saleManRows.add(saleManRow);
            }
        }
        if (list.size()%3 == 2){
            for (int i=0; i<list.size(); i+=3){
                SaleManRow saleManRow = new SaleManRow();
                if (i == list.size() -2){
                    saleManRow.setName1(list.get(i).getName());
                    saleManRow.setName2(list.get(i+1).getName());
                    saleManRow.setName3("");
                } else {
                    saleManRow.setName1(list.get(i).getName());
                    saleManRow.setName2(list.get(i+1).getName());
                    saleManRow.setName3(list.get(i+2).getName());

                }
                saleManRows.add(saleManRow);
            }
        }

        HttpSession session = getRequest().getSession();
        session.setAttribute("currentPage",Integer.parseInt(page));
        map.put("currentPage",Integer.parseInt(page));
        map.put("saleManRows",saleManRows);
        jsonVo.setT(map);
        jsonVo.setResult(true);
        jsonVo.setMsg("获取分页成功");
        return jsonVo;
    }

    /**
     * 保存销货单
     * @param data1
     * @param data2
     * @param data3
     * @param data4
     * @return
     */
    @RequestMapping("/addSalesOrder")
    @ResponseBody
    public JsonVo addSalesOrder(String[] data1,String[] data2,String[] data3,String[] data4) {
        JsonVo jsonVo = new JsonVo();
        String customerName = "";
        String createDate = "";
        String salesmanName = "";
        String documentNumber = "";
        String inputMan = "";
        String reviewerMan = "";
        String regularCustomer = "";
        String saleMoney = "";
        String dueDate1 = "";
        String dueDate2 = "";
        String dueDate3 = "";
        String dueDate4 = "";
        String payDate = "";
        String whetherExtended = "";
        String pushMoney = "";
        try {
            pushMoney = (data2[9].split("="))[1];
        } catch (Exception e){
            pushMoney = "0";
        }

        //表头数据
        String [] customerNames = data1[0].split("=");
        customerName = customerNames[1];
        String [] createDates = data1[1].split("=");
        createDate = createDates[1];
        String [] salesmanNames = data1[2].split("=");
        salesmanName = salesmanNames[1];
        String [] documentNumbers = data1[3].split("=");
        documentNumber = documentNumbers[1];

        //判断当前单号是否存在
        List<SalesOrders> salesOrders = salesService.queryOrderByNumber(documentNumber);
        if (salesOrders.size() > 0){
            jsonVo.setResult(false);
            jsonVo.setMsg("单据编号已存在");
            return jsonVo;
        }

        //表尾数据
        String [] inputMans = data3[0].split("=");
        try {
            inputMan = inputMans[1];
        } catch (Exception e){
            inputMan = "";
        }
        String reMan = "";
        try {
            reMan = (data3[1].split("="))[1];
            reviewerMan = reMan;
        } catch (Exception e){
            reviewerMan = "";
        }

        String [] regularCustomers = data3[2].split("=");
        regularCustomer = regularCustomers[1];

        String [] dueDates1 = data3[3].split("=");
        dueDate1 = dueDates1[1];
        String [] dueDates2 = data3[4].split("=");
        dueDate2 = dueDates2[1];
        String [] dueDates3 = data3[5].split("=");
        dueDate3 = dueDates3[1];
        String [] dueDates4 = data3[6].split("=");
        dueDate4 = dueDates4[1];

        String [] payDates = data3[7].split("=");
        payDate = payDates[1];
        String [] whetherExtendeds = data3[8].split("=");
        whetherExtended = whetherExtendeds[1];

        String [] saleMoneys = data3[9].split("=");
        saleMoney = saleMoneys[1];





        //多组表格数据
        String [] BProductName =  new String[data2.length/9];
        String [] BCount = new String[data2.length/9];
        String [] BPrice = new String[data2.length/9];
        String [] BMoney = new String[data2.length/9];
        String [] BBak = new String[data2.length/9];
        String [] WProductName = new String[data2.length/9];
        String [] WCount = new String[data2.length/9];
        String [] WPrice = new String[data2.length/9];
        String [] WMoney = new String[data2.length/9];
        //第一行数据
        String bProductName = "";
        try {
            BProductName[0] = (data2[0].split("="))[1].split("_")[1];
        } catch (Exception e){
            BProductName[0] = "";
        }
        try {
            BCount[0] = (data2[1].split("="))[1];
        } catch (Exception e){
            BCount[0] = "";
        }
        try {
            BPrice[0] = (data2[2].split("="))[1];
        } catch (Exception e){
            BPrice[0] = "";
        }
        try {
            BMoney[0] = (data2[3].split("="))[1];
        } catch (Exception e){
            BMoney[0] = "";
        }
        try{
            BBak[0] = (data2[4].split("="))[1];
        }catch (Exception e){
            BBak[0] = "";
        }
        try {
            WProductName[0] = (data2[5].split("="))[1].split("_")[1];
        } catch (Exception e){
            WProductName[0] = "";
        }
        try {
            WCount[0] = (data2[6].split("="))[1];
        } catch (Exception e){
            WCount[0] = "";
        }
        try {
            WPrice[0] = (data2[7]).split("=")[1];
        } catch (Exception e){
            WPrice[0] = "";
        }
        try {
            WMoney[0] = (data2[8].split("="))[1];
        } catch (Exception e){
            WMoney[0] = "";
        }
        int index = 0;
        if (data2.length > 10){
            if ((data2.length - 10)%9 == 0){
                index = (data2.length - 10)/9;
            } else {
                index = (data2.length-10)/9+1;
            }
            //第二行以上数据
            for (int i=0; i<index; i++){
                if ((data2[i*9+10+0].split("=")).length ==2){
                    try {
                        BProductName[i+1] = (data2[i*9+10+0].split("="))[1].split("_")[1];
                    } catch (Exception e){
                        BProductName[i+1] = "";
                    }
                    try {
                        BCount[i+1] = (data2[i*9+10+1].split("="))[1];
                    } catch (Exception e){
                        BCount[i+1] = "";
                    }
                    try {
                        BPrice[i+1] = (data2[i*9+10+2].split("="))[1];
                    } catch (Exception e){
                        BPrice[i+1] = "";
                    }
                    try {
                        BMoney[i+1] = (data2[i*9+10+3].split("="))[1];
                    } catch (Exception e){
                        BMoney[i+1] = "";
                    }
                    try {
                        BBak[i+1] = (data2[i*9+10+4].split("="))[1];
                    } catch (Exception e){
                        BBak[i+1] = "";
                    }
                    try {
                        WProductName[i+1] = (data2[i*9+10+5].split("="))[1].split("_")[1];
                    } catch (Exception e){
                        WProductName[i+1] = "";
                    }
                    try {
                        WCount[i+1] = (data2[i*9+10+6].split("="))[1];
                    } catch (Exception e){
                        WCount[i+1] = "";
                    }
                    try {
                        WPrice[i+1] = (data2[i*9+10+7]).split("=")[1];
                    } catch (Exception e){
                        WPrice[i+1] = "";
                    }
                    try {
                        WMoney[i+1] = (data2[i*9+10+8].split("="))[1];
                    } catch (Exception e){
                        WMoney[i+1] = "";
                    }
                }
            }
        }

        //合计数据
        String pSum = "";
        try {
            pSum = data4[0].split("=")[1];
        } catch (Exception e){
            pSum = "0";
        }

        String wSum = "";
        try {
            wSum = data4[1].split("=")[1];
        } catch (Exception e){
            wSum = "0";
        }
        //保存SalesOrders
        try {
            String customerId = salesService.queryCustomerIdByName(customerName);
            String salesManId = salesService.querySalesManId(salesmanName);
            int a = BProductName.length;
            int m=0;
            try{
                for (int i=0; i<BProductName.length; i++){
                    String  p=BProductName[i];
                    m++;
                }
            }catch (Exception e){
                e.printStackTrace();
            }

            for (int i=0; i<m; i++){
                if (StringUtils.isNullOrEmpty(BProductName[i]) || BProductName[i].equals("")){
                    //没有本场
                    if (StringUtils.isNullOrEmpty(WProductName[i]) || WProductName[i].equals("")){
                        //没有外购
                        if (i == 0){
                            //为选择商品
                            jsonVo.setResult(false);
                            jsonVo.setMsg("请选择商品");
                            return jsonVo;
                        }
                        continue;
                    } else {
                        String wProductId = salesService.queryProductId(WProductName[i]);
                        //销货单信息保存（只有外购商品）
                        salesService.addPaymentDetails(documentNumber,"","","","","",wProductId,WCount[i],WPrice[i],WMoney[i]);
                    }
                } else {
                    //有本场
                    String bProductId = salesService.queryProductId(BProductName[i]);
                    if (WProductName[i] != "" || WProductName[i] != null || WProductName[i].length() != 0){
                        //有外购
                        String wProductId = salesService.queryProductId(WProductName[i]);
                        //销货单信息保存
                        salesService.addPaymentDetails(documentNumber,bProductId,BCount[i],BPrice[i],BMoney[i],BBak[i],wProductId,WCount[i],WPrice[i],WMoney[i]);
                    } else {
                        //没有外购
                        //销货单信息保存
                        salesService.addPaymentDetails(documentNumber,bProductId,BCount[i],BPrice[i],BMoney[i],BBak[i],"","0","0","0");
                    }
                }
            }
            if (payDate.equals("未付")){
                if (pSum .equals("0")){
                    if (wSum.equals("0")){
                        salesService.addCustomerArrears(customerId,documentNumber,"0","0");
                    }
                    //欠款单保存
                    salesService.addCustomerArrears(customerId,documentNumber,"0",wSum);
                } else {
                    if (wSum.equals("0")){
                        //欠款单保存
                        salesService.addCustomerArrears(customerId,documentNumber,pSum,"0");
                    } else {
                        //欠款单保存
                        salesService.addCustomerArrears(customerId,documentNumber,pSum,wSum);
                    }
                }
            }

            //销货单保存
            salesService.addSalesOrder(customerId,createDate,salesManId,documentNumber,inputMan,reviewerMan,regularCustomer,saleMoney,dueDate1,dueDate2,dueDate3,dueDate4,payDate,whetherExtended,pushMoney,pSum,wSum);
            jsonVo.setResult(true);
            jsonVo.setMsg("销货单据保存成功");
        } catch (Exception e){
            e.printStackTrace();
            jsonVo.setResult(false);
            jsonVo.setMsg("销货单据保存失败");
            return jsonVo;
        }
        return jsonVo;
    }

    /**
     * 删除销货单
     * @param id
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public JsonVo delete(String id, HttpServletRequest request){
        JsonVo json = new JsonVo();
        if (request.getSession().getAttribute("aid") == null){
            json.setResult(false);
            json.setMsg("请登陆");
        } else {
            SalesOrders salesOrders = salesService.querySalesOrderById(id);
            try {
                //删除销货单
                salesService.deleteSalesOrders(salesOrders.getId(),salesOrders.getDocumentNumber());
                //删除回款单
                salesService.deleteMoneyAccept(salesOrders.getDocumentNumber());
                //删除欠款单
                salesService.deleteArrears(salesOrders.getDocumentNumber());
                json.setResult(true);
                json.setMsg("销货单删除成功");
            } catch (Exception e){
                json.setResult(false);
                json.setMsg("销货单删除失败");
                e.printStackTrace();
            }
        }

        return json;
    }

    @RequestMapping("/editSalesOrder")
    public ModelAndView editSalesOrder(String id, HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            mv.setViewName("login");
        } else {
            mv.addObject("id",id);
            mv.setViewName("sales/editSalesPage");
        }
        return mv;
    }

    /**
     * 查询销货单信息详情
     * @param id
     * @param page
     * @return
     */
    @RequestMapping("/salesInfo")
    public ModelAndView salesInfo(String id,String page,HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            mv.setViewName("login");
        } else {
            //显示商品列表
            List<Row> rows = new ArrayList<Row>();
            //查询商品列表
            int pageSize = 21;
            List<ProductBase> productBases = salesService.queryAllProduct();
            //总数据量
            mv.addObject("total", productBases.size());
            //总页数
            int pageTimes;
            if(productBases.size()%pageSize == 0)
            {
                pageTimes = productBases.size()/pageSize;
            }else {
                pageTimes = productBases.size()/pageSize + 1;
            }
            mv.addObject("pageCount", pageTimes);
            //页面初始的时候page没有值
            if (page == null || page == ""){
                page = "1";
            }
            //每页开始的第几条记录
            int startRow = (Integer.parseInt(page)-1) * pageSize;
            productBases = salesService.queryAllProductByPage(startRow, pageSize);
            //商品分组
            if (productBases.size()%3 == 0){
                for (int i=0; i<productBases.size()-2; i+=3){
                    Row row = new Row();
                    row.setId1(productBases.get(i).getId());
                    row.setName1(productBases.get(i).getNameQual());
                    row.setType1(productBases.get(i).getProductType());

                    row.setId2(productBases.get(i+1).getId());
                    row.setName2(productBases.get(i+1).getNameQual());
                    row.setType2(productBases.get(i+1).getProductType());

                    row.setId3(productBases.get(i+2).getId());
                    row.setName3(productBases.get(i+2).getNameQual());
                    row.setType3(productBases.get(i+2).getProductType());
                    rows.add(row);
                }
            }
            if (productBases.size()%3 == 1){
                for (int i=0; i<productBases.size()-2; i+=3){
                    Row row = new Row();
                    row.setId1(productBases.get(i).getId());
                    row.setName1(productBases.get(i).getNameQual());
                    row.setType1(productBases.get(i).getProductType());

                    row.setId2(productBases.get(i+1).getId());
                    row.setName2(productBases.get(i+1).getNameQual());
                    row.setType2(productBases.get(i+1).getProductType());

                    row.setId3(productBases.get(i+2).getId());
                    row.setName3(productBases.get(i+2).getNameQual());
                    row.setType3(productBases.get(i+2).getProductType());

                    if (i == productBases.size() -3){
                        row.setId1(productBases.get(i).getId());
                        row.setName1(productBases.get(i).getNameQual());
                        row.setType1(productBases.get(i).getProductType());
                    }
                    rows.add(row);
                }
            }
            if (productBases.size()%3 == 2){
                for (int i=0; i<productBases.size()-2; i+=3){
                    Row row = new Row();
                    row.setId1(productBases.get(i).getId());
                    row.setName1(productBases.get(i).getNameQual());
                    row.setType1(productBases.get(i).getProductType());

                    row.setId2(productBases.get(i+1).getId());
                    row.setName2(productBases.get(i+1).getNameQual());
                    row.setType2(productBases.get(i+1).getProductType());

                    row.setId3(productBases.get(i+2).getId());
                    row.setName3(productBases.get(i+2).getNameQual());
                    row.setType3(productBases.get(i+2).getProductType());
                    if (i == productBases.size() -4){
                        row.setId1(productBases.get(i).getId());
                        row.setName1(productBases.get(i).getNameQual());
                        row.setType1(productBases.get(i).getProductType());
                    }
                    rows.add(row);
                }
            }
            //查询客户列表
            List<Customer> list = salesService.queryAllCustomer();
            List<Customer> customers = salesService.queryAllCustomerByPage(startRow,pageSize);
            List<CustomerRow> customerRows = new ArrayList<CustomerRow>();
            if (customers.size()%3 == 0){
                for (int i=0; i<customers.size(); i+=3){
                    CustomerRow customerRow = new CustomerRow();
                    customerRow.setName1(customers.get(i).getCustomerName());
                    customerRow.setName2(customers.get(i+1).getCustomerName());
                    customerRow.setName3(customers.get(i+2).getCustomerName());
                    customerRows.add(customerRow);
                }
            }
            if (customers.size()%3 == 1){
                for (int i=0; i<customers.size(); i+=3){
                    CustomerRow customerRow = new CustomerRow();
                    if (i == customers.size() -1){
                        customerRow.setName1(customers.get(i).getCustomerName());
                        customerRow.setName2("");
                        customerRow.setName3("");
                    } else {
                        customerRow.setName1(customers.get(i).getCustomerName());
                        customerRow.setName2(customers.get(i+1).getCustomerName());
                        customerRow.setName3(customers.get(i+2).getCustomerName());
                    }
                    customerRows.add(customerRow);
                }
            }
            if (customers.size()%3 == 2){
                for (int i=0; i<customers.size(); i+=3){
                    CustomerRow customerRow = new CustomerRow();
                    if (i == customers.size() -1){
                        customerRow.setName1(customers.get(i).getCustomerName());
                        customerRow.setName2(customers.get(i+1).getCustomerName());
                        customerRow.setName3("");
                    } else {
                        customerRow.setName1(customers.get(i).getCustomerName());
                        customerRow.setName2(customers.get(i+1).getCustomerName());
                        customerRow.setName3(customers.get(i+2).getCustomerName());
                    }
                    customerRows.add(customerRow);
                }
            }



        /*查询销售人员列表*/
            List<SalesMan> salesMan = salesService.queeeryAllSalesMan();
            List<SalesMan> salesMEN = salesService.querySalesManByPage(startRow,pageSize);
            List<SaleManRow> saleManRows = new ArrayList<SaleManRow>();
            //销售人员分组
            if (salesMEN.size()%3 == 0){
                for (int i=0; i<salesMEN.size(); i+=3){
                    SaleManRow saleManRow = new SaleManRow();
                    saleManRow.setName1(salesMEN.get(i).getName());
                    saleManRow.setName2(salesMEN.get(i+1).getName());
                    saleManRow.setName3(salesMEN.get(i+2).getName());
                    saleManRows.add(saleManRow);
                }
            }
            if (salesMEN.size()%3 == 1){
                for (int i=0; i<salesMEN.size(); i+=3){
                    SaleManRow saleManRow = new SaleManRow();
                    if (i == salesMEN.size() -1){
                        saleManRow.setName1(salesMEN.get(i).getName());
                        saleManRow.setName2("");
                        saleManRow.setName3("");
                    } else {
                        saleManRow.setName1(salesMEN.get(i).getName());
                        saleManRow.setName2(salesMEN.get(i+1).getName());
                        saleManRow.setName3(salesMEN.get(i+2).getName());

                    }
                    saleManRows.add(saleManRow);
                }
            }
            if (salesMEN.size()%3 == 2){
                for (int i=0; i<salesMEN.size(); i+=3){
                    SaleManRow saleManRow = new SaleManRow();
                    if (i == salesMEN.size() -2){
                        saleManRow.setName1(salesMEN.get(i).getName());
                        saleManRow.setName2(salesMEN.get(i+1).getName());
                        saleManRow.setName3("");
                    } else {
                        saleManRow.setName1(salesMEN.get(i).getName());
                        saleManRow.setName2(salesMEN.get(i+1).getName());
                        saleManRow.setName3(salesMEN.get(i+2).getName());

                    }
                    saleManRows.add(saleManRow);
                }
            }

            //销货单数据
            SalesOrders salesOrders = salesService.querySalesOrderById(id);
            List<Payment> payment = salesService.queryPaymentByNumber(salesOrders.getDocumentNumber());
            for (int i=0; i<payment.size(); i++){
                if(i==0){
                    String h="<td rowspan=\""+payment.size()+"\">\n" +
                            "                                <span class=\"ui-combo-wrap\">\n" +
                            "                                  <input type=\"text\" class=\"input-txt\" style=\"width: 40px\" autocomplete=\"off\" value=\""+salesOrders.getPushMoney()+"\" name=\"pushMoney\" id=\"pushMoney\" style=\"width: 120px\">\n" +
                            "                                </span>\n" +
                            "                                </td>";
                    payment.get(i).setHtml(h);
                }
                payment.get(i).setIndex(i+1);
            }
//        mv.addObject("size",payment.size());
            mv.addObject("customerRows",customerRows);//客户列表数据
            mv.addObject("saleManRows",saleManRows);//销售列表数据
            mv.addObject("saleManSize",salesMan.size());//销售数量
            mv.addObject("id",id);//销货单ID
            mv.addObject("rows",rows);//商品列表数据
            mv.addObject("salesOrders",salesOrders);//销货单数据
            mv.addObject("payment",payment);
            mv.setViewName("sales/salesInfo");
        }
        return mv;
    }

    /**
     *
     * @param data1
     * @param data2
     * @param data3
     * @param data4
     * @return
     */
    @RequestMapping("/editSales")
    @ResponseBody
    public JsonVo editSales(String[] data1,String[] data2,String[] data3,String[] data4,HttpServletRequest request){
        JsonVo json = new JsonVo();
        if (request.getSession().getAttribute("aid") == null){
            json.setResult(false);
            json.setMsg("请登陆");
        } else {
            //第一张表数据
            //表头数据
            String [] ids = data1[0].split("=");
            String id = ids[1];
            String [] customerNames = data1[1].split("=");
            String customerName = customerNames[1];
            String [] createDates = data1[2].split("=");
            String createDate = createDates[1];
            String [] salesmanNames = data1[3].split("=");
            String salesmanName = salesmanNames[1];
            String [] documentNumbers = data1[4].split("=");
            String documentNumber = documentNumbers[1];
            //表尾数据


            String [] inputMans = data3[0].split("=");
            String inputMan = inputMans[1];
            String reviewerMan = "";
            try {
                String [] reviewerMans = data3[1].split("=");
                reviewerMan = reviewerMans[1];
            } catch (Exception e){
                reviewerMan = "";
            }
            String [] regularCustomers = data3[2].split("=");
            String regularCustomer = regularCustomers[1];
            String [] saleMoneys = data3[9].split("=");
            String saleMoney = saleMoneys[1];

            String dueDate1 = "";
            String dueDate2 = "";
            String dueDate3 = "";
            String dueDate4 = "";

            try {
                String [] dueDates1 = data3[3].split("=");
                dueDate1 = dueDates1[1];
            } catch (Exception e){
                dueDate1 = "";
            }
            try {
                String [] dueDates2 = data3[4].split("=");
                dueDate2 = dueDates2[1];
            } catch (Exception e){
                dueDate2 = "";
            }
            try {
                String [] dueDates3 = data3[5].split("=");
                dueDate3 = dueDates3[1];
            } catch (Exception e){
                dueDate3 = "";
            }
            try {
                String [] dueDates4 = data3[6].split("=");
                dueDate4 = dueDates4[1];
            } catch (Exception e){
                dueDate4 = "";
            }


            String [] payDates = data3[7].split("=");
            String payDate = payDates[1];
            String [] whetherExtendeds = data3[8].split("=");
            String whetherExtended = whetherExtendeds[1];
            //运费
            String pushMoney = "";
            try {
                pushMoney = (data2[9].split("="))[1];
            } catch (Exception e){
                pushMoney = "0";
            }
            //合计数据
            String[] pSums = data4[0].split("=");
            String pSum = pSums[1];
            String[] wSums = data4[1].split("=");
            String wSum = wSums[1];


            //多组表格数据
            String [] BProductName =  new String[data2.length/9];
            String [] BCount = new String[data2.length/9];
            String [] BPrice = new String[data2.length/9];
            String [] BMoney = new String[data2.length/9];
            String [] BBak = new String[data2.length/9];
            String [] WProductName = new String[data2.length/9];
            String [] WCount = new String[data2.length/9];
            String [] WPrice = new String[data2.length/9];
            String [] WMoney = new String[data2.length/9];
            //第一行数据
            try {
                BProductName[0] = (data2[0].split("="))[1];
            } catch (Exception e){
                BProductName[0] = "";
            }
            try {
                BCount[0] = (data2[1].split("="))[1];
            } catch (Exception e){
                BCount[0] = "";
            }
            try {
                BPrice[0] = (data2[2].split("="))[1];
            } catch (Exception e){
                BPrice[0] = "";
            }
            try {
                BMoney[0] = (data2[3].split("="))[1];
            } catch (Exception e){
                BMoney[0] = "";
            }
            try{
                BBak[0] = (data2[4].split("="))[1];
            }catch (Exception e){
                BBak[0] = "";
            }
            try {
                WProductName[0] = (data2[5].split("="))[1];
            } catch (Exception e){
                WProductName[0] = "";
            }
            try {
                WCount[0] = (data2[6].split("="))[1];
            } catch (Exception e){
                WCount[0] = "";
            }
            try {
                WPrice[0] = (data2[7]).split("=")[1];
            } catch (Exception e){
                WPrice[0] = "";
            }
            try {
                WMoney[0] = (data2[8].split("="))[1];
            } catch (Exception e){
                WMoney[0] = "";
            }
            int index = 0;
            if (data2.length > 10){
                if ((data2.length - 10)%9 == 0){
                    index = (data2.length - 10)/9;
                } else {
                    index = (data2.length-10)/9+1;
                }
                //第二行以上数据
                for (int i=0; i<index; i++){
                    try {
                        BProductName[i+1] = (data2[i*9+10+0].split("="))[1];
                    } catch (Exception e){
                        BProductName[i+1] = "";
                    }
                    try {
                        BCount[i+1] = (data2[i*9+10+1].split("="))[1];
                    } catch (Exception e){
                        BCount[i+1] = "";
                    }
                    try {
                        BPrice[i+1] = (data2[i*9+10+2].split("="))[1];
                    } catch (Exception e){
                        BPrice[i+1] = "";
                    }
                    try {
                        BMoney[i+1] = (data2[i*9+10+3].split("="))[1];
                    } catch (Exception e){
                        BMoney[i+1] = "";
                    }
                    try {
                        BBak[i+1] = (data2[i*9+10+4].split("="))[1];
                    } catch (Exception e){
                        BBak[i+1] = "";
                    }
                    try {
                        WProductName[i+1] = (data2[i*9+10+5].split("="))[1];
                    } catch (Exception e){
                        WProductName[i+1] = "";
                    }
                    try {
                        WCount[i+1] = (data2[i*9+10+6].split("="))[1];
                    } catch (Exception e){
                        WCount[i+1] = "";
                    }
                    try {
                        WPrice[i+1] = (data2[i*9+10+7]).split("=")[1];
                    } catch (Exception e){
                        WPrice[i+1] = "";
                    }
                    try {
                        WMoney[i+1] = (data2[i*9+10+8].split("="))[1];
                    } catch (Exception e){
                        WMoney[i+1] = "";
                    }
                }
            }

            //修改SalesOrders
            try {
                String customerId = salesService.queryCustomerIdByName(customerName);
                String salesManId = salesService.querySalesManId(salesmanName);
                salesService.editSalesOrder(id,customerId,createDate,salesManId,documentNumber,inputMan,reviewerMan,regularCustomer,saleMoney,dueDate1,dueDate2,dueDate3,dueDate4,payDate,whetherExtended,pushMoney,pSum,wSum);
                salesService.deletePaymentDetails(documentNumber);
                int a = BProductName.length;
                int m=0;
                try{
                    for (int i=0; i<BProductName.length; i++){
                        String  p=BProductName[i];
                        m++;
                    }
                }catch (Exception e){}
                for (int i=0; i<m; i++){
                    if (StringUtils.isNullOrEmpty(BProductName[i]) || BProductName.equals("")){
                        if (StringUtils.isNullOrEmpty(WProductName[i]) || WProductName[i].equals("")){
                            continue;
                        } else {
                            String wProductId = salesService.queryProductId(WProductName[i]);
                            salesService.addPaymentDetails(documentNumber,"","","","","",wProductId,WCount[i],WPrice[i],WMoney[i]);
                        }
                    } else {
                        String bProductId = salesService.queryProductId(BProductName[i]);
                        if (WProductName[i] != "" || WProductName[i] != null || WProductName[i].length() != 0){
                            String wProductId = salesService.queryProductId(WProductName[i]);
                            salesService.addPaymentDetails(documentNumber,bProductId,BCount[i],BPrice[i],BMoney[i],BBak[i],wProductId,WCount[i],WPrice[i],WMoney[i]);
                        } else {
                            salesService.addPaymentDetails(documentNumber,bProductId,BCount[i],BPrice[i],BMoney[i],BBak[i],"","0","0","0");
                        }
                    }
                }
                if (payDate.equals("未付")){
                    salesService.deleteArrears(documentNumber);
                    if (pSum .equals("0")){
                        if (wSum.equals("0")){
                            salesService.addCustomerArrears(customerId,documentNumber,"0","0");
                        }
                        salesService.addCustomerArrears(customerId,documentNumber,"0",wSum);
                    } else {
                        salesService.addCustomerArrears(customerId,documentNumber,pSum,"0");
                    }
                }
                json.setResult(true);
                json.setMsg("销货单修改成功");
            } catch (Exception e){
                e.printStackTrace();
                json.setResult(false);
                json.setMsg("销货单修改失败");
            }
        }

        return json;
    }

























    @RequestMapping("/isEdit")
    @ResponseBody
    public JsonVo isEdit(String id){
        JsonVo json = new JsonVo();
        SalesOrders salesOrders = salesService.querySalesOrderById(id);
        if (salesOrders.getPayDate().equals("未付清")){
            json.setResult(false);
            json.setMsg("此销货单未付清,无法修改");
        } else {
            json.setMsg("可以修改");
            json.setResult(true);
        }
        return json;
    }



    @RequestMapping("/pagination")
    @ResponseBody
    public JsonVo pagination(String page){
        JsonVo jsonVo = new JsonVo();
        List<Row> rows = new ArrayList<Row>();
        int pageSize = 21;
        List<ProductBase> list = salesService.queryAllProduct();
        //总数据量
        Map<String,Object> map = new HashMap<String, Object>();
        map.put("total", list.size());
        //总页数
        int pageTimes;
        if(list.size()%pageSize == 0)
        {
            pageTimes = list.size()/pageSize;
        }else {
            pageTimes = list.size()/pageSize + 1;
        }
        map.put("pageCount",pageTimes);
        //页面初始的时候page没有值
        if (page == null || page == ""){
            page = "1";
        }
        //每页开始的第几条记录
        int startRow = (Integer.parseInt(page)-1) * pageSize;

        list = salesService.queryAllProductByPage(startRow, pageSize);
        //商品分组
        if (list.size()%3 == 0){
            for (int i=0; i<list.size()-2; i+=3){
                Row row = new Row();
                row.setId1(list.get(i).getId());
                row.setName1(list.get(i).getNameQual());
                row.setType1(list.get(i).getProductType());

                row.setId2(list.get(i+1).getId());
                row.setName2(list.get(i+1).getNameQual());
                row.setType2(list.get(i+1).getProductType());

                row.setId3(list.get(i+2).getId());
                row.setName3(list.get(i+2).getNameQual());
                row.setType3(list.get(i+2).getProductType());
                rows.add(row);
            }
        }
        if (list.size()%3 == 1){
            for (int i=0; i<list.size()-2; i+=3){
                Row row = new Row();
                row.setId1(list.get(i).getId());
                row.setName1(list.get(i).getNameQual());
                row.setType1(list.get(i).getProductType());

                row.setId2(list.get(i+1).getId());
                row.setName2(list.get(i+1).getNameQual());
                row.setType2(list.get(i+1).getProductType());

                row.setId3(list.get(i+2).getId());
                row.setName3(list.get(i+2).getNameQual());
                row.setType3(list.get(i+2).getProductType());

                if (i == list.size() -3){
                    row.setId1(list.get(i).getId());
                    row.setName1(list.get(i).getNameQual());
                    row.setType1(list.get(i).getProductType());
                }
                rows.add(row);
            }
        }
        if (list.size()%3 == 2){
            for (int i=0; i<list.size()-2; i+=3){
                Row row = new Row();
                row.setId1(list.get(i).getId());
                row.setName1(list.get(i).getNameQual());
                row.setType1(list.get(i).getProductType());

                row.setId2(list.get(i+1).getId());
                row.setName2(list.get(i+1).getNameQual());
                row.setType2(list.get(i+1).getProductType());

                row.setId3(list.get(i+2).getId());
                row.setName3(list.get(i+2).getNameQual());
                row.setType3(list.get(i+2).getProductType());
                if (i == list.size() -4){
                    row.setId1(list.get(i).getId());
                    row.setName1(list.get(i).getNameQual());
                    row.setType1(list.get(i).getProductType());
                }
                rows.add(row);
            }
        }

        for (int i=0; i<list.size(); i++){
            list.get(i).setIndex((Integer.parseInt(page)-1)*10+1+i);
        }
        HttpSession session = getRequest().getSession();
        session.setAttribute("currentPage",Integer.parseInt(page));
        map.put("currentPage",Integer.parseInt(page));
        map.put("row",rows);
        jsonVo.setT(map);
        jsonVo.setResult(true);
        jsonVo.setMsg("获取分页成功");
        return jsonVo;
    }

    @RequestMapping("/paginationCustomer")
    @ResponseBody
    public JsonVo paginationCustomer(String page){
        JsonVo jsonVo = new JsonVo();

        List<CustomerRow> customerRows = new ArrayList<CustomerRow>();
        int pageSize = 21;
        List<Customer> list = salesService.queryAllCustomer();
        //总数据量
        Map<String,Object> map = new HashMap<String, Object>();
        map.put("total", list.size());
        //总页数
        int pageTimes;
        if(list.size()%pageSize == 0)
        {
            pageTimes = list.size()/pageSize;
        }else {
            pageTimes = list.size()/pageSize + 1;
        }
        map.put("pageCount",pageTimes);
        //页面初始的时候page没有值
        if (page == null || page == ""){
            page = "1";
        }
        //每页开始的第几条记录
        int startRow = (Integer.parseInt(page)-1) * pageSize;

        list = salesService.queryAllCustomerByPage(startRow, pageSize);
        //商品分组
        if (list.size()%3 == 0){
            for (int i=0; i<list.size(); i+=3){
                CustomerRow customerRow = new CustomerRow();
                customerRow.setName1(list.get(i).getCustomerName());
                customerRow.setName2(list.get(i+1).getCustomerName());
                customerRow.setName3(list.get(i+2).getCustomerName());
                customerRows.add(customerRow);
            }
        }
        if (list.size()%3 == 1){
            for (int i=0; i<list.size(); i+=3){
                CustomerRow customerRow = new CustomerRow();
                if (i == list.size() -1){
                    customerRow.setName1(list.get(i).getCustomerName());
                    customerRow.setName2("");
                    customerRow.setName3("");
                } else {
                    customerRow.setName1(list.get(i).getCustomerName());
                    customerRow.setName2(list.get(i+1).getCustomerName());
                    customerRow.setName3(list.get(i+2).getCustomerName());
                }
                customerRows.add(customerRow);
            }
        }
        if (list.size()%3 == 2){
            for (int i=0; i<list.size(); i+=3){
                CustomerRow customerRow = new CustomerRow();
                if (i == list.size() -2){
                    customerRow.setName1(list.get(i).getCustomerName());
                    customerRow.setName2(list.get(i+1).getCustomerName());
                    customerRow.setName3("");
                } else {
                    customerRow.setName1(list.get(i).getCustomerName());
                    customerRow.setName2(list.get(i+1).getCustomerName());
                    customerRow.setName3(list.get(i+2).getCustomerName());
                }
                customerRows.add(customerRow);
            }
        }

        for (int i=0; i<list.size(); i++){
            list.get(i).setIndex((Integer.parseInt(page)-1)*10+1+i);
        }
        HttpSession session = getRequest().getSession();
        session.setAttribute("currentPage",Integer.parseInt(page));
        map.put("currentPage",Integer.parseInt(page));
        map.put("customerRows",customerRows);
        jsonVo.setT(map);
        jsonVo.setResult(true);
        jsonVo.setMsg("获取分页成功");
        return jsonVo;
    }




}
