package com.soecode.lyf.web;

import com.mysql.jdbc.StringUtils;
import com.soecode.lyf.entity.*;
import com.soecode.lyf.entity.vo.JsonVo;
import com.soecode.lyf.service.CommissionService;
import com.soecode.lyf.service.SalesService;
import com.soecode.lyf.web.base.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/8/24.
 */
@Controller
@RequestMapping("/commission")
public class CommissionController extends BaseController {

    @Autowired
    CommissionService commissionService;

    /**
     * 读取业务员提成比例数据
     * @param userId
     * @return
     */
    @RequestMapping("/salesManCommission")
    public ModelAndView salesManCommission(String userId, HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            mv.setViewName("login");
        } else {
            List<SalesManCommission> list = new ArrayList<SalesManCommission>();
            if (userId.equals("1")){
                list = commissionService.queryAllCommission();
            } else {
                list = commissionService.queryAllCommissionByUser(userId);
            }
            mv.addObject("list",list);
            mv.setViewName("commission/commissionInfo");
        }
        return mv;
    }

    /**
     * 修改业务员提成比例
     * @param id
     * @param commission
     * @return
     */
    @RequestMapping("/editCommission")
    @ResponseBody
    public JsonVo editCommission(Integer id,Double commission,HttpServletRequest request){
        JsonVo json = new JsonVo();
        if (request.getSession().getAttribute("aid") == null){
            json.setResult(false);
            json.setMsg("请登陆");
        } else {
            try {
                commissionService.editCommission(id,commission);
                json.setResult(true);
                json.setMsg("修改成功");
            } catch (Exception e){
                e.printStackTrace();
                json.setResult(false);
                json.setMsg("修改失败");
            }
        }
        return json;
    }
}
