package com.soecode.lyf.web;

import com.soecode.lyf.entity.*;
import com.soecode.lyf.entity.vo.JsonVo;
import com.soecode.lyf.service.SalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/8/24.
 */
@Controller
@RequestMapping("/return")
public class ReturnController {
//
//    @Autowired
//    SalesService salesService;
//
//    @RequestMapping("/editSalesOrder")
//    public ModelAndView editSalesOrder(String id) {
//        ModelAndView mv = new ModelAndView();
//        mv.addObject("id", id);
//        mv.setViewName("sales/editSales");
//        return mv;
//    }
//
//    @RequestMapping("/salesInfo")
////    public ModelAndView salesInfo(String id) {
////        ModelAndView mv = new ModelAndView();
////        //显示商品列表
////        List<Row> rows = new ArrayList<Row>();
////        //查询商品列表
//////        List<ProductBase> productBases = salesService.queryAllProduct();
////        //商品分组
////        if (productBases.size() % 3 == 0) {
////            for (int i = 0; i < productBases.size() - 2; i += 3) {
////                Row row = new Row();
////                row.setId1(productBases.get(i).getId());
////                row.setName1(productBases.get(i).getNameQual());
////                row.setId2(productBases.get(i + 1).getId());
////                row.setName2(productBases.get(i + 1).getNameQual());
////                row.setId3(productBases.get(i + 2).getId());
////                row.setName3(productBases.get(i + 2).getNameQual());
////                rows.add(row);
////            }
////        }
////        if (productBases.size() % 3 == 1) {
////            for (int i = 0; i < productBases.size() - 2; i += 3) {
////                Row row = new Row();
////                row.setId1(productBases.get(i).getId());
////                row.setName1(productBases.get(i).getNameQual());
////                row.setId2(productBases.get(i + 1).getId());
////                row.setName2(productBases.get(i + 1).getNameQual());
////                row.setId3(productBases.get(i + 2).getId());
////                row.setName3(productBases.get(i + 2).getNameQual());
////                if (i == productBases.size() - 3) {
////                    row.setId1(productBases.get(i).getId());
////                    row.setName1(productBases.get(i).getNameQual());
////                }
////                rows.add(row);
////            }
////        }
////        if (productBases.size() % 3 == 2) {
////            for (int i = 0; i < productBases.size() - 2; i += 3) {
////                Row row = new Row();
////                row.setId1(productBases.get(i).getId());
////                row.setName1(productBases.get(i).getNameQual());
////                row.setId2(productBases.get(i + 1).getId());
////                row.setName2(productBases.get(i + 1).getNameQual());
////                row.setId3(productBases.get(i + 2).getId());
////                row.setName3(productBases.get(i + 2).getNameQual());
////                if (i == productBases.size() - 4) {
////                    row.setId1(productBases.get(i).getId());
////                    row.setName1(productBases.get(i).getNameQual());
////                }
////                rows.add(row);
////            }
////        }
////        //查询客户列表
////        List<Customer> customers = salesService.queryAllCustomer();
////        /*查询销售人员列表*/
////        List<SalesMan> salesMan = salesService.queeeryAllSalesMan();
////        //销货单数据
////        SalesOrders salesOrders = salesService.querySalesOrderById(id);
//////        List<Payment> list = salesService.queryPaymentByNumber(salesOrders.getDocumentNumber());
////        mv.addObject("salesMan", salesMan);
////        mv.addObject("customers", customers);
////        mv.addObject("id", id);
////        mv.addObject("rows", rows);
////        mv.addObject("salesOrders", salesOrders);
//////        mv.addObject("list", list);
////        mv.setViewName("sales/salesInfo");
////        return mv;
////    }
//
//    @RequestMapping("/editSales")
//    public ModelAndView editSales(String id, String customerName, String salesmanName, String documentNumber,
//                                  String[] productName, String[] size, String[] cang, String[] count, String[] price, String[] money, String[] pushMoney, String[] bak,
//                                  String dateIssuance, String entryPersonnel, String reviewOfficer, String whetherElectint, String regularCustomer) {
//        ModelAndView mv = new ModelAndView();
//        String customerId = salesService.queryCustomerIdByName(customerName);
//        double sumCount = 0;
//        double sumPrice = 0;
//        double sumPushMoney = 0;
//        String delStatus = "1";
//        String orderType = "1";
//        /*数量合*/
//        for (int i = 0; i < productName.length; i++) {
//            if (count[i] != "") {
//                sumCount += Double.parseDouble(count[i]);
//            }
//        }
//        /*总金额*/
//        for (int i = 0; i < productName.length; i++) {
//            if (price[i] != "" && count[i] != "") {
//                sumPrice += (Double.parseDouble(price[i])) * (Double.parseDouble(count[i]));
//            }
//        }
//        /*总运费*/
//        for (int i = 0; i < productName.length; i++) {
//            if (pushMoney[i] != "") {
//                sumPushMoney += Double.parseDouble(pushMoney[i]);
//            }
//        }
//        /*修改salesOrder*/
//        String salesManId = salesService.querySalesManId(salesmanName);
//        salesService.editSalesOrder(id, documentNumber, customerName, customerId, salesManId, sumCount, sumPrice, sumPushMoney, reviewOfficer, entryPersonnel, dateIssuance, delStatus, orderType, whetherElectint, regularCustomer);
//        /**修改payment_details
//         * 所需参数：
//         *
//         * */
//        Date date = new Date();
//        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
//        String date1 = f.format(date);
//        String whetherDate = "2";
//        /*多组数据循环修改payment_details*/
//        for (int i = 0; i < productName.length; i++) {
//            String name = "";
//            String unitSize = "";
//            String unitCount = "";
//            String unitPrice = "";
//            String unitMoney = "";
//            String unitBak = "";
//            String unitPushMouny = "";
//            if (productName.length > 0) {
//                name = productName[i];
//            }
//            if (size.length > 0) {
//                unitSize = size[i];
//            }
//            if (count.length > 0) {
//                unitCount = count[i];
//            }
//            if (price.length > 0) {
//                unitPrice = price[i];
//            }
//            if (money.length > 0) {
//                unitMoney = money[i];
//            }
//            if (bak.length > 0) {
//                unitBak = bak[i];
//            }
//            if (pushMoney.length > 0) {
//                unitPushMouny = pushMoney[i];
//            }
//            String itemType = salesService.queryItemTypeByName(name);
//            String cangId = "";
//            if (cang.length > 0) {
//                cangId = salesService.queryCangId(cang[i]);
//            }
//            String nameId = salesService.queryIdByProductName(name);
//            salesService.editPaymentDetails(nameId, unitSize, unitCount, unitPrice, unitMoney, unitBak, documentNumber, delStatus, date1, unitPushMouny, whetherDate, whetherElectint, itemType, cangId);
//        }
//
//        //查询新列表
//
//        //每页显示的条数
//        int pageSize = 10;
//        List<SalesOrders> list = salesService.queryAllSalesOrders();
//        //总数据量
//        mv.addObject("total", list.size());
//        //总页数
//        int pageTimes;
//        if (list.size() % pageSize == 0) {
//            pageTimes = list.size() / pageSize;
//        } else {
//            pageTimes = list.size() / pageSize + 1;
//        }
//        mv.addObject("pageCount", pageTimes);
//        //页面初始的时候page没有值
//        String page = "1";
//        //每页开始的第几条记录
//        int startRow = (Integer.parseInt(page) - 1) * pageSize;
//        list = salesService.queryAllSalesOrdersByPage(startRow, pageSize);
////        for (int i = 0; i < list.size(); i++) {
////            list.get(i).setIndex((Integer.parseInt(page) - 1) * 10 + 1 + i);
////        }
//        mv.addObject("currentPage", Integer.parseInt(page));
//        mv.addObject("list", list);
//        mv.setViewName("sales/salesOrdersList");
//        return mv;
//    }
//
//    @RequestMapping("/delete")
//    @ResponseBody
//    public JsonVo delete(String id) {
//        JsonVo jsonVo = new JsonVo();
//        SalesOrders salesOrders = salesService.querySalesOrderById(id);
//        if (salesOrders != null) {
//            try {
//                salesService.delete(id);
//                jsonVo.setResult(true);
//                jsonVo.setMsg("删除成功");
//            } catch (Exception e) {
//                e.printStackTrace();
//                jsonVo.setResult(false);
//                jsonVo.setMsg("删除失败");
//            }
//        } else {
//            jsonVo.setResult(false);
//            jsonVo.setMsg("单据不存在，删除失败");
//        }
//        return jsonVo;
//    }
//
//    @RequestMapping("/validationAccount")
//    @ResponseBody
//    public JsonVo validationAccount(String account) {
//        JsonVo jsonVo = new JsonVo();
//        String id = salesService.querySalesOrderByNo(account);
//        if (id == null || id == "") {
//            jsonVo.setResult(true);
//        } else {
//            jsonVo.setResult(false);
//        }
//        return jsonVo;
//    }
}
