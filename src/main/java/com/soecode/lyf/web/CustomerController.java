package com.soecode.lyf.web;

import com.soecode.lyf.entity.Customer;
import com.soecode.lyf.entity.SalesMan;
import com.soecode.lyf.entity.SuggestResult;
import com.soecode.lyf.entity.vo.JsonVo;
import com.soecode.lyf.service.CustomerService;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/8/23.
 */
@Controller
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    CustomerService customerService;

    /**
     * 读取客户列表数据
     * @param page
     * @return
     */
    @RequestMapping("/customerList")
    public ModelAndView customerList(String page, HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            //未登录
            mv.setViewName("login");
        } else {
            //每页显示的条数
            int pageSize = 10;
            List<Customer> list = customerService.queryAllCustomer();
            //总数据量
            mv.addObject("total", list.size());
            //总页数
            int pageTimes;
            if (list.size() % pageSize == 0) {
                pageTimes = list.size() / pageSize;
            } else {
                pageTimes = list.size() / pageSize + 1;
            }
            mv.addObject("pageCount", pageTimes);
            //页面初始的时候page没有值
            if (null == page || page.equals("0")) {
                page = "1";
            }
            //每页开始的第几条记录
            int startRow = (Integer.parseInt(page) - 1) * pageSize;
            list = customerService.queryAllCustomerByPage(startRow, pageSize);
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setIndex((Integer.parseInt(page) - 1) * 10 + 1 + i);
            }
            mv.addObject("currentPage", Integer.parseInt(page));
            mv.addObject("list", list);
            mv.setViewName("customer/customerList");
        }
        return mv;
    }

    /**
     * 查询数据并跳转到修改页面
     * @param id
     * @param request
     * @return
     */
    @RequestMapping("/customerEditPage")
    public ModelAndView customerEdit(String id,HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            //未登录
            mv.setViewName("login");
        } else {
            //查询业务员列表
            List<SalesMan> list = customerService.queryAllSalesMan();
            Customer customer = customerService.queryCustomerById(id);
            mv.addObject("list", list);
            mv.addObject("customer", customer);
            mv.setViewName("customer/editCustomer");
        }
        return mv;
    }

    /**
     * 修改客户数据
     * @param id
     * @param customerName
     * @param customerPhone
     * @param customerAddress
     * @param customerPeople
     * @param peoplePhone
     * @param stateCustomer
     * @return
     */
    @RequestMapping("/editCustomer")
    public ModelAndView editCustomer(String id,String customerName,String customerPhone,String customerAddress,String customerPeople,String peoplePhone,String stateCustomer,HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            mv.setViewName("login");
        } else {
            try {
                customerService.editCustomer(id,customerName,customerPhone,customerAddress,customerPeople,peoplePhone,stateCustomer);
            } catch (Exception e){
                e.printStackTrace();
            }

            //每页显示的条数
            int pageSize = 10;
            List<Customer> list = customerService.queryAllCustomer();
            //总数据量
            mv.addObject("total", list.size());
            //总页数
            int pageTimes;
            if(list.size()%pageSize == 0)
            {
                pageTimes = list.size()/pageSize;
            }else {
                pageTimes = list.size()/pageSize + 1;
            }
            mv.addObject("pageCount", pageTimes);
            //页面初始的时候page没有值
            String page = "1";
            //每页开始的第几条记录
            int startRow = (Integer.parseInt(page)-1) * pageSize;
            list = customerService.queryAllCustomerByPage(startRow, pageSize);
            mv.addObject("currentPage", Integer.parseInt(page));
            mv.addObject("list", list);
            mv.setViewName("customer/customerListPage");
        }
        return mv;
    }

    /**
     * 删除客户信息
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value = "/deleteCustomer",method = RequestMethod.POST)
    @ResponseBody
    public JsonVo deleteCustomer(String id,HttpServletRequest request){
        JsonVo json=new JsonVo();
        if (request.getSession().getAttribute("aid") == null){
            json.setResult(false);
            json.setMsg("请登陆");
        } else {
            try{
                customerService.delete(id);
                json.setResult(true);
                json.setMsg("删除成功");
            }catch (Exception e){
                json.setResult(false);
                json.setMsg("删除失败");
                e.printStackTrace();
            }
        }
        return json;
    }

    /**
     * 查询数据，并跳转到客户添加页面
     * @param request
     * @return
     */
    @RequestMapping("/queryaddCustomerInfo")
    public ModelAndView queryaddCustomerInfo(HttpServletRequest request){
        ModelAndView mv = new ModelAndView();
        if (request.getSession().getAttribute("aid") == null){
            mv.setViewName("login");
        } else {
            //查询业务员列表
            List<SalesMan> list = customerService.queryAllSalesMan();
            mv.addObject("list",list);
            mv.setViewName("customer/addCustomer");
        }
        return mv;
    }

    /**
     * 保存新客户信息
     * @param customerName
     * @param customerPhone
     * @param customerAddress
     * @param customerPeople
     * @param peoplePhone
     * @param stateCustomer
     * @param salesMan
     * @param request
     * @return
     */
    @RequestMapping("/addCustomer")
    @ResponseBody
    public JsonVo addCustomer(String customerName,String customerPhone,String customerAddress,String customerPeople,String peoplePhone,String stateCustomer,String salesMan,HttpServletRequest request){
        JsonVo json = new JsonVo();
        if (request.getSession().getAttribute("aid") == null){
            json.setResult(false);
            json.setMsg("尚未登陆，请登陆");
            return json;
        } else {
            String customer_name = customerName;
            List<Customer> list = customerService.queryAllCustomerByName(customer_name);
            if(list.size() == 0){
                String customer_phone = customerPhone;
                String customer_address = customerAddress;
                String customer_people = customerPeople;
                String people_phone = peoplePhone;
                int state_customer;
                if(stateCustomer.equals("1")){
                    state_customer = 1;
                } else{
                    state_customer = 2;
                }
                customerService.addCustomer(customer_name,customer_phone,customer_address,customer_people,people_phone,state_customer,salesMan);
                json.setResult(true);
                json.setMsg("");
            } else {
                json.setResult(false);
                json.setMsg("客户已存在");
            }
        }
        return json;
    }

    @RequestMapping("/loadCustomerName")
    @ResponseBody
    public String loadCustomerName(String str){
        SuggestResult result = new SuggestResult();
        List<String[]> res = new ArrayList<String[]>();
        String query = "%"+str+"%";
        List<Customer> customers = customerService.loadCustomerName(query);
        for (Customer list : customers) {
            res.add(new String[]{list.getCustomerId(),list.getCustomerName(),list.getCustomerPhone()});
        }
        result.setResult(res);
        result.setTmall(str);
        JSONObject json1 = JSONObject.fromObject(result);
        return json1.toString();
    }
}
